
.PHONY: clean, mrproper

.SUFFIXES:


DEBUG = yes

export AR = ar
export RM = rm -rf
export MK = mkdir
export CD = cd
export CC = g++
export GFLAGS = -fopenmp
export BCFLAGS = -W -Wall -Werror -std=c++17 -Wc++17-compat
export BLDFLAGS= -lm -lpthread
export ARFLAGS =
ifeq ($(DEBUG),yes)
	export PCFLAGS = -g $(BCFLAGS)
else
	export PCFLAGS = $(BCFLAGS) -O3
endif
export CFLAGS = $(PCFLAGS) $(GFLAGS)
export LDFLAGS = $(BLDFLAGS) $(GFLAGS)
export BIN = dnn
export DBUILD = build
export DOUT = output
export PRG = ../
PRX = ../


all: $(BIN)

$(BIN): $(PRX)$(DOUT)/$(BIN)

$(PRX)$(DOUT)/$(BIN): $(PRX)$(DBUILD)/main.o $(PRX)$(DBUILD)/helpers.o $(PRX)$(DBUILD)/mlnn.o $(PRX)$(DBUILD)/mnist.o $(PRX)$(DBUILD)/femnist.o $(PRX)$(DBUILD)/vsn.o $(PRX)$(DBUILD)/lstm.o $(PRX)$(DBUILD)/metro.o $(PRX)$(DBUILD)/binseq.o $(PRX)$(DBUILD)/assoc.o $(PRX)$(DBUILD)/sdr.o $(PRX)$(DBUILD)/avg.o $(PRX)$(DBUILD)/order.o $(PRX)$(DBUILD)/sgd.o $(PRX)$(DBUILD)/recom.o $(PRX)$(DBUILD)/recoref.o
	$(CC) $^ -o $@ $(LDFLAGS)

$(PRX)$(DBUILD)/helpers.o: helpers/helpers.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/mlnn.o: nets/mlnn/mlnn.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/mnist.o: data_src/mnist/mnist.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/femnist.o: data_src/femnist/femnist.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/vsn.o: data_src/vsn/vsn.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/lstm.o: nets/lstm/lstm.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/metro.o: data_src/metro/metro.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/binseq.o: data_src/binseq/binseq.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/assoc.o: nets/assoc/assoc.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/sdr.o: data_src/sdr/sdr.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/avg.o: dnn_tools/avg/avg.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/order.o: dnn_tools/order/order.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/sgd.o: dnn_tools/sgd/sgd.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/recom.o: recom/recom.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/recoref.o: recom/ref.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

$(PRX)$(DBUILD)/%.o: %.c++
	$(CC) -c $^ -o $@ $(CFLAGS)

clean:
	$(RM) $(PRX)$(DBUILD)/*

mrproper: clean
	$(RM) $(PRX)$(DOUT)/*
