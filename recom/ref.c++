#include <limits>
#include <chrono>
#include <random>
#include <algorithm>
#include "ref.h++"


void update(const std::vector<std::valarray<double>>&,const std::vector<bool>&,const std::vector<size_t>&,const std::valarray<double>&,std::vector<size_t>&,std::vector<double>&,unsigned,const std::function<double(std::valarray<double>,std::valarray<double>)>&);

std::vector<size_t> getNeighbors(const std::vector<std::valarray<double>>&,size_t,double,const std::function<double(std::valarray<double>,std::valarray<double>)>&);


std::vector<std::pair<size_t,double>> optics(const std::vector<std::valarray<double>>& db, double eps, unsigned minPts, const std::function<double(std::valarray<double>,std::valarray<double>)>& d)
{
    std::vector<std::pair<size_t,double>> ans;
    ans.reserve(db.size());
    std::vector<bool> nprcsd(db.size(),true);
    std::vector<double> rd(db.size(),std::numeric_limits<double>::quiet_NaN());
    for(size_t p = 0; p < db.size(); ++p)
    {
        if(nprcsd[p])
        {
            std::vector<size_t> n = getNeighbors(db,p,eps,d);
            nprcsd[p] = false;
            ans.push_back(std::pair<size_t,double>(p,rd[p]));
            if(n.size() >= minPts)
            {
                std::vector<size_t> seeds;
                update(db,nprcsd,n,db[p],seeds,rd,minPts,d);
                while(!seeds.empty())
                {
                    size_t q = seeds.back();
                    seeds.pop_back();
                    std::vector<size_t> np = getNeighbors(db,q,eps,d);
                    nprcsd[q] = false;
                    ans.push_back(std::pair<size_t,double>(q,rd[q]));
                    if(np.size() >= minPts)
                    {
                        update(db,nprcsd,np,db[q],seeds,rd,minPts,d);
                    }
                }
            }
        }
    }
    return ans;
}

void update(const std::vector<std::valarray<double>>& db, const std::vector<bool>& nprcsd, const std::vector<size_t>& n, const std::valarray<double>& p, std::vector<size_t>& seeds, std::vector<double>& rd, unsigned minPts, const std::function<double(std::valarray<double>,std::valarray<double>)>& d)
{
    double cd = d(db[n[minPts-1]],p);
    for(const auto& o: n)
    {
        if(nprcsd[o])
        {
            double nrd = std::max(cd,d(db[o],p));
            size_t i;
            bool change = true;
            for(i = seeds.size()-1; i < seeds.size() && rd[seeds[i]] < nrd; --i)
            {
                if(seeds[i] == o)
                {
                    change = false;
                    break;
                }
            }
            if(change)
            {
                seeds.insert(seeds.begin()+i+1,o);
                rd[o] = nrd;
                for(; i < seeds.size() && seeds[i] != o; --i);
                if(i < seeds.size())
                {
                    seeds.erase(seeds.begin()+i);
                }
            }
        }
    }
}

std::vector<size_t> getNeighbors(const std::vector<std::valarray<double>>& db, size_t p, double eps, const std::function<double(std::valarray<double>,std::valarray<double>)>& d)
{
    std::vector<size_t> ans;
    std::vector<double> dists;
    for(size_t n = 0; n < db.size(); ++n)
    {
        if(n != p)
        {
            double di = d(db[n],db[p]);
            if(di <= eps)
            {
                size_t i;
                for(i = 0; i < ans.size() && dists[i] < di; ++i);
                ans.insert(ans.begin()+i,n);
                dists.insert(dists.begin()+i,di);
            }
        }
    }
    return ans;
}

std::vector<std::vector<size_t>> opticsExtract(const std::vector<std::valarray<double>>& db, const std::vector<std::pair<size_t,double>>& oout, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v)
{
    std::vector<std::vector<size_t>> ans;
    double rv = 0.0;
    std::vector<double> ths;
    ths.reserve(oout.size());
    for(size_t i = 0; i < oout.size(); ++i)
    {
        if(std::isfinite(oout[i].second))
        {
            ths.push_back(oout[i].second);
        }
    }
    for(size_t i = 0; i < ths.size(); ++i)
    {
        std::vector<std::vector<size_t>> tmp = opticsExtract(oout,ths[i]);
        double tv = gvalueFull(db,tmp,d,n,v);
        if(tv > rv)
        {
            ans = tmp;
            rv = tv;
        }
    }
    return ans;
}

std::vector<std::vector<size_t>> opticsExtract(const std::vector<std::pair<size_t,double>>& oout, double thld)
{
    std::vector<std::vector<size_t>> ans;
    for(size_t i = 0; i < oout.size(); ++i)
    {
        if(oout[i].second <= thld)
        {
            ans.back().push_back(oout[i].first);
        }
        else
        {
            ans.push_back(std::vector<size_t>(1,oout[i].first));
        }
    }
    return ans;
}

std::vector<std::vector<size_t>> kmeans(const std::vector<std::valarray<double>>& testr, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v, unsigned prc, unsigned mmt)
{
    std::vector<std::vector<size_t>> ans;
    size_t m = testr.size();
    double la = 0.0, vla;
    size_t k = 0;
    unsigned pmm = mmt;
    do
    {
        vla = la;
        double lat;
        ++k;
        for(unsigned tra = 0; tra < prc; ++tra)
        {
            std::vector<std::valarray<double>> ctr;
            std::vector<std::vector<size_t>> ngrp;
            std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());
            std::uniform_int_distribution<size_t> randGen(0,m-1);
            size_t bv = randGen(gen);
            ctr.reserve(k);
            ngrp.reserve(k);
            ctr.push_back(testr[bv]);
            ngrp.push_back(std::vector<size_t>(1,bv));
            std::valarray<double> ds(std::numeric_limits<double>::infinity(),m);
            for(size_t i = 0; i < m; ++i)
            {
                double dv = d(testr[i],testr[bv]);
                ds[i] = dv*dv;
            }
            for(size_t i = 1; i < k; ++i)
            {
                std::uniform_real_distribution<double> randGen2(0.0,ds.sum());
                double rv = randGen2(gen);
                size_t nv;
                for(nv = 0; ds[nv] < rv; ++nv)
                {
                    rv-= ds[nv];
                }
                ctr.push_back(testr[nv]);
                ngrp.push_back(std::vector<size_t>(1,nv));
                for(size_t j = 0; j < m; ++j)
                {
                    double dist = d(testr[j],testr[nv]);
                    double d2 = dist*dist;
                    if(d2 < ds[j])
                    {
                        ds[j] = d2;
                    }
                }
            }
            std::vector<std::vector<size_t>> grp;
            do
            {
                grp = ngrp;
                ngrp.clear();
                ngrp.resize(k);
                for(size_t i = 0; i < k; ++i)
                {
                    ctr[i] = 0.0;
                    for(size_t j = 0; j < grp[i].size(); ++j)
                    {
                        ctr[i]+= testr[grp[i][j]];
                    }
                    ctr[i]/= grp[i].size();
                }
                std::vector<size_t> bgrp;
                bgrp.resize(m);
                for(size_t i = 0; i < m; ++i)
                {
                    double bdis = std::numeric_limits<double>::infinity();
                    bgrp[i] = k;
                    for(size_t j = 0; j < k; ++j)
                    {
                        double dis = d(testr[i],ctr[j]);
                        if(dis < bdis)
                        {
                            bgrp[i] = j;
                            bdis = dis;
                        }
                    }
                    ngrp[bgrp[i]].push_back(i);
                }
            }
            while(ngrp != grp);
            lat = 0.0;
            for(size_t i = 0; i < k; ++i)
            {
                double gv = v(grp[i].size());
                for(size_t j = 0; j < grp[i].size(); ++j)
                {
                    lat+= n(d(testr[grp[i][j]],ctr[i]))*gv;
                }
            }
            if(lat > la)
            {
                if(lat > vla)
                {
                    ans = grp;
                }
                la = lat;
            }
        }
        if(la > vla)
        {
            pmm = mmt;
        }
        else
        {
            --pmm;
        }
    }
    while(pmm);
    return ans;
}

std::vector<std::vector<std::valarray<double>>> makeGrp(const std::vector<std::valarray<double>>& db, const std::vector<std::vector<size_t>>& af)
{
    std::vector<bool> vst(db.size(),true);
    std::vector<std::vector<std::valarray<double>>> ans;
    for(const auto& c: af)
    {
        ans.push_back(std::vector<std::valarray<double>>());
        ans.back().reserve(c.size());
        for(const auto& v: c)
        {
            ans.back().push_back(db[v]);
            vst[v] = false;
        }
    }
    for(size_t i = 0; i < db.size(); ++i)
    {
        if(vst[i])
        {
            ans.push_back(std::vector<std::valarray<double>>(1,db[i]));
        }
    }
    return ans;
}

std::vector<std::vector<std::valarray<double>>> makeGrpFull(const std::vector<std::valarray<double>>& db, const std::vector<std::vector<size_t>>& af)
{
    std::vector<std::vector<std::valarray<double>>> ans;
    for(const auto& c: af)
    {
        ans.push_back(std::vector<std::valarray<double>>());
        ans.back().reserve(c.size());
        for(const auto& v: c)
        {
            ans.back().push_back(db[v]);
        }
    }
    return ans;
}

double gvalue(const std::vector<std::vector<std::valarray<double>>>& a, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v)
{
    double ans = 0.0;
    for(const auto& c: a)
    {
        if(c.size() > 0)
        {
            std::valarray<double> ctr = c[0];
            for(size_t i = 1; i < c.size(); ++i)
            {
                ctr+= c[i];
            }
            ctr/= c.size();
            double vg = v(c.size());
            for(const auto& p: c)
            {
                ans+= n(d(p,ctr))*vg;
            }
        }
    }
    return ans;
}

double gvalue(const std::vector<std::valarray<double>>& db, const std::vector<std::vector<size_t>>& a, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v)
{
    std::vector<bool> vst(db.size(),true);
    double ans = 0.0;
    for(const auto& c: a)
    {
        if(c.size() > 0)
        {
            std::valarray<double> ctr = db[c[0]];
            for(size_t i = 1; i < c.size(); ++i)
            {
                ctr+= db[c[i]];
            }
            ctr/= c.size();
            double vg = v(c.size());
            for(const auto& p: c)
            {
                ans+= n(d(db[p],ctr))*vg;
                vst[p] = false;
            }
        }
    }
    for(size_t i = 0; i < db.size(); ++i)
    {
        if(vst[i])
        {
            ans+= 1.0;
        }
    }
    return ans;
}

double gvalueFull(const std::vector<std::valarray<double>>& db, const std::vector<std::vector<size_t>>& a, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v)
{
    double ans = 0.0;
    for(const auto& c: a)
    {
        if(c.size() > 0)
        {
            std::valarray<double> ctr = db[c[0]];
            for(size_t i = 1; i < c.size(); ++i)
            {
                ctr+= db[c[i]];
            }
            ctr/= c.size();
            double vg = v(c.size());
            for(const auto& p: c)
            {
                ans+= n(d(db[p],ctr))*vg;
            }
        }
    }
    return ans;
}

double gloss(const std::vector<std::vector<std::valarray<double>>>& a, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v)
{
    double ans = 0.0;
    std::vector<std::valarray<double>> ctr(a.size(),std::valarray<double>());
    for(size_t i = 0; i < a.size(); ++i)
    {
        if(a[i].size() > 0)
        {
            ctr[i] = a[i][0];
            for(size_t j = 1; j < a[i].size(); ++j)
            {
                ctr[i]+= a[i][j];
            }
            ctr[i]/= a[i].size();
        }
    }
    for(size_t i = 0; i < a.size(); ++i)
    {
        for(const auto& p: a[i])
        {
            size_t bgrp = ctr.size();
            double bintr = 1.0;
            for(size_t k = 0; k < ctr.size(); ++k)
            {
                if(a[k].size() > 0)
                {
                    double intr = n(d(p,ctr[k]))*v(a[k].size());
                    if(intr > bintr)
                    {
                        bgrp = k;
                        bintr = intr;
                    }
                }
            }
            if(bgrp != i)
            {
                ans+= (bintr-(n(d(p,ctr[i]))*v(a[i].size())));
            }
        }
    }
    return ans;
}

double gloss(const std::vector<std::valarray<double>>& db, const std::vector<std::vector<size_t>>& a, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v)
{
    std::vector<bool> vst(db.size(),true);
    double ans = 0.0;
    std::vector<std::valarray<double>> ctr(a.size(),std::valarray<double>());
    for(size_t i = 0; i < a.size(); ++i)
    {
        if(a[i].size() > 0)
        {
            ctr[i] = db[a[i][0]];
            for(size_t j = 1; j < a[i].size(); ++j)
            {
                ctr[i]+= db[a[i][j]];
            }
            ctr[i]/= a[i].size();
        }
    }
    for(size_t i = 0; i < a.size(); ++i)
    {
        for(const auto& p: a[i])
        {
            size_t bgrp = ctr.size();
            double bintr = 1.0;
            for(size_t k = 0; k < ctr.size(); ++k)
            {
                if(a[k].size() > 0)
                {
                    double intr = n(d(db[p],ctr[k]))*v(a[k].size());
                    if(intr > bintr)
                    {
                        bgrp = k;
                        bintr = intr;
                    }
                }
            }
            if(bgrp != i)
            {
                ans+= (bintr-(n(d(db[p],ctr[i]))*v(a[i].size())));
            }
            vst[p] = false;
        }
    }
    for(size_t i = 0; i < db.size(); ++i)
    {
        if(vst[i])
        {
            double bintr = 1.0;
            for(size_t k = 0; k < ctr.size(); ++k)
            {
                if(a[k].size() > 0)
                {
                    double intr = n(d(db[i],ctr[k]))*v(a[k].size());
                    if(intr > bintr)
                    {
                        bintr = intr;
                    }
                }
            }
            if(bintr > 1.0)
            {
                ans+= (bintr - 1.0);
            }
        }
    }
    return ans;
}

double glossFull(const std::vector<std::valarray<double>>& db, const std::vector<std::vector<size_t>>& a, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v)
{
    double ans = 0.0;
    std::vector<std::valarray<double>> ctr(a.size(),std::valarray<double>());
    for(size_t i = 0; i < a.size(); ++i)
    {
        if(a[i].size() > 0)
        {
            ctr[i] = db[a[i][0]];
            for(size_t j = 1; j < a[i].size(); ++j)
            {
                ctr[i]+= db[a[i][j]];
            }
            ctr[i]/= a[i].size();
        }
    }
    for(size_t i = 0; i < a.size(); ++i)
    {
        for(const auto& p: a[i])
        {
            size_t bgrp = ctr.size();
            double bintr = 1.0;
            for(size_t k = 0; k < ctr.size(); ++k)
            {
                if(a[k].size() > 0)
                {
                    double intr = n(d(db[p],ctr[k]))*v(a[k].size());
                    if(intr > bintr)
                    {
                        bgrp = k;
                        bintr = intr;
                    }
                }
            }
            if(bgrp != i)
            {
                ans+= (bintr-(n(d(db[p],ctr[i]))*v(a[i].size())));
            }
        }
    }
    return ans;
}

unsigned glossing(const std::vector<std::vector<std::valarray<double>>>& a, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v)
{
    unsigned ans = 0;
    std::vector<std::valarray<double>> ctr(a.size(),std::valarray<double>());
    for(size_t i = 0; i < a.size(); ++i)
    {
        if(a[i].size() > 0)
        {
            ctr[i] = a[i][0];
            for(size_t j = 1; j < a[i].size(); ++j)
            {
                ctr[i]+= a[i][j];
            }
            ctr[i]/= a[i].size();
        }
    }
    for(size_t i = 0; i < a.size(); ++i)
    {
        for(const auto& p: a[i])
        {
            double bintr = n(d(p,ctr[i]))*v(a[i].size());
            if(bintr < 1.0)
            {
                ++ans;
            }
            else
            {
                for(size_t k = 0; k < ctr.size(); ++k)
                {
                    if(a[k].size() > 0)
                    {
                        if(n(d(p,ctr[k]))*v(a[k].size()) > bintr)
                        {
                            ++ans;
                            break;
                        }
                    }
                }
            }
        }
    }
    return ans;
}

unsigned glossing(const std::vector<std::valarray<double>>& db, const std::vector<std::vector<size_t>>& a, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v)
{
    std::vector<bool> vst(db.size(),true);
    unsigned ans = 0;
    std::vector<std::valarray<double>> ctr(a.size(),std::valarray<double>());
    for(size_t i = 0; i < a.size(); ++i)
    {
        if(a[i].size() > 0)
        {
            ctr[i] = db[a[i][0]];
            for(size_t j = 1; j < a[i].size(); ++j)
            {
                ctr[i]+= db[a[i][j]];
            }
            ctr[i]/= a[i].size();
        }
    }
    for(size_t i = 0; i < a.size(); ++i)
    {
        for(const auto& p: a[i])
        {
            double bintr = n(d(db[p],ctr[i]))*v(a[i].size());
            if(bintr < 1.0)
            {
                ++ans;
            }
            else
            {
                for(size_t k = 0; k < ctr.size(); ++k)
                {
                    if(a[k].size() > 0)
                    {
                        if(n(d(db[p],ctr[k]))*v(a[k].size()) > bintr)
                        {
                            ++ans;
                            break;
                        }
                    }
                }
            }
            vst[p] = false;
        }
    }
    for(size_t i = 0; i < db.size(); ++i)
    {
        if(vst[i])
        {
            double bintr = 1.0;
            for(size_t k = 0; k < ctr.size(); ++k)
            {
                if(a[k].size() > 0)
                {
                    if(n(d(db[i],ctr[k]))*v(a[k].size()) > bintr)
                    {
                        ++ans;
                        break;
                    }
                }
            }
        }
    }
    return ans;
}

unsigned glossingFull(const std::vector<std::valarray<double>>& db, const std::vector<std::vector<size_t>>& a, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v)
{
    unsigned ans = 0;
    std::vector<std::valarray<double>> ctr(a.size(),std::valarray<double>());
    for(size_t i = 0; i < a.size(); ++i)
    {
        if(a[i].size() > 0)
        {
            ctr[i] = db[a[i][0]];
            for(size_t j = 1; j < a[i].size(); ++j)
            {
                ctr[i]+= db[a[i][j]];
            }
            ctr[i]/= a[i].size();
        }
    }
    for(size_t i = 0; i < a.size(); ++i)
    {
        for(const auto& p: a[i])
        {
            double bintr = n(d(db[p],ctr[i]))*v(a[i].size());
            if(bintr < 1.0)
            {
                ++ans;
            }
            else
            {
                for(size_t k = 0; k < ctr.size(); ++k)
                {
                    if(a[k].size() > 0)
                    {
                        if(n(d(db[p],ctr[k]))*v(a[k].size()) > bintr)
                        {
                            ++ans;
                            break;
                        }
                    }
                }
            }
        }
    }
    return ans;
}

std::vector<size_t> makeLst(size_t dbs, const std::vector<std::vector<size_t>>& af)
{
    std::vector<bool> vst(dbs,true);
    std::vector<size_t> ans(dbs);
    for(size_t i = 0; i < af.size(); ++i)
    {
        for(const auto& v: af[i])
        {
            ans[v] = i;
            vst[v] = false;
        }
    }
    size_t k = af.size();
    for(size_t i = 0; i < dbs; ++i)
    {
        if(vst[i])
        {
            ans[i] = k++;
        }
    }
    return ans;
}

std::vector<size_t> makeLstFull(size_t dbs, const std::vector<std::vector<size_t>>& af)
{
    std::vector<size_t> ans(dbs);
    for(size_t i = 0; i < af.size(); ++i)
    {
        for(const auto& v: af[i])
        {
            ans[v] = i;
        }
    }
    return ans;
}
