#include <cstddef>
#include <utility>
#include <vector>
#include <valarray>
#include <functional>


#ifndef RECOREF
std::vector<std::pair<size_t,double>> optics(const std::vector<std::valarray<double>>&,double,unsigned,const std::function<double(std::valarray<double>,std::valarray<double>)>&);

std::vector<std::vector<size_t>> opticsExtract(const std::vector<std::valarray<double>>&,const std::vector<std::pair<size_t,double>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&);

std::vector<std::vector<size_t>> opticsExtract(const std::vector<std::pair<size_t,double>>&,double);

std::vector<std::vector<size_t>> kmeans(const std::vector<std::valarray<double>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&,unsigned,unsigned);

std::vector<std::vector<std::valarray<double>>> makeGrp(const std::vector<std::valarray<double>>&,const std::vector<std::vector<size_t>>&);

std::vector<std::vector<std::valarray<double>>> makeGrpFull(const std::vector<std::valarray<double>>&,const std::vector<std::vector<size_t>>&);

double gvalue(const std::vector<std::vector<std::valarray<double>>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&);

double gvalue(const std::vector<std::valarray<double>>&,const std::vector<std::vector<size_t>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&);

double gvalueFull(const std::vector<std::valarray<double>>&,const std::vector<std::vector<size_t>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&);

double gloss(const std::vector<std::vector<std::valarray<double>>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&);

double gloss(const std::vector<std::valarray<double>>&,const std::vector<std::vector<size_t>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&);

double glossFull(const std::vector<std::valarray<double>>&,const std::vector<std::vector<size_t>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&);

unsigned glossing(const std::vector<std::vector<std::valarray<double>>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&);

unsigned glossing(const std::vector<std::valarray<double>>&,const std::vector<std::vector<size_t>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&);

unsigned glossingFull(const std::vector<std::valarray<double>>&,const std::vector<std::vector<size_t>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&);

std::vector<size_t> makeLst(size_t,const std::vector<std::vector<size_t>>&);

std::vector<size_t> makeLstFull(size_t,const std::vector<std::vector<size_t>>&);
#define RECOREF
#endif
