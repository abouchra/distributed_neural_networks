#include <cstddef>
#include <utility>
#include <vector>
#include <valarray>
#include <functional>


#ifndef RECOM
std::vector<std::pair<size_t,std::vector<std::vector<size_t>>>> recommend(const std::vector<std::vector<std::valarray<double>>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&,size_t,bool,bool,unsigned,unsigned);

std::vector<std::vector<size_t>> recommend(const std::vector<std::valarray<double>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&,bool,bool,unsigned,unsigned);

std::vector<std::vector<size_t>> recommend(const std::vector<std::valarray<double>>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&,const std::vector<std::valarray<double>>&,bool,bool);

size_t recommend(const std::valarray<double>&,const std::function<double(std::valarray<double>,std::valarray<double>)>&,const std::function<double(double)>&,const std::function<double(unsigned)>&,const std::vector<std::valarray<double>>&,const std::vector<size_t>&,bool);
#define RECOM
#endif
