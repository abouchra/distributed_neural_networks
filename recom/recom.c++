#include <limits>
#include <chrono>
#include <random>
#include "recom.h++"


std::vector<std::pair<size_t,std::vector<std::vector<size_t>>>> recommend(const std::vector<std::vector<std::valarray<double>>>& testr, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v, size_t nvects, bool conv, bool atom, unsigned prc, unsigned mmt)
{
    std::vector<std::pair<size_t,std::vector<std::vector<size_t>>>> ans(nvects,std::pair<size_t,std::vector<std::vector<size_t>>>(testr.size(),std::vector<std::vector<size_t>>()));
    std::vector<double> las(nvects,1.0);
    for(size_t ts = 0; ts < testr.size(); ++ts)
    {
        size_t m = testr[ts].size();
        double la = (double)m, vla;
        std::vector<std::vector<size_t>> vgrp;
        size_t k = 0;
        unsigned pmm = mmt;
        do
        {
            vla = la;
            la = (double)m;
            double lat;
            ++k;
            for(unsigned tra = 0; tra < prc; ++tra)
            {
                std::vector<std::valarray<double>> ctr;
                std::vector<std::vector<size_t>> ngrp;
                std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());
                std::uniform_int_distribution<size_t> randGen(0,m-1);
                std::vector<size_t> obgrp(m,k);
                size_t bv = randGen(gen);
                ctr.reserve(k);
                ngrp.reserve(k);
                ctr.push_back(testr[ts][bv]);
                ngrp.push_back(std::vector<size_t>(1,bv));
                std::valarray<double> ds(std::numeric_limits<double>::infinity(),m);
                for(size_t i = 0; i < m; ++i)
                {
                    double dv = d(testr[ts][i],testr[ts][bv]);
                    ds[i] = dv*dv;
                }
                for(size_t i = 1; i < k; ++i)
                {
                    std::uniform_real_distribution<double> randGen2(0.0,ds.sum());
                    double rv = randGen2(gen);
                    size_t nv;
                    for(nv = 0; ds[nv] < rv; ++nv)
                    {
                        rv-= ds[nv];
                    }
                    obgrp[nv] = ngrp.size();
                    ctr.push_back(testr[ts][nv]);
                    ngrp.push_back(std::vector<size_t>(1,nv));
                    for(size_t j = 0; j < m; ++j)
                    {
                        double dist = d(testr[ts][j],testr[ts][nv]);
                        double d2 = dist*dist;
                        if(d2 < ds[j])
                        {
                            ds[j] = d2;
                        }
                    }
                }
                std::vector<std::vector<size_t>> grp;
                double vrr = 0.0;
                do
                {
                    grp = ngrp;
                    ngrp.clear();
                    ngrp.resize(k);
                    for(size_t i = 0; i < k; ++i)
                    {
                        ctr[i] = 0.0;
                        for(size_t j = 0; j < grp[i].size(); ++j)
                        {
                            ctr[i]+= testr[ts][grp[i][j]];
                        }
                        ctr[i]/= grp[i].size();
                    }
                    if(conv)
                    {
                        double vr = 0.0;
                        for(size_t i = 0; i < k; ++i)
                        {
                            double vg = v(grp[i].size());
                            for(const auto& p: grp[i])
                            {
                                vr+= n(d(testr[ts][p],ctr[i]))*vg;
                            }
                        }
                        if(vr <= vrr)
                        {
                            break;
                        }
                        vrr = vr;
                    }
                    std::vector<unsigned> siz;
                    std::vector<unsigned> nsiz(k,m);
                    std::vector<size_t> bgrp;
                    bgrp.resize(m);
                    do
                    {
                        siz = nsiz;
                        for(size_t i = 0; i < k; ++i)
                        {
                            nsiz[i] = 0;
                        }
                        for(size_t i = 0; i < m; ++i)
                        {
                            double bintr = 1.0;
                            bgrp[i] = k;
                            for(size_t j = 0; j < k; ++j)
                            {
                                double intr = (atom)?(n(d(testr[ts][i],ctr[j]))*v(siz[j])):((obgrp[i] == j)?(n(d(testr[ts][i],ctr[j]))*v(siz[j])):(n(d(testr[ts][i],((siz[j]*ctr[j])+testr[ts][i])/(siz[j]+1)))*v(siz[j]+1)));
                                if(intr > bintr)
                                {
                                    bgrp[i] = j;
                                    bintr = intr;
                                }
                            }
                            if(bgrp[i] < k)
                            {
                                ++nsiz[bgrp[i]];
                            }
                        }
                    }
                    while((nsiz != siz) && (!conv || std::accumulate(nsiz.begin(),nsiz.end(),0) < std::accumulate(siz.begin(),siz.end(),0)));
                    for(size_t i = 0; i < m; ++i)
                    {
                        if(bgrp[i] < k)
                        {
                            ngrp[bgrp[i]].push_back(i);
                        }
                    }
                    obgrp = bgrp;
                }
                while(ngrp != grp);
                lat = 0.0;
                size_t sl = m;
                for(size_t i = 0; i < k; ++i)
                {
                    double gv = v(grp[i].size());
                    for(size_t j = 0; j < grp[i].size(); ++j)
                    {
                        lat+= n(d(testr[ts][grp[i][j]],ctr[i]))*gv;
                        --sl;
                    }
                }
                lat+= (double)sl;
                if(lat > la)
                {
                    if(lat > vla)
                    {
                        vgrp = grp;
                    }
                    la = lat;
                }
            }
            if(la > vla)
            {
                pmm = mmt;
            }
            else
            {
                --pmm;
            }
        }
        while(pmm);
        vla/= (double)m;
        double tla, tla2;
        std::pair<size_t,std::vector<std::vector<size_t>>> tmp, tmp2;
        size_t i;
        for(i = 0; i < nvects; ++i)
        {
            if(las[i] < vla)
            {
                tla = las[i];
                tmp = ans[i];
                las[i] = vla;
                ans[i] = std::pair<size_t,std::vector<std::vector<size_t>>>(ts,vgrp);
                ++i;
                break;
            }
        }
        for(; i < nvects; ++i)
        {
            tla2 = las[i];
            tmp2 = ans[i];
            las[i] = tla;
            ans[i] = tmp;
            tla = tla2;
            tmp = tmp2;
        }
    }
    return ans;
}

std::vector<std::vector<size_t>> recommend(const std::vector<std::valarray<double>>& testr, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v, bool conv, bool atom, unsigned prc, unsigned mmt)
{
    std::vector<std::vector<size_t>> ans;
    size_t m = testr.size();
    double la = (double)m, vla;
    size_t k = 0;
    unsigned pmm = mmt;
    do
    {
        vla = la;
        double lat;
        ++k;
        for(unsigned tra = 0; tra < prc; ++tra)
        {
            std::vector<std::valarray<double>> ctr;
            std::vector<std::vector<size_t>> ngrp;
            std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());
            std::uniform_int_distribution<size_t> randGen(0,m-1);
            std::vector<size_t> obgrp(m,k);
            size_t bv = randGen(gen);
            ctr.reserve(k);
            ngrp.reserve(k);
            ctr.push_back(testr[bv]);
            ngrp.push_back(std::vector<size_t>(1,bv));
            obgrp[bv] = 0;
            std::valarray<double> ds(std::numeric_limits<double>::infinity(),m);
            for(size_t i = 0; i < m; ++i)
            {
                double dv = d(testr[i],testr[bv]);
                ds[i] = dv*dv;
            }
            for(size_t i = 1; i < k; ++i)
            {
                std::uniform_real_distribution<double> randGen2(0.0,ds.sum());
                double rv = randGen2(gen);
                size_t nv;
                for(nv = 0; ds[nv] < rv; ++nv)
                {
                    rv-= ds[nv];
                }
                obgrp[nv] = ngrp.size();
                ctr.push_back(testr[nv]);
                ngrp.push_back(std::vector<size_t>(1,nv));
                for(size_t j = 0; j < m; ++j)
                {
                    double dist = d(testr[j],testr[nv]);
                    double d2 = dist*dist;
                    if(d2 < ds[j])
                    {
                        ds[j] = d2;
                    }
                }
            }
            std::vector<std::vector<size_t>> grp;
            double vrr = 0.0;
            do
            {
                grp = ngrp;
                ngrp.clear();
                ngrp.resize(k);
                for(size_t i = 0; i < k; ++i)
                {
                    ctr[i] = 0.0;
                    for(size_t j = 0; j < grp[i].size(); ++j)
                    {
                        ctr[i]+= testr[grp[i][j]];
                    }
                    ctr[i]/= grp[i].size();
                }
                if(conv)
                {
                    double vr = 0.0;
                    for(size_t i = 0; i < k; ++i)
                    {
                        double vg = v(grp[i].size());
                        for(const auto& p: grp[i])
                        {
                            vr+= n(d(testr[p],ctr[i]))*vg;
                        }
                    }
                    if(vr <= vrr)
                    {
                        break;
                    }
                    vrr = vr;
                }
                std::vector<unsigned> siz;
                std::vector<unsigned> nsiz(k,m);
                std::vector<size_t> bgrp;
                bgrp.resize(m);
                do
                {
                    siz = nsiz;
                    for(size_t i = 0; i < k; ++i)
                    {
                        nsiz[i] = 0;
                    }
                    for(size_t i = 0; i < m; ++i)
                    {
                        double bintr = 1.0;
                        bgrp[i] = k;
                        for(size_t j = 0; j < k; ++j)
                        {
                            double intr = (atom)?(n(d(testr[i],ctr[j]))*v(siz[j])):((obgrp[i] == j)?(n(d(testr[i],ctr[j]))*v(siz[j])):(n(d(testr[i],((siz[j]*ctr[j])+testr[i])/(siz[j]+1)))*v(siz[j]+1)));
                            if(intr > bintr)
                            {
                                bgrp[i] = j;
                                bintr = intr;
                            }
                        }
                        if(bgrp[i] < k)
                        {
                            ++nsiz[bgrp[i]];
                        }
                    }
                }
                while((nsiz != siz) && (!conv || std::accumulate(nsiz.begin(),nsiz.end(),0) < std::accumulate(siz.begin(),siz.end(),0)));
                for(size_t i = 0; i < m; ++i)
                {
                    if(bgrp[i] < k)
                    {
                        ngrp[bgrp[i]].push_back(i);
                    }
                }
                obgrp = bgrp;
            }
            while(ngrp != grp);
            lat = 0.0;
            size_t sl = m;
            for(size_t i = 0; i < k; ++i)
            {
                double gv = v(grp[i].size());
                for(size_t j = 0; j < grp[i].size(); ++j)
                {
                    lat+= n(d(testr[grp[i][j]],ctr[i]))*gv;
                    --sl;
                }
            }
            lat+= (double)sl;
            if(lat > la)
            {
                if(lat > vla)
                {
                    ans = grp;
                }
                la = lat;
            }
        }
        if(la > vla)
        {
            pmm = mmt;
        }
        else
        {
            --pmm;
        }
    }
    while(pmm);
    return ans;
}

std::vector<std::vector<size_t>> recommend(const std::vector<std::valarray<double>>& testr, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v, const std::vector<std::valarray<double>>& sd, bool conv, bool atom)
{
    std::vector<std::vector<size_t>> ans;
    size_t m = testr.size();
    size_t k = sd.size();
    std::vector<std::valarray<double>> ctr(sd);
    std::vector<std::vector<size_t>> ngrp;
    std::vector<size_t> obgrp(m,k);
    double vrr = 0.0;
    goto stf3;
    do
    {
        ans = ngrp;
        ngrp.clear();
        for(size_t i = 0; i < k; ++i)
        {
            ctr[i] = 0.0;
            for(size_t j = 0; j < ans[i].size(); ++j)
            {
                ctr[i]+= testr[ans[i][j]];
            }
            ctr[i]/= ans[i].size();
        }
        if(conv)
        {
            double vr = 0.0;
            for(size_t i = 0; i < k; ++i)
            {
                double vg = v(ans[i].size());
                for(const auto& p: ans[i])
                {
                    vr+= n(d(testr[p],ctr[i]))*vg;
                }
            }
            if(vr <= vrr)
            {
                break;
            }
            vrr = vr;
        }
        stf3:
        ngrp.reserve(k);
        std::vector<unsigned> siz;
        std::vector<unsigned> nsiz(k,m);
        std::vector<size_t> bgrp;
        bgrp.resize(m);
        do
        {
            siz = nsiz;
            for(size_t i = 0; i < k; ++i)
            {
                nsiz[i] = 0;
            }
            for(size_t i = 0; i < m; ++i)
            {
                double bintr = 1.0;
                bgrp[i] = k;
                for(size_t j = 0; j < k; ++j)
                {
                    double intr = (atom)?(n(d(testr[i],ctr[j]))*v(siz[j])):((obgrp[i] == j)?(n(d(testr[i],ctr[j]))*v(siz[j])):(n(d(testr[i],((siz[j]*ctr[j])+testr[i])/(siz[j]+1)))*v(siz[j]+1)));
                    if(intr > bintr)
                    {
                        bgrp[i] = j;
                        bintr = intr;
                    }
                }
                if(bgrp[i] < k)
                {
                    ++nsiz[bgrp[i]];
                }
            }
        }
        while((nsiz != siz) && (!conv || std::accumulate(nsiz.begin(),nsiz.end(),0) < std::accumulate(siz.begin(),siz.end(),0)));
        for(size_t i = 0; i < m; ++i)
        {
            if(bgrp[i] < k)
            {
                ngrp[bgrp[i]].push_back(i);
            }
        }
        obgrp = bgrp;
    }
    while(ngrp != ans);
    return ans;
}

size_t recommend(const std::valarray<double>& testr, const std::function<double(std::valarray<double>,std::valarray<double>)>& d, const std::function<double(double)>& n, const std::function<double(unsigned)>& v, const std::vector<std::valarray<double>>& ctr, const std::vector<size_t>& siz, bool atom)
{
    size_t ans = ctr.size();
    double gn = 1.0;
    for(size_t i = 0; i < ctr.size(); ++i)
    {
        double intr = (atom)?(n(d(testr,ctr[i]))*v(siz[i])):(n(d(testr,((siz[i]*ctr[i])+testr)/(siz[i]+1)))*v(siz[i]+1));
        if(intr > gn)
        {
            ans = i;
            gn = intr;
        }
    }
    return ans;
}
