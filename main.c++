#include <cstddef>
#include <cstdio>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <random>
#include <utility>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include "helpers/helpers.h++"
#include "dnn_tools/sgd/sgd.h++"
#include "data_src/mnist/mnist.h++"
#include "data_src/femnist/femnist.h++"
#include "data_src/vsn/vsn.h++"
#include "data_src/metro/metro.h++"
#include "data_src/binseq/binseq.h++"
#include "data_src/sdr/sdr.h++"
#include "nets/mlnn/mlnn.h++"
#include "nets/lstm/lstm.h++"
#include "nets/assoc/assoc.h++"
#include "recom/recom.h++"
#include "recom/ref.h++"

#define PRX "../data/"
#define TRI PRX"train-images-idx3-ubyte"
#define TRV PRX"train-labels-idx1-ubyte"
#define TSI PRX"t10k-images-idx3-ubyte"
#define TSV PRX"t10k-labels-idx1-ubyte"
#define FMI PRX"femnist/%s/fmp-%s-%u-img-ubyte"
#define FMV PRX"femnist/%s/fmp-%s-%u-lab-ubyte"
#define VSI PRX"VSN/x%u"
#define VSV PRX"VSN/y%u"
#define MLT PRX"Data_Metro_norm2.csv"
#define ARG_ERR "Error! Incorrect arguments!\nCorrect usage:\ndnn <test file>"


int main(int argc, char *argv[])
{
    bool tra = false, itra = false, seq = true, reo = false, reod = false, idinit = true, clpr = false, rncv = false, rndcl = true, afff = false;
    unsigned rs, rd, sync, actn, noise, nsets, drprc, drmmt;
    size_t n, bs, mbs, tst, tnd, tev, itev, ttst, ttnd, ittst, ittnd, sqlen, csqlen, tcsqlen, itcsqlen, prlen, tprlen, itprlen, drnv, tline, mrcls;
    double lr, thld;
    std::string type, rcf1, rcf2, rcf3, rcf4, rcf5, rcf6, afcf1, afcf2, afcf3, afcf4, afcf5, afcf6, fmntp = "dlu";
    std::vector<size_t> layers, players, mlines;
    std::function<double(size_t)> weightd;
    std::function<double(double)> normf, dnormf;
    std::function<std::valarray<double>(void)> actfpd, gactfpd, pactfpd, pgactfpd, ngactfpd;
    std::function<double(double,std::valarray<double>)> actf, gactf, pactf;
    std::vector<std::function<double(double,std::valarray<double>)>> dactf, gdactf, pdactf;
    std::unordered_map<size_t,std::array<size_t,DNN_MNIST_C>> perm;
    std::unordered_map<size_t,std::array<size_t,DNN_MNIST_N>> iperm;
    std::unordered_map<size_t,std::pair<unsigned,unsigned>> aperm;
    std::vector<std::array<uint8_t,DNN_MNIST_V>> mask;
    std::unordered_map<size_t,size_t> amask;
    std::vector<std::vector<size_t>> models, pmodels;
    std::vector<std::list<size_t>> modelDeps;
    std::vector<std::unordered_set<size_t>> modelNets;
    std::vector<std::valarray<double>> sr, vv;
    std::vector<std::vector<std::valarray<double>>> trace, itrace;
    std::vector<std::vector<std::vector<std::vector<bool>>>> mcls;
    std::vector<std::vector<std::vector<std::vector<size_t>>>> mpos;
    std::vector<size_t> am;
    double dgenpt = 0.0;
    std::vector<double> dgenp;
    std::vector<std::function<std::valarray<double>(void)>> dgen;
    std::function<double(std::valarray<double>,std::valarray<double>)> ddf; std::function<double(double)> dnf;
    std::function<double(unsigned)> dvf;
    if(argc < 2)
    {
        std::cout << ARG_ERR << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string pfilen(argv[1]);
    std::ifstream pfile = std::ifstream(pfilen,std::ifstream::in);
    while(!pfile.eof())
    {
        std::string line, token;
        std::getline(pfile,line);
        std::stringstream ls(line);
        std::getline(ls,token,' ');
        if(token == "Nnet")
        {
            ls >> n;
            am.resize(n);
        }
        else if(token == "Bsize")
        {
            ls >> bs;
        }
        else if(token == "Mbsize")
        {
            ls >> mbs;
        }
        else if(token == "Sqlen")
        {
            ls >> sqlen;
        }
        else if(token == "Csqlen")
        {
            ls >> csqlen;
        }
        else if(token == "Tcsqlen")
        {
            ls >> tcsqlen;
        }
        else if(token == "Itqlen")
        {
            ls >> itcsqlen;
        }
        else if(token == "Prlen")
        {
            ls >> prlen;
        }
        else if(token == "Tprlen")
        {
            ls >> tprlen;
        }
        else if(token == "Itprlen")
        {
            ls >> itprlen;
        }
        else if(token == "Testrg")
        {
            ls >> tst >> tnd;
        }
        else if(token == "Trtrg")
        {
            ls >> ttst >> ttnd;
        }
        else if(token == "Itrtrg")
        {
            ls >> ittst >> ittnd;
        }
        else if(token == "Lrate")
        {
            ls >> lr;
        }
        else if(token == "Seq")
        {
            seq = true;
        }
        else if(token == "Nseq")
        {
            seq = false;
        }
        else if(token == "Reo")
        {
            reo = true;
        }
        else if(token == "Nreo")
        {
            reo = false;
        }
        else if(token == "Reod")
        {
            reod = true;
        }
        else if(token == "Nreod")
        {
            reod = false;
        }
        else if(token == "Idinit")
        {
            idinit = true;
        }
        else if(token == "Nidinit")
        {
            idinit = false;
        }
        else if(token == "Actn")
        {
            ls >> actn;
        }
        else if(token == "Noise")
        {
            ls >> noise;
        }
        else if(token == "Thld")
        {
            ls >> thld;
        }
        else if(token == "Nsets")
        {
            ls >> nsets;
        }
        else if(token == "Type")
        {
            ls >> type;
            if(type == "digit" || type == "femnist" || type == "vsn"  || type == "metro" || type == "march" || type == "assoc")
            {
                weightd = []([[maybe_unused]] size_t n){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-1.0,1.0);return randGen(gen);};
            }
            else
            {
                exit(EXIT_FAILURE);
            }
        }
        else if(token == "Perm")
        {
            std::array<size_t,DNN_MNIST_C> p;
            for(size_t i = 0; i < DNN_MNIST_C; ++i)
            {
                ls >> p[i];
            }
            while(!ls.eof())
            {
                size_t mr;
                ls >> mr;
                perm[mr] = p;
            }
        }
        else if(token == "Aperm")
        {
            unsigned a, b;
            ls >> a;
            ls >> b;
            while(!ls.eof())
            {
                size_t mr;
                ls >> mr;
                aperm[mr] = std::pair(a,b);
            }
        }
        else if(token == "Mask")
        {
            std::array<uint8_t,DNN_MNIST_V> m;
            m.fill(0);
            while(!ls.eof())
            {
                size_t mr;
                ls >> mr;
                m[mr] = 255;
            }
            mask.push_back(m);
        }
        else if(token == "Amask")
        {
            unsigned m;
            ls >> m;
            while(!ls.eof())
            {
                size_t mr;
                ls >> mr;
                amask[mr] = m;
            }
        }
        else if(token == "Rota")
        {
            std::array<size_t,DNN_MNIST_N> p;
            uint8_t d;
            ls >> d;
            switch(d)
            {
                case 1:
                    p = {27,55,83,111,139,167,195,223,251,279,307,335,363,391,419,447,475,503,531,559,587,615,643,671,699,727,755,783,26,54,82,110,138,166,194,222,250,278,306,334,362,390,418,446,474,502,530,558,586,614,642,670,698,726,754,782,25,53,81,109,137,165,193,221,249,277,305,333,361,389,417,445,473,501,529,557,585,613,641,669,697,725,753,781,24,52,80,108,136,164,192,220,248,276,304,332,360,388,416,444,472,500,528,556,584,612,640,668,696,724,752,780,23,51,79,107,135,163,191,219,247,275,303,331,359,387,415,443,471,499,527,555,583,611,639,667,695,723,751,779,22,50,78,106,134,162,190,218,246,274,302,330,358,386,414,442,470,498,526,554,582,610,638,666,694,722,750,778,21,49,77,105,133,161,189,217,245,273,301,329,357,385,413,441,469,497,525,553,581,609,637,665,693,721,749,777,20,48,76,104,132,160,188,216,244,272,300,328,356,384,412,440,468,496,524,552,580,608,636,664,692,720,748,776,19,47,75,103,131,159,187,215,243,271,299,327,355,383,411,439,467,495,523,551,579,607,635,663,691,719,747,775,18,46,74,102,130,158,186,214,242,270,298,326,354,382,410,438,466,494,522,550,578,606,634,662,690,718,746,774,17,45,73,101,129,157,185,213,241,269,297,325,353,381,409,437,465,493,521,549,577,605,633,661,689,717,745,773,16,44,72,100,128,156,184,212,240,268,296,324,352,380,408,436,464,492,520,548,576,604,632,660,688,716,744,772,15,43,71,99,127,155,183,211,239,267,295,323,351,379,407,435,463,491,519,547,575,603,631,659,687,715,743,771,14,42,70,98,126,154,182,210,238,266,294,322,350,378,406,434,462,490,518,546,574,602,630,658,686,714,742,770,13,41,69,97,125,153,181,209,237,265,293,321,349,377,405,433,461,489,517,545,573,601,629,657,685,713,741,769,12,40,68,96,124,152,180,208,236,264,292,320,348,376,404,432,460,488,516,544,572,600,628,656,684,712,740,768,11,39,67,95,123,151,179,207,235,263,291,319,347,375,403,431,459,487,515,543,571,599,627,655,683,711,739,767,10,38,66,94,122,150,178,206,234,262,290,318,346,374,402,430,458,486,514,542,570,598,626,654,682,710,738,766,9,37,65,93,121,149,177,205,233,261,289,317,345,373,401,429,457,485,513,541,569,597,625,653,681,709,737,765,8,36,64,92,120,148,176,204,232,260,288,316,344,372,400,428,456,484,512,540,568,596,624,652,680,708,736,764,7,35,63,91,119,147,175,203,231,259,287,315,343,371,399,427,455,483,511,539,567,595,623,651,679,707,735,763,6,34,62,90,118,146,174,202,230,258,286,314,342,370,398,426,454,482,510,538,566,594,622,650,678,706,734,762,5,33,61,89,117,145,173,201,229,257,285,313,341,369,397,425,453,481,509,537,565,593,621,649,677,705,733,761,4,32,60,88,116,144,172,200,228,256,284,312,340,368,396,424,452,480,508,536,564,592,620,648,676,704,732,760,3,31,59,87,115,143,171,199,227,255,283,311,339,367,395,423,451,479,507,535,563,591,619,647,675,703,731,759,2,30,58,86,114,142,170,198,226,254,282,310,338,366,394,422,450,478,506,534,562,590,618,646,674,702,730,758,1,29,57,85,113,141,169,197,225,253,281,309,337,365,393,421,449,477,505,533,561,589,617,645,673,701,729,757,0,28,56,84,112,140,168,196,224,252,280,308,336,364,392,420,448,476,504,532,560,588,616,644,672,700,728,756};
                    break;
                case 2:
                    p = {783,782,781,780,779,778,777,776,775,774,773,772,771,770,769,768,767,766,765,764,763,762,761,760,759,758,757,756,755,754,753,752,751,750,749,748,747,746,745,744,743,742,741,740,739,738,737,736,735,734,733,732,731,730,729,728,727,726,725,724,723,722,721,720,719,718,717,716,715,714,713,712,711,710,709,708,707,706,705,704,703,702,701,700,699,698,697,696,695,694,693,692,691,690,689,688,687,686,685,684,683,682,681,680,679,678,677,676,675,674,673,672,671,670,669,668,667,666,665,664,663,662,661,660,659,658,657,656,655,654,653,652,651,650,649,648,647,646,645,644,643,642,641,640,639,638,637,636,635,634,633,632,631,630,629,628,627,626,625,624,623,622,621,620,619,618,617,616,615,614,613,612,611,610,609,608,607,606,605,604,603,602,601,600,599,598,597,596,595,594,593,592,591,590,589,588,587,586,585,584,583,582,581,580,579,578,577,576,575,574,573,572,571,570,569,568,567,566,565,564,563,562,561,560,559,558,557,556,555,554,553,552,551,550,549,548,547,546,545,544,543,542,541,540,539,538,537,536,535,534,533,532,531,530,529,528,527,526,525,524,523,522,521,520,519,518,517,516,515,514,513,512,511,510,509,508,507,506,505,504,503,502,501,500,499,498,497,496,495,494,493,492,491,490,489,488,487,486,485,484,483,482,481,480,479,478,477,476,475,474,473,472,471,470,469,468,467,466,465,464,463,462,461,460,459,458,457,456,455,454,453,452,451,450,449,448,447,446,445,444,443,442,441,440,439,438,437,436,435,434,433,432,431,430,429,428,427,426,425,424,423,422,421,420,419,418,417,416,415,414,413,412,411,410,409,408,407,406,405,404,403,402,401,400,399,398,397,396,395,394,393,392,391,390,389,388,387,386,385,384,383,382,381,380,379,378,377,376,375,374,373,372,371,370,369,368,367,366,365,364,363,362,361,360,359,358,357,356,355,354,353,352,351,350,349,348,347,346,345,344,343,342,341,340,339,338,337,336,335,334,333,332,331,330,329,328,327,326,325,324,323,322,321,320,319,318,317,316,315,314,313,312,311,310,309,308,307,306,305,304,303,302,301,300,299,298,297,296,295,294,293,292,291,290,289,288,287,286,285,284,283,282,281,280,279,278,277,276,275,274,273,272,271,270,269,268,267,266,265,264,263,262,261,260,259,258,257,256,255,254,253,252,251,250,249,248,247,246,245,244,243,242,241,240,239,238,237,236,235,234,233,232,231,230,229,228,227,226,225,224,223,222,221,220,219,218,217,216,215,214,213,212,211,210,209,208,207,206,205,204,203,202,201,200,199,198,197,196,195,194,193,192,191,190,189,188,187,186,185,184,183,182,181,180,179,178,177,176,175,174,173,172,171,170,169,168,167,166,165,164,163,162,161,160,159,158,157,156,155,154,153,152,151,150,149,148,147,146,145,144,143,142,141,140,139,138,137,136,135,134,133,132,131,130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0};
                    break;
                case 3:
                    p = {756,728,700,672,644,616,588,560,532,504,476,448,420,392,364,336,308,280,252,224,196,168,140,112,84,56,28,0,757,729,701,673,645,617,589,561,533,505,477,449,421,393,365,337,309,281,253,225,197,169,141,113,85,57,29,1,758,730,702,674,646,618,590,562,534,506,478,450,422,394,366,338,310,282,254,226,198,170,142,114,86,58,30,2,759,731,703,675,647,619,591,563,535,507,479,451,423,395,367,339,311,283,255,227,199,171,143,115,87,59,31,3,760,732,704,676,648,620,592,564,536,508,480,452,424,396,368,340,312,284,256,228,200,172,144,116,88,60,32,4,761,733,705,677,649,621,593,565,537,509,481,453,425,397,369,341,313,285,257,229,201,173,145,117,89,61,33,5,762,734,706,678,650,622,594,566,538,510,482,454,426,398,370,342,314,286,258,230,202,174,146,118,90,62,34,6,763,735,707,679,651,623,595,567,539,511,483,455,427,399,371,343,315,287,259,231,203,175,147,119,91,63,35,7,764,736,708,680,652,624,596,568,540,512,484,456,428,400,372,344,316,288,260,232,204,176,148,120,92,64,36,8,765,737,709,681,653,625,597,569,541,513,485,457,429,401,373,345,317,289,261,233,205,177,149,121,93,65,37,9,766,738,710,682,654,626,598,570,542,514,486,458,430,402,374,346,318,290,262,234,206,178,150,122,94,66,38,10,767,739,711,683,655,627,599,571,543,515,487,459,431,403,375,347,319,291,263,235,207,179,151,123,95,67,39,11,768,740,712,684,656,628,600,572,544,516,488,460,432,404,376,348,320,292,264,236,208,180,152,124,96,68,40,12,769,741,713,685,657,629,601,573,545,517,489,461,433,405,377,349,321,293,265,237,209,181,153,125,97,69,41,13,770,742,714,686,658,630,602,574,546,518,490,462,434,406,378,350,322,294,266,238,210,182,154,126,98,70,42,14,771,743,715,687,659,631,603,575,547,519,491,463,435,407,379,351,323,295,267,239,211,183,155,127,99,71,43,15,772,744,716,688,660,632,604,576,548,520,492,464,436,408,380,352,324,296,268,240,212,184,156,128,100,72,44,16,773,745,717,689,661,633,605,577,549,521,493,465,437,409,381,353,325,297,269,241,213,185,157,129,101,73,45,17,774,746,718,690,662,634,606,578,550,522,494,466,438,410,382,354,326,298,270,242,214,186,158,130,102,74,46,18,775,747,719,691,663,635,607,579,551,523,495,467,439,411,383,355,327,299,271,243,215,187,159,131,103,75,47,19,776,748,720,692,664,636,608,580,552,524,496,468,440,412,384,356,328,300,272,244,216,188,160,132,104,76,48,20,777,749,721,693,665,637,609,581,553,525,497,469,441,413,385,357,329,301,273,245,217,189,161,133,105,77,49,21,778,750,722,694,666,638,610,582,554,526,498,470,442,414,386,358,330,302,274,246,218,190,162,134,106,78,50,22,779,751,723,695,667,639,611,583,555,527,499,471,443,415,387,359,331,303,275,247,219,191,163,135,107,79,51,23,780,752,724,696,668,640,612,584,556,528,500,472,444,416,388,360,332,304,276,248,220,192,164,136,108,80,52,24,781,753,725,697,669,641,613,585,557,529,501,473,445,417,389,361,333,305,277,249,221,193,165,137,109,81,53,25,782,754,726,698,670,642,614,586,558,530,502,474,446,418,390,362,334,306,278,250,222,194,166,138,110,82,54,26,783,755,727,699,671,643,615,587,559,531,503,475,447,419,391,363,335,307,279,251,223,195,167,139,111,83,55,27};
                    break;
                default:
                    p = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,650,651,652,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,701,702,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761,762,763,764,765,766,767,768,769,770,771,772,773,774,775,776,777,778,779,780,781,782,783};
                    break;
            }
            while(!ls.eof())
            {
                size_t mr;
                ls >> mr;
                iperm[mr] = p;
            }
        }
        else if(token == "Mlines")
        {
            mlines.clear();
            while(!ls.eof())
            {
                mlines.push_back(0);
                ls >> mlines.back();
            }
        }
        else if(token == "Tline")
        {
            ls >> tline;
        }
        else if(token == "Rperms")
        {
            perm.clear();
        }
        else if(token == "Raperms")
        {
            aperm.clear();
        }
        else if(token == "Riperms")
        {
            iperm.clear();
        }
        else if(token == "Rmasks")
        {
            mask.clear();
            amask.clear();
        }
        else if(token == "Rmlines")
        {
            mlines.clear();
        }
        else if(token == "Nmar")
        {
            mcls.push_back(std::vector<std::vector<std::vector<bool>>>());
            mpos.push_back(std::vector<std::vector<std::vector<size_t>>>());
        }
        else if(token == "Mclass")
        {
            mcls.back().push_back(std::vector<std::vector<bool>>());
            size_t k = 0;
            while(!ls.eof())
            {
                mcls.back().back().push_back(std::vector<bool>());
                std::string vec;
                std::getline(ls,vec,' ');
                for(size_t i = 0; i < vec.length(); ++i)
                {
                    mcls.back().back()[k].push_back((vec[i] == '1')?true:false);
                }
                ++k;
            }
        }
        else if(token == "Mseq")
        {
            mpos.back().push_back(std::vector<std::vector<size_t>>());
            size_t k = 0;
            while(!ls.eof())
            {
                mpos.back().back().push_back(std::vector<size_t>());
                std::string sequ;
                std::getline(ls,sequ,'!');
                std::stringstream sq(sequ);
                while(!sq.eof())
                {
                    std::string v;
                    std::getline(sq,v,' ');
                    mpos.back().back()[k].push_back(std::stoul(v));
                }
                ++k;
            }
        }
        else if(token == "Rmm")
        {
            mcls.clear();
            mpos.clear();
        }
        else if(token == "Mrcls")
        {
            ls >> mrcls;
        }
        else if(token == "Ascha")
        {
            size_t pr, ch;
            ls >> ch;
            while(!ls.eof())
            {
                ls >> pr;
                am[pr] = ch;
            }
        }
        else if(token == "Layers")
        {
            layers.clear();
            while(!ls.eof())
            {
                layers.push_back(0);
                ls >> layers.back();
            }
        }
        else if(token == "Players")
        {
            players.clear();
            while(!ls.eof())
            {
                players.push_back(0);
                ls >> players.back();
            }
        }
        else if(token == "Model")
        {
            size_t mi = models.size();
            std::vector<size_t> mod(layers.size());
            for(size_t i = 0; i < layers.size(); ++i)
            {
                ls >> mod[i];
            }
            models.push_back(mod);
            modelDeps.push_back({});
            modelNets.push_back({});
            while(!ls.eof())
            {
                size_t mn;
                ls >> mn;
                modelDeps[mi].push_back(mn);
            }
        }
        else if(token == "Pmodel")
        {
            std::vector<size_t> mod(players.size());
            for(size_t i = 0; i < players.size(); ++i)
            {
                ls >> mod[i];
            }
            pmodels.push_back(mod);
        }
        else if(token == "Asmod")
        {
            size_t mi;
            ls >> mi;
            while(!ls.eof())
            {
                size_t nn;
                ls >> nn;
                modelNets[mi].insert(nn);
            }
        }
        else if(token == "Rmod")
        {
            models.clear();
            pmodels.clear();
            modelDeps.clear();
            modelNets.clear();
        }
        else if(token == "Trace")
        {
            tra = true;
            ls >> tev;
        }
        else if(token == "Ntrace")
        {
            tra = false;
        }
        else if(token == "Itrace")
        {
            itra = true;
            ls >> itev;
        }
        else if(token == "Nitrace")
        {
            itra = false;
        }
        else if(token == "Clpr")
        {
            std::getline(ls,rcf1,' ');
            std::getline(ls,rcf2,' ');
            std::getline(ls,rcf3,' ');
            std::getline(ls,rcf4,' ');
            std::getline(ls,rcf5,' ');
            std::getline(ls,rcf6,' ');
            clpr = true;
        }
        else if(token == "Nclpr")
        {
            clpr = false;
        }
        else if(token == "Aff")
        {
            std::getline(ls,afcf1,' ');
            std::getline(ls,afcf2,' ');
            std::getline(ls,afcf3,' ');
            std::getline(ls,afcf4,' ');
            std::getline(ls,afcf5,' ');
            std::getline(ls,afcf6,' ');
            afff = true;
        }
        else if(token == "NAff")
        {
            afff = false;
        }
        else if(token == "Rncv")
        {
            rncv = true;
        }
        else if(token == "Nrncv")
        {
            rncv = false;
        }
        else if(token == "Delmod")
        {
            size_t mn;
            ls >> mn;
            models.erase(models.begin()+mn);
            modelDeps.erase(modelDeps.begin()+mn);
            modelNets.erase(modelNets.begin()+mn);
            for(size_t i = 0; i < modelDeps.size(); ++i)
            {
                std::list<size_t> ndeps;
                for(const auto& d : modelDeps[i])
                {
                    if(d < mn)
                    {
                        ndeps.push_back(d);
                    }
                    else if(d > mn)
                    {
                        ndeps.push_back(d-1);
                    }
                }
            }
        }
        else if(token == "Actf")
        {
            std::string atf;
            std::getline(ls,atf,' ');
            if(atf == "lin")
            {
                actf = [](double x,[[maybe_unused]] std::valarray<double> p){return x;};
                dactf = {[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 1.0;},[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 0.0;}};
                actfpd = [](void){return std::valarray<double>(0);};
            }
            else if(atf == "afi")
            {
                actf = [](double x, std::valarray<double> p){return x+p[0];};
                dactf = {[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 1.0;},[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 1.0;}};
                actfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-1.0,1.0);return std::valarray<double>(randGen(gen),1);};
            }
            else if(atf == "tanh")
            {
                actf = [](double x, std::valarray<double> p){return tanh(x+p[0]);};
                dactf = {[](double x, std::valarray<double> p){return 1.0-pow(tanh(x+p[0]),2);},[](double x, std::valarray<double> p){return 1.0-pow(tanh(x+p[0]),2);}};
                actfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-1.0,1.0);return std::valarray<double>(randGen(gen),1);};
            }
            else if(atf == "sigm")
            {
                double lambda;
                ls >> lambda;
                actf = [lambda](double x, std::valarray<double> p){return 1.0/(1.0+exp(-lambda*(x+p[0])));};
                dactf = {[lambda](double x, std::valarray<double> p){return (lambda*exp(-lambda*(x+p[0])))/pow(1.0+exp(-lambda*(x+p[0])),2);},[lambda](double x, std::valarray<double> p){return (lambda*exp(-lambda*(x+p[0])))/pow(1.0+exp(-lambda*(x+p[0])),2);}};
                actfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-1.0,1.0);return std::valarray<double>(randGen(gen),1);};
            }
            else
            {
                exit(EXIT_FAILURE);
            }
        }
        else if(token == "Gactf")
        {
            std::string atf;
            std::getline(ls,atf,' ');
            if(atf == "lin")
            {
                gactf = [](double x,[[maybe_unused]] std::valarray<double> p){return x;};
                gdactf = {[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 1.0;},[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 0.0;}};
                gactfpd = [](void){return std::valarray<double>(0);};
                pgactfpd = [](void){return std::valarray<double>(0);};
                ngactfpd = [](void){return std::valarray<double>(0);};
            }
            else if(atf == "afi")
            {
                gactf = [](double x, std::valarray<double> p){return x+p[0];};
                gdactf = {[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 1.0;},[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 1.0;}};
                gactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-1.0,1.0);return std::valarray<double>(randGen(gen),1);};
                pgactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(1.0,3.0);return std::valarray<double>(randGen(gen),1);};
                ngactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-3.0,-1.0);return std::valarray<double>(randGen(gen),1);};
            }
            else if(atf == "tanh")
            {
                gactf = [](double x, std::valarray<double> p){return tanh(x+p[0]);};
                gdactf = {[](double x, std::valarray<double> p){return 1.0-pow(tanh(x+p[0]),2);},[](double x, std::valarray<double> p){return 1.0-pow(tanh(x+p[0]),2);}};
                gactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-1.0,1.0);return std::valarray<double>(randGen(gen),1);};
                pgactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(1.0,3.0);return std::valarray<double>(randGen(gen),1);};
                ngactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-3.0,-1.0);return std::valarray<double>(randGen(gen),1);};
            }
            else if(atf == "sigm")
            {
                double lambda;
                ls >> lambda;
                gactf = [lambda](double x, std::valarray<double> p){return 1.0/(1.0+exp(-lambda*(x+p[0])));};
                gdactf = {[lambda](double x, std::valarray<double> p){return (lambda*exp(-lambda*(x+p[0])))/pow(1.0+exp(-lambda*(x+p[0])),2);},[lambda](double x, std::valarray<double> p){return (lambda*exp(-lambda*(x+p[0])))/pow(1.0+exp(-lambda*(x+p[0])),2);}};
                gactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-1.0,1.0);return std::valarray<double>(randGen(gen),1);};
                pgactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(1.0,3.0);return std::valarray<double>(randGen(gen),1);};
                ngactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-3.0,-1.0);return std::valarray<double>(randGen(gen),1);};
            }
            else
            {
                exit(EXIT_FAILURE);
            }
        }
        else if(token == "Pactf")
        {
            std::string atf;
            std::getline(ls,atf,' ');
            if(atf == "lin")
            {
                pactf = [](double x,[[maybe_unused]] std::valarray<double> p){return x;};
                pdactf = {[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 1.0;},[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 0.0;}};
                pactfpd = [](void){return std::valarray<double>(0);};
            }
            else if(atf == "afi")
            {
                pactf = [](double x, std::valarray<double> p){return x+p[0];};
                pdactf = {[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 1.0;},[]([[maybe_unused]] double x,[[maybe_unused]] std::valarray<double> p){return 1.0;}};
                pactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-1.0,1.0);return std::valarray<double>(randGen(gen),1);};
            }
            else if(atf == "tanh")
            {
                pactf = [](double x, std::valarray<double> p){return tanh(x+p[0]);};
                pdactf = {[](double x, std::valarray<double> p){return 1.0-pow(tanh(x+p[0]),2);},[](double x, std::valarray<double> p){return 1.0-pow(tanh(x+p[0]),2);}};
                pactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-1.0,1.0);return std::valarray<double>(randGen(gen),1);};
            }
            else if(atf == "sigm")
            {
                double lambda;
                ls >> lambda;
                pactf = [lambda](double x, std::valarray<double> p){return 1.0/(1.0+exp(-lambda*(x+p[0])));};
                pdactf = {[lambda](double x, std::valarray<double> p){return (lambda*exp(-lambda*(x+p[0])))/pow(1.0+exp(-lambda*(x+p[0])),2);},[lambda](double x, std::valarray<double> p){return (lambda*exp(-lambda*(x+p[0])))/pow(1.0+exp(-lambda*(x+p[0])),2);}};
                pactfpd = [](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::uniform_real_distribution<double> randGen(-1.0,1.0);return std::valarray<double>(randGen(gen),1);};
            }
            else
            {
                exit(EXIT_FAILURE);
            }
        }
        else if(token == "Normf")
        {
            std::string atf;
            std::getline(ls,atf,' ');
            if(atf == "lin")
            {
                normf = [](double x){return x;};
                dnormf = []([[maybe_unused]] double x){return 1.0;};
            }
            else if(atf == "tanh")
            {
                normf = [](double x){return tanh(x);};
                dnormf = [](double x){return 1.0-pow(tanh(x),2);};
            }
            else if(atf == "sigm")
            {
                double lambda;
                ls >> lambda;
                normf = [lambda](double x){return 1.0/(1.0+exp(-lambda*x));};
                dnormf = [lambda](double x){return (lambda*exp(-lambda*x))/pow(1.0+exp(-lambda*x),2);};
            }
            else
            {
                exit(EXIT_FAILURE);
            }
        }
        else if(token == "Ddf")
        {
            std::string atf;
            std::getline(ls,atf,' ');
            if(atf == "euc")
            {
                ddf = [](std::valarray<double> x, std::valarray<double> y){return sqrt(((x-y)*(x-y)).sum());};
            }
            else
            {
                exit(EXIT_FAILURE);
            }
        }
        else if(token == "Dnf")
        {
            std::string atf;
            std::getline(ls,atf,' ');
            double scf = 1.0;
            ls >> scf;
            if(atf == "expm")
            {
                dnf = [scf](double x){return exp(-(scf*x));};
            }
            else if(atf == "gauss")
            {
                dnf = [scf](double x){return (1.0/(scf*sqrt(2.0*M_PI)))*exp(-((x*x)/(2.0*scf*scf)));};
            }
            else if(atf == "1s1x")
            {
                dnf = [scf](double x){return 1.0/(1.0+(scf*x));};
            }
            else
            {
                exit(EXIT_FAILURE);
            }
        }
        else if(token == "Dvf")
        {
            std::string atf;
            std::getline(ls,atf,' ');
            double scf = 1.0;
            ls >> scf;
            if(atf == "1ln")
            {
                dvf = [scf](double x){return 1+log(scf*x);};
            }
            else if(atf == "lin")
            {
                dvf = [scf](double x){return scf*x;};
            }
            else if(atf == "sqrt")
            {
                dvf = [scf](double x){return sqrt(scf*x);};
            }
            else if(atf == "sqrtt")
            {
                std::getline(ls,atf,' ');
                double thd = 1.0;
                ls >> thd;
                dvf = [scf=thd,thd=scf](double x){return (x>thd)?sqrt(scf*thd):sqrt(scf*x);};
            }
            else if(atf == "sqrtr")
            {
                std::getline(ls,atf,' ');
                double thd = 1.0;
                ls >> thd;
                dvf = [scf=thd,thd=scf](double x){return (x>thd)?sqrt((scf*thd)/(1+(abs(thd-x)/thd))):sqrt(scf*x);};
            }
            else
            {
                exit(EXIT_FAILURE);
            }
        }
        else if(token == "Dgen")
        {
            std::string dist;
            std::getline(ls,dist,' ');
            double prb;
            ls >> prb;
            dgenp.push_back(prb);
            dgenpt+= prb;
            if(dist == "gaus")
            {
                double sig;
                ls >> sig;
                std::vector<std::normal_distribution<double>> randGen;
                while(!ls.eof())
                {
                    double mu;
                    ls >> mu;
                    randGen.push_back(std::normal_distribution<double>(mu,sig));
                }
                dgen.push_back([randGen](void){std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());std::vector<std::normal_distribution<double>>rg(randGen);std::valarray<double> ans(rg.size());for(size_t i=0;i<rg.size();++i){ans[i]=rg[i](gen);}return ans;});
            }
            else
            {
                exit(EXIT_FAILURE);
            }
        }
        else if(token == "Rdgen")
        {
            dgen.clear();
        }
        else if(token == "Cdata")
        {
            rndcl = false;
            double cscale = 1.0;
            std::string cfilen;
            std::getline(ls,cfilen,' ');
            if(!ls.eof())
            {
                ls >> cscale;
            }
            vv.clear();
            std::ifstream cfile = std::ifstream(cfilen,std::ifstream::in);
            while(!cfile.eof())
            {
                std::vector<double> ctmp;
                std::string linn;
                std::getline(cfile,linn);
                if(linn.size()>0)
                {
                    std::stringstream lin(linn);
                    while(!lin.eof())
                    {
                        std::string ccoef;
                        std::getline(lin,ccoef,';');
                        ctmp.push_back(std::stod(ccoef)*cscale);
                    }
                    vv.push_back(vectovaa(ctmp));
                }
            }
            drnv = vv.size();
        }
        else if(token == "Fmtype")
        {
            std::string fts;
            ls >> fts;
            if(fts.find('a') == std::string::npos)
            {
                fmntp = "";
                if(fts.find('d') != std::string::npos)
                {
                    fmntp.push_back('d');
                }
                if(fts.find('c') != std::string::npos)
                {
                    fmntp.append("lu");
                }
                else
                {
                    if(fts.find('l') != std::string::npos)
                    {
                        fmntp.push_back('l');
                    }
                    if(fts.find('u') != std::string::npos)
                    {
                        fmntp.push_back('u');
                    }
                }
            }
            else
            {
                fmntp = "dlu";
            }
            if(fts.find('m') != std::string::npos)
            {
                fmntp.push_back('m');
            }
        }
        else if(token == "Krnd")
        {
            rndcl = false;
        }
        else if(token == "Crnd")
        {
            rndcl = true;
        }
        else if(token == "Drec")
        {
            if(rndcl)
            {
                ls >> drnv >> drprc >> drmmt;
            }
            else
            {
                ls >> drprc >> drmmt;
            }
            rs = 1;
            goto drrn;
        }
        else if(token == "Mdrec")
        {
            if(rndcl)
            {
                ls >> drnv >> drprc >> drmmt >> rs;
            }
            else
            {
                ls >> drprc >> drmmt >> rs;
            }
            drrn:
            std::valarray<double> gt(rs), gta(rs), gtc(rs), gtd(rs), gtk(rs), gto(rs), lt(rs), lta(rs), ltc(rs), ltd(rs), ltk(rs), lto(rs), yt(rs), yta(rs), ytc(rs), ytd(rs), ytk(rs), yto(rs);
            std::vector<std::vector<size_t>> red, reda, redc, redd, redk, redo;
            for(unsigned run = 0; run < rs; ++run)
            {
                if(rndcl)
                {
                    vv.clear();
                    vv.resize(drnv);
                    std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());
                    std::uniform_real_distribution<double> randGen(0.0,dgenpt);
                    for(size_t i = 0; i < drnv; ++i)
                    {
                        double rv = randGen(gen);
                        size_t nf;
                        for(nf = 0; dgenp[nf] < rv; ++nf)
                        {
                            rv-= dgenp[nf];
                        }
                        vv[i] = dgen[nf]();
                    }
                }
                if(rncv)
                {
                    red = recommend(vv,ddf,dnf,dvf,false,false,drprc,drmmt);
                    reda = recommend(vv,ddf,dnf,dvf,false,true,drprc,drmmt);
                }
                redc = recommend(vv,ddf,dnf,dvf,true,false,drprc,drmmt);
                redd = recommend(vv,ddf,dnf,dvf,true,true,drprc,drmmt);
                redk = kmeans(vv,ddf,dnf,dvf,drprc,drmmt);
                redo = opticsExtract(vv,optics(vv,std::numeric_limits<double>::infinity(),1,ddf),ddf,dnf,dvf);
                if(rncv)
                {
                    gt[run] = gvalue(vv,red,ddf,dnf,dvf)/((double)drnv);
                    lt[run] = gloss(vv,red,ddf,dnf,dvf)/((double)drnv);
                    yt[run] = ((double)glossing(vv,red,ddf,dnf,dvf))/((double)drnv);
                    gta[run] = gvalue(vv,reda,ddf,dnf,dvf)/((double)drnv);
                    lta[run] = gloss(vv,reda,ddf,dnf,dvf)/((double)drnv);
                    yta[run] = ((double)glossing(vv,reda,ddf,dnf,dvf))/((double)drnv);
                }
                gtc[run] = gvalue(vv,redc,ddf,dnf,dvf)/((double)drnv);
                ltc[run] = gloss(vv,redc,ddf,dnf,dvf)/((double)drnv);
                ytc[run] = ((double)glossing(vv,redc,ddf,dnf,dvf))/((double)drnv);
                gtd[run] = gvalue(vv,redd,ddf,dnf,dvf)/((double)drnv);
                ltd[run] = gloss(vv,redd,ddf,dnf,dvf)/((double)drnv);
                ytd[run] = ((double)glossing(vv,redd,ddf,dnf,dvf))/((double)drnv);
                gtk[run] = gvalueFull(vv,redk,ddf,dnf,dvf)/((double)drnv);
                ltk[run] = glossFull(vv,redk,ddf,dnf,dvf)/((double)drnv);
                ytk[run] = ((double)glossingFull(vv,redk,ddf,dnf,dvf))/((double)drnv);
                gto[run] = gvalueFull(vv,redo,ddf,dnf,dvf)/((double)drnv);
                lto[run] = glossFull(vv,redo,ddf,dnf,dvf)/((double)drnv);
                yto[run] = ((double)glossingFull(vv,redo,ddf,dnf,dvf))/((double)drnv);
            }
            if(rncv)
            {
                std::cout << "Global utility (average):\n             AUCCCR    AUCCCR-A   AUCCCR-C   AUCCCR-CA   k-means    OPTICS" << std::endl;
            }
            else
            {
                std::cout << "Global utility (average):\n            AUCCCR-C   AUCCCR-CA   k-means    OPTICS" << std::endl;
            }
            for(size_t i = 0; i < rs; ++i)
            {
                if(rncv)
                {
                    std::cout << "Run " << std::setw(5) << (i+1) << ": " << flofix(gt[i],10) << ' ' << flofix(gta[i],10) << ' ' << flofix(gtc[i],10) << ' ' << flofix(gtd[i],10) << ' ' << flofix(gtk[i],10) << ' ' << flofix(gto[i],10) << std::endl;
                }
                else
                {
                    std::cout << "Run " << std::setw(5) << (i+1) << ": " << flofix(gtc[i],10) << ' ' << flofix(gtd[i],10) << ' ' << flofix(gtk[i],10) << ' ' << flofix(gto[i],10) << std::endl;
                }
            }
            if(rncv)
            {
                std::cout << "Average  : " << flofix(gt.sum()/rs,10) << ' ' << flofix(gta.sum()/rs,10) << ' ' << flofix(gtc.sum()/rs,10) << ' ' << flofix(gtd.sum()/rs,10) << ' ' << flofix(gtk.sum()/rs,10) << ' ' << flofix(gto.sum()/rs,10) << "\nSD       : " << flofix(sqrt(((gt*gt).sum()/rs)-((gt.sum()/rs)*(gt.sum()/rs))),10) << ' ' << flofix(sqrt(((gta*gta).sum()/rs)-((gta.sum()/rs)*(gta.sum()/rs))),10) << ' ' << flofix(sqrt(((gtc*gtc).sum()/rs)-((gtc.sum()/rs)*(gtc.sum()/rs))),10) << ' ' << flofix(sqrt(((gtd*gtd).sum()/rs)-((gtd.sum()/rs)*(gtd.sum()/rs))),10) << ' ' << flofix(sqrt(((gtk*gtk).sum()/rs)-((gtk.sum()/rs)*(gtk.sum()/rs))),10) << ' ' << flofix(sqrt(((gto*gto).sum()/rs)-((gto.sum()/rs)*(gto.sum()/rs))),10) << '\n' << std::endl;
            }
            else
            {
                std::cout << "Average  : " << flofix(gtc.sum()/rs,10) << ' ' << flofix(gtd.sum()/rs,10) << ' ' << flofix(gtk.sum()/rs,10) << ' ' << flofix(gto.sum()/rs,10) << "\nSD       : " << flofix(sqrt(((gtc*gtc).sum()/rs)-((gtc.sum()/rs)*(gtc.sum()/rs))),10) << ' ' << flofix(sqrt(((gtd*gtd).sum()/rs)-((gtd.sum()/rs)*(gtd.sum()/rs))),10) << ' ' << flofix(sqrt(((gtk*gtk).sum()/rs)-((gtk.sum()/rs)*(gtk.sum()/rs))),10) << ' ' << flofix(sqrt(((gto*gto).sum()/rs)-((gto.sum()/rs)*(gto.sum()/rs))),10) << '\n' << std::endl;
            }
            if(rncv)
            {
                std::cout << "Agents' losses (average):\n             AUCCCR    AUCCCR-A   AUCCCR-C   AUCCCR-CA   k-means    OPTICS" << std::endl;
            }
            else
            {
                std::cout << "Agents' losses (average):\n            AUCCCR-C   AUCCCR-CA   k-means    OPTICS" << std::endl;
            }
            for(size_t i = 0; i < rs; ++i)
            {
                if(rncv)
                {
                    std::cout << "Run " << std::setw(5) << (i+1) << ": " << flofix(lt[i],10) << ' ' << flofix(lta[i],10) << ' ' << flofix(ltc[i],10) << ' ' << flofix(ltd[i],10) << ' ' << flofix(ltk[i],10) << ' ' << flofix(lto[i],10) << std::endl;
                }
                else
                {
                    std::cout << "Run " << std::setw(5) << (i+1) << ": " << flofix(ltc[i],10) << ' ' << flofix(ltd[i],10) << ' ' << flofix(ltk[i],10) << ' ' << flofix(lto[i],10) << std::endl;
                }
            }
            if(rncv)
            {
                std::cout << "Average  : " << flofix(lt.sum()/rs,10) << ' ' << flofix(lta.sum()/rs,10) << ' ' << flofix(ltc.sum()/rs,10) << ' ' << flofix(ltd.sum()/rs,10) << ' ' << flofix(ltk.sum()/rs,10) << ' ' << flofix(lto.sum()/rs,10) << "\nSD       : " << flofix(sqrt(((lt*lt).sum()/rs)-((lt.sum()/rs)*(lt.sum()/rs))),10) << ' ' << flofix(sqrt(((lta*lta).sum()/rs)-((lta.sum()/rs)*(lta.sum()/rs))),10) << ' ' << flofix(sqrt(((ltc*ltc).sum()/rs)-((ltc.sum()/rs)*(ltc.sum()/rs))),10) << ' ' << flofix(sqrt(((ltd*ltd).sum()/rs)-((ltd.sum()/rs)*(ltd.sum()/rs))),10) << ' ' << flofix(sqrt(((ltk*ltk).sum()/rs)-((ltk.sum()/rs)*(ltk.sum()/rs))),10) << ' ' << flofix(sqrt(((lto*lto).sum()/rs)-((lto.sum()/rs)*(lto.sum()/rs))),10) << '\n' << std::endl;
            }
            else
            {
                std::cout << "Average  : " << flofix(ltc.sum()/rs,10) << ' ' << flofix(ltd.sum()/rs,10) << ' ' << flofix(ltk.sum()/rs,10) << ' ' << flofix(lto.sum()/rs,10) << "\nSD       : " << flofix(sqrt(((ltc*ltc).sum()/rs)-((ltc.sum()/rs)*(ltc.sum()/rs))),10) << ' ' << flofix(sqrt(((ltd*ltd).sum()/rs)-((ltd.sum()/rs)*(ltd.sum()/rs))),10) << ' ' << flofix(sqrt(((ltk*ltk).sum()/rs)-((ltk.sum()/rs)*(ltk.sum()/rs))),10) << ' ' << flofix(sqrt(((lto*lto).sum()/rs)-((lto.sum()/rs)*(lto.sum()/rs))),10) << '\n' << std::endl;
            }
            if(rncv)
            {
                std::cout << "Agents lossing (rate):\n             AUCCCR    AUCCCR-A   AUCCCR-C   AUCCCR-CA   k-means    OPTICS" << std::endl;
            }
            else
            {
                std::cout << "Agents lossing (rate):\n            AUCCCR-C   AUCCCR-CA   k-means    OPTICS" << std::endl;
            }
            for(size_t i = 0; i < rs; ++i)
            {
                if(rncv)
                {
                    std::cout << "Run " << std::setw(5) << (i+1) << ": " << flofix(yt[i],10) << ' ' << flofix(yta[i],10) << ' ' << flofix(ytc[i],10) << ' ' << flofix(ytd[i],10) << ' ' << flofix(ytk[i],10) << ' ' << flofix(yto[i],10) << std::endl;
                }
                else
                {
                    std::cout << "Run " << std::setw(5) << (i+1) << ": " << flofix(ytc[i],10) << ' ' << flofix(ytd[i],10) << ' ' << flofix(ytk[i],10) << ' ' << flofix(yto[i],10) << std::endl;
                }
            }
            if(rncv)
            {
                std::cout << "Average  : " << flofix(yt.sum()/rs,10) << ' ' << flofix(yta.sum()/rs,10) << ' ' << flofix(ytc.sum()/rs,10) << ' ' << flofix(ytd.sum()/rs,10) << ' ' << flofix(ytk.sum()/rs,10) << ' ' << flofix(yto.sum()/rs,10) << "\nSD       : " << flofix(sqrt(((yt*yt).sum()/rs)-((yt.sum()/rs)*(yt.sum()/rs))),10) << ' ' << flofix(sqrt(((yta*yta).sum()/rs)-((yta.sum()/rs)*(yta.sum()/rs))),10) << ' ' << flofix(sqrt(((ytc*ytc).sum()/rs)-((ytc.sum()/rs)*(ytc.sum()/rs))),10) << ' ' << flofix(sqrt(((ytd*ytd).sum()/rs)-((ytd.sum()/rs)*(ytd.sum()/rs))),10) << ' ' << flofix(sqrt(((ytk*ytk).sum()/rs)-((ytk.sum()/rs)*(ytk.sum()/rs))),10) << ' ' << flofix(sqrt(((yto*yto).sum()/rs)-((yto.sum()/rs)*(yto.sum()/rs))),10) << std::endl;
            }
            else
            {
                std::cout << "Average  : " << flofix(ytc.sum()/rs,10) << ' ' << flofix(ytd.sum()/rs,10) << ' ' << flofix(ytk.sum()/rs,10) << ' ' << flofix(yto.sum()/rs,10) << "\nSD       : " << flofix(sqrt(((ytc*ytc).sum()/rs)-((ytc.sum()/rs)*(ytc.sum()/rs))),10) << ' ' << flofix(sqrt(((ytd*ytd).sum()/rs)-((ytd.sum()/rs)*(ytd.sum()/rs))),10) << ' ' << flofix(sqrt(((ytk*ytk).sum()/rs)-((ytk.sum()/rs)*(ytk.sum()/rs))),10) << ' ' << flofix(sqrt(((yto*yto).sum()/rs)-((yto.sum()/rs)*(yto.sum()/rs))),10) << std::endl;
            }
            if(clpr)
            {
                std::vector<std::vector<std::valarray<double>>> pgt, pgta, pgtc = makeGrp(vv,redc), pgtd = makeGrp(vv,redd), pgtk = makeGrpFull(vv,redk), pgto = makeGrpFull(vv,redo);
                if(rncv)
                {
                    pgt = makeGrp(vv,red);
                    pgta = makeGrp(vv,reda);
                }
                unsigned ck = 0;
                std::ofstream tf(rcf1,std::ifstream::out);
                if(rncv)
                {
                    for(size_t i = 0; i < pgt.size(); ++i)
                    {
                        unsigned ckv;
                        if(pgt[i].size() > 1)
                        {
                            ckv = ++ck;
                        }
                        else
                        {
                            ckv = 0;
                        }
                        for(size_t j = 0; j < pgt[i].size(); ++j)
                        {
                            for(size_t k = 0; k < pgt[i][j].size(); ++k)
                            {
                                tf << pgt[i][j][k] << ' ';
                            }
                            tf << ckv << std::endl;
                        }
                    }
                    ck = 0;
                    tf.close();
                    tf.open(rcf2,std::ifstream::out);
                    for(size_t i = 0; i < pgta.size(); ++i)
                    {
                        unsigned ckv;
                        if(pgta[i].size() > 1)
                        {
                            ckv = ++ck;
                        }
                        else
                        {
                            ckv = 0;
                        }
                        for(size_t j = 0; j < pgta[i].size(); ++j)
                        {
                            for(size_t k = 0; k < pgta[i][j].size(); ++k)
                            {
                                tf << pgta[i][j][k] << ' ';
                            }
                            tf << ckv << std::endl;
                        }
                    }
                    ck = 0;
                    tf.close();
                    tf.open(rcf3,std::ifstream::out);
                }
                for(size_t i = 0; i < pgtc.size(); ++i)
                {
                    unsigned ckv;
                    if(pgtc[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < pgtc[i].size(); ++j)
                    {
                        for(size_t k = 0; k < pgtc[i][j].size(); ++k)
                        {
                            tf << pgtc[i][j][k] << ' ';
                        }
                        tf << ckv << std::endl;
                    }
                }
                ck = 0;
                tf.close();
                tf.open((rncv)?rcf4:rcf2,std::ifstream::out);
                for(size_t i = 0; i < pgtd.size(); ++i)
                {
                    unsigned ckv;
                    if(pgtd[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < pgtd[i].size(); ++j)
                    {
                        for(size_t k = 0; k < pgtd[i][j].size(); ++k)
                        {
                            tf << pgtd[i][j][k] << ' ';
                        }
                        tf << ckv << std::endl;
                    }
                }
                ck = 0;
                tf.close();
                tf.open((rncv)?rcf5:rcf3,std::ifstream::out);
                for(size_t i = 0; i < pgtk.size(); ++i)
                {
                    unsigned ckv;
                    if(pgtk[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < pgtk[i].size(); ++j)
                    {
                        for(size_t k = 0; k < pgtk[i][j].size(); ++k)
                        {
                            tf << pgtk[i][j][k] << ' ';
                        }
                        tf << ckv << std::endl;
                    }
                }
                ck = 0;
                tf.close();
                tf.open((rncv)?rcf6:rcf4,std::ifstream::out);
                for(size_t i = 0; i < pgto.size(); ++i)
                {
                    unsigned ckv;
                    if(pgto[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < pgto[i].size(); ++j)
                    {
                        for(size_t k = 0; k < pgto[i][j].size(); ++k)
                        {
                            tf << pgto[i][j][k] << ' ';
                        }
                        tf << ckv << std::endl;
                    }
                }
            }
        }
        else if(token == "Run")
        {
            ls >> rd >> sync;
            rs = 1;
            goto rning;
        }
        else if(token == "Mrun")
        {
            ls >> rs >> rd >> sync;
            rning:
            MLNN mlnn[n];
            LSTM lstm[n];
            Assoc assoc[n];
            std::vector<std::vector<MLNN*>> mlnnp(models.size());
            std::vector<std::vector<LSTM*>> lstmp(models.size());
            std::vector<std::vector<Assoc*>> assocp(models.size());
            MNIST mnist[n], testd[n];
            FEMNIST femnist[n];
            VSN vsn[n];
            Metro metro[n];
            BinSeq binseq[n];
            SDR sdr[n];
            sr.resize(rs);
            for(size_t i = 0; i < rs; ++i)
            {
                sr[i].resize(n);
            }
            if(tra)
            {
                trace.resize(rs);
                for(size_t i = 0; i < rs; ++i)
                {
                    trace[i].resize(n);
                    for(size_t j = 0; j < n; ++j)
                    {
                        trace[i][j].resize(rd/tev);
                    }
                }
            }
            if(itra)
            {
                itrace.resize(rs);
                for(size_t i = 0; i < rs; ++i)
                {
                    itrace[i].resize(n);
                    for(size_t j = 0; j < n; ++j)
                    {
                        itrace[i][j].resize((rd*sync)/itev);
                    }
                }
            }
            if(type == "digit")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    if(perm.find(i) == perm.end())
                    {
                        if(amask.find(i) == amask.end())
                        {
                            if(iperm.find(i) == iperm.end())
                            {
                                mnist[i].init(TRI,TRV,i*bs);
                                testd[i].init(TSI,TSV);
                            }
                            else
                            {
                                mnist[i].init(TRI,TRV,iperm[i],i*bs);
                                testd[i].init(TSI,TSV,iperm[i]);
                            }
                        }
                        else
                        {
                            if(iperm.find(i) == iperm.end())
                            {
                                mnist[i].init(TRI,TRV,mask[amask[i]],i*bs);
                                testd[i].init(TSI,TSV,mask[amask[i]]);
                            }
                            else
                            {
                                mnist[i].init(TRI,TRV,mask[amask[i]],iperm[i],i*bs);
                                testd[i].init(TSI,TSV,mask[amask[i]],iperm[i]);
                            }
                        }
                    }
                    else
                    {
                        if(amask.find(i) == amask.end())
                        {
                            if(iperm.find(i) == iperm.end())
                            {
                                mnist[i].init(TRI,TRV,perm[i],i*bs);
                                testd[i].init(TSI,TSV,perm[i]);
                            }
                            else
                            {
                                mnist[i].init(TRI,TRV,perm[i],iperm[i],i*bs);
                                testd[i].init(TSI,TSV,perm[i],iperm[i]);
                            }
                        }
                        else
                        {
                            if(iperm.find(i) == iperm.end())
                            {
                                mnist[i].init(TRI,TRV,perm[i],mask[amask[i]],i*bs);
                                testd[i].init(TSI,TSV,perm[i],mask[amask[i]]);
                            }
                            else
                            {
                                mnist[i].init(TRI,TRV,perm[i],mask[amask[i]],iperm[i],i*bs);
                                testd[i].init(TSI,TSV,perm[i],mask[amask[i]],iperm[i]);
                            }
                        }
                    }
                }
                for(size_t i = 0; i < mlnnp.size(); ++i)
                {
                    mlnnp[i].resize(modelNets[i].size());
                    auto nn = modelNets[i].cbegin();
                    for(size_t j = 0; j < mlnnp[i].size(); ++j)
                    {
                        mlnnp[i][j] = (mlnn+*nn);
                        ++nn;
                    }
                }
            }
            else if(type == "femnist")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    femnist[i].init(FMI,FMV,fmntp,i);
                }
                for(size_t i = 0; i < mlnnp.size(); ++i)
                {
                    mlnnp[i].resize(modelNets[i].size());
                    auto nn = modelNets[i].cbegin();
                    for(size_t j = 0; j < mlnnp[i].size(); ++j)
                    {
                        mlnnp[i][j] = (mlnn+*nn);
                        ++nn;
                    }
                }
            }
            else if(type == "vsn")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    vsn[i].init(VSI,VSV,i);
                }
                for(size_t i = 0; i < mlnnp.size(); ++i)
                {
                    mlnnp[i].resize(modelNets[i].size());
                    auto nn = modelNets[i].cbegin();
                    for(size_t j = 0; j < mlnnp[i].size(); ++j)
                    {
                        mlnnp[i][j] = (mlnn+*nn);
                        ++nn;
                    }
                }
            }
            else if(type == "metro")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    metro[i].init(MLT,mlines[i]);
                }
                for(size_t i = 0; i < lstmp.size(); ++i)
                {
                    lstmp[i].resize(modelNets[i].size());
                    auto nn = modelNets[i].cbegin();
                    for(size_t j = 0; j < lstmp[i].size(); ++j)
                    {
                        lstmp[i][j] = (lstm+*nn);
                        ++nn;
                    }
                }
            }
            else if(type == "march")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    binseq[i].init(mcls[am[i]],mpos[am[i]]);
                }
                for(size_t i = 0; i < lstmp.size(); ++i)
                {
                    lstmp[i].resize(modelNets[i].size());
                    auto nn = modelNets[i].cbegin();
                    for(size_t j = 0; j < lstmp[i].size(); ++j)
                    {
                        lstmp[i][j] = (lstm+*nn);
                        ++nn;
                    }
                }
            }
            else if(type == "assoc")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    if(aperm.find(i) == aperm.end())
                    {
                        sdr[i].init(layers[0],actn,nsets,noise);
                    }
                    else
                    {
                        sdr[i].init(layers[0],actn,nsets,noise,aperm[i].first,aperm[i].second);
                    }
                }
                for(size_t i = 0; i < assocp.size(); ++i)
                {
                    assocp[i].resize(modelNets[i].size());
                    auto nn = modelNets[i].cbegin();
                    for(size_t j = 0; j < assocp[i].size(); ++j)
                    {
                        assocp[i][j] = (assoc+*nn);
                        ++nn;
                    }
                }
            }
            for(unsigned run = 0; run < rs; ++run)
            {
                if(type == "digit" || type == "femnist" || type == "vsn")
                {
                    if(idinit)
                    {
                        mlnn[0] = MLNN(layers,actf,dactf,weightd,actfpd);
                        for(size_t i = 1; i < n; ++i)
                        {
                            mlnn[i] = MLNN(mlnn[0]);
                        }
                    }
                    else
                    {
                        for(size_t i = 0; i < n; ++i)
                        {
                            mlnn[i] = MLNN(layers,actf,dactf,weightd,actfpd);
                        }
                    }
                }
                else if(type == "metro" || type == "march")
                {
                    std::array<std::function<double(double,std::valarray<double>)>,5> flt;
                    std::array<std::vector<std::function<double(double,std::valarray<double>)>>,5> dflt;
                    std::array<std::function<double(size_t)>,5> wdlt;
                    std::array<std::function<std::valarray<double>(void)>,5> afdlt;
                    flt[0] = actf;
                    flt[1] = gactf;
                    flt[2] = gactf;
                    flt[3] = gactf;
                    flt[4] = pactf;
                    dflt[0] = dactf;
                    dflt[1] = gdactf;
                    dflt[2] = gdactf;
                    dflt[3] = gdactf;
                    dflt[4] = pdactf;
                    wdlt[0] = weightd;
                    wdlt[1] = weightd;
                    wdlt[2] = weightd;
                    wdlt[3] = weightd;
                    wdlt[4] = weightd;
                    afdlt[0] = actfpd;
                    afdlt[1] = pgactfpd;
                    afdlt[2] = ngactfpd;
                    afdlt[3] = ngactfpd;
                    afdlt[4] = pactfpd;
                    if(idinit)
                    {
                        lstm[0] = LSTM(layers,players,flt,dflt,wdlt,afdlt,normf,dnormf);
                        for(size_t i = 1; i < n; ++i)
                        {
                            lstm[i] = LSTM(lstm[0]);
                        }
                    }
                    else
                    {
                        for(size_t i = 0; i < n; ++i)
                        {
                            lstm[i] = LSTM(layers,players,flt,dflt,wdlt,afdlt,normf,dnormf);
                        }
                    }
                }
                else if(type == "assoc")
                {
                    if(idinit)
                    {
                        assoc[0] = Assoc(layers[0],layers[1],thld,weightd);
                        for(size_t i = 1; i < n; ++i)
                        {
                            assoc[i] = Assoc(assoc[0]);
                        }
                    }
                    else
                    {
                        for(size_t i = 0; i < n; ++i)
                        {
                            assoc[i] = Assoc(layers[0],layers[1],thld,weightd);
                        }
                    }
                }
                for(size_t i = 0; i < modelNets.size(); ++i)
                {
                    for(const auto& nn : modelNets[i])
                    {
                        if(type == "digit" || type == "femnist" || type == "vsn")
                        {
                            mlnn[nn].addModelLast(i,models[i],modelDeps[i]);
                        }
                        else if(type == "metro" || type == "march")
                        {
                            lstm[nn].addModelLast(i,models[i],pmodels[i],modelDeps[i]);
                        }
                        else if(type == "assoc")
                        {
                            assoc[nn].addModelLast(i,models[i][0],models[i][1],modelDeps[i]);
                        }
                    }
                }
                for(unsigned i = 0; i < rd; ++i)
                {
                    #pragma omp parallel for
                    for(size_t j = 0; j < n; ++j)
                    {
                        for(unsigned k = 0; k < sync; ++k)
                        {
                            std::vector<size_t> minib = getMinibatch(bs,mbs);
                            if(seq)
                            {
                                if(type == "digit")
                                {
                                    for(size_t l = 0; l < mbs; ++l)
                                    {
                                        mlnn[j].train(mnist[j].ip(minib[l]),mnist[j].vp(minib[l]),lr);
                                    }
                                }
                                else if(type == "femnist")
                                {
                                    for(size_t l = 0; l < mbs; ++l)
                                    {
                                        mlnn[j].train(femnist[j].ip(minib[l]),femnist[j].vp(minib[l]),lr);
                                    }
                                }
                                else if(type == "vsn")
                                {
                                    for(size_t l = 0; l < mbs; ++l)
                                    {
                                        mlnn[j].train(vsn[j].ip(minib[l]),vsn[j].vp(minib[l]),lr);
                                    }
                                }
                                else if(type == "metro")
                                {
                                    for(size_t l = 0; l < mbs; ++l)
                                    {
                                        lstm[j].train(metro[j].sp(minib[l],sqlen),metro[j].sp(minib[l]+1,sqlen),lr);
                                    }
                                }
                                else if(type == "march")
                                {
                                    for(size_t l = 0; l < mbs; ++l)
                                    {
                                        lstm[j].train(binseq[j].sp(minib[l]+j*(bs+sqlen),sqlen),binseq[j].sp(minib[l]+j*(bs+sqlen)+1,sqlen),lr);
                                    }
                                }
                                else if(type == "assoc")
                                {
                                    for(size_t l = 0; l < mbs; ++l)
                                    {
                                        assoc[j].train(sdr[j].p(minib[l]+j*bs),lr);
                                    }
                                }
                            }
                            else
                            {
                                if(type == "digit")
                                {
                                    mlnn[j].train(mnist[j].ip(minib),mnist[j].vp(minib),lr);
                                }
                                else if(type == "femnist")
                                {
                                    mlnn[j].train(femnist[j].ip(minib),femnist[j].vp(minib),lr);
                                }
                                else if(type == "vsn")
                                {
                                    mlnn[j].train(vsn[j].ip(minib),vsn[j].vp(minib),lr);
                                }
                                else if(type == "metro")
                                {
                                    lstm[j].train(metro[j].sp(minib,sqlen),metro[j].sp(minib+1,sqlen),lr);
                                }
                                else if(type == "march")
                                {
                                    lstm[j].train(binseq[j].sp(minib+j*(bs+sqlen),sqlen),binseq[j].sp(minib+j*(bs+sqlen)+1,sqlen),lr);
                                }
                                else if(type == "assoc")
                                {
                                    assoc[j].train(sdr[j].p(minib),lr);
                                }
                            }
                            if(itra && ((((i*sync)+k)%itev) == 0))
                            {
                                itrace[run][j][((i*sync)+k)/itev] = 0.0;
                                for(size_t l = ittst; l < ittnd; ++l)
                                {
                                    if(type == "digit")
                                    {
                                        std::valarray<double> ans = mlnn[j].compute(testd[j].ip(l));
                                        uint8_t res = 0;
                                        for(size_t m = 1; m < DNN_MNIST_C; ++m)
                                        {
                                            if(ans[m] > ans[res])
                                            {
                                                res = m;
                                            }
                                        }
                                        if(res == testd[j].v(l))
                                        {
                                            itrace[run][j][((i*sync)+k)/itev]+= 1.0;
                                        }
                                    }
                                    else if(type == "femnist")
                                    {
                                        std::valarray<double> ans = mlnn[j].compute(femnist[j].ip(l));
                                        uint8_t res = 0;
                                        for(size_t m = 1; m < femnist[j].getOutSize(); ++m)
                                        {
                                            if(ans[m] > ans[res])
                                            {
                                                res = m;
                                            }
                                        }
                                        if(res == femnist[j].v(l))
                                        {
                                            itrace[run][j][((i*sync)+k)/itev]+= 1.0;
                                        }
                                    }
                                    else if(type == "vsn")
                                    {
                                        std::valarray<double> ans = mlnn[j].compute(vsn[j].ip(l));
                                        if((ans[0] > 0.5) == vsn[j].v(l))
                                        {
                                            itrace[run][j][((i*sync)+k)/itev]+= 1.0;
                                        }
                                    }
                                    else if(type == "metro")
                                    {
                                        std::vector<std::valarray<double>> ans = lstm[j].compute(metro[j].sp(l,itcsqlen),itprlen-1);
                                        itrace[run][j][((i*sync)+k)/itev]+= 1.0-(2.0*fabs(ans[itprlen-1][0]-metro[j][l+itcsqlen+itprlen]));
                                    }
                                    else if(type == "march")
                                    {
                                        std::vector<std::valarray<double>> ans = lstm[j].compute(binseq[j].sp(l,itcsqlen),itprlen-1);
                                        std::valarray<double> ctrl = binseq[j].p(l+tcsqlen+tprlen);
                                        bool okv = true;
                                        for(size_t m = 0; m < ctrl.size(); ++m)
                                        {
                                            if(fabs(ans[itprlen-1][m]-ctrl[m]) >= 0.5)
                                            {
                                                okv = false;
                                                break;
                                            }
                                        }
                                        if(okv)
                                        {
                                            itrace[run][j][((i*sync)+k)/itev]+= 1.0;
                                        }
                                    }
                                    else if(type == "assoc")
                                    {
                                        std::valarray<double> res = assoc[j].compute(sdr[j].ip(l)), ref = sdr[j].p(l);
                                        int acc = 0;
                                        for(size_t m = 0; m < res.size(); ++m)
                                        {
                                            if((res[m] > 0.5) || (ref[m] > 0.5))
                                            {
                                                if((res[m] > 0.5) && (ref[m] > 0.5))
                                                {
                                                    acc++;
                                                }
                                                else
                                                {
                                                    acc--;
                                                }
                                            }
                                        }
                                        if(acc >= 0)
                                        {
                                            itrace[run][j][((i*sync)+k)/itev]+= 1.0;
                                        }
                                    }
                                }
                                itrace[run][j][((i*sync)+k)/itev]/= ((double)(ittnd-ittst));
                            }
                        }
                    }
                    if(type == "digit" || type == "femnist" || type == "vsn")
                    {
                        if(i == 0)
                        {
                            if(reod)
                            {
                                for(size_t j = 0; j < models.size(); ++j)
                                {
                                    reorderSimModelDef(mlnnp[j],j);
                                }
                            }
                            else if(reo)
                            {
                                for(size_t j = 0; j < models.size(); ++j)
                                {
                                    reorderSimModel(mlnnp[j],j);
                                }
                            }
                        }
                        else
                        {
                            if(reo)
                            {
                                for(size_t j = 0; j < models.size(); ++j)
                                {
                                    reorderSimModel(mlnnp[j],j);
                                }
                            }
                        }
                        for(size_t j = 0; j < models.size(); ++j)
                        {
                            averageModelMult(mlnnp[j],j);
                        }
                    }
                    else if(type == "metro"|| type == "march")
                    {
                        if(i == 0)
                        {
                            if(reod)
                            {
                                for(size_t j = 0; j < models.size(); ++j)
                                {
                                    reorderSimModelDef(lstmp[j],j);
                                }
                            }
                            else if(reo)
                            {
                                for(size_t j = 0; j < models.size(); ++j)
                                {
                                    reorderSimModel(lstmp[j],j);
                                }
                            }
                        }
                        else
                        {
                            if(reo)
                            {
                                for(size_t j = 0; j < models.size(); ++j)
                                {
                                    reorderSimModel(lstmp[j],j);
                                }
                            }
                        }
                        for(size_t j = 0; j < models.size(); ++j)
                        {
                            averageModelMult(lstmp[j],j);
                        }
                    }
                    else if(type == "assoc")
                    {
                        if(i == 0)
                        {
                            if(reod)
                            {
                                for(size_t j = 0; j < models.size(); ++j)
                                {
                                    reorderSimModelDef(assocp[j],j);
                                }
                            }
                            else if(reo)
                            {
                                for(size_t j = 0; j < models.size(); ++j)
                                {
                                    reorderSimModel(assocp[j],j);
                                }
                            }
                        }
                        else
                        {
                            if(reo)
                            {
                                for(size_t j = 0; j < models.size(); ++j)
                                {
                                    reorderSimModel(assocp[j],j);
                                }
                            }
                        }
                        for(size_t j = 0; j < models.size(); ++j)
                        {
                            averageModelMult(assocp[j],j);
                        }
                    }
                    if(tra && ((i%tev) == 0))
                    {
                        #pragma omp parallel for
                        for(size_t j = 0; j < n; ++j)
                        {
                            trace[run][j][i/tev] = 0.0;
                            for(size_t k = ttst; k < ttnd; ++k)
                            {
                                if(type == "digit")
                                {
                                    std::valarray<double> ans = mlnn[j].compute(testd[j].ip(k));
                                    uint8_t res = 0;
                                    for(size_t l = 1; l < DNN_MNIST_C; ++l)
                                    {
                                        if(ans[l] > ans[res])
                                        {
                                            res = l;
                                        }
                                    }
                                    if(res == testd[j].v(k))
                                    {
                                        trace[run][j][i/tev]+= 1.0;
                                    }
                                }
                                else if(type == "femnist")
                                {
                                    std::valarray<double> ans = mlnn[j].compute(femnist[j].ip(k));
                                    uint8_t res = 0;
                                    for(size_t l = 1; l < femnist[j].getOutSize(); ++l)
                                    {
                                        if(ans[l] > ans[res])
                                        {
                                            res = l;
                                        }
                                    }
                                    if(res == femnist[j].v(k))
                                    {
                                        trace[run][j][i/tev]+= 1.0;
                                    }
                                }
                                else if(type == "vsn")
                                {
                                    std::valarray<double> ans = mlnn[j].compute(vsn[j].ip(k));
                                    if((ans[0] > 0.5) == vsn[j].v(k))
                                    {
                                        trace[run][j][i/tev]+= 1.0;
                                    }
                                }
                                else if(type == "metro")
                                {
                                    std::vector<std::valarray<double>> ans = lstm[j].compute(metro[j].sp(k,tcsqlen),tprlen-1);
                                    trace[run][j][i/tev]+= 1.0-(2.0*fabs(ans[tprlen-1][0]-metro[j][k+tcsqlen+tprlen]));
                                }
                                else if(type == "march")
                                {
                                    std::vector<std::valarray<double>> ans = lstm[j].compute(binseq[j].sp(k,tcsqlen),tprlen-1);
                                    std::valarray<double> ctrl = binseq[j].p(k+tcsqlen+tprlen);
                                    bool okv = true;
                                    for(size_t l = 0; l < ctrl.size(); ++l)
                                    {
                                        if(fabs(ans[tprlen-1][l]-ctrl[l]) >= 0.5)
                                        {
                                            okv = false;
                                            break;
                                        }
                                    }
                                    if(okv)
                                    {
                                        trace[run][j][i/tev]+= 1.0;
                                    }
                                }
                                else if(type == "assoc")
                                {
                                    std::valarray<double> res = assoc[j].compute(sdr[j].ip(k)), ref = sdr[j].p(k);
                                    int acc = 0;
                                    for(size_t m = 0; m < res.size(); ++m)
                                    {
                                        if((res[m] > 0.5) || (ref[m] > 0.5))
                                        {
                                            if((res[m] > 0.5) && (ref[m] > 0.5))
                                            {
                                                acc++;
                                            }
                                            else
                                            {
                                                acc--;
                                            }
                                        }
                                    }
                                    if(acc >= 0)
                                    {
                                        trace[run][j][i/tev]+= 1.0;
                                    }
                                }
                            }
                            trace[run][j][i/tev]/= ((double)(ttnd-ttst));
                        }
                    }
                }
                #pragma omp parallel for
                for(size_t i = 0; i < n; ++i)
                {
                    sr[run][i] = 0.0;
                    for(size_t j = tst; j < tnd; ++j)
                    {
                        if(type == "digit")
                        {
                            std::valarray<double> ans = mlnn[i].compute(testd[i].ip(j));
                            uint8_t res = 0;
                            for(size_t k = 1; k < DNN_MNIST_C; ++k)
                            {
                                if(ans[k] > ans[res])
                                {
                                    res = k;
                                }
                            }
                            if(res == testd[i].v(j))
                            {
                                sr[run][i]+= 1.0;
                            }
                        }
                        else if(type == "femnist")
                        {
                            std::valarray<double> ans = mlnn[i].compute(femnist[i].ip(j));
                            uint8_t res = 0;
                            for(size_t k = 1; k < femnist[i].getOutSize(); ++k)
                            {
                                if(ans[k] > ans[res])
                                {
                                    res = k;
                                }
                            }
                            if(res == femnist[i].v(j))
                            {
                                sr[run][i]+= 1.0;
                            }
                        }
                        else if(type == "vsn")
                        {
                            std::valarray<double> ans = mlnn[i].compute(vsn[i].ip(j));
                            if((ans[0] > 0.5) == vsn[i].v(j))
                            {
                                sr[run][i]+= 1.0;
                            }
                        }
                        else if(type == "metro")
                        {
                            std::vector<std::valarray<double>> ans = lstm[i].compute(metro[i].sp(j,csqlen),prlen-1);
                            sr[run][i]+= 1.0-(2.0*fabs(ans[prlen-1][0]-metro[i][j+csqlen+prlen]));
                        }
                        else if(type == "march")
                        {
                            std::vector<std::valarray<double>> ans = lstm[i].compute(binseq[i].sp(j,csqlen),prlen-1);
                            std::valarray<double> ctrl = binseq[i].p(j+csqlen+prlen);
                            bool okv = true;
                            for(size_t k = 0; k < ctrl.size(); ++k)
                            {
                                if(fabs(ans[prlen-1][k]-ctrl[k]) >= 0.5)
                                {
                                    okv = false;
                                    break;
                                }
                            }
                            if(okv)
                            {
                                sr[run][i]+= 1.0;
                            }
                        }
                        else if(type == "assoc")
                        {
                            std::valarray<double> res = assoc[i].compute(sdr[i].ip(j)), ref = sdr[i].p(j);
                            int acc = 0;
                            for(size_t m = 0; m < res.size(); ++m)
                            {
                                if((res[m] > 0.5) || (ref[m] > 0.5))
                                {
                                    if((res[m] > 0.5) && (ref[m] > 0.5))
                                    {
                                        acc++;
                                    }
                                    else
                                    {
                                        acc--;
                                    }
                                }
                            }
                            if(acc >= 0)
                            {
                                sr[run][i]+= 1.0;
                            }
                        }
                    }
                    sr[run][i]/= ((double)(tnd-tst));
                }
            }
        }
        else if(token == "Out")
        {
            std::string traf, itraf;
            std::cout << "Accuracy:\n       ";
            for(size_t i = 0; i < n; ++i)
            {
                std::cout << "  " << std::setw(5) << i << "  ";
            }
            std::cout << "     Average" << std::endl;
            for(size_t i = 0; i < rs; ++i)
            {
                std::cout << "Run " << std::setw(4) << (i+1) << ": ";
                for(size_t j = 0; j < n; ++j)
                {
                    std::cout << std::fixed << std::setprecision(6) << sr[i][j] << ' ';
                }
                std::cout << "| " << std::fixed << std::setprecision(6) << (sr[i].sum()/n) << std::endl;
            }
            double avg = 0.0;
            std::cout << "Average : ";
            for(size_t i = 0; i < n; ++i)
            {
                double la = 0.0;
                for(size_t j = 0; j < rs; ++j)
                {
                    la+= sr[j][i];
                }
                la/= rs;
                avg+= la;
                std::cout << std::fixed << std::setprecision(6) << la << ' ';
            }
            std::cout << "| " << std::fixed << std::setprecision(6) << (avg/n) << std::endl;
            if(tra)
            {
                if(itra)
                {
                    std::getline(ls,traf,' '); 
                    ls >> itraf;
                }
                else
                {
                    ls >> traf;
                }
            }
            else if(itra)
            {
                ls >> itraf;
            }
            if(tra)
            {
                std::ofstream tf(traf,std::ifstream::out);
                for(size_t i = 0; i < rs; ++i)
                {
                    for(size_t j = 0; j < trace[i][0].size(); ++j)
                    {
                        double avg = 0.0;
                        for(size_t k = 0; k < n; ++k)
                        {
                            avg+= trace[i][k][j];
                            tf << trace[i][k][j] << ' ';
                        }
                        avg/= n;
                        tf << avg << std::endl;
                    }
                    tf << std::endl;
                }
                tf << std::endl;
            }
            if(itra)
            {
                std::ofstream tf(itraf,std::ifstream::out);
                for(size_t i = 0; i < rs; ++i)
                {
                    for(size_t j = 0; j < itrace[i][0].size(); ++j)
                    {
                        double avg = 0.0;
                        for(size_t k = 0; k < n; ++k)
                        {
                            avg+= itrace[i][k][j];
                            tf << itrace[i][k][j] << ' ';
                        }
                        avg/= n;
                        tf << avg << std::endl;
                    }
                    tf << std::endl;
                }
                tf << std::endl;
            }
        }
        else if(token == "Automod")
        {
            ls >> drprc >> drmmt >> sync;
            rs = 1;
            goto autm;
        }
        else if(token == "Mautomod")
        {
            ls >> drprc >> drmmt >> sync >> rs;
            autm:
            std::valarray<double> gt((double)0.0,rs), gta((double)0.0,rs), gtc((double)0.0,rs), gtd((double)0.0,rs), gtk((double)0.0,rs), gto((double)0.0,rs), lt((double)0.0,rs), lta((double)0.0,rs), ltc((double)0.0,rs), ltd((double)0.0,rs), ltk((double)0.0,rs), lto((double)0.0,rs);
            std::vector<std::vector<size_t>> red, reda, redc, redd, redk, redo;
            vv.clear();
            vv.resize(n,std::valarray<double>(DNN_MNIST_C*DNN_MNIST_C));
            MLNN mlnn[n];
            LSTM lstm[n];
            Assoc assoc[n];
            MNIST mnist[n], affctd[n];
            FEMNIST femnist[n], affctf[n];
            VSN vsn[n], affctv[n];
            Metro metro[n], affctm[n];
            BinSeq binseq[n], affctb[n];
            SDR sdr[n], affcts[n];
            if(type == "digit")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    if(perm.find(i) == perm.end())
                    {
                        if(amask.find(i) == amask.end())
                        {
                            if(iperm.find(i) == iperm.end())
                            {
                                mnist[i].init(TRI,TRV,i*bs);
                            }
                            else
                            {
                                mnist[i].init(TRI,TRV,iperm[i],i*bs);
                            }
                        }
                        else
                        {
                            if(iperm.find(i) == iperm.end())
                            {
                                mnist[i].init(TRI,TRV,mask[amask[i]],i*bs);
                            }
                            else
                            {
                                mnist[i].init(TRI,TRV,mask[amask[i]],iperm[i],i*bs);
                            }
                        }
                    }
                    else
                    {
                        if(amask.find(i) == amask.end())
                        {
                            if(iperm.find(i) == iperm.end())
                            {
                                mnist[i].init(TRI,TRV,perm[i],i*bs);
                            }
                            else
                            {
                                mnist[i].init(TRI,TRV,perm[i],iperm[i],i*bs);
                            }
                        }
                        else
                        {
                            if(iperm.find(i) == iperm.end())
                            {
                                mnist[i].init(TRI,TRV,perm[i],mask[amask[i]],i*bs);
                            }
                            else
                            {
                                mnist[i].init(TRI,TRV,perm[i],mask[amask[i]],iperm[i],i*bs);
                            }
                        }
                    }
                    affctd[i].init(TSI,TSV);
                }
            }
            else if(type == "femnist")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    femnist[i].init(FMI,FMV,fmntp,i);
                    affctf[i].init(FMI,FMV,fmntp,n);
                }
            }
            else if(type == "vsn")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    vsn[i].init(VSI,VSV,i);
                    affctv[i].init(VSI,VSV,n);
                }
            }
            else if(type == "metro")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    metro[i].init(MLT,mlines[i]);
                    affctm[i].init(MLT,tline);
                }
            }
            else if(type == "march")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    binseq[i].init(mcls[am[i]],mpos[am[i]]);
                    affctb[i].init(mcls[mrcls],mpos[mrcls]);
                }
            }
            else if(type == "assoc")
            {
                for(size_t i = 0; i < n; ++i)
                {
                    if(aperm.find(i) == aperm.end())
                    {
                        sdr[i].init(layers[0],actn,nsets,noise);
                    }
                    else
                    {
                        sdr[i].init(layers[0],actn,nsets,noise,aperm[i].first,aperm[i].second);
                    }
                    if(aperm.find(n) == aperm.end())
                    {
                        affcts[n].init(layers[0],actn,nsets,noise);
                    }
                    else
                    {
                        affcts[n].init(layers[0],actn,nsets,noise,aperm[n].first,aperm[n].second);
                    }
                }
            }
            for(unsigned run = 0; run < rs; ++run)
            {
                if(type == "digit" || type == "femnist" || type == "vsn")
                {
                    if(idinit)
                    {
                        mlnn[0] = MLNN(layers,actf,dactf,weightd,actfpd);
                        for(size_t i = 1; i < n; ++i)
                        {
                            mlnn[i] = MLNN(mlnn[0]);
                        }
                    }
                    else
                    {
                        for(size_t i = 0; i < n; ++i)
                        {
                            mlnn[i] = MLNN(layers,actf,dactf,weightd,actfpd);
                        }
                    }
                }
                else if(type == "metro" || type == "march")
                {
                    std::array<std::function<double(double,std::valarray<double>)>,5> flt;
                    std::array<std::vector<std::function<double(double,std::valarray<double>)>>,5> dflt;
                    std::array<std::function<double(size_t)>,5> wdlt;
                    std::array<std::function<std::valarray<double>(void)>,5> afdlt;
                    flt[0] = actf;
                    flt[1] = gactf;
                    flt[2] = gactf;
                    flt[3] = gactf;
                    flt[4] = pactf;
                    dflt[0] = dactf;
                    dflt[1] = gdactf;
                    dflt[2] = gdactf;
                    dflt[3] = gdactf;
                    dflt[4] = pdactf;
                    wdlt[0] = weightd;
                    wdlt[1] = weightd;
                    wdlt[2] = weightd;
                    wdlt[3] = weightd;
                    wdlt[4] = weightd;
                    afdlt[0] = actfpd;
                    afdlt[1] = pgactfpd;
                    afdlt[2] = ngactfpd;
                    afdlt[3] = ngactfpd;
                    afdlt[4] = pactfpd;
                    if(idinit)
                    {
                        lstm[0] = LSTM(layers,players,flt,dflt,wdlt,afdlt,normf,dnormf);
                        for(size_t i = 1; i < n; ++i)
                        {
                            lstm[i] = LSTM(lstm[0]);
                        }
                    }
                    else
                    {
                        for(size_t i = 0; i < n; ++i)
                        {
                            lstm[i] = LSTM(layers,players,flt,dflt,wdlt,afdlt,normf,dnormf);
                        }
                    }
                }
                else if(type == "assoc")
                {
                    if(idinit)
                    {
                        assoc[0] = Assoc(layers[0],layers[1],thld,weightd);
                        for(size_t i = 1; i < n; ++i)
                        {
                            assoc[i] = Assoc(assoc[0]);
                        }
                    }
                    else
                    {
                        for(size_t i = 0; i < n; ++i)
                        {
                            assoc[i] = Assoc(layers[0],layers[1],thld,weightd);
                        }
                    }
                }
                #pragma omp parallel for
                for(size_t j = 0; j < n; ++j)
                {
                    for(unsigned k = 0; k < sync; ++k)
                    {
                        std::vector<size_t> minib = getMinibatch(bs,mbs);
                        if(seq)
                        {
                            if(type == "digit")
                            {
                                for(size_t l = 0; l < mbs; ++l)
                                {
                                    mlnn[j].train(mnist[j].ip(minib[l]),mnist[j].vp(minib[l]),lr);
                                }
                            }
                            else if(type == "femnist")
                            {
                                for(size_t l = 0; l < mbs; ++l)
                                {
                                    mlnn[j].train(femnist[j].ip(minib[l]),femnist[j].vp(minib[l]),lr);
                                }
                            }
                            else if(type == "vsn")
                            {
                                for(size_t l = 0; l < mbs; ++l)
                                {
                                    mlnn[j].train(vsn[j].ip(minib[l]),vsn[j].vp(minib[l]),lr);
                                }
                            }
                            else if(type == "metro")
                            {
                                for(size_t l = 0; l < mbs; ++l)
                                {
                                    lstm[j].train(metro[j].sp(minib[l],sqlen),metro[j].sp(minib[l]+1,sqlen),lr);
                                }
                            }
                            else if(type == "march")
                            {
                                for(size_t l = 0; l < mbs; ++l)
                                {
                                    lstm[j].train(binseq[j].sp(minib[l]+j*(bs+sqlen),sqlen),binseq[j].sp(minib[l]+j*(bs+sqlen)+1,sqlen),lr);
                                }
                            }
                            else if(type == "assoc")
                            {
                                for(size_t l = 0; l < mbs; ++l)
                                {
                                    assoc[j].train(sdr[j].p(minib[l]+j*bs),lr);
                                }
                            }
                        }
                        else
                        {
                            if(type == "digit")
                            {
                                mlnn[j].train(mnist[j].ip(minib),mnist[j].vp(minib),lr);
                            }
                            else if(type == "femnist")
                            {
                                mlnn[j].train(femnist[j].ip(minib),femnist[j].vp(minib),lr);
                            }
                            else if(type == "vsn")
                            {
                                mlnn[j].train(vsn[j].ip(minib),vsn[j].vp(minib),lr);
                            }
                            else if(type == "metro")
                            {
                                lstm[j].train(metro[j].sp(minib,sqlen),metro[j].sp(minib+1,sqlen),lr);
                            }
                            else if(type == "march")
                            {
                                lstm[j].train(binseq[j].sp(minib+j*(bs+sqlen),sqlen),binseq[j].sp(minib+j*(bs+sqlen)+1,sqlen),lr);
                            }
                            else if(type == "assoc")
                            {
                                assoc[j].train(sdr[j].p(minib),lr);
                            }
                        }
                    }
                    std::vector<unsigned> etc;
                    if(type == "digit")
                    {
                        vv[j] = std::valarray<double>(0.0,DNN_MNIST_C*DNN_MNIST_C);
                        etc = std::vector<unsigned>(DNN_MNIST_C,0);
                    }
                    else if(type == "femnist")
                    {
                        vv[j] = std::valarray<double>(0.0,femnist[j].getOutSize()*femnist[j].getOutSize());
                        etc = std::vector<unsigned>(femnist[j].getOutSize(),0);
                    }
                    else if(type == "vsn")
                    {
                        vv[j] = std::valarray<double>(0.0,2);
                        etc = std::vector<unsigned>(2,0);
                    }
                    else if(type == "metro")
                    {
                        vv[j] = std::valarray<double>(0.0,tnd-tst);
                        etc = std::vector<unsigned>(tnd-tst,0);
                    }
                    else if(type == "march")
                    {
                        vv[j] = std::valarray<double>(0.0,tnd-tst);
                        etc = std::vector<unsigned>(tnd-tst,0);
                    }
                    else if(type == "assoc")
                    {
                        vv[j] = std::valarray<double>(0.0,(tnd-tst)*layers[0]);
                        etc = std::vector<unsigned>(layers[0],0);
                    }
                    for(size_t k = tst; k < tnd; ++k)
                    {
                        if(type == "digit")
                        {
                            std::valarray<double> ans = mlnn[j].compute(affctd[j].ip(k));
                            for(size_t l = 0; l < DNN_MNIST_C; ++l)
                            {
                                vv[j][(affctd[j].v(k)*DNN_MNIST_C)+l]+= ans[l];
                                ++etc[affctd[j].v(k)];
                            }
                        }
                        else if(type == "femnist")
                        {
                            std::valarray<double> ans = mlnn[j].compute(affctf[j].ip(k));
                            for(size_t l = 0; l < ans.size(); ++l)
                            {
                                vv[j][(affctf[j].v(k)*femnist[j].getOutSize())+l]+= ans[l];
                                ++etc[affctf[j].v(k)];
                            }
                        }
                        else if(type == "vsn")
                        {
                            std::valarray<double> ans = mlnn[j].compute(affctv[j].ip(k));
                            for(size_t l = 0; l < ans.size(); ++l)
                            {
                                vv[j][(((affctv[j].v(k))?1:0)*1)+l]+= ans[l];
                                ++etc[((affctv[j].v(k))?1:0)];
                            }
                        }
                        else if(type == "metro")
                        {
                            std::valarray<double> ans = lstm[j].compute(affctm[j].sp(k,itcsqlen),itprlen-1)[0];
                            for(size_t l = 0; l < ans.size(); ++l)
                            {
                                vv[j][((k-tst)*(tnd-tst))+l]+= ans[l];
                                ++etc[k-tst];
                            }
                        }
                        else if(type == "march")
                        {
                            std::valarray<double> ans = lstm[j].compute(affctb[j].sp(k,itcsqlen),itprlen-1)[0];
                            for(size_t l = 0; l < ans.size(); ++l)
                            {
                                vv[j][((k-tst)*(tnd-tst))+l]+= ans[l];
                                ++etc[k-tst];
                            }
                        }
                        else if(type == "assoc")
                        {
                            std::valarray<double> ans = assoc[j].compute(affcts[j].ip(k));
                            for(size_t l = 0; l < ans.size(); ++l)
                            {
                                vv[j][((k-tst)*(tnd-tst))+l]+= ans[l];
                                ++etc[k-tst];
                            }
                        }
                    }
                    for(size_t k = 0; k < etc.size(); ++k)
                    {
                        for(size_t l = 0; l < (vv[j].size()/etc.size()); ++l)
                        {
                            if(etc[k] > 0)
                            {
                                vv[j][(k*(vv[j].size()/etc.size()))+l]/= etc[k];
                            }
                        }
                    }
                }
                if(rncv)
                {
                    red = recommend(vv,ddf,dnf,dvf,false,false,drprc,drmmt);
                    reda = recommend(vv,ddf,dnf,dvf,false,true,drprc,drmmt);
                }
                redc = recommend(vv,ddf,dnf,dvf,true,false,drprc,drmmt);
                redd = recommend(vv,ddf,dnf,dvf,true,true,drprc,drmmt);
                redk = kmeans(vv,ddf,dnf,dvf,drprc,drmmt);
                redo = opticsExtract(vv,optics(vv,std::numeric_limits<double>::infinity(),1,ddf),ddf,dnf,dvf);
                std::vector<size_t> led, leda, ledc = makeLst(n,redc), ledd = makeLst(n,redd), ledk = makeLstFull(n,redk), ledo = makeLstFull(n,redo);
                unsigned gct = 0, lct = 0;
                for(size_t i = 0; i < n; ++i)
                {
                    for(size_t j = 0; j < n; ++j)
                    {
                        if(type == "digit")
                        {
                            if(mnist[i].getPerm() == mnist[j].getPerm())
                            {
                                ++gct;
                                if(ledc[i] == ledc[j])
                                {
                                    gtc[run]+= 1.0;
                                }
                                if(ledd[i] == ledd[j])
                                {
                                    gtd[run]+= 1.0;
                                }
                                if(ledk[i] == ledk[j])
                                {
                                    gtk[run]+= 1.0;
                                }
                                if(ledo[i] == ledo[j])
                                {
                                    gto[run]+= 1.0;
                                }
                            }
                            else
                            {
                                ++lct;
                                if(ledc[i] != ledc[j])
                                {
                                    ltc[run]+= 1.0;
                                }
                                if(ledd[i] != ledd[j])
                                {
                                    ltd[run]+= 1.0;
                                }
                                if(ledk[i] != ledk[j])
                                {
                                    ltk[run]+= 1.0;
                                }
                                if(ledo[i] != ledo[j])
                                {
                                    lto[run]+= 1.0;
                                }
                            }
                        }
                        else if(type == "femnist")
                        {
                            if(i == j)
                            {
                                ++gct;
                                if(ledc[i] == ledc[j])
                                {
                                    gtc[run]+= 1.0;
                                }
                                if(ledd[i] == ledd[j])
                                {
                                    gtd[run]+= 1.0;
                                }
                                if(ledk[i] == ledk[j])
                                {
                                    gtk[run]+= 1.0;
                                }
                                if(ledo[i] == ledo[j])
                                {
                                    gto[run]+= 1.0;
                                }
                            }
                            else
                            {
                                ++lct;
                                if(ledc[i] != ledc[j])
                                {
                                    ltc[run]+= 1.0;
                                }
                                if(ledd[i] != ledd[j])
                                {
                                    ltd[run]+= 1.0;
                                }
                                if(ledk[i] != ledk[j])
                                {
                                    ltk[run]+= 1.0;
                                }
                                if(ledo[i] != ledo[j])
                                {
                                    lto[run]+= 1.0;
                                }
                            }
                        }
                        else if(type == "vsn")
                        {
                            if(i == j)
                            {
                                ++gct;
                                if(ledc[i] == ledc[j])
                                {
                                    gtc[run]+= 1.0;
                                }
                                if(ledd[i] == ledd[j])
                                {
                                    gtd[run]+= 1.0;
                                }
                                if(ledk[i] == ledk[j])
                                {
                                    gtk[run]+= 1.0;
                                }
                                if(ledo[i] == ledo[j])
                                {
                                    gto[run]+= 1.0;
                                }
                            }
                            else
                            {
                                ++lct;
                                if(ledc[i] != ledc[j])
                                {
                                    ltc[run]+= 1.0;
                                }
                                if(ledd[i] != ledd[j])
                                {
                                    ltd[run]+= 1.0;
                                }
                                if(ledk[i] != ledk[j])
                                {
                                    ltk[run]+= 1.0;
                                }
                                if(ledo[i] != ledo[j])
                                {
                                    lto[run]+= 1.0;
                                }
                            }
                        }
                        else if(type == "metro")
                        {
                            if(mlines[i] == mlines[j])
                            {
                                ++gct;
                                if(ledc[i] == ledc[j])
                                {
                                    gtc[run]+= 1.0;
                                }
                                if(ledd[i] == ledd[j])
                                {
                                    gtd[run]+= 1.0;
                                }
                                if(ledk[i] == ledk[j])
                                {
                                    gtk[run]+= 1.0;
                                }
                                if(ledo[i] == ledo[j])
                                {
                                    gto[run]+= 1.0;
                                }
                            }
                            else
                            {
                                ++lct;
                                if(ledc[i] != ledc[j])
                                {
                                    ltc[run]+= 1.0;
                                }
                                if(ledd[i] != ledd[j])
                                {
                                    ltd[run]+= 1.0;
                                }
                                if(ledk[i] != ledk[j])
                                {
                                    ltk[run]+= 1.0;
                                }
                                if(ledo[i] != ledo[j])
                                {
                                    lto[run]+= 1.0;
                                }
                            }
                        }
                        else if(type == "march")
                        {
                            if(am[i] == am[j])
                            {
                                ++gct;
                                if(ledc[i] == ledc[j])
                                {
                                    gtc[run]+= 1.0;
                                }
                                if(ledd[i] == ledd[j])
                                {
                                    gtd[run]+= 1.0;
                                }
                                if(ledk[i] == ledk[j])
                                {
                                    gtk[run]+= 1.0;
                                }
                                if(ledo[i] == ledo[j])
                                {
                                    gto[run]+= 1.0;
                                }
                            }
                            else
                            {
                                ++lct;
                                if(ledc[i] != ledc[j])
                                {
                                    ltc[run]+= 1.0;
                                }
                                if(ledd[i] != ledd[j])
                                {
                                    ltd[run]+= 1.0;
                                }
                                if(ledk[i] != ledk[j])
                                {
                                    ltk[run]+= 1.0;
                                }
                                if(ledo[i] != ledo[j])
                                {
                                    lto[run]+= 1.0;
                                }
                            }
                        }
                        else if(type == "assoc")
                        {
                            if((((aperm.find(i) != aperm.end()) && (aperm.find(j) != aperm.end())) && (aperm[i] == aperm[j])) || (aperm.find(i) == aperm.find(j)))
                            {
                                ++gct;
                                if(ledc[i] == ledc[j])
                                {
                                    gtc[run]+= 1.0;
                                }
                                if(ledd[i] == ledd[j])
                                {
                                    gtd[run]+= 1.0;
                                }
                                if(ledk[i] == ledk[j])
                                {
                                    gtk[run]+= 1.0;
                                }
                                if(ledo[i] == ledo[j])
                                {
                                    gto[run]+= 1.0;
                                }
                            }
                            else
                            {
                                ++lct;
                                if(ledc[i] != ledc[j])
                                {
                                    ltc[run]+= 1.0;
                                }
                                if(ledd[i] != ledd[j])
                                {
                                    ltd[run]+= 1.0;
                                }
                                if(ledk[i] != ledk[j])
                                {
                                    ltk[run]+= 1.0;
                                }
                                if(ledo[i] != ledo[j])
                                {
                                    lto[run]+= 1.0;
                                }
                            }
                        }
                    }
                }
                gtc[run]-=1.0;
                gtd[run]-=1.0;
                gtk[run]-=1.0;
                gto[run]-=1.0;
                gtc[run]/= ((double)(gct-1));
                gtd[run]/= ((double)(gct-1));
                gtk[run]/= ((double)(gct-1));
                gto[run]/= ((double)(gct-1));
                ltc[run]/= ((double)lct);
                ltd[run]/= ((double)lct);
                ltk[run]/= ((double)lct);
                lto[run]/= ((double)lct);
                if(rncv)
                {
                    led = makeLst(n,red);
                    leda = makeLst(n,reda);
                    for(size_t i = 0; i < n; ++i)
                    {
                        for(size_t j = 0; j < n; ++j)
                        {
                            if(type == "digit")
                            {
                                if(mnist[i].getPerm() == mnist[j].getPerm())
                                {
                                    if(led[i] == led[j])
                                    {
                                        gt[run]+= 1.0;
                                    }
                                    if(leda[i] == leda[j])
                                    {
                                        gta[run]+= 1.0;
                                    }
                                }
                                else
                                {
                                    if(led[i] != led[j])
                                    {
                                        lt[run]+= 1.0;
                                    }
                                    if(leda[i] != leda[j])
                                    {
                                        lta[run]+= 1.0;
                                    }
                                }
                            }
                            else if(type == "femnist")
                            {
                                if(i == j)
                                {
                                    if(led[i] == led[j])
                                    {
                                        gt[run]+= 1.0;
                                    }
                                    if(leda[i] == leda[j])
                                    {
                                        gta[run]+= 1.0;
                                    }
                                }
                                else
                                {
                                    if(led[i] != led[j])
                                    {
                                        lt[run]+= 1.0;
                                    }
                                    if(leda[i] != leda[j])
                                    {
                                        lta[run]+= 1.0;
                                    }
                                }
                            }
                            else if(type == "vsn")
                            {
                                if(i == j)
                                {
                                    if(led[i] == led[j])
                                    {
                                        gt[run]+= 1.0;
                                    }
                                    if(leda[i] == leda[j])
                                    {
                                        gta[run]+= 1.0;
                                    }
                                }
                                else
                                {
                                    if(led[i] != led[j])
                                    {
                                        lt[run]+= 1.0;
                                    }
                                    if(leda[i] != leda[j])
                                    {
                                        lta[run]+= 1.0;
                                    }
                                }
                            }
                            else if(type == "metro")
                            {
                                if(mlines[i] == mlines[j])
                                {
                                    if(led[i] == led[j])
                                    {
                                        gt[run]+= 1.0;
                                    }
                                    if(leda[i] == leda[j])
                                    {
                                        gta[run]+= 1.0;
                                    }
                                }
                                else
                                {
                                    if(led[i] != led[j])
                                    {
                                        lt[run]+= 1.0;
                                    }
                                    if(leda[i] != leda[j])
                                    {
                                        lta[run]+= 1.0;
                                    }
                                }
                            }
                            else if(type == "march")
                            {
                                if(am[i] == am[j])
                                {
                                    if(led[i] == led[j])
                                    {
                                        gt[run]+= 1.0;
                                    }
                                    if(leda[i] == leda[j])
                                    {
                                        gta[run]+= 1.0;
                                    }
                                }
                                else
                                {
                                    if(led[i] != led[j])
                                    {
                                        lt[run]+= 1.0;
                                    }
                                    if(leda[i] != leda[j])
                                    {
                                        lta[run]+= 1.0;
                                    }
                                }
                            }
                            else if(type == "assoc")
                            {
                                if((((aperm.find(i) != aperm.end()) && (aperm.find(j) != aperm.end())) && (aperm[i] == aperm[j])) || (aperm.find(i) == aperm.find(j)))
                                {
                                    if(led[i] == led[j])
                                    {
                                        gt[run]+= 1.0;
                                    }
                                    if(leda[i] == leda[j])
                                    {
                                        gta[run]+= 1.0;
                                    }
                                }
                                else
                                {
                                    if(led[i] != led[j])
                                    {
                                        lt[run]+= 1.0;
                                    }
                                    if(leda[i] != leda[j])
                                    {
                                        lta[run]+= 1.0;
                                    }
                                }
                            }
                        }
                    }
                    gt[run]=-1.0;
                    gta[run]=-1.0;
                    gt[run]/= ((double)(gct-1));
                    gta[run]/= ((double)(gct-1));
                    lt[run]/= ((double)lct);
                    lta[run]/= ((double)lct);
                }
            }
            if(rncv)
            {
                std::cout << "Identification rate:\n             AUCCCR    AUCCCR-A   AUCCCR-C   AUCCCR-CA   k-means    OPTICS" << std::endl;
            }
            else
            {
                std::cout << "Identification rate:\n            AUCCCR-C   AUCCCR-CA   k-means    OPTICS" << std::endl;
            }
            for(size_t i = 0; i < rs; ++i)
            {
                if(rncv)
                {
                    std::cout << "Run " << std::setw(5) << (i+1) << ": " << flofix(gt[i],10) << ' ' << flofix(gta[i],10) << ' ' << flofix(gtc[i],10) << ' ' << flofix(gtd[i],10) << ' ' << flofix(gtk[i],10) << ' ' << flofix(gto[i],10) << std::endl;
                }
                else
                {
                    std::cout << "Run " << std::setw(5) << (i+1) << ": " << flofix(gtc[i],10) << ' ' << flofix(gtd[i],10) << ' ' << flofix(gtk[i],10) << ' ' << flofix(gto[i],10) << std::endl;
                }
            }
            if(rncv)
            {
                std::cout << "Average  : " << flofix(gt.sum()/rs,10) << ' ' << flofix(gta.sum()/rs,10) << ' ' << flofix(gtc.sum()/rs,10) << ' ' << flofix(gtd.sum()/rs,10) << ' ' << flofix(gtk.sum()/rs,10) << ' ' << flofix(gto.sum()/rs,10) << "\nSD       : " << flofix(sqrt(((gt*gt).sum()/rs)-((gt.sum()/rs)*(gt.sum()/rs))),10) << ' ' << flofix(sqrt(((gta*gta).sum()/rs)-((gta.sum()/rs)*(gta.sum()/rs))),10) << ' ' << flofix(sqrt(((gtc*gtc).sum()/rs)-((gtc.sum()/rs)*(gtc.sum()/rs))),10) << ' ' << flofix(sqrt(((gtd*gtd).sum()/rs)-((gtd.sum()/rs)*(gtd.sum()/rs))),10) << ' ' << flofix(sqrt(((gtk*gtk).sum()/rs)-((gtk.sum()/rs)*(gtk.sum()/rs))),10) << ' ' << flofix(sqrt(((gto*gto).sum()/rs)-((gto.sum()/rs)*(gto.sum()/rs))),10) << '\n' << std::endl;
            }
            else
            {
                std::cout << "Average  : " << flofix(gtc.sum()/rs,10) << ' ' << flofix(gtd.sum()/rs,10) << ' ' << flofix(gtk.sum()/rs,10) << ' ' << flofix(gto.sum()/rs,10) << "\nSD       : " << flofix(sqrt(((gtc*gtc).sum()/rs)-((gtc.sum()/rs)*(gtc.sum()/rs))),10) << ' ' << flofix(sqrt(((gtd*gtd).sum()/rs)-((gtd.sum()/rs)*(gtd.sum()/rs))),10) << ' ' << flofix(sqrt(((gtk*gtk).sum()/rs)-((gtk.sum()/rs)*(gtk.sum()/rs))),10) << ' ' << flofix(sqrt(((gto*gto).sum()/rs)-((gto.sum()/rs)*(gto.sum()/rs))),10) << '\n' << std::endl;
            }
            if(rncv)
            {
                std::cout << "Differentiation rate:\n             AUCCCR    AUCCCR-A   AUCCCR-C   AUCCCR-CA   k-means    OPTICS" << std::endl;
            }
            else
            {
                std::cout << "Differentiation rate:\n            AUCCCR-C   AUCCCR-CA   k-means    OPTICS" << std::endl;
            }
            for(size_t i = 0; i < rs; ++i)
            {
                if(rncv)
                {
                    std::cout << "Run " << std::setw(5) << (i+1) << ": " << flofix(lt[i],10) << ' ' << flofix(lta[i],10) << ' ' << flofix(ltc[i],10) << ' ' << flofix(ltd[i],10) << ' ' << flofix(ltk[i],10) << ' ' << flofix(lto[i],10) << std::endl;
                }
                else
                {
                    std::cout << "Run " << std::setw(5) << (i+1) << ": " << flofix(ltc[i],10) << ' ' << flofix(ltd[i],10) << ' ' << flofix(ltk[i],10) << ' ' << flofix(lto[i],10) << std::endl;
                }
            }
            if(rncv)
            {
                std::cout << "Average  : " << flofix(lt.sum()/rs,10) << ' ' << flofix(lta.sum()/rs,10) << ' ' << flofix(ltc.sum()/rs,10) << ' ' << flofix(ltd.sum()/rs,10) << ' ' << flofix(ltk.sum()/rs,10) << ' ' << flofix(lto.sum()/rs,10) << "\nSD       : " << flofix(sqrt(((lt*lt).sum()/rs)-((lt.sum()/rs)*(lt.sum()/rs))),10) << ' ' << flofix(sqrt(((lta*lta).sum()/rs)-((lta.sum()/rs)*(lta.sum()/rs))),10) << ' ' << flofix(sqrt(((ltc*ltc).sum()/rs)-((ltc.sum()/rs)*(ltc.sum()/rs))),10) << ' ' << flofix(sqrt(((ltd*ltd).sum()/rs)-((ltd.sum()/rs)*(ltd.sum()/rs))),10) << ' ' << flofix(sqrt(((ltk*ltk).sum()/rs)-((ltk.sum()/rs)*(ltk.sum()/rs))),10) << ' ' << flofix(sqrt(((lto*lto).sum()/rs)-((lto.sum()/rs)*(lto.sum()/rs))),10) << '\n' << std::endl;
            }
            else
            {
                std::cout << "Average  : " << flofix(ltc.sum()/rs,10) << ' ' << flofix(ltd.sum()/rs,10) << ' ' << flofix(ltk.sum()/rs,10) << ' ' << flofix(lto.sum()/rs,10) << "\nSD       : " << flofix(sqrt(((ltc*ltc).sum()/rs)-((ltc.sum()/rs)*(ltc.sum()/rs))),10) << ' ' << flofix(sqrt(((ltd*ltd).sum()/rs)-((ltd.sum()/rs)*(ltd.sum()/rs))),10) << ' ' << flofix(sqrt(((ltk*ltk).sum()/rs)-((ltk.sum()/rs)*(ltk.sum()/rs))),10) << ' ' << flofix(sqrt(((lto*lto).sum()/rs)-((lto.sum()/rs)*(lto.sum()/rs))),10) << '\n' << std::endl;
            }
            if(clpr)
            {
                std::vector<std::vector<std::valarray<double>>> pgt, pgta, pgtc = makeGrp(vv,redc), pgtd = makeGrp(vv,redd), pgtk = makeGrpFull(vv,redk), pgto = makeGrpFull(vv,redo);
                if(rncv)
                {
                    pgt = makeGrp(vv,red);
                    pgta = makeGrp(vv,reda);
                }
                unsigned ck = 0;
                std::ofstream tf(rcf1,std::ifstream::out);
                if(rncv)
                {
                    for(size_t i = 0; i < pgt.size(); ++i)
                    {
                        unsigned ckv;
                        if(pgt[i].size() > 1)
                        {
                            ckv = ++ck;
                        }
                        else
                        {
                            ckv = 0;
                        }
                        for(size_t j = 0; j < pgt[i].size(); ++j)
                        {
                            for(size_t k = 0; k < pgt[i][j].size(); ++k)
                            {
                                tf << pgt[i][j][k] << ' ';
                            }
                            tf << ckv << std::endl;
                        }
                    }
                    ck = 0;
                    tf.close();
                    tf.open(rcf2,std::ifstream::out);
                    for(size_t i = 0; i < pgta.size(); ++i)
                    {
                        unsigned ckv;
                        if(pgta[i].size() > 1)
                        {
                            ckv = ++ck;
                        }
                        else
                        {
                            ckv = 0;
                        }
                        for(size_t j = 0; j < pgta[i].size(); ++j)
                        {
                            for(size_t k = 0; k < pgta[i][j].size(); ++k)
                            {
                                tf << pgta[i][j][k] << ' ';
                            }
                            tf << ckv << std::endl;
                        }
                    }
                    ck = 0;
                    tf.close();
                    tf.open(rcf3,std::ifstream::out);
                }
                for(size_t i = 0; i < pgtc.size(); ++i)
                {
                    unsigned ckv;
                    if(pgtc[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < pgtc[i].size(); ++j)
                    {
                        for(size_t k = 0; k < pgtc[i][j].size(); ++k)
                        {
                            tf << pgtc[i][j][k] << ' ';
                        }
                        tf << ckv << std::endl;
                    }
                }
                ck = 0;
                tf.close();
                tf.open((rncv)?rcf4:rcf2,std::ifstream::out);
                for(size_t i = 0; i < pgtd.size(); ++i)
                {
                    unsigned ckv;
                    if(pgtd[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < pgtd[i].size(); ++j)
                    {
                        for(size_t k = 0; k < pgtd[i][j].size(); ++k)
                        {
                            tf << pgtd[i][j][k] << ' ';
                        }
                        tf << ckv << std::endl;
                    }
                }
                ck = 0;
                tf.close();
                tf.open((rncv)?rcf5:rcf3,std::ifstream::out);
                for(size_t i = 0; i < pgtk.size(); ++i)
                {
                    unsigned ckv;
                    if(pgtk[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < pgtk[i].size(); ++j)
                    {
                        for(size_t k = 0; k < pgtk[i][j].size(); ++k)
                        {
                            tf << pgtk[i][j][k] << ' ';
                        }
                        tf << ckv << std::endl;
                    }
                }
                ck = 0;
                tf.close();
                tf.open((rncv)?rcf6:rcf4,std::ifstream::out);
                for(size_t i = 0; i < pgto.size(); ++i)
                {
                    unsigned ckv;
                    if(pgto[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < pgto[i].size(); ++j)
                    {
                        for(size_t k = 0; k < pgto[i][j].size(); ++k)
                        {
                            tf << pgto[i][j][k] << ' ';
                        }
                        tf << ckv << std::endl;
                    }
                }
            }
            if(afff)
            {
                unsigned ck = 0;
                std::ofstream tf(afcf1,std::ifstream::out);
                if(rncv)
                {
                    for(size_t i = 0; i < red.size(); ++i)
                    {
                        unsigned ckv;
                        if(red[i].size() > 1)
                        {
                            ckv = ++ck;
                        }
                        else
                        {
                            ckv = 0;
                        }
                        for(size_t j = 0; j < red[i].size(); ++j)
                        {
                            tf << red[i][j] << ' ' << ckv << std::endl;
                        }
                    }
                    ck = 0;
                    tf.close();
                    tf.open(afcf2,std::ifstream::out);
                    for(size_t i = 0; i < reda.size(); ++i)
                    {
                        unsigned ckv;
                        if(reda[i].size() > 1)
                        {
                            ckv = ++ck;
                        }
                        else
                        {
                            ckv = 0;
                        }
                        for(size_t j = 0; j < reda[i].size(); ++j)
                        {
                            tf << reda[i][j] << ' ' << ckv << std::endl;
                        }
                    }
                    ck = 0;
                    tf.close();
                    tf.open(afcf3,std::ifstream::out);
                }
                for(size_t i = 0; i < redc.size(); ++i)
                {
                    unsigned ckv;
                    if(redc[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < redc[i].size(); ++j)
                    {
                        tf << redc[i][j] << ' ' << ckv << std::endl;
                    }
                }
                ck = 0;
                tf.close();
                tf.open((rncv)?afcf4:afcf2,std::ifstream::out);
                for(size_t i = 0; i < redd.size(); ++i)
                {
                    unsigned ckv;
                    if(redd[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < redd[i].size(); ++j)
                    {
                        tf << redd[i][j] << ' ' << ckv << std::endl;
                    }
                }
                ck = 0;
                tf.close();
                tf.open((rncv)?afcf5:afcf3,std::ifstream::out);
                for(size_t i = 0; i < redk.size(); ++i)
                {
                    unsigned ckv;
                    if(redk[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < redk[i].size(); ++j)
                    {
                        tf << redk[i][j] << ' ' << ckv << std::endl;
                    }
                }
                ck = 0;
                tf.close();
                tf.open((rncv)?afcf6:afcf4,std::ifstream::out);
                for(size_t i = 0; i < redo.size(); ++i)
                {
                    unsigned ckv;
                    if(redo[i].size() > 1)
                    {
                        ckv = ++ck;
                    }
                    else
                    {
                        ckv = 0;
                    }
                    for(size_t j = 0; j < redo[i].size(); ++j)
                    {
                        tf << redo[i][j] << ' ' << ckv << std::endl;
                    }
                }
            }
        }
    }
    return EXIT_SUCCESS;
}
