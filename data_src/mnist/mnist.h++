#include <cstddef>
#include <cstdint>
#include <utility>
#include <string>
#include <array>
#include <vector>
#include <valarray>

#define DNN_MNIST_N 784
#define DNN_MNIST_C 10
#define DNN_MNIST_V 28


#ifndef DNN_MNIST
class MNIST
{
    std::ifstream img;
    
    std::ifstream val;
    
    size_t start;
    
    std::array<size_t,DNN_MNIST_C> perm;
    
    std::array<uint8_t,DNN_MNIST_V> mask;
    
    std::array<size_t,DNN_MNIST_N> ipr;
    
public:
    MNIST(void) = default;
    
    MNIST(const MNIST&) = delete;
    
    MNIST(const std::string&,const std::string&,size_t=0);
    
    MNIST(const std::string&,const std::string&,const std::array<size_t,DNN_MNIST_N>&,size_t=0);
    
    MNIST(const std::string&,const std::string&,const std::array<size_t,DNN_MNIST_C>&,size_t=0);
    
    MNIST(const std::string&,const std::string&,const std::array<size_t,DNN_MNIST_C>&,const std::array<size_t,DNN_MNIST_N>&,size_t=0);
    
    MNIST(const std::string&,const std::string&,const std::array<uint8_t,DNN_MNIST_V>&,size_t=0);
    
    MNIST(const std::string&,const std::string&,const std::array<uint8_t,DNN_MNIST_V>&,const std::array<size_t,DNN_MNIST_N>&,size_t=0);
    
    MNIST(const std::string&,const std::string&,const std::array<size_t,DNN_MNIST_C>&,const std::array<uint8_t,DNN_MNIST_V>&,size_t=0);
    
    MNIST(const std::string&,const std::string&,const std::array<size_t,DNN_MNIST_C>&,const std::array<uint8_t,DNN_MNIST_V>&,const std::array<size_t,DNN_MNIST_N>&,size_t=0);
    
    ~MNIST(void) = default;
    
    MNIST& operator=(const MNIST&) = delete;

    void init(const std::string&,const std::string&,size_t=0);

    void init(const std::string&,const std::string&,const std::array<size_t,DNN_MNIST_N>&,size_t=0);

    void init(const std::string&,const std::string&,const std::array<size_t,DNN_MNIST_C>&,size_t=0);

    void init(const std::string&,const std::string&,const std::array<size_t,DNN_MNIST_C>&,const std::array<size_t,DNN_MNIST_N>&,size_t=0);

    void init(const std::string&,const std::string&,const std::array<uint8_t,DNN_MNIST_V>&,size_t=0);

    void init(const std::string&,const std::string&,const std::array<uint8_t,DNN_MNIST_V>&,const std::array<size_t,DNN_MNIST_N>&,size_t=0);

    void init(const std::string&,const std::string&,const std::array<size_t,DNN_MNIST_C>&,const std::array<uint8_t,DNN_MNIST_V>&,size_t=0);

    void init(const std::string&,const std::string&,const std::array<size_t,DNN_MNIST_C>&,const std::array<uint8_t,DNN_MNIST_V>&,const std::array<size_t,DNN_MNIST_N>&,size_t=0);
    
    void swap(MNIST&);
    
    void setStart(size_t);

    void setPerm(const std::array<size_t,DNN_MNIST_C>&);

    void setMask(const std::array<uint8_t,DNN_MNIST_V>&);

    void setIPerm(const std::array<size_t,DNN_MNIST_N>&);

    std::array<size_t,DNN_MNIST_C> getPerm(void) const;

    std::array<uint8_t,DNN_MNIST_V> getMask(void) const;

    std::array<size_t,DNN_MNIST_N> getIPerm(void) const;

    void rewind(void);

    void last(void);

    void operator++(void);

    void operator--(void);

    void operator+=(size_t);

    void operator-=(size_t);

    void set(size_t);

    std::pair<std::array<uint8_t,DNN_MNIST_N>,uint8_t> peek(void);

    std::array<uint8_t,DNN_MNIST_N> peeki(void);

    uint8_t peekv(void);

    std::pair<std::valarray<double>,std::valarray<double>> peekp(void);

    std::valarray<double> peekip(void);

    std::valarray<double> peekvp(void);
    
    std::pair<std::array<uint8_t,DNN_MNIST_N>,uint8_t> get(void);

    std::array<uint8_t,DNN_MNIST_N> geti(void);

    uint8_t getv(void);

    std::pair<std::valarray<double>,std::valarray<double>> getp(void);

    std::valarray<double> getip(void);

    std::valarray<double> getvp(void);

    std::pair<std::array<uint8_t,DNN_MNIST_N>,uint8_t> operator[](size_t);

    std::array<uint8_t,DNN_MNIST_N> i(size_t);

    uint8_t v(size_t);

    std::pair<std::valarray<double>,std::valarray<double>> p(size_t);

    std::valarray<double> ip(size_t);

    std::valarray<double> vp(size_t);

    std::vector<std::pair<std::array<uint8_t,DNN_MNIST_N>,uint8_t>> operator[](const std::vector<size_t>&);

    std::vector<std::array<uint8_t,DNN_MNIST_N>> i(const std::vector<size_t>&);

    std::vector<uint8_t> v(const std::vector<size_t>&);

    std::vector<std::pair<std::valarray<double>,std::valarray<double>>> p(const std::vector<size_t>&);

    std::vector<std::valarray<double>> ip(const std::vector<size_t>&);

    std::vector<std::valarray<double>> vp(const std::vector<size_t>&);
};
#define DNN_MNIST
#endif
