#include <limits>
#include <fstream>
#include "sdr.h++"


SDR::SDR(size_t neur, unsigned act, unsigned step, unsigned noise, unsigned aa, unsigned bb)
{
    n = neur;
    m = act;
    s = step;
    z = noise;
    a = aa;
    b = bb;
}

void SDR::init(size_t neur, unsigned act, unsigned step, unsigned noise, unsigned aa, unsigned bb)
{
    n = neur;
    m = act;
    s = step;
    z = noise;
    a = aa;
    b = bb;
}

void SDR::setNeur(size_t neur)
{
    n = neur;
}

void SDR::setAct(unsigned act)
{
    m = act;
}

void SDR::setStep(unsigned step)
{
    s = step;
}

void SDR::setNoise(unsigned noise)
{
    z = noise;
}

std::vector<bool> SDR::operator[](size_t k)
{
    std::vector<bool> ans(n,false);
    if((k%b)==a)
    {
        for(unsigned i = 0; i < m; ++i)
        {
            ans[((k%s)*(3*m))+i] = true;
            ans[((k%s)*(3*m))+(2*m)+i] = true;
        }
    }
    else
    {
        for(unsigned i = 0; i < m; ++i)
        {
            ans[((k%s)*(3*m))+i] = true;
            ans[((k%s)*(3*m))+m+i] = true;
        }
    }
    for(unsigned i = 0; i < z; ++i)
    {
        ans[(((k/s))+i*(n/z))%n] = !ans[(((k/s))+i*(n/z))%n];
    }
    return ans;
}

std::vector<bool> SDR::i(size_t k)
{
    std::vector<bool> ans(n,false);
    for(unsigned i = 0; i < m; ++i)
    {
        ans[((k%s)*(3*m))+i] = true;
    }
    for(unsigned i = 0; i < z; ++i)
    {
        ans[(((k/s))+i*(n/z))%n] = !ans[(((k/s))+i*(n/z))%n];
    }
    return ans;
}

std::vector<bool> SDR::v(size_t k)
{
    std::vector<bool> ans(n,false);
    if((k%b)==a)
    {
        for(unsigned i = 0; i < m; ++i)
        {
            ans[((k%s)*(3*m))+(2*m)+i] = true;
        }
    }
    else
    {
        for(unsigned i = 0; i < m; ++i)
        {
            ans[((k%s)*(3*m))+m+i] = true;
        }
    }
    for(unsigned i = 0; i < z; ++i)
    {
        ans[(((k/s))+i*(n/z))%n] = !ans[(((k/s))+i*(n/z))%n];
    }
    return ans;
}

std::valarray<double> SDR::p(size_t k)
{
    std::valarray<double> ans(0.0,n);
    if((k%b)==a)
    {
        for(unsigned i = 0; i < m; ++i)
        {
            ans[((k%s)*(3*m))+i] = 1.0;
            ans[((k%s)*(3*m))+(2*m)+i] = 1.0;
        }
    }
    else
    {
        for(unsigned i = 0; i < m; ++i)
        {
            ans[((k%s)*(3*m))+i] = 1.0;
            ans[((k%s)*(3*m))+m+i] = 1.0;
        }
    }
    for(unsigned i = 0; i < z; ++i)
    {
        ans[(((k/s))+i*(n/z))%n] = 1.0-ans[(((k/s))+i*(n/z))%n];
    }
    return ans;
}

std::valarray<double> SDR::ip(size_t k)
{
    std::valarray<double> ans(0.0,n);
    for(unsigned i = 0; i < m; ++i)
    {
        ans[((k%s)*(3*m))+i] = 1.0;
    }
    for(unsigned i = 0; i < z; ++i)
    {
        ans[(((k/s))+i*(n/z))%n] = 1.0-ans[(((k/s))+i*(n/z))%n];
    }
    return ans;
}

std::valarray<double> SDR::vp(size_t k)
{
    std::valarray<double> ans(0.0,n);
    if((k%b)==a)
    {
        for(unsigned i = 0; i < m; ++i)
        {
            ans[((k%s)*(3*m))+(2*m)+i] = 1.0;
        }
    }
    else
    {
        for(unsigned i = 0; i < m; ++i)
        {
            ans[((k%s)*(3*m))+m+i] = 1.0;
        }
    }
    for(unsigned i = 0; i < z; ++i)
    {
        ans[(((k/s))+i*(n/z))%n] = 1.0-ans[(((k/s))+i*(n/z))%n];
    }
    return ans;
}

std::vector<std::vector<bool>> SDR::operator[](const std::vector<size_t>& k)
{
    std::vector<std::vector<bool>> ans(k.size());
    for(size_t j = 0; j < k.size(); ++j)
    {
        ans[j] = std::vector<bool>(n,false);
        if((k[j]%b)==a)
        {
            for(unsigned i = 0; i < m; ++i)
            {
                ans[j][((k[j]%s)*(3*m))+i] = true;
                ans[j][((k[j]%s)*(3*m))+(2*m)+i] = true;
            }
        }
        else
        {
            for(unsigned i = 0; i < m; ++i)
            {
                ans[j][((k[j]%s)*(3*m))+i] = true;
                ans[j][((k[j]%s)*(3*m))+m+i] = true;
            }
        }
        for(unsigned i = 0; i < z; ++i)
        {
            ans[j][(((k[j]/s))+i*(n/z))%n] = !ans[j][(((k[j]/s))+i*(n/z))%n];
        }
    }
    return ans;
}

std::vector<std::vector<bool>> SDR::i(const std::vector<size_t>& k)
{
    std::vector<std::vector<bool>> ans(k.size());
    for(size_t j = 0; j < k.size(); ++j)
    {
        ans[j] = std::vector<bool>(n,false);
        for(unsigned i = 0; i < m; ++i)
        {
            ans[j][((k[j]%s)*(3*m))+i] = true;
        }
        for(unsigned i = 0; i < z; ++i)
        {
            ans[j][(((k[j]/s))+i*(n/z))%n] = !ans[j][(((k[j]/s))+i*(n/z))%n];
        }
    }
    return ans;
}

std::vector<std::vector<bool>> SDR::v(const std::vector<size_t>& k)
{
    std::vector<std::vector<bool>> ans(k.size());
    for(size_t j = 0; j < k.size(); ++j)
    {
        ans[j] = std::vector<bool>(n,false);
        if((k[j]%b)==a)
        {
            for(unsigned i = 0; i < m; ++i)
            {
                ans[j][((k[j]%s)*(3*m))+(2*m)+i] = true;
            }
        }
        else
        {
            for(unsigned i = 0; i < m; ++i)
            {
                ans[j][((k[j]%s)*(3*m))+m+i] = true;
            }
        }
        for(unsigned i = 0; i < z; ++i)
        {
            ans[j][(((k[j]/s))+i*(n/z))%n] = !ans[j][(((k[j]/s))+i*(n/z))%n];
        }
    }
    return ans;
}

std::vector<std::valarray<double>> SDR::p(const std::vector<size_t>& k)
{
    std::vector<std::valarray<double>> ans(k.size());
    for(size_t j = 0; j < k.size(); ++j)
    {
        ans[j] = std::valarray<double>(0.0,n);
        if((k[j]%b)==a)
        {
            for(unsigned i = 0; i < m; ++i)
            {
                ans[j][((k[j]%s)*(3*m))+i] = 1.0;
                ans[j][((k[j]%s)*(3*m))+(2*m)+i] = 1.0;
            }
        }
        else
        {
            for(unsigned i = 0; i < m; ++i)
            {
                ans[j][((k[j]%s)*(3*m))+i] = 1.0;
                ans[j][((k[j]%s)*(3*m))+m+i] = 1.0;
            }
        }
        for(unsigned i = 0; i < z; ++i)
        {
            ans[j][(((k[j]/s))+i*(n/z))%n] = 1.0-ans[j][(((k[j]/s))+i*(n/z))%n];
        }
    }
    return ans;
}

std::vector<std::valarray<double>> SDR::ip(const std::vector<size_t>& k)
{
    std::vector<std::valarray<double>> ans(k.size());
    for(size_t j = 0; j < k.size(); ++j)
    {
        ans[j] = std::valarray<double>(0.0,n);
        for(unsigned i = 0; i < m; ++i)
        {
            ans[j][((k[j]%s)*(3*m))+i] = 1.0;
        }
        for(unsigned i = 0; i < z; ++i)
        {
            ans[j][(((k[j]/s))+i*(n/z))%n] = 1.0-ans[j][(((k[j]/s))+i*(n/z))%n];
        }
    }
    return ans;
}

std::vector<std::valarray<double>> SDR::vp(const std::vector<size_t>& k)
{
    std::vector<std::valarray<double>> ans(k.size());
    for(size_t j = 0; j < k.size(); ++j)
    {
        ans[j] = std::valarray<double>(0.0,n);
        if((k[j]%b)==a)
        {
            for(unsigned i = 0; i < m; ++i)
            {
                ans[j][((k[j]%s)*(3*m))+(2*m)+i] = 1.0;
            }
        }
        else
        {
            for(unsigned i = 0; i < m; ++i)
            {
                ans[j][((k[j]%s)*(3*m))+m+i] = 1.0;
            }
        }
        for(unsigned i = 0; i < z; ++i)
        {
            ans[j][(((k[j]/s))+i*(n/z))%n] = 1.0-ans[j][(((k[j]/s))+i*(n/z))%n];
        }
    }
    return ans;
}
