#include <cstddef>
#include <cstdint>
#include <vector>
#include <valarray>


#ifndef DNN_SDR
class SDR
{
    size_t n;
    
    unsigned m;
    
    unsigned s;
    
    unsigned z;
    
    unsigned a;
    
    unsigned b;
    
public:
    SDR(void) = default;
    
    SDR(const SDR&) = default;
    
    SDR(size_t,unsigned,unsigned,unsigned,unsigned=2,unsigned=1);
    
    ~SDR(void) = default;
    
    SDR& operator=(const SDR&) = default;

    void init(size_t,unsigned,unsigned,unsigned,unsigned=2,unsigned=1);
    
    void setNeur(size_t);
    
    void setAct(unsigned);
    
    void setStep(unsigned);
    
    void setNoise(unsigned);

    std::vector<bool> operator[](size_t);

    std::vector<bool> i(size_t);

    std::vector<bool> v(size_t);

    std::valarray<double> p(size_t);

    std::valarray<double> ip(size_t);

    std::valarray<double> vp(size_t);

    std::vector<std::vector<bool>> operator[](const std::vector<size_t>&);

    std::vector<std::vector<bool>> i(const std::vector<size_t>&);

    std::vector<std::vector<bool>> v(const std::vector<size_t>&);

    std::vector<std::valarray<double>> p(const std::vector<size_t>&);

    std::vector<std::valarray<double>> ip(const std::vector<size_t>&);

    std::vector<std::valarray<double>> vp(const std::vector<size_t>&);
};
#define DNN_SDR
#endif
