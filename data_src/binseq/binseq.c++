#include <limits>
#include <fstream>
#include "binseq.h++"


BinSeq::BinSeq(const std::vector<std::vector<std::vector<bool>>>& cls, const std::vector<std::vector<std::vector<size_t>>>& pos)
{
    sl = pos[0][0].size();
    for(size_t i = 0; i < cls.size(); ++i)
    {
        for(const auto& v: pos[i])
        {
            gen[v].insert(gen[v].end(),cls[i].begin(),cls[i].end());
        }
        for(const auto& v: cls[i])
        {
            trs[v] = i;
        }
    }
    vals.resize(sl);
    for(size_t i = 0; i < sl; ++i)
    {
        vals[i] = cls[pos[0][0][i]][0];
    }
}

void BinSeq::init(const std::vector<std::vector<std::vector<bool>>>& cls, const std::vector<std::vector<std::vector<size_t>>>& pos)
{
    sl = pos[0][0].size();
    trs.clear();
    gen.clear();
    vals.clear();
    for(size_t i = 0; i < cls.size(); ++i)
    {
        for(const auto& v: pos[i])
        {
            gen[v].insert(gen[v].end(),cls[i].begin(),cls[i].end());
        }
        for(const auto& v: cls[i])
        {
            trs[v] = i;
        }
    }
    vals.resize(sl);
    for(size_t i = 0; i < sl; ++i)
    {
        vals[i] = cls[pos[0][0][i]][0];
    }
}

std::vector<bool> BinSeq::operator[](size_t n)
{
    for(size_t i = vals.size(); i <= n; ++i)
    {
        std::vector<size_t> sq(sl);
        for(size_t j = 0; j < sl; ++j)
        {
            sq[j] = trs[vals[i-sl+j]];
        }
        vals.push_back(gen[sq][i%gen[sq].size()]);
    }
    return vals[n];
}

std::valarray<double> BinSeq::p(size_t n)
{
    for(size_t i = vals.size(); i <= n; ++i)
    {
        std::vector<size_t> sq(sl);
        for(size_t j = 0; j < sl; ++j)
        {
            sq[j] = trs[vals[i-sl+j]];
        }
        vals.push_back(gen[sq][i%gen[sq].size()]);
    }
    return vbtd(vals[n]);
}

std::vector<std::vector<bool>> BinSeq::s(size_t n, size_t l)
{
    std::vector<std::vector<bool>> ans(l);
    for(size_t i = vals.size(); i < n+l; ++i)
    {
        std::vector<size_t> sq(sl);
        for(size_t j = 0; j < sl; ++j)
        {
            sq[j] = trs[vals[i-sl+j]];
        }
        vals.push_back(gen[sq][i%gen[sq].size()]);
    }
    for(size_t i = 0; i < l; ++i)
    {
        ans[i] = vals[n+i];
    }
    return ans;
}

std::vector<std::valarray<double>> BinSeq::sp(size_t n, size_t l)
{
    std::vector<std::valarray<double>> ans(l);
    for(size_t i = vals.size(); i < n+l; ++i)
    {
        std::vector<size_t> sq(sl);
        for(size_t j = 0; j < sl; ++j)
        {
            sq[j] = trs[vals[i-sl+j]];
        }
        vals.push_back(gen[sq][i%gen[sq].size()]);
    }
    for(size_t i = 0; i < l; ++i)
    {
        ans[i] = vbtd(vals[n+i]);
    }
    return ans;
}

std::vector<std::vector<bool>> BinSeq::operator[](const std::vector<size_t>& n)
{
    std::vector<std::vector<bool>> ans(n.size());
    for(size_t i = vals.size(); i <= *std::max_element(n.begin(),n.end()); ++i)
    {
        std::vector<size_t> sq(sl);
        for(size_t j = 0; j < sl; ++j)
        {
            sq[j] = trs[vals[i-sl+j]];
        }
        vals.push_back(gen[sq][i%gen[sq].size()]);
    }
    for(size_t i = 0; i < n.size(); ++i)
    {
        ans[i] = vals[n[i]];
    }
    return ans;
}

std::vector<std::valarray<double>> BinSeq::p(const std::vector<size_t>& n)
{
    std::vector<std::valarray<double>> ans(n.size());
    for(size_t i = vals.size(); i <= *std::max_element(n.begin(),n.end()); ++i)
    {
        std::vector<size_t> sq(sl);
        for(size_t j = 0; j < sl; ++j)
        {
            sq[j] = trs[vals[i-sl+j]];
        }
        vals.push_back(gen[sq][i%gen[sq].size()]);
    }
    for(size_t i = 0; i < n.size(); ++i)
    {
        ans[i] = vbtd(vals[n[i]]);
    }
    return ans;
}

std::vector<std::vector<std::vector<bool>>> BinSeq::s(const std::vector<size_t>& n, size_t l)
{
    std::vector<std::vector<std::vector<bool>>> ans(n.size());
    for(size_t i = vals.size(); i <= *std::max_element(n.begin(),n.end())+l; ++i)
    {
        std::vector<size_t> sq(sl);
        for(size_t j = 0; j < sl; ++j)
        {
            sq[j] = trs[vals[i-sl+j]];
        }
        vals.push_back(gen[sq][i%gen[sq].size()]);
    }
    for(size_t i = 0; i < n.size(); ++i)
    {
        ans[i].resize(l);
        for(size_t j = 0; j < l; ++j)
        {
            ans[i][j] = vals[n[i]+j];
        }
    }
    return ans;
}

std::vector<std::vector<std::valarray<double>>> BinSeq::sp(const std::vector<size_t>& n, size_t l)
{
    std::vector<std::vector<std::valarray<double>>> ans(n.size());
    for(size_t i = vals.size(); i <= *std::max_element(n.begin(),n.end())+l; ++i)
    {
        std::vector<size_t> sq(sl);
        for(size_t j = 0; j < sl; ++j)
        {
            sq[j] = trs[vals[i-sl+j]];
        }
        vals.push_back(gen[sq][i%gen[sq].size()]);
    }
    for(size_t i = 0; i < n.size(); ++i)
    {
        ans[i].resize(l);
        for(size_t j = 0; j < l; ++j)
        {
            ans[i][j] = vbtd(vals[n[i]+j]);
        }
    }
    return ans;
}
