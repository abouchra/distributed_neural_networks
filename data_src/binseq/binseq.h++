#include <cstddef>
#include <cstdint>
#include <array>
#include <string>
#include <vector>
#include <valarray>
#include <unordered_map>
#include "../../helpers/helpers.h++"


#ifndef DNN_BINSEQ
class BinSeq
{
    std::vector<std::vector<bool>> vals;
    
    std::unordered_map<std::vector<size_t>,std::vector<std::vector<bool>>> gen;
    
    std::unordered_map<std::vector<bool>,size_t> trs;
    
    size_t sl;
    
public:
    BinSeq(void) = default;
    
    BinSeq(const BinSeq&) = default;
    
    BinSeq(const std::vector<std::vector<std::vector<bool>>>&, const std::vector<std::vector<std::vector<size_t>>>&);
    
    ~BinSeq(void) = default;
    
    BinSeq& operator=(const BinSeq&) = default;

    void init(const std::vector<std::vector<std::vector<bool>>>&, const std::vector<std::vector<std::vector<size_t>>>&);

    std::vector<bool> operator[](size_t);

    std::valarray<double> p(size_t);

    std::vector<std::vector<bool>> s(size_t,size_t);

    std::vector<std::valarray<double>> sp(size_t,size_t);

    std::vector<std::vector<bool>> operator[](const std::vector<size_t>&);

    std::vector<std::valarray<double>> p(const std::vector<size_t>&);

    std::vector<std::vector<std::vector<bool>>> s(const std::vector<size_t>&,size_t);

    std::vector<std::vector<std::valarray<double>>> sp(const std::vector<size_t>&,size_t);
};
#define DNN_BINSEQ
#endif
