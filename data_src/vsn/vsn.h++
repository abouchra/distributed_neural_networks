#include <cstddef>
#include <utility>
#include <string>
#include <array>
#include <vector>
#include <valarray>

#define DNN_VSN_N 100


#ifndef DNN_VSN
class VSN
{
    std::ifstream img;
    
    std::ifstream val;
    
public:
    VSN(void) = default;
    
    VSN(const VSN&) = delete;
    
    VSN(const std::string&,const std::string&,unsigned=0);
    
    ~VSN(void) = default;
    
    VSN& operator=(const VSN&) = delete;

    void init(const std::string&,const std::string&,unsigned=0);
    
    void swap(VSN&);

    void rewind(void);

    void last(void);

    void operator++(void);

    void operator--(void);

    void operator+=(size_t);

    void operator-=(size_t);

    void set(size_t);

    std::pair<std::array<double,DNN_VSN_N>,bool> peek(void);

    std::array<double,DNN_VSN_N> peeki(void);

    bool peekv(void);

    std::pair<std::valarray<double>,std::valarray<double>> peekp(void);

    std::valarray<double> peekip(void);

    std::valarray<double> peekvp(void);
    
    std::pair<std::array<double,DNN_VSN_N>,bool> get(void);

    std::array<double,DNN_VSN_N> geti(void);

    bool getv(void);

    std::pair<std::valarray<double>,std::valarray<double>> getp(void);

    std::valarray<double> getip(void);

    std::valarray<double> getvp(void);

    std::pair<std::array<double,DNN_VSN_N>,bool> operator[](size_t);

    std::array<double,DNN_VSN_N> i(size_t);

    bool v(size_t);

    std::pair<std::valarray<double>,std::valarray<double>> p(size_t);

    std::valarray<double> ip(size_t);

    std::valarray<double> vp(size_t);

    std::vector<std::pair<std::array<double,DNN_VSN_N>,bool>> operator[](const std::vector<size_t>&);

    std::vector<std::array<double,DNN_VSN_N>> i(const std::vector<size_t>&);

    std::vector<bool> v(const std::vector<size_t>&);

    std::vector<std::pair<std::valarray<double>,std::valarray<double>>> p(const std::vector<size_t>&);

    std::vector<std::valarray<double>> ip(const std::vector<size_t>&);

    std::vector<std::valarray<double>> vp(const std::vector<size_t>&);
};
#define DNN_VSN
#endif
