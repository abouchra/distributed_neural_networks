#include <cmath>
#include <fstream>
#include <algorithm>
#include "vsn.h++"


VSN::VSN(const std::string& im, const std::string& vl, unsigned nm)
{
    char imgf[im.size()+((size_t)floor(log10(nm+1)))+3], valf[vl.size()+((size_t)floor(log10(nm+1)))+3];
    sprintf(imgf,im.c_str(),nm);
    sprintf(valf,vl.c_str(),nm);
    img = std::ifstream(imgf,std::ifstream::in);
    val = std::ifstream(valf,std::ifstream::in);
    img.seekg(0);
    val.seekg(0);
}

void VSN::init(const std::string& im, const std::string& vl, unsigned nm)
{
    char imgf[im.size()+((size_t)floor(log10(nm+1)))+3], valf[vl.size()+((size_t)floor(log10(nm+1)))+3];
    sprintf(imgf,im.c_str(),nm);
    sprintf(valf,vl.c_str(),nm);
    img = std::ifstream(imgf,std::ifstream::in);
    val = std::ifstream(valf,std::ifstream::in);
    img.seekg(0);
    val.seekg(0);
}

void VSN::swap(VSN& m)
{
    img.swap(m.img);
    val.swap(m.val);
}

void VSN::rewind(void)
{
    img.seekg(0);
    val.seekg(0);
}

void VSN::last(void)
{
    img.seekg(-2,img.end);
    val.seekg(-2,val.end);
    while(img.peek() != '\n')
    {
        img.seekg(-1,img.cur);
    }
    while(val.peek() != '\n')
    {
        val.seekg(-1,val.cur);
    }
    img.ignore(1);
    val.ignore(1);
}

void VSN::operator++(void)
{
    img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
}

void VSN::operator--(void)
{
    do
    {
        img.seekg(-1,img.cur);
    }
    while(img.peek() != '\n');
    do
    {
        val.seekg(-1,val.cur);
    }
    while(val.peek() != '\n');
    while(img.peek() != '\n')
    {
        img.seekg(-1,img.cur);
    }
    while(val.peek() != '\n')
    {
        val.seekg(-1,val.cur);
    }
    img.ignore(1);
    val.ignore(1);
}

void VSN::operator+=(size_t n)
{
    for(size_t i = 0; i < n; ++i)
    {
        img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
}

void VSN::operator-=(size_t n)
{
    img.seekg(-1,img.cur);
    val.seekg(-1,val.cur);
    for(size_t i = 0; i <= n; ++i)
    {
        while(img.peek() != '\n')
        {
            img.seekg(-1,img.cur);
        }
        while(val.peek() != '\n')
        {
            val.seekg(-1,val.cur);
        }
    }
    img.ignore(1);
    val.ignore(1);
}

void VSN::set(size_t n)
{
    img.seekg(0);
    val.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
}

std::pair<std::array<double,DNN_VSN_N>,bool> VSN::peek(void)
{
    std::pair<std::array<double,DNN_VSN_N>,bool> ans;
    size_t mi = img.tellg(), mv = val.tellg();
    do
    {
        img.seekg(-1,img.cur);
    }
    while(img.peek() != '\n');
    do
    {
        val.seekg(-1,val.cur);
    }
    while(val.peek() != '\n');
    img.ignore(1);
    val.ignore(1);
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans.first[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    val >> ans.second;
    img.seekg(mi);
    val.seekg(mv);
    return ans;
}

std::array<double,DNN_VSN_N> VSN::peeki(void)
{
    std::array<double,DNN_VSN_N> ans;
    size_t mi = img.tellg();
    do
    {
        img.seekg(-1,img.cur);
    }
    while(img.peek() != '\n');
    img.ignore(1);
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    img.seekg(mi);
    return ans;
}

bool VSN::peekv(void)
{
    bool ans;
    size_t mv = val.tellg();
    do
    {
        val.seekg(-1,val.cur);
    }
    while(val.peek() != '\n');
    val.ignore(1);
    val >> ans;
    val.seekg(mv);
    return ans;
}

std::pair<std::valarray<double>,std::valarray<double>> VSN::peekp(void)
{
    std::pair<std::valarray<double>,std::valarray<double>> ans;
    ans.first.resize(DNN_VSN_N);
    ans.second = std::valarray<double>(1);
    size_t mi = img.tellg(), mv = val.tellg();
    do
    {
        img.seekg(-1,img.cur);
    }
    while(img.peek() != '\n');
    do
    {
        val.seekg(-1,val.cur);
    }
    while(val.peek() != '\n');
    img.ignore(1);
    val.ignore(1);
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans.first[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    val >> ans.second[0];
    img.seekg(mi);
    val.seekg(mv);
    return ans;
}

std::valarray<double> VSN::peekip(void)
{
    std::valarray<double> ans(DNN_VSN_N);
    size_t mi = img.tellg();
    do
    {
        img.seekg(-1,img.cur);
    }
    while(img.peek() != '\n');
    img.ignore(1);
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    img.seekg(mi);
    return ans;
}

std::valarray<double> VSN::peekvp(void)
{
    std::valarray<double> ans(1);
    size_t mv = val.tellg();
    do
    {
        val.seekg(-1,val.cur);
    }
    while(val.peek() != '\n');
    val.ignore(1);
    val >> ans[0];
    val.seekg(mv);
    return ans;
}

std::pair<std::array<double,DNN_VSN_N>,bool> VSN::get(void)
{
    std::pair<std::array<double,DNN_VSN_N>,bool> ans;
    do
    {
        img.seekg(-1,img.cur);
    }
    while(img.peek() != '\n');
    do
    {
        val.seekg(-1,val.cur);
    }
    while(val.peek() != '\n');
    img.ignore(1);
    val.ignore(1);
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans.first[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    val >> ans.second;
    img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    return ans;
}

std::array<double,DNN_VSN_N> VSN::geti(void)
{
    std::array<double,DNN_VSN_N> ans;
    do
    {
        img.seekg(-1,img.cur);
    }
    while(img.peek() != '\n');
    img.ignore(1);
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    return ans;
}

bool VSN::getv(void)
{
    bool ans;
    do
    {
        val.seekg(-1,val.cur);
    }
    while(val.peek() != '\n');
    val.ignore(1);
    val >> ans;
    val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    return ans;
}

std::pair<std::valarray<double>,std::valarray<double>> VSN::getp(void)
{
    std::pair<std::valarray<double>,std::valarray<double>> ans;
    ans.first.resize(DNN_VSN_N);
    ans.second = std::valarray<double>(1);
    do
    {
        img.seekg(-1,img.cur);
    }
    while(img.peek() != '\n');
    do
    {
        val.seekg(-1,val.cur);
    }
    while(val.peek() != '\n');
    img.ignore(1);
    val.ignore(1);
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans.first[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    val >> ans.second[0];
    img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    return ans;
}

std::valarray<double> VSN::getip(void)
{
    std::valarray<double> ans(DNN_VSN_N);
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        ans[i] = ((double)((uint8_t)img.get()))/255;
    }
    return ans;
}

std::valarray<double> VSN::getvp(void)
{
    std::valarray<double> ans(0.0,1);
    ans[val.get()] = 1.0;
    return ans;
}

std::pair<std::array<double,DNN_VSN_N>,bool> VSN::operator[](size_t n)
{
    std::pair<std::array<double,DNN_VSN_N>,bool> ans;
    size_t mi = img.tellg(), mv = val.tellg();
    img.seekg(0);
    val.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans.first[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    val >> ans.second;
    img.seekg(mi);
    val.seekg(mv);
    return ans;
}

std::array<double,DNN_VSN_N> VSN::i(size_t n)
{
    std::array<double,DNN_VSN_N> ans;
    size_t mi = img.tellg();
    img.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    img.seekg(mi);
    return ans;
}

bool VSN::v(size_t n)
{
    bool ans;
    size_t mv = val.tellg();
    val.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    val >> ans;
    val.seekg(mv);
    return ans;
}

std::pair<std::valarray<double>,std::valarray<double>> VSN::p(size_t n)
{
    std::pair<std::valarray<double>,std::valarray<double>> ans;
    ans.first.resize(DNN_VSN_N);
    ans.second = std::valarray<double>(1);
    size_t mi = img.tellg(), mv = val.tellg();
    img.seekg(0);
    val.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans.first[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    val >> ans.second[0];
    img.seekg(mi);
    val.seekg(mv);
    return ans;
}

std::valarray<double> VSN::ip(size_t n)
{
    std::valarray<double> ans(DNN_VSN_N);
    size_t mi = img.tellg();
    img.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    for(size_t i = 0; i < DNN_VSN_N; ++i)
    {
        img >> ans[i];
        img.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    img.seekg(mi);
    return ans;
}

std::valarray<double> VSN::vp(size_t n)
{
    std::valarray<double> ans(1);
    size_t mv = val.tellg();
    val.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    val >> ans[0];
    val.seekg(mv);
    return ans;
}

std::vector<std::pair<std::array<double,DNN_VSN_N>,bool>> VSN::operator[](const std::vector<size_t>& n)
{
    std::vector<std::pair<std::array<double,DNN_VSN_N>,bool>> ans(n.size());
    size_t mi = img.tellg(), mv = val.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        img.seekg(0);
        val.seekg(0);
        for(size_t i = 0; i < n[k]; ++i)
        {
            img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        for(size_t i = 0; i < DNN_VSN_N; ++i)
        {
            img >> ans[k].first[i];
            img.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
        val >> ans[k].second;
    }
    img.seekg(mi);
    val.seekg(mv);
    return ans;
}

std::vector<std::array<double,DNN_VSN_N>> VSN::i(const std::vector<size_t>& n)
{
    std::vector<std::array<double,DNN_VSN_N>> ans(n.size());
    size_t mi = img.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        img.seekg(0);
        for(size_t i = 0; i < n[k]; ++i)
        {
            img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        for(size_t i = 0; i < DNN_VSN_N; ++i)
        {
            img >> ans[k][i];
            img.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
    }
    img.seekg(mi);
    return ans;
}

std::vector<bool> VSN::v(const std::vector<size_t>& n)
{
    std::vector<bool> ans(n.size());
    size_t mv = val.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        val.seekg(0);
        for(size_t i = 0; i < n[k]; ++i)
        {
            val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        bool tb;
        val >> tb;
        ans[k] = tb;
    }
    val.seekg(mv);
    return ans;
}

std::vector<std::pair<std::valarray<double>,std::valarray<double>>> VSN::p(const std::vector<size_t>& n)
{
    std::vector<std::pair<std::valarray<double>,std::valarray<double>>> ans(n.size());
    size_t mi = img.tellg(), mv = val.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        ans[k].first.resize(DNN_VSN_N);
        ans[k].second = std::valarray<double>(1);
        img.seekg(0);
        val.seekg(0);
        for(size_t i = 0; i < n[k]; ++i)
        {
            img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        for(size_t i = 0; i < DNN_VSN_N; ++i)
        {
            img >> ans[k].first[i];
            img.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
        val >> ans[k].second[0];
    }
    img.seekg(mi);
    val.seekg(mv);
    return ans;
}

std::vector<std::valarray<double>> VSN::ip(const std::vector<size_t>& n)
{
    std::vector<std::valarray<double>> ans(n.size());
    size_t mi = img.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        ans[k].resize(DNN_VSN_N);
        img.seekg(0);
        for(size_t i = 0; i < n[k]; ++i)
        {
            img.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        for(size_t i = 0; i < DNN_VSN_N; ++i)
        {
            img >> ans[k][i];
            img.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
    }
    img.seekg(mi);
    return ans;
}

std::vector<std::valarray<double>> VSN::vp(const std::vector<size_t>& n)
{
    std::vector<std::valarray<double>> ans(n.size());
    size_t mv = val.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        ans[k] = std::valarray<double>(1);
        val.seekg(0);
        for(size_t i = 0; i < n[k]; ++i)
        {
            val.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        val >> ans[k][0];
    }
    val.seekg(mv);
    return ans;
}
