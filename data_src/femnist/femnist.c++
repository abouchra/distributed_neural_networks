#include <cmath>
#include <fstream>
#include <algorithm>
#include "femnist.h++"


FEMNIST::FEMNIST(const std::string& im, const std::string& vl, const std::string& tp, unsigned nm)
{
    char imgf[im.size()+(2*tp.size())+((size_t)floor(log10(nm+1)))+3], valf[vl.size()+(2*tp.size())+((size_t)floor(log10(nm+1)))+3];
    sprintf(imgf,im.c_str(),tp.c_str(),tp.c_str(),nm);
    sprintf(valf,vl.c_str(),tp.c_str(),tp.c_str(),nm);
    img = std::ifstream(imgf,std::ifstream::in|std::ifstream::binary);
    val = std::ifstream(valf,std::ifstream::in|std::ifstream::binary);
    outs = 0;
    for(auto c: tp)
    {
        switch(c)
        {
            case 'd':
                outs+= DNN_FEMNIST_D;
                break;
            case 'l':
                outs+= DNN_FEMNIST_L;
                break;
            case 'u':
                outs+= DNN_FEMNIST_U;
                break;
            case 'm':
                outs+= DNN_FEMNIST_M;
                break;
        }
    }
    img.seekg(0);
    val.seekg(0);
}

void FEMNIST::init(const std::string& im, const std::string& vl, const std::string& tp, unsigned nm)
{
    char imgf[im.size()+(2*tp.size())+((size_t)floor(log10(nm+1)))+3], valf[vl.size()+(2*tp.size())+((size_t)floor(log10(nm+1)))+3];
    sprintf(imgf,im.c_str(),tp.c_str(),tp.c_str(),nm);
    sprintf(valf,vl.c_str(),tp.c_str(),tp.c_str(),nm);
    img = std::ifstream(imgf,std::ifstream::in|std::ifstream::binary);
    val = std::ifstream(valf,std::ifstream::in|std::ifstream::binary);
    outs = 0;
    for(auto c: tp)
    {
        switch(c)
        {
            case 'd':
                outs+= DNN_FEMNIST_D;
                break;
            case 'l':
                outs+= DNN_FEMNIST_L;
                break;
            case 'u':
                outs+= DNN_FEMNIST_U;
                break;
            case 'm':
                outs+= DNN_FEMNIST_M;
                break;
        }
    }
    img.seekg(0);
    val.seekg(0);
}

void FEMNIST::swap(FEMNIST& m)
{
    size_t os = outs;
    img.swap(m.img);
    val.swap(m.val);
    outs = m.outs;
    m.outs = os;
}

size_t FEMNIST::getOutSize(void) const
{
    return outs;
}

void FEMNIST::rewind(void)
{
    img.seekg(0);
    val.seekg(0);
}

void FEMNIST::last(void)
{
    img.seekg(-DNN_FEMNIST_N,img.end);
    val.seekg(-1,val.end);
}

void FEMNIST::operator++(void)
{
    img.seekg(DNN_FEMNIST_N,img.cur);
    val.seekg(1,val.cur);
}

void FEMNIST::operator--(void)
{
    img.seekg(-DNN_FEMNIST_N,img.cur);
    val.seekg(-1,val.cur);
}

void FEMNIST::operator+=(size_t n)
{
    img.seekg(n*DNN_FEMNIST_N,img.cur);
    val.seekg(n,val.cur);
}

void FEMNIST::operator-=(size_t n)
{
    img.seekg(-n*DNN_FEMNIST_N,img.cur);
    val.seekg(-n,val.cur);
}

void FEMNIST::set(size_t n)
{
    img.seekg(n*DNN_FEMNIST_N);
    val.seekg(n);
}

std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t> FEMNIST::peek(void)
{
    std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t> ans;
    size_t mi = img.tellg();
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans.first[i] = ((uint8_t)img.get());
    }
    ans.second = val.peek();
    img.seekg(mi);
    return ans;
}

std::array<uint8_t,DNN_FEMNIST_N> FEMNIST::peeki(void)
{
    std::array<uint8_t,DNN_FEMNIST_N> ans;
    size_t mi = img.tellg();
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans[i] = ((uint8_t)img.get());
    }
    img.seekg(mi);
    return ans;
}

uint8_t FEMNIST::peekv(void)
{
    uint8_t ans;
    ans = val.peek();
    return ans;
}

std::pair<std::valarray<double>,std::valarray<double>> FEMNIST::peekp(void)
{
    std::pair<std::valarray<double>,std::valarray<double>> ans;
    size_t mi = img.tellg();
    ans.first.resize(DNN_FEMNIST_N);
    ans.second = std::valarray<double>(0.0,outs);
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans.first[i] = ((double)((uint8_t)img.get()))/255;
    }
    ans.second[val.peek()] = 1.0;
    img.seekg(mi);
    return ans;
}

std::valarray<double> FEMNIST::peekip(void)
{
    std::valarray<double> ans(DNN_FEMNIST_N);
    size_t mi = img.tellg();
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans[i] = ((double)((uint8_t)img.get()))/255;
    }
    img.seekg(mi);
    return ans;
}

std::valarray<double> FEMNIST::peekvp(void)
{
    std::valarray<double> ans(0.0,outs);
    ans[val.peek()] = 1.0;
    return ans;
}

std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t> FEMNIST::get(void)
{
    std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t> ans;
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans.first[i] = ((uint8_t)img.get());
    }
    ans.second = val.get();
    return ans;
}

std::array<uint8_t,DNN_FEMNIST_N> FEMNIST::geti(void)
{
    std::array<uint8_t,DNN_FEMNIST_N> ans;
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans[i] = ((uint8_t)img.get());
    }
    return ans;
}

uint8_t FEMNIST::getv(void)
{
    uint8_t ans;
    ans = val.get();
    return ans;
}

std::pair<std::valarray<double>,std::valarray<double>> FEMNIST::getp(void)
{
    std::pair<std::valarray<double>,std::valarray<double>> ans;
    ans.first.resize(DNN_FEMNIST_N);
    ans.second = std::valarray<double>(0.0,outs);
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans.first[i] = ((double)((uint8_t)img.get()))/255;
    }
    ans.second[val.get()] = 1.0;
    return ans;
}

std::valarray<double> FEMNIST::getip(void)
{
    std::valarray<double> ans(DNN_FEMNIST_N);
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans[i] = ((double)((uint8_t)img.get()))/255;
    }
    return ans;
}

std::valarray<double> FEMNIST::getvp(void)
{
    std::valarray<double> ans(0.0,outs);
    ans[val.get()] = 1.0;
    return ans;
}

std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t> FEMNIST::operator[](size_t n)
{
    std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t> ans;
    size_t mi = img.tellg(), mv = val.tellg();
    img.seekg(n*DNN_FEMNIST_N);
    val.seekg(n);
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans.first[i] = ((uint8_t)img.get());
    }
    ans.second = val.peek();
    img.seekg(mi);
    val.seekg(mv);
    return ans;
}

std::array<uint8_t,DNN_FEMNIST_N> FEMNIST::i(size_t n)
{
    std::array<uint8_t,DNN_FEMNIST_N> ans;
    size_t mi = img.tellg();
    img.seekg(n*DNN_FEMNIST_N);
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans[i] = ((uint8_t)img.get());
    }
    img.seekg(mi);
    return ans;
}

uint8_t FEMNIST::v(size_t n)
{
    uint8_t ans;
    size_t mv = val.tellg();
    val.seekg(n);
    ans = val.peek();
    val.seekg(mv);
    return ans;
}

std::pair<std::valarray<double>,std::valarray<double>> FEMNIST::p(size_t n)
{
    std::pair<std::valarray<double>,std::valarray<double>> ans;
    size_t mi = img.tellg(), mv = val.tellg();
    ans.first.resize(DNN_FEMNIST_N);
    ans.second = std::valarray<double>(0.0,outs);
    img.seekg(n*DNN_FEMNIST_N);
    val.seekg(n);
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans.first[i] = ((double)((uint8_t)img.get()))/255;
    }
    ans.second[val.peek()] = 1.0;
    img.seekg(mi);
    val.seekg(mv);
    return ans;
}

std::valarray<double> FEMNIST::ip(size_t n)
{
    std::valarray<double> ans(DNN_FEMNIST_N);
    size_t mi = img.tellg();
    img.seekg(n*DNN_FEMNIST_N);
    for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
    {
        ans[i] = ((double)((uint8_t)img.get()))/255;
    }
    img.seekg(mi);
    return ans;
}

std::valarray<double> FEMNIST::vp(size_t n)
{
    std::valarray<double> ans(0.0,outs);
    size_t mv = val.tellg();
    val.seekg(n);
    ans[val.peek()] = 1.0;
    val.seekg(mv);
    return ans;
}

std::vector<std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t>> FEMNIST::operator[](const std::vector<size_t>& n)
{
    std::vector<std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t>> ans(n.size());
    size_t mi = img.tellg(), mv = val.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        img.seekg(n[k]*DNN_FEMNIST_N);
        val.seekg(n[k]);
        for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
        {
            ans[k].first[i] = ((uint8_t)img.get());
        }
        ans[k].second = val.peek();
    }
    img.seekg(mi);
    val.seekg(mv);
    return ans;
}

std::vector<std::array<uint8_t,DNN_FEMNIST_N>> FEMNIST::i(const std::vector<size_t>& n)
{
    std::vector<std::array<uint8_t,DNN_FEMNIST_N>> ans(n.size());
    size_t mi = img.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        img.seekg(n[k]*DNN_FEMNIST_N);
        for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
        {
            ans[k][i] = ((uint8_t)img.get());
        }
    }
    img.seekg(mi);
    return ans;
}

std::vector<uint8_t> FEMNIST::v(const std::vector<size_t>& n)
{
    std::vector<uint8_t> ans(n.size());
    size_t mv = val.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        val.seekg(n[k]);
        ans[k] = val.peek();
    }
    val.seekg(mv);
    return ans;
}

std::vector<std::pair<std::valarray<double>,std::valarray<double>>> FEMNIST::p(const std::vector<size_t>& n)
{
    std::vector<std::pair<std::valarray<double>,std::valarray<double>>> ans(n.size());
    size_t mi = img.tellg(), mv = val.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        ans[k].first.resize(DNN_FEMNIST_N);
        ans[k].second = std::valarray<double>(0.0,outs);
        img.seekg(n[k]*DNN_FEMNIST_N);
        val.seekg(n[k]);
        for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
        {
            ans[k].first[i] = ((double)((uint8_t)img.get()))/255;
        }
        ans[k].second[val.peek()] = 1.0;
    }
    img.seekg(mi);
    val.seekg(mv);
    return ans;
}

std::vector<std::valarray<double>> FEMNIST::ip(const std::vector<size_t>& n)
{
    std::vector<std::valarray<double>> ans(n.size());
    size_t mi = img.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        ans[k].resize(DNN_FEMNIST_N);
        img.seekg(n[k]*DNN_FEMNIST_N);
        for(size_t i = 0; i < DNN_FEMNIST_N; ++i)
        {
            ans[k][i] = ((double)((uint8_t)img.get()))/255;
        }
    }
    img.seekg(mi);
    return ans;
}

std::vector<std::valarray<double>> FEMNIST::vp(const std::vector<size_t>& n)
{
    std::vector<std::valarray<double>> ans(n.size());
    size_t mv = val.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        ans[k] = std::valarray<double>(0.0,outs);
        val.seekg(n[k]);
        ans[k][val.peek()] = 1.0;
    }
    val.seekg(mv);
    return ans;
}
