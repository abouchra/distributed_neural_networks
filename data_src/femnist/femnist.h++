#include <cstddef>
#include <cstdint>
#include <utility>
#include <string>
#include <array>
#include <vector>
#include <valarray>

#define DNN_FEMNIST_N 784
#define DNN_FEMNIST_D 10
#define DNN_FEMNIST_L 26
#define DNN_FEMNIST_U 26
#define DNN_FEMNIST_M -15


#ifndef DNN_FEMNIST
class FEMNIST
{
    std::ifstream img;
    
    std::ifstream val;
    
    size_t outs;
    
public:
    FEMNIST(void) = default;
    
    FEMNIST(const FEMNIST&) = delete;
    
    FEMNIST(const std::string&,const std::string&,const std::string&,unsigned=0);
    
    ~FEMNIST(void) = default;
    
    FEMNIST& operator=(const FEMNIST&) = delete;

    void init(const std::string&,const std::string&,const std::string&,unsigned=0);
    
    void swap(FEMNIST&);
    
    size_t getOutSize(void) const;

    void rewind(void);

    void last(void);

    void operator++(void);

    void operator--(void);

    void operator+=(size_t);

    void operator-=(size_t);

    void set(size_t);

    std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t> peek(void);

    std::array<uint8_t,DNN_FEMNIST_N> peeki(void);

    uint8_t peekv(void);

    std::pair<std::valarray<double>,std::valarray<double>> peekp(void);

    std::valarray<double> peekip(void);

    std::valarray<double> peekvp(void);
    
    std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t> get(void);

    std::array<uint8_t,DNN_FEMNIST_N> geti(void);

    uint8_t getv(void);

    std::pair<std::valarray<double>,std::valarray<double>> getp(void);

    std::valarray<double> getip(void);

    std::valarray<double> getvp(void);

    std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t> operator[](size_t);

    std::array<uint8_t,DNN_FEMNIST_N> i(size_t);

    uint8_t v(size_t);

    std::pair<std::valarray<double>,std::valarray<double>> p(size_t);

    std::valarray<double> ip(size_t);

    std::valarray<double> vp(size_t);

    std::vector<std::pair<std::array<uint8_t,DNN_FEMNIST_N>,uint8_t>> operator[](const std::vector<size_t>&);

    std::vector<std::array<uint8_t,DNN_FEMNIST_N>> i(const std::vector<size_t>&);

    std::vector<uint8_t> v(const std::vector<size_t>&);

    std::vector<std::pair<std::valarray<double>,std::valarray<double>>> p(const std::vector<size_t>&);

    std::vector<std::valarray<double>> ip(const std::vector<size_t>&);

    std::vector<std::valarray<double>> vp(const std::vector<size_t>&);
};
#define DNN_FEMNIST
#endif
