#include <limits>
#include <fstream>
#include "metro.h++"


Metro::Metro(const std::string& t, size_t l, size_t st)
{
    tab = std::ifstream(t,std::ifstream::in);
    line = l;
    start = st;
    for(size_t i = 0; i < st; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
}

void Metro::init(const std::string& t, size_t l, size_t st)
{
    tab = std::ifstream(t,std::ifstream::in);
    line = l;
    start = st;
    for(size_t i = 0; i < st; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
}

void Metro::swap(Metro& m)
{
    size_t tl = line, ts = start;
    tab.swap(m.tab);
    line = m.line;
    start = m.start;
    m.line = tl;
    m.start = ts;
}

void Metro::setStart(size_t st)
{
    start = st;
    for(size_t i = 0; i < st; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
}

void Metro::setLine(size_t l)
{
    line = l;
}

void Metro::rewind(void)
{
    tab.seekg(0);
    for(size_t i = 0; i < start; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
}

void Metro::last(void)
{
    tab.seekg(-2,tab.end);
    while(tab.peek() != '\n')
    {
        tab.seekg(-1,tab.cur);
    }
    tab.ignore(1);
}

void Metro::operator++(void)
{
    tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
}

void Metro::operator--(void)
{
    do
    {
        tab.seekg(-1,tab.cur);
    }
    while(tab.peek() != '\n');
    while(tab.peek() != '\n')
    {
        tab.seekg(-1,tab.cur);
    }
    tab.ignore(1);
}

void Metro::operator+=(size_t n)
{
    for(size_t i = 0; i < n; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
}

void Metro::operator-=(size_t n)
{
    tab.seekg(-1,tab.cur);
    for(size_t i = 0; i <= n; ++i)
    {
        while(tab.peek() != '\n')
        {
            tab.seekg(-1,tab.cur);
        }
    }
    tab.ignore(1);
}

void Metro::set(size_t n)
{
    tab.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
}

double Metro::peek(void)
{
    double ans;
    size_t mt = tab.tellg();
    do
    {
        tab.seekg(-1,tab.cur);
    }
    while(tab.peek() != '\n');
    tab.ignore(1);
    for(size_t i = 0; i < line; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    tab >> ans;
    tab.seekg(mt);
    return ans;
}

std::valarray<double> Metro::peekp(void)
{
    std::valarray<double> ans(1);
    size_t mt = tab.tellg();
    do
    {
        tab.seekg(-1,tab.cur);
    }
    while(tab.peek() != '\n');
    tab.ignore(1);
    for(size_t i = 0; i < line; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    tab >> ans[0];
    tab.seekg(mt);
    return ans;
}

std::vector<double> Metro::peeks(size_t l)
{
    std::vector<double> ans(l);
    size_t mt = tab.tellg();
    do
    {
        tab.seekg(-1,tab.cur);
    }
    while(tab.peek() != '\n');
    tab.ignore(1);
    for(size_t j = 0; j < l; ++j)
    {
        for(size_t i = 0; i < line; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
        tab >> ans[j];
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    tab.seekg(mt);
    return ans;
}

std::vector<std::valarray<double>> Metro::peeksp(size_t l)
{
    std::vector<std::valarray<double>> ans(l);
    size_t mt = tab.tellg();
    do
    {
        tab.seekg(-1,tab.cur);
    }
    while(tab.peek() != '\n');
    tab.ignore(1);
    for(size_t j = 0; j < l; ++j)
    {
        ans[j].resize(1);
        for(size_t i = 0; i < line; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
        tab >> ans[j][0];
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    tab.seekg(mt);
    return ans;
}

double Metro::get(void)
{
    double ans;
    do
    {
        tab.seekg(-1,tab.cur);
    }
    while(tab.peek() != '\n');
    tab.ignore(1);
    for(size_t i = 0; i < line; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    tab >> ans;
    tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    return ans;
}

std::valarray<double> Metro::getp(void)
{
    std::valarray<double> ans(1);
    do
    {
        tab.seekg(-1,tab.cur);
    }
    while(tab.peek() != '\n');
    tab.ignore(1);
    for(size_t i = 0; i < line; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    tab >> ans[0];
    tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    return ans;
}

std::vector<double> Metro::gets(size_t l)
{
    std::vector<double> ans(l);
    do
    {
        tab.seekg(-1,tab.cur);
    }
    while(tab.peek() != '\n');
    tab.ignore(1);
    for(size_t j = 0; j < l; ++j)
    {
        for(size_t i = 0; i < line; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
        tab >> ans[j];
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    return ans;
}

std::vector<std::valarray<double>> Metro::getsp(size_t l)
{
    std::vector<std::valarray<double>> ans(l);
    do
    {
        tab.seekg(-1,tab.cur);
    }
    while(tab.peek() != '\n');
    tab.ignore(1);
    for(size_t j = 0; j < l; ++j)
    {
        ans[j].resize(1);
        for(size_t i = 0; i < line; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
        tab >> ans[j][0];
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    return ans;
}

double Metro::operator[](size_t n)
{
    double ans;
    size_t mt = tab.tellg();
    tab.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    for(size_t i = 0; i < line; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    tab >> ans;
    tab.seekg(mt);
    return ans;
}

std::valarray<double> Metro::p(size_t n)
{
    std::valarray<double> ans(1);
    size_t mt = tab.tellg();
    tab.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    for(size_t i = 0; i < line; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
    }
    tab >> ans[0];
    tab.seekg(mt);
    return ans;
}

std::vector<double> Metro::s(size_t n, size_t l)
{
    std::vector<double> ans(l);
    size_t mt = tab.tellg();
    tab.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    for(size_t j = 0; j < l; ++j)
    {
        for(size_t i = 0; i < line; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
        tab >> ans[j];
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    tab.seekg(mt);
    return ans;
}

std::vector<std::valarray<double>> Metro::sp(size_t n, size_t l)
{
    std::vector<std::valarray<double>> ans(l);
    size_t mt = tab.tellg();
    tab.seekg(0);
    for(size_t i = 0; i < n; ++i)
    {
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    for(size_t j = 0; j < l; ++j)
    {
        ans[j].resize(1);
        for(size_t i = 0; i < line; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
        tab >> ans[j][0];
        tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    tab.seekg(mt);
    return ans;
}

std::vector<double> Metro::operator[](const std::vector<size_t>& n)
{
    std::vector<double> ans(n.size());
    size_t mt = tab.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        tab.seekg(0);
        for(size_t i = 0; i < n[k]; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        for(size_t i = 0; i < line; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
        tab >> ans[k];
    }
    tab.seekg(mt);
    return ans;
}

std::vector<std::valarray<double>> Metro::p(const std::vector<size_t>& n)
{
    std::vector<std::valarray<double>> ans(n.size());
    size_t mt = tab.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        ans[k].resize(1);
        tab.seekg(0);
        for(size_t i = 0; i < n[k]; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        for(size_t i = 0; i < line; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
        }
        tab >> ans[k][0];
    }
    tab.seekg(mt);
    return ans;
}

std::vector<std::vector<double>> Metro::s(const std::vector<size_t>& n, size_t l)
{
    std::vector<std::vector<double>> ans(n.size());
    size_t mt = tab.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        ans[k].resize(l);
        tab.seekg(0);
        for(size_t i = 0; i < n[k]; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        for(size_t j = 0; j < l; ++j)
        {
            for(size_t i = 0; i < line; ++i)
            {
                tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
            }
            tab >> ans[k][j];
            tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
    }
    tab.seekg(mt);
    return ans;
}

std::vector<std::vector<std::valarray<double>>> Metro::sp(const std::vector<size_t>& n, size_t l)
{
    std::vector<std::vector<std::valarray<double>>> ans(n.size());
    size_t mt = tab.tellg();
    for(size_t k = 0; k < n.size(); ++k)
    {
        ans[k].resize(l);
        tab.seekg(0);
        for(size_t i = 0; i < n[k]; ++i)
        {
            tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        for(size_t j = 0; j < l; ++j)
        {
            ans[k][j].resize(1);
            for(size_t i = 0; i < line; ++i)
            {
                tab.ignore(std::numeric_limits<std::streamsize>::max(),';');
            }
            tab >> ans[k][j][0];
            tab.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
    }
    tab.seekg(mt);
    return ans;
}
