#include <cstddef>
#include <cstdint>
#include <string>
#include <vector>
#include <valarray>


#ifndef DNN_METRO
class Metro
{
    std::ifstream tab;
    
    size_t line;
    
    size_t start;
    
public:
    Metro(void) = default;
    
    Metro(const Metro&) = delete;
    
    Metro(const std::string&,size_t,size_t=0);
    
    ~Metro(void) = default;
    
    Metro& operator=(const Metro&) = delete;

    void init(const std::string&,size_t,size_t=0);
    
    void swap(Metro&);
    
    void setStart(size_t);
    
    void setLine(size_t);

    void rewind(void);

    void last(void);

    void operator++(void);

    void operator--(void);

    void operator+=(size_t);

    void operator-=(size_t);

    void set(size_t);

    double peek(void);

    std::valarray<double> peekp(void);

    std::vector<double> peeks(size_t);

    std::vector<std::valarray<double>> peeksp(size_t);
    
    double get(void);
    
    std::valarray<double> getp(void);
    
    std::vector<double> gets(size_t);
    
    std::vector<std::valarray<double>> getsp(size_t);

    double operator[](size_t);

    std::valarray<double> p(size_t);

    std::vector<double> s(size_t,size_t);

    std::vector<std::valarray<double>> sp(size_t,size_t);

    std::vector<double> operator[](const std::vector<size_t>&);

    std::vector<std::valarray<double>> p(const std::vector<size_t>&);

    std::vector<std::vector<double>> s(const std::vector<size_t>&,size_t);

    std::vector<std::vector<std::valarray<double>>> sp(const std::vector<size_t>&,size_t);
};
#define DNN_METRO
#endif
