#include <cstddef>
#include <list>
#include <vector>
#include <valarray>


#ifndef DNN_AVG_TOOLS
void average(std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&);

void averageDual(std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&);

void averagePartial(std::vector<std::valarray<double>>&,size_t,const std::list<size_t>&,std::vector<std::valarray<double>>&,size_t,const std::list<size_t>&,size_t,const std::list<size_t>&);

void averagePartialNolimit(std::vector<std::valarray<double>>&,size_t,std::vector<std::valarray<double>>&,size_t,size_t);

void averagePartialDual(std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,size_t,const std::list<size_t>&,std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,size_t,const std::list<size_t>&,size_t,const std::list<size_t>&);

void averagePartialExt(std::vector<std::valarray<double>>&,const std::list<size_t>&,size_t,std::vector<std::valarray<double>>&,const std::list<size_t>&,size_t,const std::list<size_t>&,size_t);

void averagePartialX(std::vector<std::valarray<double>>&,size_t,size_t,std::vector<std::valarray<double>>&,size_t,size_t,size_t,size_t);

void averagePartialAsym(std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,size_t,const std::list<size_t>&,const std::list<size_t>&);

void averagePartialNolimitAsym(std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,size_t);

void averagePartialDualAsym(std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,size_t,const std::list<size_t>,const std::list<size_t>&);

void averagePartialExtAsym(std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,const std::list<size_t>&,size_t,const std::list<size_t>&);

void averagePartialXAsym(std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,size_t,size_t);

void averageMult(std::vector<std::vector<std::valarray<double>>*>&);

void averageDualMult(std::vector<std::vector<std::valarray<double>>*>&,std::vector<std::vector<std::valarray<double>>*>&);

void averagePartialMult(std::vector<std::vector<std::valarray<double>>*>&,const std::vector<size_t>&,const std::vector<std::list<size_t>>&,size_t,const std::list<size_t>&);

void averagePartialNolimitMult(std::vector<std::vector<std::valarray<double>>*>&,const std::vector<size_t>&,size_t);

void averagePartialDualMult(std::vector<std::vector<std::valarray<double>>*>&,std::vector<std::vector<std::valarray<double>>*>&,const std::vector<size_t>&,const std::vector<std::list<size_t>>&,size_t,const std::list<size_t>&);

void averagePartialExtMult(std::vector<std::vector<std::valarray<double>>*>&,const std::vector<std::list<size_t>>&,const std::vector<size_t>&,const std::list<size_t>&,size_t);

void averagePartialXMult(std::vector<std::vector<std::valarray<double>>*>&,const std::vector<size_t>&,const std::vector<size_t>&,size_t,size_t);

void averagePartialMultAsym(std::vector<std::valarray<double>>&,std::vector<std::vector<std::valarray<double>>*>&,const std::vector<size_t>&,const std::vector<std::list<size_t>>&,const std::list<size_t>&);

void averagePartialNolimitMultAsym(std::vector<std::valarray<double>>&,std::vector<std::vector<std::valarray<double>>*>&,const std::vector<size_t>&);

void averagePartialDualMultAsym(std::vector<std::valarray<double>>&,std::vector<std::valarray<double>>&,std::vector<std::vector<std::valarray<double>>*>&,std::vector<std::vector<std::valarray<double>>*>&,const std::vector<size_t>&,const std::vector<std::list<size_t>>&,const std::list<size_t>&);

void averagePartialExtMultAsym(std::vector<std::valarray<double>>&,std::vector<std::vector<std::valarray<double>>*>&,const std::vector<std::list<size_t>>&,const std::vector<size_t>&,const std::list<size_t>&);

void averagePartialXMult(std::vector<std::valarray<double>>&,std::vector<std::vector<std::valarray<double>>*>&,const std::vector<size_t>&,const std::vector<size_t>&);
#define DNN_AVG_TOOLS
#endif
