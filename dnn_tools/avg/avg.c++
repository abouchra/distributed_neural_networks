#include <iostream>//********
#include "avg.h++"


void average(std::vector<std::valarray<double>>& wa, std::vector<std::valarray<double>>& wb)
{
    for(size_t i = 0; i < wa.size(); ++i)
    {
        wa[i]+= wb[i];
        wa[i]/= 2.0;
    }
    wb = wa;
}

void averageDual(std::vector<std::valarray<double>>& wa, std::vector<std::valarray<double>>& pa, std::vector<std::valarray<double>>& wb, std::vector<std::valarray<double>>& pb)
{
    for(size_t i = 0; i < wa.size(); ++i)
    {
        wa[i]+= wb[i];
        wa[i]/= 2.0;
        pa[i]+= pb[i];
        pa[i]/= 2.0;
    }
    wb = wa;
    pb = pa;
}

void averagePartial(std::vector<std::valarray<double>>& wa, size_t ia, const std::list<size_t>& ma, std::vector<std::valarray<double>>& wb, size_t ib, const std::list<size_t>& mb, size_t s, const std::list<size_t>& ms)
{
    for(size_t i = 0; i < s; ++i)
    {
        auto a = ma.cbegin();
        auto b = mb.cbegin();
        for(const auto& m : ms)
        {
            for(size_t j = 0; j < m; ++j)
            {
                wa[i+ia][j+*a]+= wb[i+ib][j+*b];
                wa[i+ia][j+*a]/= 2.0;
                wb[i+ib][j+*b] = wa[i+ia][j+*a];
            }
            ++a;
            ++b;
        }
    }
}

void averagePartialNolimit(std::vector<std::valarray<double>>& wa, size_t ia, std::vector<std::valarray<double>>& wb, size_t ib, size_t s)
{
    for(size_t i = 0; i < s; ++i)
    {
        wa[i+ia]+= wb[i+ib];
        wa[i+ia]/= 2.0;
        wb[i+ib] = wa[i+ia];
    }
}

void averagePartialDual(std::vector<std::valarray<double>>& wa, std::vector<std::valarray<double>>& pa, size_t ia, const std::list<size_t>& ma, std::vector<std::valarray<double>>& wb, std::vector<std::valarray<double>>& pb, size_t ib, const std::list<size_t>& mb, size_t s, const std::list<size_t>& ms)
{
    for(size_t i = 0; i < s; ++i)
    {
        auto a = ma.cbegin();
        auto b = mb.cbegin();
        for(const auto& m : ms)
        {
            for(size_t j = 0; j < m; ++j)
            {
                wa[i+ia][j+*a]+= wb[i+ib][j+*b];
                wa[i+ia][j+*a]/= 2.0;
                wb[i+ib][j+*b] = wa[i+ia][j+*a];
            }
            ++a;
            ++b;
        }
        pa[i+ia]+= pb[i+ib];
        pa[i+ia]/= 2.0;
        pb[i+ib] = pa[i+ia];
    }
}

void averagePartialExt(std::vector<std::valarray<double>>& wa, const std::list<size_t>& ia, size_t ma, std::vector<std::valarray<double>>& wb, const std::list<size_t>& ib, size_t mb, const std::list<size_t>& s, size_t ms)
{
    auto a = ia.cbegin();
    auto b = ib.cbegin();
    for(const auto& ls : s)
    {
        for(size_t i = 0; i < ls; ++i)
        {
            for(size_t j = 0; j < ms; ++j)
            {
                wa[i+*a][j+ma]+= wb[i+*b][j+mb];
                wa[i+*a][j+ma]/= 2.0;
                wb[i+*b][j+mb] = wa[i+*a][j+ma];
            }
        }
        ++a;
        ++b;
    }
}

void averagePartialX(std::vector<std::valarray<double>>& wa, size_t ia, size_t ma, std::vector<std::valarray<double>>& wb, size_t ib, size_t mb, size_t s, size_t ms)
{
    for(size_t i = 0; i < s; ++i)
    {
        for(size_t j = 0; j < ms; ++j)
        {
            wa[i+ia][j+ma]+= wb[i+ib][j+mb];
            wa[i+ia][j+ma]/= 2.0;
            wb[i+ib][j+mb] = wa[i+ia][j+ma];
        }
    }
}

void averagePartialAsym(std::vector<std::valarray<double>>& wa, std::vector<std::valarray<double>>& wb, size_t ib, const std::list<size_t>& mb, const std::list<size_t>& ms)
{
    for(size_t i = 0; i < wa.size(); ++i)
    {
        size_t ja = 0;
        auto b = mb.cbegin();
        for(const auto& m : ms)
        {
            for(size_t j = 0; j < m; ++j)
            {
                wa[i][j+ja]+= wb[i+ib][j+*b];
                wa[i][j+ja]/= 2.0;
                wb[i+ib][j+*b] = wa[i][j+ja];
            }
            ja+= m;
            ++b;
        }
    }
}

void averagePartialNolimitAsym(std::vector<std::valarray<double>>& wa, std::vector<std::valarray<double>>& wb, size_t ib)
{
    for(size_t i = 0; i < wa.size(); ++i)
    {
        wa[i]+= wb[i+ib];
        wa[i]/= 2.0;
        wb[i+ib] = wa[i];
    }
}

void averagePartialDualAsym(std::vector<std::valarray<double>>& wa, std::vector<std::valarray<double>>& pa, std::vector<std::valarray<double>>& wb, std::vector<std::valarray<double>>& pb, size_t ib, const std::list<size_t>& mb, const std::list<size_t>& ms)
{
    for(size_t i = 0; i < wa.size(); ++i)
    {
        size_t ja = 0;
        auto b = mb.cbegin();
        for(const auto& m : ms)
        {
            for(size_t j = 0; j < m; ++j)
            {
                wa[i][j+ja]+= wb[i+ib][j+*b];
                wa[i][j+ja]/= 2.0;
                wb[i+ib][j+*b] = wa[i][j+ja];
            }
            ja+= m;
            ++b;
        }
        pa[i]+= pb[i+ib];
        pa[i]/= 2.0;
        pb[i+ib] = pa[i];
    }
}

void averagePartialExtAsym(std::vector<std::valarray<double>>& wa, std::vector<std::valarray<double>>& wb, const std::list<size_t>& ib, size_t mb, const std::list<size_t>& s)
{
    size_t ia = 0;
    auto b = ib.cbegin();
    for(const auto& ls : s)
    {
        for(size_t i = 0; i < ls; ++i)
        {
            for(size_t j = 0; j < wa[i+ia].size(); ++j)
            {
                wa[i+ia][j]+= wb[i+*b][j+mb];
                wa[i+ia][j]/= 2.0;
                wb[i+*b][j+mb] = wa[i+ia][j];
            }
        }
        ia+= ls;
        ++b;
    }
}

void averagePartialXAsym(std::vector<std::valarray<double>>& wa, std::vector<std::valarray<double>>& wb, size_t ib, size_t mb)
{
    for(size_t i = 0; i < wa.size(); ++i)
    {
        for(size_t j = 0; j < wa[i].size(); ++j)
        {
            wa[i][j]+= wb[i+ib][j+mb];
            wa[i][j]/= 2.0;
            wb[i+ib][j+mb] = wa[i][j];
        }
    }
}

void averageMult(std::vector<std::vector<std::valarray<double>>*>& w)
{
    for(size_t i = 0; i < w[0]->size(); ++i)
    {
        for(size_t v = 1; v < w.size(); ++v)
        {
            (*(w[0]))[i]+= (*(w[v]))[i];
        }
        (*(w[0]))[i]/= ((double)w.size());
    }
    for(size_t v = 1; v < w.size(); ++v)
    {
       *(w[v]) = *(w[0]);
    }
}

void averageDualMult(std::vector<std::vector<std::valarray<double>>*>& w, std::vector<std::vector<std::valarray<double>>*>& p)
{
    for(size_t i = 0; i < w[0]->size(); ++i)
    {
        for(size_t v = 1; v < w.size(); ++v)
        {
            (*(w[0]))[i]+= (*(w[v]))[i];
            (*(p[0]))[i]+= (*(p[v]))[i];
        }
        (*(w[0]))[i]/= ((double)w.size());
        (*(p[0]))[i]/= ((double)w.size());
    }
    for(size_t v = 1; v < w.size(); ++v)
    {
       *(w[v]) = *(w[0]);
       *(p[v]) = *(p[0]);
    }
}

void averagePartialMult(std::vector<std::vector<std::valarray<double>>*>& w, const std::vector<size_t>& iv, const std::vector<std::list<size_t>>& mv, size_t s, const std::list<size_t>& ms)
{
    for(size_t v = 1; v < w.size(); ++v)
    {
        for(size_t i = 0; i < s; ++i)
        {
            auto a = mv[0].cbegin();
            auto b = mv[v].cbegin();
            for(const auto& m : ms)
            {
                for(size_t j = 0; j < m; ++j)
                {
                    (*(w[0]))[i+iv[0]][j+*a]+= (*(w[v]))[i+iv[v]][j+*b];
                }
                ++a;
                ++b;
            }
        }
    }
    for(size_t i = 0; i < s; ++i)
    {
        auto a = mv[0].cbegin();
        for(const auto& m : ms)
        {
            for(size_t j = 0; j < m; ++j)
            {
                (*(w[0]))[i+iv[0]][j+*a]/= ((double)w.size());
            }
            ++a;
        }
    }
    for(size_t v = 1; v < w.size(); ++v)
    {
        for(size_t i = 0; i < s; ++i)
        {
            auto a = mv[0].cbegin();
            auto b = mv[v].cbegin();
            for(const auto& m : ms)
            {
                for(size_t j = 0; j < m; ++j)
                {
                    (*(w[v]))[i+iv[v]][j+*b] = (*(w[0]))[i+iv[0]][j+*a];
                }
                ++a;
                ++b;
            }
        }
    }
}

void averagePartialNolimitMult(std::vector<std::vector<std::valarray<double>>*>& w, const std::vector<size_t>& iv, size_t s)
{
    for(size_t i = 0; i < s; ++i)
    {
        for(size_t v = 1; v < w.size(); ++v)
        {
            (*(w[0]))[i+iv[0]]+= (*(w[v]))[i+iv[v]];
        }
        (*(w[0]))[i+iv[0]]/= ((double)w.size());
        for(size_t v = 1; v < w.size(); ++v)
        {
            (*(w[v]))[i+iv[v]] = (*(w[0]))[i+iv[0]];
        }
    }
}

void averagePartialDualMult(std::vector<std::vector<std::valarray<double>>*>& w, std::vector<std::vector<std::valarray<double>>*>& p, const std::vector<size_t>& iv, const std::vector<std::list<size_t>>& mv, size_t s, const std::list<size_t>& ms)
{
    for(size_t v = 1; v < w.size(); ++v)
    {
        for(size_t i = 0; i < s; ++i)
        {
            auto a = mv[0].cbegin();
            auto b = mv[v].cbegin();
            for(const auto& m : ms)
            {
                for(size_t j = 0; j < m; ++j)
                {
                    (*(w[0]))[i+iv[0]][j+*a]+= (*(w[v]))[i+iv[v]][j+*b];
                }
                ++a;
                ++b;
            }
            (*(p[0]))[i+iv[0]]+= (*(p[v]))[i+iv[v]];
        }
    }
    for(size_t i = 0; i < s; ++i)
    {
        auto a = mv[0].cbegin();
        for(const auto& m : ms)
        {
            for(size_t j = 0; j < m; ++j)
            {
                (*(w[0]))[i+iv[0]][j+*a]/= ((double)w.size());
            }
            ++a;
        }
        (*(p[0]))[i+iv[0]]/= ((double)w.size());
    }
    for(size_t v = 1; v < w.size(); ++v)
    {
        for(size_t i = 0; i < s; ++i)
        {
            auto a = mv[0].cbegin();
            auto b = mv[v].cbegin();
            for(const auto& m : ms)
            {
                for(size_t j = 0; j < m; ++j)
                {
                    (*(w[v]))[i+iv[v]][j+*b] = (*(w[0]))[i+iv[0]][j+*a];
                }
                ++a;
                ++b;
            }
            (*(p[v]))[i+iv[v]] = (*(p[0]))[i+iv[0]];
        }
    }
}

void averagePartialExtMult(std::vector<std::vector<std::valarray<double>>*>& w, const std::vector<std::list<size_t>>& iv, const std::vector<size_t>& mv, const std::list<size_t>& s, size_t ms)
{
    for(size_t v = 1; v < w.size(); ++v)
    {
        auto a = iv[0].cbegin();
        auto b = iv[v].cbegin();
        for(const auto& ls : s)
        {
            for(size_t i = 0; i < ls; ++i)
            {
                for(size_t j = 0; j < ms; ++j)
                {
                    (*(w[0]))[i+*a][j+mv[0]]+= (*(w[v]))[i+*b][j+mv[v]];
                }
            }
            ++a;
            ++b;
        }
    }
    auto a = iv[0].cbegin();
    for(const auto& ls : s)
    {
        for(size_t i = 0; i < ls; ++i)
        {
            for(size_t j = 0; j < ms; ++j)
            {
                (*(w[0]))[i+*a][j+mv[0]]/= ((double)w.size());
            }
        }
        ++a;
    }
    for(size_t v = 1; v < w.size(); ++v)
    {
        auto a = iv[0].cbegin();
        auto b = iv[v].cbegin();
        for(const auto& ls : s)
        {
            for(size_t i = 0; i < ls; ++i)
            {
                for(size_t j = 0; j < ms; ++j)
                {
                    (*(w[v]))[i+*b][j+mv[v]] = (*(w[0]))[i+*a][j+mv[0]];
                }
            }
            ++a;
            ++b;
        }
    }
}

void averagePartialXMult(std::vector<std::vector<std::valarray<double>>*>& w, const std::vector<size_t>& iv, const std::vector<size_t>& mv, size_t s, size_t ms)
{
    for(size_t v = 1; v < w.size(); ++v)
    {
        for(size_t i = 0; i < s; ++i)
        {
            for(size_t j = 0; j < ms; ++j)
            {
                (*(w[0]))[i+iv[0]][j+mv[0]]+= (*(w[v]))[i+iv[v]][j+mv[v]];
            }
        }
    }
    for(size_t i = 0; i < s; ++i)
    {
        for(size_t j = 0; j < ms; ++j)
        {
            (*(w[0]))[i+iv[0]][j+mv[0]]/= ((double)w.size());
        }
    }
    for(size_t v = 1; v < w.size(); ++v)
    {
        for(size_t i = 0; i < s; ++i)
        {
            for(size_t j = 0; j < ms; ++j)
            {
                (*(w[v]))[i+iv[v]][j+mv[v]] = (*(w[0]))[i+iv[0]][j+mv[0]];
            }
        }
    }
}

void averagePartialMultAsym(std::vector<std::valarray<double>>& wr, std::vector<std::vector<std::valarray<double>>*>& w, const std::vector<size_t>& iv, const std::vector<std::list<size_t>>& mv, const std::list<size_t>& ms)
{
    for(size_t v = 0; v < w.size(); ++v)
    {
        for(size_t i = 0; i < wr.size(); ++i)
        {
            size_t ja = 0;
            auto b = mv[v].cbegin();
            for(const auto& m : ms)
            {
                for(size_t j = 0; j < m; ++j)
                {
                    wr[i][j+ja]+= (*(w[v]))[i+iv[v]][j+*b];
                }
                ja+= m;
                ++b;
            }
        }
    }
    for(size_t i = 0; i < wr.size(); ++i)
    {
        wr[i]/= ((double)w.size()+1);
    }
    for(size_t v = 0; v < w.size(); ++v)
    {
        for(size_t i = 0; i < wr.size(); ++i)
        {
            size_t ja = 0;
            auto b = mv[v].cbegin();
            for(const auto& m : ms)
            {
                for(size_t j = 0; j < m; ++j)
                {
                    (*(w[v]))[i+iv[v]][j+*b] = wr[i][j+ja];
                }
                ja+= m;
                ++b;
            }
        }
    }
}

void averagePartialNolimitMultAsym(std::vector<std::valarray<double>>& wr, std::vector<std::vector<std::valarray<double>>*>& w, const std::vector<size_t>& iv)
{
    for(size_t i = 0; i < wr.size(); ++i)
    {
        for(size_t v = 0; v < w.size(); ++v)
        {
            wr[i]+= (*(w[v]))[i+iv[v]];
        }
        wr[i]/= ((double)w.size()+1);
        for(size_t v = 0; v < w.size(); ++v)
        {
            (*(w[v]))[i+iv[v]] = wr[i];
        }
    }
}

void averagePartialDualMultAsym(std::vector<std::valarray<double>>& wr, std::vector<std::valarray<double>>& pr, std::vector<std::vector<std::valarray<double>>*>& w, std::vector<std::vector<std::valarray<double>>*>& p, const std::vector<size_t>& iv, const std::vector<std::list<size_t>>& mv, const std::list<size_t>& ms)
{
    for(size_t v = 0; v < w.size(); ++v)
    {
        for(size_t i = 0; i < wr.size(); ++i)
        {
            size_t ja = 0;
            auto b = mv[v].cbegin();
            for(const auto& m : ms)
            {
                for(size_t j = 0; j < m; ++j)
                {
                    wr[i][j+ja]+= (*(w[v]))[i+iv[v]][j+*b];
                }
                ja+= m;
                ++b;
            }
            pr[i]+= (*(p[v]))[i+iv[v]];
        }
    }
    for(size_t i = 0; i < wr.size(); ++i)
    {
        wr[i]/= ((double)w.size()+1);
        pr[i]/= ((double)w.size()+1);
    }
    for(size_t v = 0; v < w.size(); ++v)
    {
        for(size_t i = 0; i < wr.size(); ++i)
        {
            size_t ja = 0;
            auto b = mv[v].cbegin();
            for(const auto& m : ms)
            {
                for(size_t j = 0; j < m; ++j)
                {
                    (*(w[v]))[i+iv[v]][j+*b] = wr[i][j+ja];
                }
                ja+= m;
                ++b;
            }
            (*(p[v]))[i+iv[v]] = pr[i];
        }
    }
}

void averagePartialExtMultAsym(std::vector<std::valarray<double>>& wr, std::vector<std::vector<std::valarray<double>>*>& w, const std::vector<std::list<size_t>>& iv, const std::vector<size_t>& mv, const std::list<size_t>& s)
{
    for(size_t v = 0; v < w.size(); ++v)
    {
        size_t ia = 0;
        auto b = iv[v].cbegin();
        for(const auto& ls : s)
        {
            for(size_t i = 0; i < ls; ++i)
            {
                for(size_t j = 0; j < wr[i+ia].size(); ++j)
                {
                    wr[i+ia][j]+= (*(w[v]))[i+*b][j+mv[v]];
                }
            }
            ia+= ls;
            ++b;
        }
    }
    for(size_t i = 0; i < wr.size(); ++i)
    {
        wr[i]/= ((double)w.size()+1);
    }
    for(size_t v = 0; v < w.size(); ++v)
    {
        size_t ia = 0;
        auto b = iv[v].cbegin();
        for(const auto& ls : s)
        {
            for(size_t i = 0; i < ls; ++i)
            {
                for(size_t j = 0; j < wr[i+ia].size(); ++j)
                {
                    (*(w[v]))[i+*b][j+mv[v]] = wr[i+ia][j];
                }
            }
            ia+= ls;
            ++b;
        }
    }
}

void averagePartialXMult(std::vector<std::valarray<double>>& wr, std::vector<std::vector<std::valarray<double>>*>& w, const std::vector<size_t>& iv, const std::vector<size_t>& mv)
{
    for(size_t v = 0; v < w.size(); ++v)
    {
        for(size_t i = 0; i < wr.size(); ++i)
        {
            for(size_t j = 0; j < wr[i].size(); ++j)
            {
                wr[i][j]+= (*(w[v]))[i+iv[v]][j+mv[v]];
            }
        }
    }
    for(size_t i = 0; i < wr.size(); ++i)
    {
        wr[i]/= ((double)w.size()+1);
    }
    for(size_t v = 0; v < w.size(); ++v)
    {
        for(size_t i = 0; i < wr.size(); ++i)
        {
            for(size_t j = 0; j < wr[i].size(); ++j)
            {
                (*(w[v]))[i+iv[v]][j+mv[v]] = wr[i][j];
            }
        }
    }
}
