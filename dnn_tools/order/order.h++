#include <cstddef>
#include <vector>
#include <valarray>


#ifndef DNN_ORD_TOOLS
std::vector<size_t> orderLike(const std::vector<std::valarray<double>>&, const std::vector<std::valarray<double>>&);

std::vector<size_t> orderLikePartial(const std::vector<std::valarray<double>>&,size_t,const std::vector<std::valarray<double>>&,size_t,size_t);

std::vector<size_t> orderLikePartialAsym(const std::vector<std::valarray<double>>&,size_t,const std::vector<std::valarray<double>>&);

std::vector<size_t> orderLikeDef(const std::vector<std::valarray<double>>&,const std::vector<std::valarray<double>>&);

std::vector<size_t> orderLikePartialDef(const std::vector<std::valarray<double>>&,size_t,size_t,const std::vector<std::valarray<double>>&,size_t,size_t);

std::vector<size_t> orderLikePartialDefFull(const std::vector<std::valarray<double>>&,size_t,const std::vector<std::valarray<double>>&,size_t,size_t);

std::vector<size_t> orderLikePartialDefAsym(const std::vector<std::valarray<double>>&,size_t,size_t,const std::vector<std::valarray<double>>&);

std::vector<size_t> orderLikePartialDefFullAsym(const std::vector<std::valarray<double>>&,size_t,const std::vector<std::valarray<double>>&);

std::vector<size_t> orderLikePartialDefDisym(const std::vector<std::valarray<double>>&,const std::vector<std::valarray<double>>&,size_t,size_t);

template<typename T>
void reorder(T& a, const std::vector<size_t>& o)
{
    T b(a);
    for(size_t i = 0; i < a.size(); ++i)
    {
        a[o[i]] = b[i];
    }
}

template<typename T>
void reorderPartial(T& a, size_t s, const std::vector<size_t>& o)
{
    T b(o.size());
    for(size_t i = 0; i < o.size(); ++i)
    {
        b[i] = a[i+s];
    }
    for(size_t i = 0; i < o.size(); ++i)
    {
        a[o[i]+s] = b[i];
    }
}
#define DNN_ORD_TOOLS
#endif
