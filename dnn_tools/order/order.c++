#include <limits>
#include <utility>
#include <list>
#include <queue>
#include "order.h++"


std::vector<size_t> orderLike(const std::vector<std::valarray<double>>& a, const std::vector<std::valarray<double>>& r)
{
    std::vector<size_t> ans(a.size(),a.size());
    std::vector<std::pair<size_t,double>> af(a.size(),std::pair<size_t,double>(a.size(),std::numeric_limits<double>::infinity()));
    std::vector<std::list<std::pair<size_t,double>>> dist(a.size());
    std::queue<size_t> q;
    for(size_t i = 0; i < a.size(); ++i)
    {
        for(size_t j = 0; j < r.size(); ++j)
        {
            dist[i].push_back(std::pair<size_t,double>(j,abs(a[i]-r[j]).sum()));
        }
        dist[i].sort([](const std::pair<size_t,double>& p1, const std::pair<size_t,double>& p2){return (p1.second < p2.second);});
        q.push(i);
    }
    while(!q.empty())
    {
        std::pair<size_t,double> na;
        do
        {
            na = dist[q.front()].front();
            dist[q.front()].pop_front();
        }
        while(na.second >= af[na.first].second);
        if(af[na.first].first < a.size())
        {
            ans[af[na.first].first] = a.size();
            q.push(af[na.first].first);
        }
        ans[q.front()] = na.first;
        af[na.first] = std::pair<size_t,double>(q.front(),na.second);
        q.pop();
    }
    return ans;
}

std::vector<size_t> orderLikePartial(const std::vector<std::valarray<double>>& a, size_t sa, const std::vector<std::valarray<double>>& r, size_t sr, size_t l)
{
    std::vector<size_t> ans(l,l);
    std::vector<std::pair<size_t,double>> af(l,std::pair<size_t,double>(l,std::numeric_limits<double>::infinity()));
    std::vector<std::list<std::pair<size_t,double>>> dist(l);
    std::queue<size_t> q;
    for(size_t i = 0; i < l; ++i)
    {
        for(size_t j = 0; j < l; ++j)
        {
            dist[i].push_back(std::pair<size_t,double>(j,abs(a[i+sa]-r[j+sr]).sum()));
        }
        dist[i].sort([](const std::pair<size_t,double>& p1, const std::pair<size_t,double>& p2){return (p1.second < p2.second);});
        q.push(i);
    }
    while(!q.empty())
    {
        std::pair<size_t,double> na;
        do
        {
            na = dist[q.front()].front();
            dist[q.front()].pop_front();
        }
        while(na.second >= af[na.first].second);
        if(af[na.first].first < l)
        {
            ans[af[na.first].first] = l;
            q.push(af[na.first].first);
        }
        ans[q.front()] = na.first;
        af[na.first] = std::pair<size_t,double>(q.front(),na.second);
        q.pop();
    }
    return ans;
}

std::vector<size_t> orderLikePartialAsym(const std::vector<std::valarray<double>>& a, size_t s, const std::vector<std::valarray<double>>& r)
{
    std::vector<size_t> ans(r.size(),r.size());
    std::vector<std::pair<size_t,double>> af(r.size(),std::pair<size_t,double>(r.size(),std::numeric_limits<double>::infinity()));
    std::vector<std::list<std::pair<size_t,double>>> dist(r.size());
    std::queue<size_t> q;
    for(size_t i = 0; i < r.size(); ++i)
    {
        for(size_t j = 0; j < r.size(); ++j)
        {
            dist[i].push_back(std::pair<size_t,double>(j,abs(a[i+s]-r[j]).sum()));
        }
        dist[i].sort([](const std::pair<size_t,double>& p1, const std::pair<size_t,double>& p2){return (p1.second < p2.second);});
        q.push(i);
    }
    while(!q.empty())
    {
        std::pair<size_t,double> na;
        do
        {
            na = dist[q.front()].front();
            dist[q.front()].pop_front();
        }
        while(na.second >= af[na.first].second);
        if(af[na.first].first < r.size())
        {
            ans[af[na.first].first] = r.size();
            q.push(af[na.first].first);
        }
        ans[q.front()] = na.first;
        af[na.first] = std::pair<size_t,double>(q.front(),na.second);
        q.pop();
    }
    return ans;
}

std::vector<size_t> orderLikeDef(const std::vector<std::valarray<double>>& a, const std::vector<std::valarray<double>>& r)
{
    std::vector<size_t> tans(r.size(),r.size()), ans(a.size(),a.size());
    std::vector<std::pair<size_t,double>> af(r.size(),std::pair<size_t,double>(r.size(),std::numeric_limits<double>::infinity()));
    std::vector<std::list<std::pair<size_t,double>>> dist(r.size());
    std::queue<size_t> q;
    for(size_t i = 0; i < r.size(); ++i)
    {
        for(size_t j = 0; j < a.size(); ++j)
        {
            dist[i].push_back(std::pair<size_t,double>(j,abs(r[i]-a[j]).sum()));
        }
        dist[i].sort([](const std::pair<size_t,double>& p1, const std::pair<size_t,double>& p2){return (p1.second < p2.second);});
        q.push(i);
    }
    while(!q.empty())
    {
        std::pair<size_t,double> na;
        do
        {
            na = dist[q.front()].front();
            dist[q.front()].pop_front();
        }
        while(na.second >= af[na.first].second);
        if(af[na.first].first < r.size())
        {
            tans[af[na.first].first] = r.size();
            q.push(af[na.first].first);
        }
        tans[q.front()] = na.first;
        af[na.first] = std::pair<size_t,double>(q.front(),na.second);
        q.pop();
    }
    for(size_t i = 0; i < r.size(); ++i)
    {
        ans[tans[i]] = i;
    }
    size_t k = r.size();
    for(size_t i = 0; i < a.size(); ++i)
    {
        if(ans[i] == a.size())
        {
            ans[i] = k;
            ++k;
        }
    }
    return ans;
}

std::vector<size_t> orderLikePartialDef(const std::vector<std::valarray<double>>& a, size_t sa, size_t la, const std::vector<std::valarray<double>>& r, size_t sr, size_t lr)
{
    std::vector<size_t> tans(lr,lr), ans(la,la);
    std::vector<std::pair<size_t,double>> af(lr,std::pair<size_t,double>(lr,std::numeric_limits<double>::infinity()));
    std::vector<std::list<std::pair<size_t,double>>> dist(lr);
    std::queue<size_t> q;
    for(size_t i = 0; i < lr; ++i)
    {
        for(size_t j = 0; j < la; ++j)
        {
            dist[i].push_back(std::pair<size_t,double>(j,abs(a[j+sa]-r[i+sr]).sum()));
        }
        dist[i].sort([](const std::pair<size_t,double>& p1, const std::pair<size_t,double>& p2){return (p1.second < p2.second);});
        q.push(i);
    }
    while(!q.empty())
    {
        std::pair<size_t,double> na;
        do
        {
            na = dist[q.front()].front();
            dist[q.front()].pop_front();
        }
        while(na.second >= af[na.first].second);
        if(af[na.first].first < lr)
        {
            tans[af[na.first].first] = lr;
            q.push(af[na.first].first);
        }
        tans[q.front()] = na.first;
        af[na.first] = std::pair<size_t,double>(q.front(),na.second);
        q.pop();
    }
    for(size_t i = 0; i < lr; ++i)
    {
        ans[tans[i]] = i;
    }
    size_t k = lr;
    for(size_t i = 0; i < la; ++i)
    {
        if(ans[i] == la)
        {
            ans[i] = k;
            ++k;
        }
    }
    return ans;
}

std::vector<size_t> orderLikePartialDefFull(const std::vector<std::valarray<double>>& a, size_t sa, const std::vector<std::valarray<double>>& r, size_t sr, size_t lr)
{
    return orderLikePartialDef(a,sa,a.size()-sa,r,sr,lr);
}

std::vector<size_t> orderLikePartialDefAsym(const std::vector<std::valarray<double>>& a, size_t s, size_t l, const std::vector<std::valarray<double>>& r)
{
    std::vector<size_t> tans(r.size(),r.size()), ans(l,l);
    std::vector<std::pair<size_t,double>> af(r.size(),std::pair<size_t,double>(r.size(),std::numeric_limits<double>::infinity()));
    std::vector<std::list<std::pair<size_t,double>>> dist(r.size());
    std::queue<size_t> q;
    for(size_t i = 0; i < r.size(); ++i)
    {
        for(size_t j = 0; j < l; ++j)
        {
            dist[i].push_back(std::pair<size_t,double>(j,abs(a[j+s]-r[i]).sum()));
        }
        dist[i].sort([](const std::pair<size_t,double>& p1, const std::pair<size_t,double>& p2){return (p1.second < p2.second);});
        q.push(i);
    }
    while(!q.empty())
    {
        std::pair<size_t,double> na;
        do
        {
            na = dist[q.front()].front();
            dist[q.front()].pop_front();
        }
        while(na.second >= af[na.first].second);
        if(af[na.first].first < r.size())
        {
            tans[af[na.first].first] = r.size();
            q.push(af[na.first].first);
        }
        tans[q.front()] = na.first;
        af[na.first] = std::pair<size_t,double>(q.front(),na.second);
        q.pop();
    }
    for(size_t i = 0; i < r.size(); ++i)
    {
        ans[tans[i]] = i;
    }
    size_t k = r.size();
    for(size_t i = 0; i < l; ++i)
    {
        if(ans[i] == l)
        {
            ans[i] = k;
            ++k;
        }
    }
    return ans;
}

std::vector<size_t> orderLikePartialDefFullAsym(const std::vector<std::valarray<double>>& a, size_t s, const std::vector<std::valarray<double>>& r)
{
    return orderLikePartialDefAsym(a,s,a.size()-s,r);
}

std::vector<size_t> orderLikePartialDefDisym(const std::vector<std::valarray<double>>& a, const std::vector<std::valarray<double>>& r, size_t s, size_t l)
{
    std::vector<size_t> tans(l,l), ans(a.size(),a.size());
    std::vector<std::pair<size_t,double>> af(l,std::pair<size_t,double>(l,std::numeric_limits<double>::infinity()));
    std::vector<std::list<std::pair<size_t,double>>> dist(l);
    std::queue<size_t> q;
    for(size_t i = 0; i < l; ++i)
    {
        for(size_t j = 0; j < a.size(); ++j)
        {
            dist[i].push_back(std::pair<size_t,double>(j,abs(a[j]-r[i+s]).sum()));
        }
        dist[i].sort([](const std::pair<size_t,double>& p1, const std::pair<size_t,double>& p2){return (p1.second < p2.second);});
        q.push(i);
    }
    while(!q.empty())
    {
        std::pair<size_t,double> na;
        do
        {
            na = dist[q.front()].front();
            dist[q.front()].pop_front();
        }
        while(na.second >= af[na.first].second);
        if(af[na.first].first < l)
        {
            tans[af[na.first].first] = l;
            q.push(af[na.first].first);
        }
        tans[q.front()] = na.first;
        af[na.first] = std::pair<size_t,double>(q.front(),na.second);
        q.pop();
    }
    for(size_t i = 0; i < l; ++i)
    {
        ans[tans[i]] = i;
    }
    size_t k = l;
    for(size_t i = 0; i < a.size(); ++i)
    {
        if(ans[i] == a.size())
        {
            ans[i] = k;
            ++k;
        }
    }
    return ans;
}
