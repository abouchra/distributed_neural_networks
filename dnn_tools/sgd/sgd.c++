#include <chrono>
#include <random>
#include "sgd.h++"


std::vector<size_t> getMinibatch(size_t bs, size_t mbs)
{
    std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());
    std::vector<size_t> av(bs), ans(mbs);
    for(size_t i = 0; i < bs; ++i)
    {
        av[i] = i;
    }
    for(size_t i = 0; i < mbs; ++i)
    {
        std::uniform_int_distribution<size_t> randGen(0,av.size()-1);
        size_t r = randGen(gen);
        ans[i] = av[r];
        av.erase(av.begin()+r);
    }
    return ans;
}

std::vector<size_t> getMinibatchOffset(size_t bs, size_t mbs, size_t os)
{
    std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());
    std::vector<size_t> av(bs), ans(mbs);
    for(size_t i = 0; i < bs; ++i)
    {
        av[i] = i+os;
    }
    for(size_t i = 0; i < mbs; ++i)
    {
        std::uniform_int_distribution<size_t> randGen(0,av.size()-1);
        size_t r = randGen(gen);
        ans[i] = av[r];
        av.erase(av.begin()+r);
    }
    return ans;
}

std::vector<size_t> getMinibatchSet(size_t mbs, std::vector<size_t> av)
{
    std::default_random_engine gen(std::chrono::system_clock::now().time_since_epoch().count());
    std::vector<size_t> ans(mbs);
    for(size_t i = 0; i < mbs; ++i)
    {
        std::uniform_int_distribution<size_t> randGen(0,av.size()-1);
        size_t r = randGen(gen);
        ans[i] = av[r];
        av.erase(av.begin()+r);
    }
    return ans;
}
