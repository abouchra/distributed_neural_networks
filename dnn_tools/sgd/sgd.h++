#include <cstddef>
#include <vector>


#ifndef DNN_SGD_TOOLS
std::vector<size_t> getMinibatch(size_t,size_t);

std::vector<size_t> getMinibatchOffset(size_t,size_t,size_t);

std::vector<size_t> getMinibatchSet(size_t,std::vector<size_t>);
#define DNN_SGD_TOOLS
#endif
