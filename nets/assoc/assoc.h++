#include <cstddef>
#include <utility>
#include <list>
#include <vector>
#include <valarray>
#include <unordered_set>
#include <unordered_map>
#include <functional>


#ifndef DNN_ASSOC
class Assoc
{
    double thld;
    
    mutable std::valarray<double> statesv;
    
    mutable std::valarray<double> statesh;
    
    std::vector<std::valarray<double>> weightsv;
    
    std::vector<std::valarray<double>> weightsh;
    
    std::function<double(size_t)> weightd;
    
    std::unordered_map<size_t,size_t> sliceBegv;
    
    std::unordered_map<size_t,size_t> sliceBegh;
    
    std::unordered_map<size_t,size_t> sliceSizev;
    
    std::unordered_map<size_t,size_t> sliceSizeh;
    
    std::unordered_map<size_t,std::list<size_t>> sliceDepBegv;
    
    std::unordered_map<size_t,std::list<size_t>> sliceDepBegh;
    
    std::unordered_map<size_t,std::list<size_t>> sliceDepSizev;
    
    std::unordered_map<size_t,std::list<size_t>> sliceDepSizeh;

public:
    Assoc(void) = default;
    
    Assoc(const Assoc&) = default;
    
    Assoc(size_t,size_t,double,const std::function<double(size_t)>&);
    
    Assoc(size_t,size_t,double,const std::function<double(size_t)>&,const std::vector<std::valarray<double>>&,const std::vector<std::valarray<double>>&);
    
    ~Assoc(void) = default;
    
    Assoc& operator=(const Assoc&) = default;
    
    void train(const std::valarray<double>&,double);

    void train(const std::vector<std::valarray<double>>&,double);
    
    std::valarray<double> compute(const std::valarray<double>&) const;
    
    void reset(void);
    
    void set(double,const std::vector<std::valarray<double>>&,const std::vector<std::valarray<double>>&);
    
    std::valarray<double> observe(void) const;

    std::pair<std::valarray<double>,std::valarray<double>> getStates(void) const;

    std::pair<std::vector<std::valarray<double>>,std::vector<std::valarray<double>>> getWeights(void) const;
    
    void reorderLike(const Assoc&);
    
    void reorderLikeModel(const Assoc&,size_t);
    
    void reorderLikeModelDef(const Assoc&,size_t);
    
    std::pair<size_t,size_t> layersSize(void) const;

    void resize(size_t,size_t);

    void resizeRelative(int,int);

    void addModel(size_t,size_t,size_t,size_t,size_t,const std::list<size_t>&);

    void addModelLast(size_t,size_t,size_t,const std::list<size_t>&);

    void rmModel(size_t);
    
    bool hasModel(size_t) const;
    
    std::unordered_set<size_t> models(void) const;

    std::pair<std::pair<size_t,size_t>,std::pair<size_t,size_t>> modelPos(size_t) const;
    
    friend void reorderSim(std::vector<Assoc*>);

    friend void reorderSimModel(std::vector<Assoc*>,size_t);

    friend void reorderSimModelDef(std::vector<Assoc*>,size_t);
    
    friend void average(Assoc&,Assoc&);
    
    friend void averageModel(Assoc&,Assoc&,size_t);
    
    friend void averageModelCross(Assoc&,Assoc&,size_t,size_t);
    
    friend void averageMult(std::vector<Assoc*>);
    
    friend void averageModelMult(std::vector<Assoc*>,size_t);
    
    friend void averageModelMultCross(std::vector<Assoc*>,size_t,size_t);
    
    friend void averageModelMultCheck(std::vector<Assoc*>,size_t);
    
    friend void averageModelMultCrossCheck(std::vector<Assoc*>,size_t,size_t);
};
#define DNN_ASSOC
#endif
