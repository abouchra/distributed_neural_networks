#include <cmath>
#include <limits>
#include "../../dnn_tools/order/order.h++"
#include "../../dnn_tools/avg/avg.h++"
#include "assoc.h++"


Assoc::Assoc(size_t vs, size_t hs, double th, const std::function<double(size_t)>& wd)
{
    thld = th;
    weightd = wd;
    statesv = std::valarray<double>(0.0,vs);
    statesh = std::valarray<double>(0.0,hs);
    weightsv = std::vector<std::valarray<double>>(vs,std::valarray<double>(hs));
    weightsh = std::vector<std::valarray<double>>(hs,std::valarray<double>(vs));
    for(size_t i = 0; i < vs; ++i)
    {
        for(size_t j = 0; j < hs; ++j)
        {
            weightsv[i][j] = weightd(hs);
            weightsh[j][i] = weightd(vs);
        }
    }
}

Assoc::Assoc(size_t vs, size_t hs, double th, const std::function<double(size_t)>& wd, const std::vector<std::valarray<double>>& wv, const std::vector<std::valarray<double>>& wh)
{
    thld = th;
    weightd = wd;
    statesv = std::valarray<double>(0.0,vs);
    statesh = std::valarray<double>(0.0,hs);
    for(size_t i = 0; i < vs; ++i)
    {
        for(size_t j = 0; j < hs; ++j)
        {
            weightsv[i][j] = weightd(hs);
            weightsh[j][i] = weightd(vs);
        }
    }
    weightsv = wv;
    weightsh = wh;
}

void Assoc::train(const std::valarray<double>& x, double h)
{
    statesv = x;
    for(size_t i = 0; i < weightsh.size(); ++i)
    {
        statesh[i] = ((weightsh[i]*statesv).sum()>=thld)?1.0:0.0;
        for(size_t j = 0; j < statesv.size(); ++j)
        {
            if(statesv[j]>0.5)
            {
                if(statesh[i]>0.5)
                {
                    weightsh[i][j]+= ((1.0-weightsh[i][j])/2.0)*h;
                }
                else
                {
                    weightsh[i][j]-= ((1.0+weightsh[i][j])/2.0)*h;
                }
            }
        }
    }
    for(size_t i = 0; i < weightsv.size(); ++i)
    {
        for(size_t j = 0; j < statesh.size(); ++j)
        {
            if(statesh[j]>0.5)
            {
                if(statesv[i]>0.5)
                {
                    weightsv[i][j]+= ((1.0-weightsv[i][j])/2.0)*h;
                }
                else
                {
                    weightsv[i][j]-= ((1.0+weightsv[i][j])/2.0)*h;
                }
            }
        }
    }
}

void Assoc::train(const std::vector<std::valarray<double>>& x, double h)
{
    std::vector<std::valarray<double>> gradv(weightsv.size(),std::valarray<double>(0.0,statesh.size())), gradh(weightsh.size(),std::valarray<double>(0.0,statesv.size()));
    for(size_t n = 0; n < x.size(); ++n)
    {
        statesv = x[n];
        for(size_t i = 0; i < weightsh.size(); ++i)
        {
            statesh[i] = ((weightsh[i]*statesv).sum()>=thld)?1.0:0.0;
            for(size_t j = 0; j < statesv.size(); ++j)
            {
                if(statesv[j]>0.5)
                {
                    if(statesh[i]>0.5)
                    {
                        gradh[i][j]+= (1.0-weightsh[i][j])/2.0;
                    }
                    else
                    {
                        gradh[i][j]-= (1.0+weightsh[i][j])/2.0;
                    }
                }
            }
        }
        for(size_t i = 0; i < weightsv.size(); ++i)
        {
            for(size_t j = 0; j < statesh.size(); ++j)
            {
                if(statesh[j]>0.5)
                {
                    if(statesv[i]>0.5)
                    {
                        gradv[i][j]+= (1.0-weightsv[i][j])/2.0;
                    }
                    else
                    {
                        gradv[i][j]-= (1.0+weightsv[i][j])/2.0;
                    }
                }
            }
        }
    }
    for(size_t i = 0; i < weightsv.size(); ++i)
    {
        weightsv[i]+= gradv[i]*(h/x.size());
    }
    for(size_t i = 0; i < weightsh.size(); ++i)
    {
        weightsh[i]+= gradh[i]*(h/x.size());
    }
}

std::valarray<double> Assoc::compute(const std::valarray<double>& x) const
{
    statesv = x;
    for(size_t i = 0; i < weightsh.size(); ++i)
    {
        statesh[i] = ((weightsh[i]*statesv).sum()>=thld)?1.0:0.0;
    }
    for(size_t i = 0; i < weightsv.size(); ++i)
    {
        statesv[i] = ((weightsv[i]*statesh).sum()>=thld)?1.0:0.0;
    }
    return statesv;
}

void Assoc::reset(void)
{
    statesv = 0.0;
    statesh = 0.0;
    for(size_t i = 0; i < weightsv.size(); ++i)
    {
        for(size_t j = 0; j < weightsh.size(); ++j)
        {
            weightsv[i][j] = weightd(weightsh.size());
            weightsh[j][i] = weightd(weightsv.size());
        }
    }
}

void Assoc::set(double th, const std::vector<std::valarray<double>>& wv, const std::vector<std::valarray<double>>& wh)
{
    thld = th;
    weightsv = wv;
    weightsh = wh;
}

std::valarray<double> Assoc::observe(void) const
{
    return statesv;
}

std::pair<std::valarray<double>,std::valarray<double>> Assoc::getStates(void) const
{
    return std::pair<std::valarray<double>,std::valarray<double>>(statesv,statesh);
}

std::pair<std::vector<std::valarray<double>>,std::vector<std::valarray<double>>> Assoc::getWeights(void) const
{
    return std::pair<std::vector<std::valarray<double>>,std::vector<std::valarray<double>>>(weightsv,weightsh);
}

void Assoc::reorderLike(const Assoc& n)
{
    std::vector<size_t> no = orderLike(weightsh,n.weightsh);
    reorder(weightsh,no);
    for(size_t i = 0; i < weightsv.size(); ++i)
    {
        reorder(weightsv[i],no);
    }
}

void Assoc::reorderLikeModel(const Assoc& n, size_t m)
{
    std::vector<size_t> no = orderLikePartial(weightsh,sliceBegh[m],n.weightsh,n.sliceBegh.at(m),sliceSizeh[m]);
    reorderPartial(weightsh,sliceBegh[m],no);
    for(size_t i = 0; i < weightsv.size(); ++i)
    {
        reorderPartial(weightsv[i],sliceBegh[m],no);
    }
}

void Assoc::reorderLikeModelDef(const Assoc& n, size_t m)
{
    std::vector<size_t> no = orderLikePartialDefFull(weightsh,sliceBegh[m],n.weightsh,n.sliceBegh.at(m),sliceSizeh[m]);
    reorderPartial(weightsh,sliceBegh[m],no);
    for(size_t i = 0; i < weightsv.size(); ++i)
    {
        reorderPartial(weightsv[i],sliceBegh[m],no);
    }
}

std::pair<size_t,size_t> Assoc::layersSize(void) const
{
    return std::pair<size_t,size_t>(statesv.size(),statesh.size());
}

void Assoc::resize(size_t lv, size_t lh)
{
    std::vector<std::valarray<double>> tweightsv(weightsv), tweightsh(weightsh);
    statesv = std::valarray<double>(0.0,lv);
    statesh = std::valarray<double>(0.0,lh);
    weightsv = std::vector<std::valarray<double>>(lv);
    weightsh = std::vector<std::valarray<double>>(lh);
    for(size_t i = 0; i < lv; ++i)
    {
        weightsv[i] = std::valarray<double>(lh);
        for(size_t j = 0; j < lh; ++j)
        {
            if((i < tweightsv.size()) && (j < tweightsv[i].size()))
            {
                weightsv[i][j] = tweightsv[i][j];
            }
            else
            {
                weightsv[i][j] = weightd(lh);
            }
        }
    }
    for(size_t i = 0; i < lh; ++i)
    {
        weightsh[i] = std::valarray<double>(lv);
        for(size_t j = 0; j < lv; ++j)
        {
            if((i < tweightsh.size()) && (j < tweightsh[i].size()))
            {
                weightsh[i][j] = tweightsh[i][j];
            }
            else
            {
                weightsh[i][j] = weightd(lv);
            }
        }
    }
}

void Assoc::resizeRelative(int lrv, int lrh)
{
    resize(statesv.size()+lrv,statesh.size()+lrh);
}

void Assoc::addModel(size_t n, size_t sbv, size_t sbh, size_t ssv, size_t ssh, const std::list<size_t>& deps)
{
    sliceBegv[n] = sbv;
    sliceBegh[n] = sbh;
    sliceSizev[n] = ssv;
    sliceSizeh[n] = ssh;
    for(const auto& d : deps)
    {
        sliceDepBegv[n].push_back(sliceBegv[d]);
        sliceDepBegh[n].push_back(sliceBegh[d]);
        sliceDepSizev[n].push_back(sliceSizev[d]);
        sliceDepSizeh[n].push_back(sliceSizeh[d]);
    }
    sliceDepBegv[n].push_back(sbv);
    sliceDepBegh[n].push_back(sbh);
    sliceDepSizev[n].push_back(ssv);
    sliceDepSizeh[n].push_back(ssh);
}

void Assoc::addModelLast(size_t n, size_t ssv, size_t ssh, const std::list<size_t>& deps)
{
    size_t sbv, sbh, b = 0, mb = std::numeric_limits<size_t>::max();
    for(const auto& m : sliceBegv)
    {
        if(m.second >= b)
        {
            mb = m.first;
            b = m.second;
        }
    }
    if(mb == std::numeric_limits<size_t>::max())
    {
        sbv = 0;
    }
    else
    {
        sbv = b+sliceSizev[mb];
    }
    b = 0;
    mb = std::numeric_limits<size_t>::max();
    for(const auto& m : sliceBegh)
    {
        if(m.second >= b)
        {
            mb = m.first;
            b = m.second;
        }
    }
    if(mb == std::numeric_limits<size_t>::max())
    {
        sbh = 0;
    }
    else
    {
        sbh = b+sliceSizeh[mb];
    }
    addModel(n,sbv,sbh,ssv,ssh,deps);
}

void Assoc::rmModel(size_t n)
{
    sliceBegv.erase(n);
    sliceBegh.erase(n);
    sliceSizev.erase(n);
    sliceSizeh.erase(n);
    sliceDepBegv.erase(n);
    sliceDepBegh.erase(n);
    sliceDepSizev.erase(n);
    sliceDepSizeh.erase(n);
}
    
bool Assoc::hasModel(size_t m) const
{
    return (sliceBegv.find(m) != sliceBegv.end());
}
    
std::unordered_set<size_t> Assoc::models(void) const
{
    std::unordered_set<size_t> ans;
    ans.reserve(sliceBegv.size());
    for(const auto& m : sliceBegv)
    {
        ans.insert(m.first);
    }
    return ans;
}

std::pair<std::pair<size_t,size_t>,std::pair<size_t,size_t>> Assoc::modelPos(size_t m) const
{
    return std::pair<std::pair<size_t,size_t>,std::pair<size_t,size_t>>(std::pair<size_t,size_t>(sliceBegv.at(m),sliceBegh.at(m)),std::pair<size_t,size_t>(sliceSizev.at(m),sliceSizeh.at(m)));
}

void reorderSim(std::vector<Assoc*> n)
{
    for(size_t v = 1; v < n.size(); ++v)
    {
        std::vector<size_t> no = orderLike(n[v]->weightsh,n[0]->weightsh);
        reorder(n[v]->weightsh,no);
        for(size_t i = 0; i < n[v]->weightsv.size(); ++i)
        {
            reorder(n[v]->weightsv[i],no);
        }
    }
}

void reorderSimModel(std::vector<Assoc*> n, size_t m)
{
    for(size_t v = 1; v < n.size(); ++v)
    {
        std::vector<size_t> no = orderLikePartial(n[v]->weightsh,n[v]->sliceBegh[m],n[0]->weightsh,n[0]->sliceBegh.at(m),n[v]->sliceSizeh[m]);
        reorderPartial(n[v]->weightsh,n[v]->sliceBegh[m],no);
        for(size_t i = 0; i < n[v]->weightsv.size(); ++i)
        {
            reorderPartial(n[v]->weightsv[i],n[v]->sliceBegh[m],no);
        }
    }
}

void reorderSimModelDef(std::vector<Assoc*> n, size_t m)
{
    for(size_t v = 1; v < n.size(); ++v)
    {
        std::vector<size_t> no = orderLikePartialDefFull(n[v]->weightsh,n[v]->sliceBegh[m],n[0]->weightsh,n[0]->sliceBegh.at(m),n[v]->sliceSizeh[m]);
        reorderPartial(n[v]->weightsh,n[v]->sliceBegh[m],no);
        for(size_t i = 0; i < n[v]->weightsv.size(); ++i)
        {
            reorderPartial(n[v]->weightsv[i],n[v]->sliceBegh[m],no);
        }
    }
}

void average(Assoc& n1, Assoc& n2)
{
    average(n1.weightsv,n2.weightsv);
    average(n1.weightsh,n2.weightsh);
}

void averageModel(Assoc& n1, Assoc& n2, size_t m)
{
    averagePartial(n1.weightsv,n1.sliceBegv[m],n1.sliceDepBegh[m],n2.weightsv,n2.sliceBegv[m],n2.sliceDepBegh[m],n1.sliceSizev[m],n1.sliceDepSizeh[m]);
    averagePartial(n1.weightsh,n1.sliceBegh[m],n1.sliceDepBegv[m],n2.weightsh,n2.sliceBegh[m],n2.sliceDepBegv[m],n1.sliceSizeh[m],n1.sliceDepSizev[m]);
    averagePartialExt(n1.weightsv,n1.sliceDepBegv[m],n1.sliceBegh[m],n2.weightsv,n2.sliceDepBegv[m],n2.sliceBegh[m],n1.sliceDepSizev[m],n1.sliceSizeh[m]);
    averagePartialExt(n1.weightsh,n1.sliceDepBegh[m],n1.sliceBegv[m],n2.weightsh,n2.sliceDepBegh[m],n2.sliceBegv[m],n1.sliceDepSizeh[m],n1.sliceSizev[m]);
}

void averageModelCross(Assoc& n1, Assoc& n2, size_t m1, size_t m2)
{
    averagePartialX(n1.weightsv,n1.sliceBegv[m1],n1.sliceBegh[m2],n2.weightsv,n2.sliceBegv[m1],n2.sliceBegh[m1],n1.sliceSizev[m1],n1.sliceSizeh[m2]);
    averagePartialX(n1.weightsv,n1.sliceBegv[m1],n1.sliceBegh[m2],n2.weightsv,n2.sliceBegv[m1],n2.sliceBegh[m1],n1.sliceSizev[m1],n1.sliceSizeh[m2]);
    averagePartialX(n1.weightsh,n1.sliceBegh[m1],n1.sliceBegv[m2],n2.weightsh,n2.sliceBegh[m1],n2.sliceBegv[m1],n1.sliceSizeh[m1],n1.sliceSizev[m2]);
    averagePartialX(n1.weightsh,n1.sliceBegh[m1],n1.sliceBegv[m2],n2.weightsh,n2.sliceBegh[m1],n2.sliceBegv[m1],n1.sliceSizeh[m1],n1.sliceSizev[m2]);
}

void averageMult(std::vector<Assoc*> n)
{
    std::vector<std::vector<std::valarray<double>>*> wv(n.size()), wh(n.size());
    for(size_t i = 0; i < n.size(); ++i)
    {
        wv[i] = &(n[i]->weightsv);
        wh[i] = &(n[i]->weightsh);
    }
    averageMult(wv);
    averageMult(wh);
}

void averageModelMult(std::vector<Assoc*> n, size_t m)
{
    std::vector<std::vector<std::valarray<double>>*> wv(n.size()), wh(n.size());
    std::vector<size_t> iv(n.size()), iv2(n.size());
    std::vector<std::list<size_t>> mv(n.size()), mv2(n.size());
    for(size_t i = 0; i < n.size(); ++i)
    {
        wv[i] = &(n[i]->weightsv);
        wh[i] = &(n[i]->weightsh);
        iv[i] = n[i]->sliceBegv[m];
        mv[i] = n[i]->sliceDepBegh[m];
        iv2[i] = n[i]->sliceBegh[m];
        mv2[i] = n[i]->sliceDepBegv[m];
    }
    averagePartialMult(wv,iv,mv,n[0]->sliceSizev[m],n[0]->sliceDepSizeh[m]);
    averagePartialMult(wh,iv2,mv2,n[0]->sliceSizeh[m],n[0]->sliceDepSizev[m]);
    averagePartialExtMult(wv,mv2,iv2,n[0]->sliceDepSizev[m],n[0]->sliceSizeh[m]);
    averagePartialExtMult(wh,mv,iv,n[0]->sliceDepSizeh[m],n[0]->sliceSizev[m]);
}

void averageModelMultCross(std::vector<Assoc*> n, size_t m1, size_t m2)
{
    std::vector<std::vector<std::valarray<double>>*> wv(n.size()), wh(n.size());
    std::vector<size_t> iv1(n.size()), iv2(n.size()), mv1(n.size()), mv2(n.size());
    for(size_t i = 0; i < n.size(); ++i)
    {
        wv[i] = &(n[i]->weightsv);
        wh[i] = &(n[i]->weightsh);
        iv1[i] = n[i]->sliceBegv[m1];
        mv1[i] = n[i]->sliceBegh[m2];
        iv2[i] = n[i]->sliceBegv[m2];
        mv2[i] = n[i]->sliceBegh[m1];
    }
    averagePartialXMult(wv,iv1,mv1,n[0]->sliceSizev[m1],n[0]->sliceSizeh[m2]);
    averagePartialXMult(wh,iv2,mv2,n[0]->sliceSizeh[m1],n[0]->sliceSizev[m2]);
    averagePartialXMult(wv,iv2,mv2,n[0]->sliceSizev[m2],n[0]->sliceSizeh[m1]);
    averagePartialXMult(wh,iv1,mv1,n[0]->sliceSizeh[m2],n[0]->sliceSizev[m1]);
}

void averageModelMultCheck(std::vector<Assoc*> n, size_t m)
{
    std::vector<std::vector<std::valarray<double>>*> wv(n.size()), wh(n.size());
    std::vector<size_t> iv(n.size()), iv2(n.size());
    std::vector<std::list<size_t>> mv(n.size()), mv2(n.size());
    size_t k = 0, r;
    for(size_t i = 0; i < n.size(); ++i)
    {
        if(n[i]->sliceBegv.find(m) != n[i]->sliceBegv.end())
        {
            wv[k] = &(n[i]->weightsv);
            wh[k] = &(n[i]->weightsh);
            iv[k] = n[i]->sliceBegv[m];
            mv[k] = n[i]->sliceDepBegh[m];
            iv2[k] = n[i]->sliceBegh[m];
            mv2[k] = n[i]->sliceDepBegv[m];
            k++;
            r = i;
        }
    }
    if(k > 1)
    {
        averagePartialMult(wv,iv,mv,n[r]->sliceSizev[m],n[r]->sliceDepSizeh[m]);
        averagePartialMult(wh,iv2,mv2,n[r]->sliceSizeh[m],n[r]->sliceDepSizev[m]);
        averagePartialExtMult(wv,mv2,iv2,n[r]->sliceDepSizev[m],n[r]->sliceSizeh[m]);
        averagePartialExtMult(wh,mv,iv,n[r]->sliceDepSizeh[m],n[r]->sliceSizev[m]);
    }
}

void averageModelMultCrossCheck(std::vector<Assoc*> n, size_t m1, size_t m2)
{
    std::vector<std::vector<std::valarray<double>>*> wv(n.size()), wh(n.size());
    std::vector<size_t> iv1(n.size()), iv2(n.size()), mv1(n.size()), mv2(n.size());
    size_t k = 0, r;
    for(size_t i = 0; i < n.size(); ++i)
    {
        if((n[i]->sliceBegv.find(m1) != n[i]->sliceBegv.end()) && (n[i]->sliceBegv.find(m2) != n[i]->sliceBegv.end()))
        {
            wv[i] = &(n[i]->weightsv);
            wh[i] = &(n[i]->weightsh);
            iv1[i] = n[i]->sliceBegv[m1];
            mv1[i] = n[i]->sliceBegh[m2];
            iv2[i] = n[i]->sliceBegv[m2];
            mv2[i] = n[i]->sliceBegh[m1];
            k++;
            r = i;
        }
    }
    if(k > 1)
    {
        averagePartialXMult(wv,iv1,mv1,n[r]->sliceSizev[m1],n[r]->sliceSizeh[m2]);
        averagePartialXMult(wh,iv2,mv2,n[r]->sliceSizeh[m1],n[r]->sliceSizev[m2]);
        averagePartialXMult(wv,iv2,mv2,n[r]->sliceSizev[m2],n[r]->sliceSizeh[m1]);
        averagePartialXMult(wh,iv1,mv1,n[r]->sliceSizeh[m2],n[r]->sliceSizev[m1]);
    }
}
