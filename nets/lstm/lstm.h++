#include <cstddef>
#include <utility>
#include <list>
#include <array>
#include <vector>
#include <valarray>
#include <unordered_set>
#include <unordered_map>
#include <functional>


#ifndef DNN_LSTM
class LSTM
{
    struct Gate
    {
        std::function<double(double,std::valarray<double>)> actf;
    
        std::vector<std::function<double(double,std::valarray<double>)>> dactf;
    
        std::vector<std::vector<std::valarray<double>>> weights;

        std::vector<std::vector<std::valarray<double>>> actfp;
    
        std::function<double(size_t)> weightd;
    
        std::function<std::valarray<double>(void)> actfpd;
    };
    
    Gate main;
    
    Gate forg;
    
    Gate inpu;
    
    Gate outp;
    
    Gate post;
    
    mutable std::vector<std::vector<std::valarray<double>>> csv;
    
    mutable std::vector<std::vector<std::valarray<double>>> out;
    
    std::function<double(double)> norf;
    
    std::function<double(double)> dnorf;
    
    std::unordered_map<size_t,std::vector<size_t>> sliceBeg;

    std::unordered_map<size_t,std::vector<size_t>> sliceSize;

    std::unordered_map<size_t,std::vector<std::list<size_t>>> sliceDepBeg;

    std::unordered_map<size_t,std::vector<std::list<size_t>>> sliceDepSize;
    
    std::unordered_map<size_t,std::vector<size_t>> sliceBegp;

    std::unordered_map<size_t,std::vector<size_t>> sliceSizep;

    std::unordered_map<size_t,std::vector<std::list<size_t>>> sliceDepBegp;

    std::unordered_map<size_t,std::vector<std::list<size_t>>> sliceDepSizep;

public:
    LSTM(void) = default;
    
    LSTM(const LSTM&) = default;
    
    LSTM(const std::vector<size_t>&,const std::vector<size_t>&,const std::array<std::function<double(double,std::valarray<double>)>,5>&,const std::array<std::vector<std::function<double(double,std::valarray<double>)>>,5>&,const std::array<std::function<double(size_t)>,5>&,const std::array<std::function<std::valarray<double>(void)>,5>&,const std::function<double(double)>&,const std::function<double(double)>&);
    
    LSTM(const std::array<std::function<double(double,std::valarray<double>)>,5>&,const std::array<std::vector<std::function<double(double,std::valarray<double>)>>,5>&,const std::array<std::vector<std::vector<std::valarray<double>>>,5>&,const std::array<std::vector<std::vector<std::valarray<double>>>,5>&,const std::array<std::function<double(size_t)>,5>&,const std::array<std::function<std::valarray<double>(void)>,5>&,const std::function<double(double)>&,const std::function<double(double)>&);
    
    ~LSTM(void) = default;
    
    LSTM& operator=(const LSTM&) = default;

    void train(const std::vector<std::valarray<double>>&,const std::vector<std::valarray<double>>&,double);

    void train(const std::vector<std::vector<std::valarray<double>>>&,const std::vector<std::vector<std::valarray<double>>>&,double);
    
    std::vector<std::valarray<double>> compute(const std::vector<std::valarray<double>>&,size_t=0) const;
    
    void reset(void);
    
    void set(const std::array<std::vector<std::vector<std::valarray<double>>>,5>&,const std::array<std::vector<std::vector<std::valarray<double>>>,5>&);
    
    std::vector<std::valarray<double>> getInput(void) const;

    std::vector<std::valarray<double>> getOutput(void) const;

    std::vector<std::vector<std::valarray<double>>> getStates(void) const;

    std::array<std::vector<std::vector<std::valarray<double>>>,5> getWeights(void) const;

    std::array<std::vector<std::vector<std::valarray<double>>>,5> getActfp(void) const;
    
    void reorderLike(const LSTM&);
    
    void reorderLikeModel(const LSTM&,size_t);
    
    void reorderLikeModelDef(const LSTM&,size_t);
    
    std::pair<std::vector<size_t>,std::vector<size_t>> layersSize(void) const;

    void resize(const std::vector<size_t>&,const std::vector<size_t>&);

    void resizeRelative(const std::vector<int>&,const std::vector<int>&);

    void addModel(size_t,const std::vector<size_t>&,const std::vector<size_t>&,const std::vector<size_t>&,const std::vector<size_t>&,const std::list<size_t>&);

    void addModelLast(size_t,const std::vector<size_t>&,const std::vector<size_t>&,const std::list<size_t>&);

    void rmModel(size_t);
    
    bool hasModel(size_t) const;
    
    std::unordered_set<size_t> models(void) const;

    std::pair<std::pair<std::vector<size_t>,std::vector<size_t>>,std::pair<std::vector<size_t>,std::vector<size_t>>> modelPos(size_t) const;
    
    friend void reorderSim(std::vector<LSTM*>);

    friend void reorderSimModel(std::vector<LSTM*>,size_t);

    friend void reorderSimModelDef(std::vector<LSTM*>,size_t);
    
    friend void average(LSTM&,LSTM&);
    
    friend void averageModel(LSTM&,LSTM&,size_t);
    
    friend void averageModelCross(LSTM&,LSTM&,size_t,size_t);
    
    friend void averageMult(std::vector<LSTM*>);
    
    friend void averageModelMult(std::vector<LSTM*>,size_t);
    
    friend void averageModelMultCross(std::vector<LSTM*>,size_t,size_t);
    
    friend void averageModelMultCheck(std::vector<LSTM*>,size_t);
    
    friend void averageModelMultCrossCheck(std::vector<LSTM*>,size_t,size_t);
};
#define DNN_LSTM
#endif
