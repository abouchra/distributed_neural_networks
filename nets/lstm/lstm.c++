#include <limits>
#include "../../dnn_tools/order/order.h++"
#include "../../dnn_tools/avg/avg.h++"
#include "lstm.h++"


LSTM::LSTM(const std::vector<size_t>& l, const std::vector<size_t>& lp, const std::array<std::function<double(double,std::valarray<double>)>,5>& f, const std::array<std::vector<std::function<double(double,std::valarray<double>)>>,5>& df, const std::array<std::function<double(size_t)>,5>& wd, const std::array<std::function<std::valarray<double>(void)>,5>& afd, const std::function<double(double)>& n, const std::function<double(double)>& dn)
{
    main.actf = f[0];
    forg.actf = f[1];
    inpu.actf = f[2];
    outp.actf = f[3];
    post.actf = f[4];
    main.dactf = df[0];
    forg.dactf = df[1];
    inpu.dactf = df[2];
    outp.dactf = df[3];
    post.dactf = df[4];
    main.weightd = wd[0];
    forg.weightd = wd[1];
    inpu.weightd = wd[2];
    outp.weightd = wd[3];
    post.weightd = wd[4];
    main.actfpd = afd[0];
    forg.actfpd = afd[1];
    inpu.actfpd = afd[2];
    outp.actfpd = afd[3];
    post.actfpd = afd[4];
    main.weights = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    forg.weights = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    inpu.weights = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    outp.weights = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    post.weights = std::vector<std::vector<std::valarray<double>>>(lp.size());
    main.actfp = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    forg.actfp = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    inpu.actfp = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    outp.actfp = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    post.actfp = std::vector<std::vector<std::valarray<double>>>(lp.size());
    for(size_t i = 1; i < l.size(); ++i)
    {
        main.weights[i-1] = std::vector<std::valarray<double>>(l[i]);
        forg.weights[i-1] = std::vector<std::valarray<double>>(l[i]);
        inpu.weights[i-1] = std::vector<std::valarray<double>>(l[i]);
        outp.weights[i-1] = std::vector<std::valarray<double>>(l[i]);
        main.actfp[i-1] = std::vector<std::valarray<double>>(l[i]);
        forg.actfp[i-1] = std::vector<std::valarray<double>>(l[i]);
        inpu.actfp[i-1] = std::vector<std::valarray<double>>(l[i]);
        outp.actfp[i-1] = std::vector<std::valarray<double>>(l[i]);
        for(size_t j = 0; j < l[i]; ++j)
        {
            main.weights[i-1][j] = std::valarray<double>(l[i-1]+l[i]);
            forg.weights[i-1][j] = std::valarray<double>(l[i-1]+l[i]);
            inpu.weights[i-1][j] = std::valarray<double>(l[i-1]+l[i]);
            outp.weights[i-1][j] = std::valarray<double>(l[i-1]+l[i]);
            for(size_t k = 0; k < main.weights[i-1][j].size(); ++k)
            {
                main.weights[i-1][j][k] = main.weightd(l[i-1]+l[i]);
                forg.weights[i-1][j][k] = forg.weightd(l[i-1]+l[i]);
                inpu.weights[i-1][j][k] = inpu.weightd(l[i-1]+l[i]);
                outp.weights[i-1][j][k] = outp.weightd(l[i-1]+l[i]);
            }
            main.actfp[i-1][j] = main.actfpd();
            forg.actfp[i-1][j] = forg.actfpd();
            inpu.actfp[i-1][j] = inpu.actfpd();
            outp.actfp[i-1][j] = outp.actfpd();
        }
    }
    for(size_t i = 0; i < lp.size(); ++i)
    {
        post.weights[i] = std::vector<std::valarray<double>>(lp[i]);
        post.actfp[i] = std::vector<std::valarray<double>>(lp[i]);
        for(size_t j = 0; j < lp[i]; ++j)
        {
            if(i == 0)
            {
                post.weights[i][j] = std::valarray<double>(l.back());
                for(size_t k = 0; k < l.back(); ++k)
                {
                    post.weights[i][j][k] = post.weightd(l.back());
                }
                post.actfp[i][j] = post.actfpd();
            }
            else
            {
                post.weights[i][j] = std::valarray<double>(lp[i-1]);
                for(size_t k = 0; k < lp[i-1]; ++k)
                {
                    post.weights[i][j][k] = post.weightd(lp[i-1]);
                }
                post.actfp[i][j] = post.actfpd();
            }
        }
    }
    norf = n;
    dnorf = dn;
}

LSTM::LSTM(const std::array<std::function<double(double,std::valarray<double>)>,5>& f, const std::array<std::vector<std::function<double(double,std::valarray<double>)>>,5>& df, const std::array<std::vector<std::vector<std::valarray<double>>>,5>& w, const std::array<std::vector<std::vector<std::valarray<double>>>,5>& a, const std::array<std::function<double(size_t)>,5>& wd, const std::array<std::function<std::valarray<double>(void)>,5>& afd, const std::function<double(double)>& n, const std::function<double(double)>& dn)
{
    main.actf = f[0];
    forg.actf = f[1];
    inpu.actf = f[2];
    outp.actf = f[3];
    post.actf = f[4];
    main.dactf = df[0];
    forg.dactf = df[1];
    inpu.dactf = df[2];
    outp.dactf = df[3];
    post.dactf = df[4];
    main.weightd = wd[0];
    forg.weightd = wd[1];
    inpu.weightd = wd[2];
    outp.weightd = wd[3];
    post.weightd = wd[4];
    main.actfpd = afd[0];
    forg.actfpd = afd[1];
    inpu.actfpd = afd[2];
    outp.actfpd = afd[3];
    post.actfpd = afd[4];
    norf = n;
    dnorf = dn;
    main.weights = w[0];
    forg.weights = w[1];
    inpu.weights = w[2];
    outp.weights = w[3];
    post.weights = w[4];
    main.actfp = a[0];
    forg.actfp = a[1];
    inpu.actfp = a[2];
    outp.actfp = a[3];
    post.actfp = a[4];
}

void LSTM::train(const std::vector<std::valarray<double>>& x, const std::vector<std::valarray<double>>& y, double h)
{
    std::vector<std::vector<std::valarray<double>>> psm(x.size()), psf(x.size()), psi(x.size()), pso(x.size()), psp(x.size()), erm(x.size()), erf(x.size()), eri(x.size()), ero(x.size()), erp(x.size()), erc(x.size()+1), ery(x.size()), otm(x.size()), otf(x.size()), oti(x.size()), oto(x.size()), otn(x.size()), gradwm(main.weights.size()), gradpm(main.weights.size()), gradwf(forg.weights.size()), gradpf(forg.weights.size()), gradwi(inpu.weights.size()), gradpi(inpu.weights.size()), gradwo(outp.weights.size()), gradpo(outp.weights.size()), gradwp(post.weights.size()), gradpp(post.weights.size()), inp(x.size());
    csv.resize(x.size()+1);
    out.resize(x.size()+1);
    csv[0].resize(1+main.weights.size());
    out[0].resize(1+outp.weights.size());
    csv[0][0] = std::valarray<double>(0.0,main.weights[0][0].size());
    out[0][0] = std::valarray<double>(0.0,outp.weights[0][0].size());
    for(size_t i = 0; i < main.weights.size(); ++i)
    {
        csv[0][i+1] = std::valarray<double>(0.0,main.weights[i].size());
        out[0][i+1] = std::valarray<double>(0.0,outp.weights[i].size());
    }
    for(size_t r = 1; r < out.size(); ++r)
    {
        psm[r-1].resize(main.weights.size());
        psf[r-1].resize(forg.weights.size());
        psi[r-1].resize(inpu.weights.size());
        pso[r-1].resize(outp.weights.size());
        psp[r-1].resize(post.weights.size());
        psm[r-1].resize(main.weights.size());
        otm[r-1].resize(main.weights.size());
        otf[r-1].resize(forg.weights.size());
        oti[r-1].resize(inpu.weights.size());
        oto[r-1].resize(outp.weights.size());
        otn[r-1].resize(main.weights.size());
        inp[r-1].resize(main.weights.size());
        csv[r].resize(1+main.weights.size());
        out[r].resize(1+outp.weights.size()+post.weights.size());
        out[r][0] = x[r-1];
        for(size_t i = 0; i < main.weights.size(); ++i)
        {
            inp[r-1][i].resize(out[r][i].size()+out[r-1][i+1].size());
            for(size_t j = 0; j < out[r][i].size(); ++j)
            {
                inp[r-1][i][j] = out[r][i][j];
            }
            for(size_t j = 0; j < out[r-1][i+1].size(); ++j)
            {
                inp[r-1][i][j+out[r][i].size()] = out[r-1][i+1][j];
            }
            psm[r-1][i].resize(main.weights[i].size());
            psf[r-1][i].resize(forg.weights[i].size());
            psi[r-1][i].resize(inpu.weights[i].size());
            pso[r-1][i].resize(outp.weights[i].size());
            otm[r-1][i].resize(main.weights[i].size());
            otf[r-1][i].resize(forg.weights[i].size());
            oti[r-1][i].resize(inpu.weights[i].size());
            oto[r-1][i].resize(outp.weights[i].size());
            otn[r-1][i].resize(main.weights[i].size());
            csv[r][i+1].resize(main.weights[i].size());
            out[r][i+1].resize(outp.weights[i].size());
            for(size_t j = 0; j < main.weights[i].size(); ++j)
            {
                psm[r-1][i][j] = (inp[r-1][i]*main.weights[i][j]).sum();
                psf[r-1][i][j] = (inp[r-1][i]*forg.weights[i][j]).sum();
                psi[r-1][i][j] = (inp[r-1][i]*inpu.weights[i][j]).sum();
                pso[r-1][i][j] = (inp[r-1][i]*outp.weights[i][j]).sum();
                otm[r-1][i][j] = main.actf(psm[r-1][i][j],main.actfp[i][j]);
                otf[r-1][i][j] = forg.actf(psf[r-1][i][j],forg.actfp[i][j]);
                oti[r-1][i][j] = inpu.actf(psi[r-1][i][j],inpu.actfp[i][j]);
                oto[r-1][i][j] = outp.actf(pso[r-1][i][j],outp.actfp[i][j]);
                csv[r][i+1][j] = csv[r-1][i+1][j]*otf[r-1][i][j]+otm[r-1][i][j]*oti[r-1][i][j];
                otn[r-1][i][j] = norf(csv[r][i+1][j]);
                out[r][i+1][j] = otn[r-1][i][j]*oto[r-1][i][j];
            }
        }
        for(size_t i = 0; i < post.weights.size(); ++i)
        {
            psp[r-1][i].resize(post.weights[i].size());
            out[r][i+outp.weights.size()+1].resize(post.weights[i].size());
            for(size_t j = 0; j < post.weights[i].size(); ++j)
            {
                psp[r-1][i][j] = (out[r][i+main.weights.size()]*post.weights[i][j]).sum();
                out[r][i+outp.weights.size()+1][j] = post.actf(psp[r-1][i][j],post.actfp[i][j]);
            }
        }
    }
    for(size_t i = 0; i < gradwm.size(); ++i)
    {
        gradwm[i] = std::vector<std::valarray<double>>(main.weights[i].size(),std::valarray<double>(0.0,main.weights[i][0].size()));
        gradwf[i] = std::vector<std::valarray<double>>(forg.weights[i].size(),std::valarray<double>(0.0,forg.weights[i][0].size()));
        gradwi[i] = std::vector<std::valarray<double>>(inpu.weights[i].size(),std::valarray<double>(0.0,inpu.weights[i][0].size()));
        gradwo[i] = std::vector<std::valarray<double>>(outp.weights[i].size(),std::valarray<double>(0.0,outp.weights[i][0].size()));
        gradpm[i] = std::vector<std::valarray<double>>(main.actfp[i].size(),std::valarray<double>(0.0,main.actfp[i][0].size()));
        gradpf[i] = std::vector<std::valarray<double>>(forg.actfp[i].size(),std::valarray<double>(0.0,forg.actfp[i][0].size()));
        gradpi[i] = std::vector<std::valarray<double>>(inpu.actfp[i].size(),std::valarray<double>(0.0,inpu.actfp[i][0].size()));
        gradpo[i] = std::vector<std::valarray<double>>(outp.actfp[i].size(),std::valarray<double>(0.0,outp.actfp[i][0].size()));
    }
    for(size_t i = 0; i < gradwp.size(); ++i)
    {
        gradwp[i] = std::vector<std::valarray<double>>(post.weights[i].size(),std::valarray<double>(0.0,post.weights[i][0].size()));
        gradpp[i] = std::vector<std::valarray<double>>(post.actfp[i].size(),std::valarray<double>(0.0,post.actfp[i][0].size()));
    }
    erc.back().resize(main.weights.size());
    for(size_t i = 0; i < main.weights.size(); ++i)
    {
        erc.back()[i] = std::valarray<double>(0.0,main.weights[i].size());
    }
    for(size_t r = x.size()-1; r < x.size(); --r)
    {
        erm[r].resize(main.weights.size());
        erf[r].resize(forg.weights.size());
        eri[r].resize(inpu.weights.size());
        ero[r].resize(outp.weights.size());
        erp[r].resize(post.weights.size());
        erc[r].resize(main.weights.size());
        ery[r].resize(outp.weights.size());
        erp[r].back() = y[r]-out[r+1].back();
        for(size_t i = post.weights.size()-2; i < post.weights.size(); --i)
        {
            erp[r][i].resize(post.weights[i].size());
            for(size_t j = 0; j < post.weights[i].size(); ++j)
            {
                erp[r][i][j] = 0.0;
                for(size_t k = 0; k < post.weights[i+1].size(); ++k)
                {
                    erp[r][i][j]+= post.weights[i+1][k][j]*post.dactf[0](psp[r][i+1][k],post.actfp[i+1][k])*erp[r][i+1][k];
                }
            }
        }
        for(size_t i = main.weights.size()-1; i < main.weights.size(); --i)
        {
            ery[r][i].resize(outp.weights[i].size());
            for(size_t j = 0; j < outp.weights.back().size(); ++j)
            {
                ery[r][i][j] = 0.0;
                if(i == main.weights.size()-1)
                {
                    for(size_t k = 0; k < post.weights[0].size(); ++k)
                    {
                        ery[r].back()[j]+= post.weights[0][k][j]*post.dactf[0](psp[r][0][k],post.actfp[0][k])*erp[r][0][k];
                    }
                }
                else
                {
                    for(size_t k = 0; k < main.weights[i+1].size(); ++k)
                    {
                        ery[r][i][j]+= main.weights[i+1][k][j]*main.dactf[0](psm[r][i+1][k],main.actfp[i+1][k])*erm[r][i+1][k];
                        ery[r][i][j]+= forg.weights[i+1][k][j]*forg.dactf[0](psf[r][i+1][k],forg.actfp[i+1][k])*erf[r][i+1][k];
                        ery[r][i][j]+= inpu.weights[i+1][k][j]*inpu.dactf[0](psi[r][i+1][k],inpu.actfp[i+1][k])*eri[r][i+1][k];
                        ery[r][i][j]+= outp.weights[i+1][k][j]*outp.dactf[0](pso[r][i+1][k],outp.actfp[i+1][k])*ero[r][i+1][k];
                    }
                }
            }
            ero[r][i] = ery[r][i]*otn[r][i];
            erc[r][i].resize(main.weights[i].size());
            for(size_t j = 0; j < erc[r][i].size(); ++j)
            {
                erc[r][i][j] = dnorf(csv[r+1][i+1][j])*ery[r][i][j]*oto[r][i][j]+erc[r+1][i][j];
            }
            erm[r][i] = erc[r][i]*oti[r][i];
            eri[r][i] = erc[r][i]*otm[r][i];
            erf[r][i] = erc[r][i]*csv[r][i+1];
            erc[r][i]*= otf[r][i];
        }
        for(size_t i = 0; i < main.weights.size(); ++i)
        {
            for(size_t j = 0; j < main.weights[i].size(); ++j)
            {
                gradwm[i][j]+= erm[r][i][j]*main.dactf[0](psm[r][i][j],main.actfp[i][j])*inp[r][i];
                gradwf[i][j]+= erf[r][i][j]*forg.dactf[0](psf[r][i][j],forg.actfp[i][j])*inp[r][i];
                gradwi[i][j]+= eri[r][i][j]*inpu.dactf[0](psi[r][i][j],inpu.actfp[i][j])*inp[r][i];
                gradwo[i][j]+= ero[r][i][j]*outp.dactf[0](pso[r][i][j],outp.actfp[i][j])*inp[r][i];
                for(size_t k = 0; k < main.actfp[i][j].size(); ++k)
                {
                    gradpm[i][j][k]+= erm[r][i][j]*main.dactf[k+1](psm[r][i][j],main.actfp[i][j]);
                }
                for(size_t k = 0; k < forg.actfp[i][j].size(); ++k)
                {
                    gradpf[i][j][k]+= erf[r][i][j]*forg.dactf[k+1](psf[r][i][j],forg.actfp[i][j]);
                }
                for(size_t k = 0; k < inpu.actfp[i][j].size(); ++k)
                {
                    gradpi[i][j][k]+= eri[r][i][j]*inpu.dactf[k+1](psi[r][i][j],inpu.actfp[i][j]);
                }
                for(size_t k = 0; k < outp.actfp[i][j].size(); ++k)
                {
                    gradpo[i][j][k]+= ero[r][i][j]*outp.dactf[k+1](pso[r][i][j],outp.actfp[i][j]);
                }
            }
        }
        for(size_t i = 0; i < post.weights.size(); ++i)
        {
            for(size_t j = 0; j < post.weights[i].size(); ++j)
            {
                gradwp[i][j]+= erp[r][i][j]*post.dactf[0](psp[r][i][j],post.actfp[i][j])*out[r+1][i+main.weights.size()];
                for(size_t k = 0; k < post.actfp[i][j].size(); ++k)
                {
                    gradpp[i][j][k]+= erp[r][i][j]*post.dactf[k+1](psp[r][i][j],post.actfp[i][j]);
                }
            }
        }
    }
    for(size_t i = 0; i < main.weights.size(); ++i)
    {
        for(size_t j = 0; j < main.weights[i].size(); ++j)
        {
            main.weights[i][j]+= gradwm[i][j]*(h/((double)x.size()));
            forg.weights[i][j]+= gradwf[i][j]*(h/((double)x.size()));
            inpu.weights[i][j]+= gradwi[i][j]*(h/((double)x.size()));
            outp.weights[i][j]+= gradwo[i][j]*(h/((double)x.size()));
            main.actfp[i][j]+= gradpm[i][j]*(h/((double)x.size()));
            forg.actfp[i][j]+= gradpf[i][j]*(h/((double)x.size()));
            inpu.actfp[i][j]+= gradpi[i][j]*(h/((double)x.size()));
            outp.actfp[i][j]+= gradpo[i][j]*(h/((double)x.size()));
        }
    }
    for(size_t i = 0; i < post.weights.size(); ++i)
    {
        for(size_t j = 0; j < post.weights[i].size(); ++j)
        {
            post.weights[i][j]+= gradwp[i][j]*(h/((double)x.size()));
            post.actfp[i][j]+= gradpp[i][j]*(h/((double)x.size()));
        }
    }
}

void LSTM::train(const std::vector<std::vector<std::valarray<double>>>& x, const std::vector<std::vector<std::valarray<double>>>& y, double h)
{
    std::vector<std::vector<std::valarray<double>>> gradwm(main.weights.size()), gradpm(main.weights.size()), gradwf(forg.weights.size()), gradpf(forg.weights.size()), gradwi(inpu.weights.size()), gradpi(inpu.weights.size()), gradwo(outp.weights.size()), gradpo(outp.weights.size()), gradwp(post.weights.size()), gradpp(post.weights.size());
    for(size_t i = 0; i < gradwm.size(); ++i)
    {
        gradwm[i] = std::vector<std::valarray<double>>(main.weights[i].size(),std::valarray<double>(0.0,main.weights[i][0].size()));
        gradwf[i] = std::vector<std::valarray<double>>(forg.weights[i].size(),std::valarray<double>(0.0,forg.weights[i][0].size()));
        gradwi[i] = std::vector<std::valarray<double>>(inpu.weights[i].size(),std::valarray<double>(0.0,inpu.weights[i][0].size()));
        gradwo[i] = std::vector<std::valarray<double>>(outp.weights[i].size(),std::valarray<double>(0.0,outp.weights[i][0].size()));
        gradpm[i] = std::vector<std::valarray<double>>(main.actfp[i].size(),std::valarray<double>(0.0,main.actfp[i][0].size()));
        gradpf[i] = std::vector<std::valarray<double>>(forg.actfp[i].size(),std::valarray<double>(0.0,forg.actfp[i][0].size()));
        gradpi[i] = std::vector<std::valarray<double>>(inpu.actfp[i].size(),std::valarray<double>(0.0,inpu.actfp[i][0].size()));
        gradpo[i] = std::vector<std::valarray<double>>(outp.actfp[i].size(),std::valarray<double>(0.0,outp.actfp[i][0].size()));
    }
    for(size_t i = 0; i < gradwp.size(); ++i)
    {
        gradwp[i] = std::vector<std::valarray<double>>(post.weights[i].size(),std::valarray<double>(0.0,post.weights[i][0].size()));
        gradpp[i] = std::vector<std::valarray<double>>(post.actfp[i].size(),std::valarray<double>(0.0,post.actfp[i][0].size()));
    }
    for(size_t n = 0; n < x.size(); ++n)
    {
        std::vector<std::vector<std::valarray<double>>> psm(x[n].size()), psf(x[n].size()), psi(x[n].size()), pso(x[n].size()), psp(x[n].size()), erm(x[n].size()), erf(x[n].size()), eri(x[n].size()), ero(x[n].size()), erp(x[n].size()), erc(x[n].size()+1), ery(x[n].size()), otm(x[n].size()), otf(x[n].size()), oti(x[n].size()), oto(x[n].size()), otn(x[n].size()), inp(x[n].size());
        csv.resize(x[n].size()+1);
        out.resize(x[n].size()+1);
        csv[0].resize(1+main.weights.size());
        out[0].resize(1+outp.weights.size());
        csv[0][0] = std::valarray<double>(0.0,main.weights[0][0].size());
        out[0][0] = std::valarray<double>(0.0,outp.weights[0][0].size());
        for(size_t i = 0; i < main.weights.size(); ++i)
        {
            csv[0][i+1] = std::valarray<double>(0.0,main.weights[i].size());
            out[0][i+1] = std::valarray<double>(0.0,outp.weights[i].size());
        }
        for(size_t r = 1; r < out.size(); ++r)
        {
            psm[r-1].resize(main.weights.size());
            psf[r-1].resize(forg.weights.size());
            psi[r-1].resize(inpu.weights.size());
            pso[r-1].resize(outp.weights.size());
            psp[r-1].resize(post.weights.size());
            psm[r-1].resize(main.weights.size());
            otm[r-1].resize(main.weights.size());
            otf[r-1].resize(forg.weights.size());
            oti[r-1].resize(inpu.weights.size());
            oto[r-1].resize(outp.weights.size());
            otn[r-1].resize(main.weights.size());
            inp[r-1].resize(main.weights.size());
            csv[r].resize(1+main.weights.size());
            out[r].resize(1+outp.weights.size()+post.weights.size());
            out[r][0] = x[n][r-1];
            for(size_t i = 0; i < main.weights.size(); ++i)
            {
                inp[r-1][i].resize(out[r][i].size()+out[r-1][i+1].size());
                for(size_t j = 0; j < out[r][i].size(); ++j)
                {
                    inp[r-1][i][j] = out[r][i][j];
                }
                for(size_t j = 0; j < out[r-1][i+1].size(); ++j)
                {
                    inp[r-1][i][j+out[r][i].size()] = out[r-1][i+1][j];
                }
                psm[r-1][i].resize(main.weights[i].size());
                psf[r-1][i].resize(forg.weights[i].size());
                psi[r-1][i].resize(inpu.weights[i].size());
                pso[r-1][i].resize(outp.weights[i].size());
                otm[r-1][i].resize(main.weights[i].size());
                otf[r-1][i].resize(forg.weights[i].size());
                oti[r-1][i].resize(inpu.weights[i].size());
                oto[r-1][i].resize(outp.weights[i].size());
                otn[r-1][i].resize(main.weights[i].size());
                csv[r][i+1].resize(main.weights[i].size());
                out[r][i+1].resize(outp.weights[i].size());
                for(size_t j = 0; j < main.weights[i].size(); ++j)
                {
                    psm[r-1][i][j] = (inp[r-1][i]*main.weights[i][j]).sum();
                    psf[r-1][i][j] = (inp[r-1][i]*forg.weights[i][j]).sum();
                    psi[r-1][i][j] = (inp[r-1][i]*inpu.weights[i][j]).sum();
                    pso[r-1][i][j] = (inp[r-1][i]*outp.weights[i][j]).sum();
                    otm[r-1][i][j] = main.actf(psm[r-1][i][j],main.actfp[i][j]);
                    otf[r-1][i][j] = forg.actf(psf[r-1][i][j],forg.actfp[i][j]);
                    oti[r-1][i][j] = inpu.actf(psi[r-1][i][j],inpu.actfp[i][j]);
                    oto[r-1][i][j] = outp.actf(pso[r-1][i][j],outp.actfp[i][j]);
                    csv[r][i+1][j] = csv[r-1][i+1][j]*otf[r-1][i][j]+otm[r-1][i][j]*oti[r-1][i][j];
                    otn[r-1][i][j] = norf(csv[r][i+1][j]);
                    out[r][i+1][j] = otn[r-1][i][j]*oto[r-1][i][j];
                }
            }
            for(size_t i = 0; i < post.weights.size(); ++i)
            {
                psp[r-1][i].resize(post.weights[i].size());
                out[r][i+outp.weights.size()+1].resize(post.weights[i].size());
                for(size_t j = 0; j < post.weights[i].size(); ++j)
                {
                    psp[r-1][i][j] = (out[r][i+main.weights.size()]*post.weights[i][j]).sum();
                    out[r][i+outp.weights.size()+1][j] = post.actf(psp[r-1][i][j],post.actfp[i][j]);
                }
            }
        }
        for(size_t i = 0; i < gradwm.size(); ++i)
        {
            gradwm[i] = std::vector<std::valarray<double>>(main.weights[i].size(),std::valarray<double>(0.0,main.weights[i][0].size()));
            gradwf[i] = std::vector<std::valarray<double>>(forg.weights[i].size(),std::valarray<double>(0.0,forg.weights[i][0].size()));
            gradwi[i] = std::vector<std::valarray<double>>(inpu.weights[i].size(),std::valarray<double>(0.0,inpu.weights[i][0].size()));
            gradwo[i] = std::vector<std::valarray<double>>(outp.weights[i].size(),std::valarray<double>(0.0,outp.weights[i][0].size()));
            gradpm[i] = std::vector<std::valarray<double>>(main.actfp[i].size(),std::valarray<double>(0.0,main.actfp[i][0].size()));
            gradpf[i] = std::vector<std::valarray<double>>(forg.actfp[i].size(),std::valarray<double>(0.0,forg.actfp[i][0].size()));
            gradpi[i] = std::vector<std::valarray<double>>(inpu.actfp[i].size(),std::valarray<double>(0.0,inpu.actfp[i][0].size()));
            gradpo[i] = std::vector<std::valarray<double>>(outp.actfp[i].size(),std::valarray<double>(0.0,outp.actfp[i][0].size()));
        }
        for(size_t i = 0; i < gradwp.size(); ++i)
        {
            gradwp[i] = std::vector<std::valarray<double>>(post.weights[i].size(),std::valarray<double>(0.0,post.weights[i][0].size()));
            gradpp[i] = std::vector<std::valarray<double>>(post.actfp[i].size(),std::valarray<double>(0.0,post.actfp[i][0].size()));
        }
        erc.back().resize(main.weights.size());
        for(size_t i = 0; i < main.weights.size(); ++i)
        {
            erc.back()[i] = std::valarray<double>(0.0,main.weights[i].size());
        }
        for(size_t r = out.size()-2; r < out.size(); --r)
        {
            erm[r].resize(main.weights.size());
            erf[r].resize(forg.weights.size());
            eri[r].resize(inpu.weights.size());
            ero[r].resize(outp.weights.size());
            erp[r].resize(post.weights.size());
            erc[r].resize(main.weights.size());
            ery[r].resize(outp.weights.size());
            erp[r].back() = y[n][r]-out[r+1].back();
            for(size_t i = post.weights.size()-2; i < post.weights.size(); --i)
            {
                erp[r][i].resize(post.weights[i].size());
                for(size_t j = 0; j < post.weights[i].size(); ++j)
                {
                    erp[r][i][j] = 0.0;
                    for(size_t k = 0; k < post.weights[i+1].size(); ++k)
                    {
                        erp[r][i][j]+= post.weights[i+1][k][j]*post.dactf[0](psp[r][i+1][k],post.actfp[i+1][k])*erp[r][i+1][k];
                    }
                }
            }
            for(size_t i = main.weights.size()-1; i < main.weights.size(); --i)
            {
                ery[r][i].resize(outp.weights[i].size());
                for(size_t j = 0; j < outp.weights.back().size(); ++j)
                {
                    ery[r][i][j] = 0.0;
                    if(i == main.weights.size()-1)
                    {
                        for(size_t k = 0; k < post.weights[0].size(); ++k)
                        {
                            ery[r].back()[j]+= post.weights[0][k][j]*post.dactf[0](psp[r][0][k],post.actfp[0][k])*erp[r][0][k];
                        }
                    }
                    else
                    {
                        for(size_t k = 0; k < main.weights[i+1].size(); ++k)
                        {
                            ery[r][i][j]+= main.weights[i+1][k][j]*main.dactf[0](psm[r][i+1][k],main.actfp[i+1][k])*erm[r][i+1][k];
                            ery[r][i][j]+= forg.weights[i+1][k][j]*forg.dactf[0](psf[r][i+1][k],forg.actfp[i+1][k])*erf[r][i+1][k];
                            ery[r][i][j]+= inpu.weights[i+1][k][j]*inpu.dactf[0](psi[r][i+1][k],inpu.actfp[i+1][k])*eri[r][i+1][k];
                            ery[r][i][j]+= outp.weights[i+1][k][j]*outp.dactf[0](pso[r][i+1][k],outp.actfp[i+1][k])*ero[r][i+1][k];
                        }
                    }
                }
                ero[r][i] = ery[r][i]*otn[r][i];
                erc[r][i].resize(main.weights[i].size());
                for(size_t j = 0; j < erc[r][i].size(); ++j)
                {
                    erc[r][i][j] = dnorf(csv[r+1][i+1][j])*ery[r][i][j]*oto[r][i][j]+erc[r+1][i][j];
                }
                erm[r][i] = erc[r][i]*oti[r][i];
                eri[r][i] = erc[r][i]*otm[r][i];
                erf[r][i] = erc[r][i]*csv[r][i+1];
                erc[r][i]*= otf[r][i];
            }
            for(size_t i = 0; i < main.weights.size(); ++i)
            {
                for(size_t j = 0; j < main.weights[i].size(); ++j)
                {
                    gradwm[i][j]+= erm[r][i][j]*main.dactf[0](psm[r][i][j],main.actfp[i][j])*inp[r][i];
                    gradwf[i][j]+= erf[r][i][j]*forg.dactf[0](psf[r][i][j],forg.actfp[i][j])*inp[r][i];
                    gradwi[i][j]+= eri[r][i][j]*inpu.dactf[0](psi[r][i][j],inpu.actfp[i][j])*inp[r][i];
                    gradwo[i][j]+= ero[r][i][j]*outp.dactf[0](pso[r][i][j],outp.actfp[i][j])*inp[r][i];
                    for(size_t k = 0; k < main.actfp[i][j].size(); ++k)
                    {
                        gradpm[i][j][k]+= erm[r][i][j]*main.dactf[k+1](psm[r][i][j],main.actfp[i][j]);
                    }
                    for(size_t k = 0; k < forg.actfp[i][j].size(); ++k)
                    {
                        gradpf[i][j][k]+= erf[r][i][j]*forg.dactf[k+1](psf[r][i][j],forg.actfp[i][j]);
                    }
                    for(size_t k = 0; k < inpu.actfp[i][j].size(); ++k)
                    {
                        gradpi[i][j][k]+= eri[r][i][j]*inpu.dactf[k+1](psi[r][i][j],inpu.actfp[i][j]);
                    }
                    for(size_t k = 0; k < outp.actfp[i][j].size(); ++k)
                    {
                        gradpo[i][j][k]+= ero[r][i][j]*outp.dactf[k+1](pso[r][i][j],outp.actfp[i][j]);
                    }
                }
            }
            for(size_t i = 0; i < post.weights.size(); ++i)
            {
                for(size_t j = 0; j < post.weights[i].size(); ++j)
                {
                    gradwp[i][j]+= erp[r][i][j]*post.dactf[0](psp[r][i][j],post.actfp[i][j])*out[r+1][i+main.weights.size()];
                    for(size_t k = 0; k < post.actfp[i][j].size(); ++k)
                    {
                        gradpp[i][j][k]+= erp[r][i][j]*post.dactf[k+1](psp[r][i][j],post.actfp[i][j]);
                    }
                }
            }
        }
    }
    for(size_t i = 0; i < main.weights.size(); ++i)
    {
        for(size_t j = 0; j < main.weights[i].size(); ++j)
        {
            main.weights[i][j]+= gradwm[i][j]*(h/((double)(x.size()*x[0].size())));
            forg.weights[i][j]+= gradwf[i][j]*(h/((double)(x.size()*x[0].size())));
            inpu.weights[i][j]+= gradwi[i][j]*(h/((double)(x.size()*x[0].size())));
            outp.weights[i][j]+= gradwo[i][j]*(h/((double)(x.size()*x[0].size())));
            main.actfp[i][j]+= gradpm[i][j]*(h/((double)(x.size()*x[0].size())));
            forg.actfp[i][j]+= gradpf[i][j]*(h/((double)(x.size()*x[0].size())));
            inpu.actfp[i][j]+= gradpi[i][j]*(h/((double)(x.size()*x[0].size())));
            outp.actfp[i][j]+= gradpo[i][j]*(h/((double)(x.size()*x[0].size())));
        }
    }
    for(size_t i = 0; i < post.weights.size(); ++i)
    {
        for(size_t j = 0; j < post.weights[i].size(); ++j)
        {
            post.weights[i][j]+= gradwp[i][j]*(h/((double)(x.size()*x[0].size())));
            post.actfp[i][j]+= gradpp[i][j]*(h/((double)(x.size()*x[0].size())));
        }
    }
}

std::vector<std::valarray<double>> LSTM::compute(const std::vector<std::valarray<double>>& x, size_t n) const
{
    csv.resize(x.size()+n+1);
    out.resize(x.size()+n+1);
    csv[0].resize(1+main.weights.size());
    out[0].resize(1+outp.weights.size());
    csv[0][0] = std::valarray<double>(0.0,main.weights[0][0].size());
    out[0][0] = std::valarray<double>(0.0,outp.weights[0][0].size());
    for(size_t i = 0; i < main.weights.size(); ++i)
    {
        csv[0][i+1] = std::valarray<double>(0.0,main.weights[i].size());
        out[0][i+1] = std::valarray<double>(0.0,outp.weights[i].size());
    }
    for(size_t r = 1; r < out.size(); ++r)
    {
        csv[r].resize(1+main.weights.size());
        out[r].resize(1+outp.weights.size()+post.weights.size());
        out[r][0] = (r <= x.size())?x[r-1]:out[r-1].back();
        for(size_t i = 0; i < main.weights.size(); ++i)
        {
            std::valarray<double> inp(out[r][i].size()+out[r-1][i+1].size());
            for(size_t j = 0; j < out[r][i].size(); ++j)
            {
                inp[j] = out[r][i][j];
            }
            for(size_t j = 0; j < out[r-1][i+1].size(); ++j)
            {
                inp[j+out[r][i].size()] = out[r-1][i+1][j];
            }
            csv[r][i+1].resize(main.weights[i].size());
            out[r][i+1].resize(outp.weights[i].size());
            for(size_t j = 0; j < main.weights[i].size(); ++j)
            {
                csv[r][i+1][j] = csv[r-1][i+1][j]*forg.actf((inp*forg.weights[i][j]).sum(),forg.actfp[i][j])+main.actf((inp*main.weights[i][j]).sum(),main.actfp[i][j])*inpu.actf((inp*inpu.weights[i][j]).sum(),inpu.actfp[i][j]);
                out[r][i+1][j] = norf(csv[r][i+1][j])*outp.actf((inp*outp.weights[i][j]).sum(),outp.actfp[i][j]);
            }
        }
        for(size_t i = 0; i < post.weights.size(); ++i)
        {
            out[r][i+main.weights.size()+1].resize(post.weights[i].size());
            for(size_t j = 0; j < post.weights[i].size(); ++j)
            {
                out[r][i+main.weights.size()+1][j] = post.actf((out[r][i+main.weights.size()]*post.weights[i][j]).sum(),post.actfp[i][j]);
            }
        }
    }
    std::vector<std::valarray<double>> ans(x.size()+n);
    for(size_t i = 0; i < x.size()+n; ++i)
    {
        ans[i] = out[i+1].back();
    }
    return ans;
}

void LSTM::reset(void)
{
    for(size_t i = 0; i < main.weights.size(); ++i)
    {
        for(size_t j = 0; j < main.weights[i].size(); ++j)
        {
            for(size_t k = 0; k < main.weights[i][j].size(); ++k)
            {
                main.weights[i][j][k] = main.weightd(main.weights[i][j].size());
                forg.weights[i][j][k] = forg.weightd(forg.weights[i][j].size());
                inpu.weights[i][j][k] = inpu.weightd(inpu.weights[i][j].size());
                outp.weights[i][j][k] = outp.weightd(outp.weights[i][j].size());
                post.weights[i][j][k] = post.weightd(post.weights[i][j].size());
            }
            main.actfp[i][j] = main.actfpd();
            forg.actfp[i][j] = forg.actfpd();
            inpu.actfp[i][j] = inpu.actfpd();
            outp.actfp[i][j] = outp.actfpd();
        }
    }
    for(size_t i = 0; i < post.weights.size(); ++i)
    {
        for(size_t j = 0; j < post.weights[i].size(); ++j)
        {
            for(size_t k = 0; k < post.weights[i][j].size(); ++k)
            {
                post.weights[i][j][k] = post.weightd(post.weights[i][j].size());
            }
            post.actfp[i][j] = post.actfpd();
        }
    }
}

void LSTM::set(const std::array<std::vector<std::vector<std::valarray<double>>>,5>& w, const std::array<std::vector<std::vector<std::valarray<double>>>,5>& a)
{
    main.weights = w[0];
    forg.weights = w[1];
    inpu.weights = w[2];
    outp.weights = w[2];
    post.weights = w[4];
    main.actfp = a[0];
    forg.actfp = a[1];
    inpu.actfp = a[2];
    outp.actfp = a[3];
    post.actfp = a[4];
}

std::vector<std::valarray<double>> LSTM::getInput(void) const
{
    std::vector<std::valarray<double>> ans(out.size()-1);
    for(size_t k = 1; k < out.size(); ++k)
    {
        ans[k-1] = out[k][0];
    }
    return ans;
}

std::vector<std::valarray<double>> LSTM::getOutput(void) const
{
    std::vector<std::valarray<double>> ans(out.size()-1);
    for(size_t k = 1; k < out.size(); ++k)
    {
        ans[k-1] = out[k].back();
    }
    return ans;
}

std::vector<std::vector<std::valarray<double>>> LSTM::getStates(void) const
{
    return out;
}

std::array<std::vector<std::vector<std::valarray<double>>>,5> LSTM::getWeights(void) const
{
    std::array<std::vector<std::vector<std::valarray<double>>>,5> ans;
    ans[0] = main.weights;
    ans[1] = forg.weights;
    ans[2] = inpu.weights;
    ans[3] = outp.weights;
    ans[4] = post.weights;
    return ans;
}

std::array<std::vector<std::vector<std::valarray<double>>>,5> LSTM::getActfp(void) const
{
    std::array<std::vector<std::vector<std::valarray<double>>>,5> ans;
    ans[0] = main.actfp;
    ans[1] = forg.actfp;
    ans[2] = inpu.actfp;
    ans[3] = outp.actfp;
    ans[4] = post.actfp;
    return ans;
}

void LSTM::reorderLike(const LSTM& n)
{
    for(size_t i = 0; i < (post.weights.size()>0)?main.weights.size():main.weights.size()-1; ++i)
    {
        std::vector<std::valarray<double>> w1(main.weights[i].size()), w2(n.main.weights[i].size());
        for(size_t j = 0; j < main.weights[i].size(); ++j)
        {
            w1[j].resize(main.weights[i][j].size()<<2);
            w2[j].resize(n.main.weights[i][j].size()<<2);
            for(size_t k = 0; k < main.weights[i][j].size(); ++k)
            {
                w1[j][k] = main.weights[i][j][k];
                w1[j][k+forg.weights[i].size()] = forg.weights[i][j][k];
                w1[j][k+(inpu.weights[i].size()<<1)] = inpu.weights[i][j][k];
                w1[j][k+(outp.weights[i].size()*3)] = outp.weights[i][j][k];
                w2[j][k] = n.main.weights[i][j][k];
                w2[j][k+n.forg.weights[i].size()] = n.forg.weights[i][j][k];
                w2[j][k+(n.inpu.weights[i].size()<<1)] = n.inpu.weights[i][j][k];
                w2[j][k+(n.outp.weights[i].size()*3)] = n.outp.weights[i][j][k];
            }
        }
        std::vector<size_t> no = orderLike(w1,w2);
        reorder(main.weights[i],no);
        reorder(forg.weights[i],no);
        reorder(inpu.weights[i],no);
        reorder(outp.weights[i],no);
        reorder(main.actfp[i],no);
        reorder(forg.actfp[i],no);
        reorder(inpu.actfp[i],no);
        reorder(outp.actfp[i],no);
        if(i == main.weights.size()-1)
        {
            for(size_t j = 0; j < post.weights[0].size(); ++j)
            {
                reorder(post.weights[0][j],no);
            }
        }
        else
        {
            for(size_t j = 0; j < main.weights[i+1].size(); ++j)
            {
                reorder(main.weights[i+1][j],no);
                reorder(forg.weights[i+1][j],no);
                reorder(inpu.weights[i+1][j],no);
                reorder(outp.weights[i+1][j],no);
            }
        }
    }
    for(size_t i = 0; i < post.weights.size()-1; ++i)
    {
        std::vector<size_t> no = orderLike(post.weights[i],n.post.weights[i]);
        reorder(post.weights[i],no);
        reorder(post.actfp[i],no);
        for(size_t j = 0; j < post.weights[i+1].size(); ++j)
        {
            reorder(post.weights[i+1][j],no);
        }
    }
}

void LSTM::reorderLikeModel(const LSTM& n, size_t m)
{
    for(size_t i = 0; i < (post.weights.size()>0)?main.weights.size():main.weights.size()-1; ++i)
    {
        std::vector<std::valarray<double>> w1(main.weights[i].size()), w2(n.main.weights[i].size());
        for(size_t j = 0; j < main.weights[i].size(); ++j)
        {
            w1[j].resize(main.weights[i][j].size()<<2);
            w2[j].resize(n.main.weights[i][j].size()<<2);
            for(size_t k = 0; k < main.weights[i][j].size(); ++k)
            {
                w1[j][k] = main.weights[i][j][k];
                w1[j][k+forg.weights[i].size()] = forg.weights[i][j][k];
                w1[j][k+(inpu.weights[i].size()<<1)] = inpu.weights[i][j][k];
                w1[j][k+(outp.weights[i].size()*3)] = outp.weights[i][j][k];
                w2[j][k] = n.main.weights[i][j][k];
                w2[j][k+n.forg.weights[i].size()] = n.forg.weights[i][j][k];
                w2[j][k+(n.inpu.weights[i].size()<<1)] = n.inpu.weights[i][j][k];
                w2[j][k+(n.outp.weights[i].size()*3)] = n.outp.weights[i][j][k];
            }
        }
        std::vector<size_t> no = orderLikePartial(w1,sliceBeg[m][i+1],w2,n.sliceBeg.at(m)[i+1],sliceSize[m][i+1]);
        reorderPartial(main.weights[i],sliceBeg[m][i+1],no);
        reorderPartial(forg.weights[i],sliceBeg[m][i+1],no);
        reorderPartial(inpu.weights[i],sliceBeg[m][i+1],no);
        reorderPartial(outp.weights[i],sliceBeg[m][i+1],no);
        reorderPartial(main.actfp[i],sliceBeg[m][i+1],no);
        reorderPartial(forg.actfp[i],sliceBeg[m][i+1],no);
        reorderPartial(inpu.actfp[i],sliceBeg[m][i+1],no);
        reorderPartial(outp.actfp[i],sliceBeg[m][i+1],no);
        if(i == main.weights.size()-1)
        {
            for(size_t j = 0; j < post.weights[0].size(); ++j)
            {
                reorderPartial(post.weights[0][j],sliceBeg[m][i+1],no);
            }
        }
        else
        {
            for(size_t j = 0; j < main.weights[i+1].size(); ++j)
            {
                reorderPartial(main.weights[i+1][j],sliceBeg[m][i+1],no);
                reorderPartial(forg.weights[i+1][j],sliceBeg[m][i+1],no);
                reorderPartial(inpu.weights[i+1][j],sliceBeg[m][i+1],no);
                reorderPartial(outp.weights[i+1][j],sliceBeg[m][i+1],no);
            }
        }
    }
    for(size_t i = 0; i < post.weights.size()-1; ++i)
    {
        std::vector<size_t> no = orderLikePartial(post.weights[i],sliceBegp[m][i],n.post.weights[i],n.sliceBegp.at(m)[i],sliceSizep[m][i]);
        reorderPartial(post.weights[i],sliceBegp[m][i],no);
        reorderPartial(post.actfp[i],sliceBegp[m][i],no);
        for(size_t j = 0; j < post.weights[i+1].size(); ++j)
        {
            reorderPartial(post.weights[i+1][j],sliceBegp[m][i],no);
        }
    }
}

void LSTM::reorderLikeModelDef(const LSTM& n, size_t m)
{
    for(size_t i = 0; i < (post.weights.size()>0)?main.weights.size():main.weights.size()-1; ++i)
    {
        std::vector<std::valarray<double>> w1(main.weights[i].size()), w2(n.main.weights[i].size());
        for(size_t j = 0; j < main.weights[i].size(); ++j)
        {
            w1[j].resize(main.weights[i][j].size()<<2);
            w2[j].resize(n.main.weights[i][j].size()<<2);
            for(size_t k = 0; k < main.weights[i][j].size(); ++k)
            {
                w1[j][k] = main.weights[i][j][k];
                w1[j][k+forg.weights[i].size()] = forg.weights[i][j][k];
                w1[j][k+(inpu.weights[i].size()<<1)] = inpu.weights[i][j][k];
                w1[j][k+(outp.weights[i].size()*3)] = outp.weights[i][j][k];
                w2[j][k] = n.main.weights[i][j][k];
                w2[j][k+n.forg.weights[i].size()] = n.forg.weights[i][j][k];
                w2[j][k+(n.inpu.weights[i].size()<<1)] = n.inpu.weights[i][j][k];
                w2[j][k+(n.outp.weights[i].size()*3)] = n.outp.weights[i][j][k];
            }
        }
        std::vector<size_t> no = orderLikePartialDefFull(w1,sliceBeg[m][i+1],w2,n.sliceBeg.at(m)[i+1],sliceSize[m][i+1]);
        reorderPartial(main.weights[i],sliceBeg[m][i+1],no);
        reorderPartial(forg.weights[i],sliceBeg[m][i+1],no);
        reorderPartial(inpu.weights[i],sliceBeg[m][i+1],no);
        reorderPartial(outp.weights[i],sliceBeg[m][i+1],no);
        reorderPartial(main.actfp[i],sliceBeg[m][i+1],no);
        reorderPartial(forg.actfp[i],sliceBeg[m][i+1],no);
        reorderPartial(inpu.actfp[i],sliceBeg[m][i+1],no);
        reorderPartial(outp.actfp[i],sliceBeg[m][i+1],no);
        if(i == main.weights.size()-1)
        {
            for(size_t j = 0; j < post.weights[0].size(); ++j)
            {
                reorderPartial(post.weights[0][j],sliceBeg[m][i+1],no);
            }
        }
        else
        {
            for(size_t j = 0; j < main.weights[i+1].size(); ++j)
            {
                reorderPartial(main.weights[i+1][j],sliceBeg[m][i+1],no);
                reorderPartial(forg.weights[i+1][j],sliceBeg[m][i+1],no);
                reorderPartial(inpu.weights[i+1][j],sliceBeg[m][i+1],no);
                reorderPartial(outp.weights[i+1][j],sliceBeg[m][i+1],no);
            }
        }
    }
    for(size_t i = 0; i < post.weights.size()-1; ++i)
    {
        std::vector<size_t> no = orderLikePartialDefFull(post.weights[i],sliceBegp[m][i],n.post.weights[i],n.sliceBegp.at(m)[i],sliceSizep[m][i]);
        reorderPartial(post.weights[i],sliceBegp[m][i],no);
        reorderPartial(post.actfp[i],sliceBegp[m][i],no);
        for(size_t j = 0; j < post.weights[i+1].size(); ++j)
        {
            reorderPartial(post.weights[i+1][j],sliceBegp[m][i],no);
        }
    }
}

std::pair<std::vector<size_t>,std::vector<size_t>> LSTM::layersSize(void) const
{
    std::pair<std::vector<size_t>,std::vector<size_t>> ans;
    ans.first.resize(main.weights.size()+1);
    ans.second.resize(post.weights.size());
    ans.first[0] = main.weights[0][0].size();
    for(size_t i = 0; i < main.weights.size(); ++i)
    {
        ans.first[i+1] = main.weights[i].size();
    }
    for(size_t i = 0; i < post.weights.size(); ++i)
    {
        ans.second[i] = post.weights[i].size();
    }
    return ans;
}

void LSTM::resize(const std::vector<size_t>& l, const std::vector<size_t>& lp)
{
    std::vector<std::vector<std::valarray<double>>> tweights(main.weights), t2weights(forg.weights), t3weights(inpu.weights), t4weights(outp.weights), tactfp(main.actfp), t2actfp(forg.actfp), t3actfp(inpu.actfp), t4actfp(outp.actfp);
    main.weights = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    forg.weights = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    inpu.weights = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    outp.weights = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    main.actfp = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    forg.actfp = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    inpu.actfp = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    outp.actfp = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    for(size_t i = 1; i < l.size(); ++i)
    {
        main.weights[i-1] = std::vector<std::valarray<double>>(l[i]);
        forg.weights[i-1] = std::vector<std::valarray<double>>(l[i]);
        inpu.weights[i-1] = std::vector<std::valarray<double>>(l[i]);
        outp.weights[i-1] = std::vector<std::valarray<double>>(l[i]);
        main.actfp[i-1] = std::vector<std::valarray<double>>(l[i]);
        forg.actfp[i-1] = std::vector<std::valarray<double>>(l[i]);
        inpu.actfp[i-1] = std::vector<std::valarray<double>>(l[i]);
        outp.actfp[i-1] = std::vector<std::valarray<double>>(l[i]);
        for(size_t j = 0; j < l[i]; ++j)
        {
            main.weights[i-1][j] = std::valarray<double>(l[i-1]);
            forg.weights[i-1][j] = std::valarray<double>(l[i-1]);
            inpu.weights[i-1][j] = std::valarray<double>(l[i-1]);
            outp.weights[i-1][j] = std::valarray<double>(l[i-1]);
            for(size_t k = 0; k < l[i-1]; ++k)
            {
                if(((i-1) < tweights.size()) && (j < tweights[i-1].size()) && (k < tweights[i-1][j].size()))
                {
                    main.weights[i-1][j][k] = tweights[i-1][j][k];
                    forg.weights[i-1][j][k] = t2weights[i-1][j][k];
                    inpu.weights[i-1][j][k] = t3weights[i-1][j][k];
                    outp.weights[i-1][j][k] = t4weights[i-1][j][k];
                }
                else
                {
                    main.weights[i-1][j][k] = main.weightd(l[i-1]);
                    forg.weights[i-1][j][k] = forg.weightd(l[i-1]);
                    inpu.weights[i-1][j][k] = inpu.weightd(l[i-1]);
                    outp.weights[i-1][j][k] = outp.weightd(l[i-1]);
                }
            }
            if(((i-1) < tactfp.size()) && (j < tactfp[i-1].size()))
            {
                main.actfp[i-1][j] = tactfp[i-1][j];
                forg.actfp[i-1][j] = t2actfp[i-1][j];
                inpu.actfp[i-1][j] = t3actfp[i-1][j];
                outp.actfp[i-1][j] = t4actfp[i-1][j];
            }
            else
            {
                main.actfp[i-1][j] = main.actfpd();
                forg.actfp[i-1][j] = forg.actfpd();
                inpu.actfp[i-1][j] = inpu.actfpd();
                outp.actfp[i-1][j] = outp.actfpd();
            }
        }
    }
    tweights = post.weights;
    tactfp = post.actfp;
    post.weights = std::vector<std::vector<std::valarray<double>>>(lp.size());
    post.actfp = std::vector<std::vector<std::valarray<double>>>(lp.size());
    for(size_t i = 0; i < lp.size(); ++i)
    {
        post.weights[i] = std::vector<std::valarray<double>>(lp[i]);
        post.actfp[i] = std::vector<std::valarray<double>>(lp[i]);
        for(size_t j = 0; j < lp[i]; ++j)
        {
            post.weights[i][j] = std::valarray<double>((i == 0)?l.back():lp[i-1]);
            for(size_t k = 0; k < lp[i]; ++k)
            {
                if((i < tweights.size()) && (j < tweights[i].size()) && (k < tweights[i][j].size()))
                {
                    post.weights[i][j][k] = tweights[i][j][k];
                }
                else
                {
                    post.weights[i][j][k] = post.weightd(lp[i]);
                }
            }
            if((i < tactfp.size()) && (j < tactfp[i].size()))
            {
                post.actfp[i][j] = tactfp[i][j];
            }
            else
            {
                post.actfp[i][j] = post.actfpd();
            }
        }
    }
    csv.resize(0);
    out.resize(0);
}

void LSTM::resizeRelative(const std::vector<int>& lr, const std::vector<int>& lpr)
{
    std::vector<size_t> l(lr.size()), lp(lpr.size());
    l[0] = main.weights[0][0].size()+lr[0];
    for(size_t i = 0; i < main.weights.size(); ++i)
    {
        l[i+1] = main.weights[i].size()+lr[i+1];
    }
    for(size_t i = 0; i < lr.size(); ++i)
    {
        lp[i] = post.weights[i].size()+lpr[i];
    }
    resize(l,lp);
}

void LSTM::addModel(size_t n, const std::vector<size_t>& sb, const std::vector<size_t>& ss, const std::vector<size_t>& sbpv, const std::vector<size_t>& sspv, const std::list<size_t>& deps)
{
    std::vector<size_t> sbp(sbpv.size()+1), ssp(sspv.size()+1);
    for(size_t i = sbpv.size(); i > 0; --i)
    {
        sbp[i] = sbpv[i-1];
        ssp[i] = sspv[i-1];
    }
    sbp[0] = sb.back();
    ssp[0] = ss.back();
    sliceBeg[n] = sb;
    sliceBegp[n] = sbp;
    sliceSize[n] = ss;
    sliceSizep[n] = ssp;
    sliceDepBeg[n].resize(main.weights.size()+1);
    sliceDepBegp[n].resize(post.weights.size()+1);
    sliceDepSize[n].resize(main.weights.size()+1);
    sliceDepSizep[n].resize(post.weights.size()+1);
    for(const auto& d : deps)
    {
        for(size_t i = 0; i < sliceBeg[d].size(); ++i)
        {
            sliceDepBeg[n][i].push_back(sliceBeg[d][i]);
            sliceDepSize[n][i].push_back(sliceSize[d][i]);
        }
        for(size_t i = 0; i < sliceBegp[d].size(); ++i)
        {
            sliceDepBegp[n][i].push_back(sliceBegp[d][i]);
            sliceDepSizep[n][i].push_back(sliceSizep[d][i]);
        }
    }
    for(size_t i = 0; i < sliceDepBeg[n].size(); ++i)
    {
        sliceDepBeg[n][i].push_back(sb[i]);
        sliceDepSize[n][i].push_back(ss[i]);
    }
    for(size_t i = 0; i < sliceDepBegp[n].size(); ++i)
    {
        sliceDepBegp[n][i].push_back(sbp[i]);
        sliceDepSizep[n][i].push_back(ssp[i]);
    }
}

void LSTM::addModelLast(size_t n, const std::vector<size_t>& ss, const std::vector<size_t>& ssp, const std::list<size_t>& deps)
{
    std::vector<size_t> sb(ss.size()), sbp(ssp.size());
    for(size_t i = 0; i < sb.size(); ++i)
    {
        size_t b = 0, mb = std::numeric_limits<size_t>::max();
        for(const auto& m : sliceBeg)
        {
            if(m.second[i] >= b)
            {
                mb = m.first;
                b = m.second[i];
            }
        }
        if(mb == std::numeric_limits<size_t>::max())
        {
            sb[i] = 0;
        }
        else
        {
            sb[i] = b+sliceSize[mb][i];
        }
    }
    for(size_t i = 0; i < sbp.size(); ++i)
    {
        size_t b = 0, mb = std::numeric_limits<size_t>::max();
        for(const auto& m : sliceBegp)
        {
            if(m.second[i] >= b)
            {
                mb = m.first;
                b = m.second[i];
            }
        }
        if(mb == std::numeric_limits<size_t>::max())
        {
            sbp[i] = 0;
        }
        else
        {
            sbp[i] = b+sliceSizep[mb][i];
        }
    }
    addModel(n,sb,ss,sbp,ssp,deps);
}

void LSTM::rmModel(size_t n)
{
    sliceBeg.erase(n);
    sliceBegp.erase(n);
    sliceSize.erase(n);
    sliceSizep.erase(n);
    sliceDepBeg.erase(n);
    sliceDepBegp.erase(n);
    sliceDepSize.erase(n);
    sliceDepSizep.erase(n);
}
    
bool LSTM::hasModel(size_t m) const
{
    return (sliceBeg.find(m) != sliceBeg.end());
}
    
std::unordered_set<size_t> LSTM::models(void) const
{
    std::unordered_set<size_t> ans;
    ans.reserve(sliceBeg.size());
    for(const auto& m : sliceBeg)
    {
        ans.insert(m.first);
    }
    return ans;
}

std::pair<std::pair<std::vector<size_t>,std::vector<size_t>>,std::pair<std::vector<size_t>,std::vector<size_t>>> LSTM::modelPos(size_t m) const
{
    return std::pair<std::pair<std::vector<size_t>,std::vector<size_t>>,std::pair<std::vector<size_t>,std::vector<size_t>>>(std::pair<std::vector<size_t>,std::vector<size_t>>(sliceBeg.at(m),sliceSize.at(m)),std::pair<std::vector<size_t>,std::vector<size_t>>(sliceBegp.at(m),sliceSizep.at(m)));
}

void reorderSim(std::vector<LSTM*> n)
{
    for(size_t i = 0; i < (n[0]->post.weights.size()>0)?n[0]->main.weights.size():n[0]->main.weights.size()-1; ++i)
    {
        std::vector<std::valarray<double>> wr(n[0]->main.weights[i].size());
        for(size_t j = 0; j < n[0]->main.weights[i].size(); ++j)
        {
            wr[j].resize(n[0]->main.weights[i][j].size()<<2);
            for(size_t k = 0; k < n[0]->main.weights[i][j].size(); ++k)
            {
                wr[j][k] = n[0]->main.weights[i][j][k];
                wr[j][k+n[0]->forg.weights[i].size()] = n[0]->forg.weights[i][j][k];
                wr[j][k+(n[0]->inpu.weights[i].size()<<1)] = n[0]->inpu.weights[i][j][k];
                wr[j][k+(n[0]->outp.weights[i].size()*3)] = n[0]->outp.weights[i][j][k];
            }
        }
        for(size_t v = 1; v < n.size(); ++v)
        {
            std::vector<std::valarray<double>> w(n[v]->main.weights[i].size());
            for(size_t j = 0; j < n[v]->main.weights[i].size(); ++j)
            {
                w[j].resize(n[v]->main.weights[i][j].size()<<2);
                for(size_t k = 0; k < n[v]->main.weights[i][j].size(); ++k)
                {
                    w[j][k] = n[v]->main.weights[i][j][k];
                    w[j][k+n[v]->forg.weights[i].size()] = n[v]->forg.weights[i][j][k];
                    w[j][k+(n[v]->inpu.weights[i].size()<<1)] = n[v]->inpu.weights[i][j][k];
                    w[j][k+(n[v]->outp.weights[i].size()*3)] = n[v]->outp.weights[i][j][k];
                }
            }
            std::vector<size_t> no = orderLike(w,wr);
            reorder(n[v]->main.weights[i],no);
            reorder(n[v]->forg.weights[i],no);
            reorder(n[v]->inpu.weights[i],no);
            reorder(n[v]->outp.weights[i],no);
            reorder(n[v]->main.actfp[i],no);
            reorder(n[v]->forg.actfp[i],no);
            reorder(n[v]->inpu.actfp[i],no);
            reorder(n[v]->outp.actfp[i],no);
            if(i == n[v]->main.weights.size()-1)
            {
                for(size_t j = 0; j < n[v]->post.weights[0].size(); ++j)
                {
                    reorder(n[v]->post.weights[0][j],no);
                }
            }
            else
            {
                for(size_t j = 0; j < n[v]->main.weights[i+1].size(); ++j)
                {
                    reorder(n[v]->main.weights[i+1][j],no);
                    reorder(n[v]->forg.weights[i+1][j],no);
                    reorder(n[v]->inpu.weights[i+1][j],no);
                    reorder(n[v]->outp.weights[i+1][j],no);
                }
            }
        }
    }
    for(size_t v = 1; v < n.size(); ++v)
    {
        for(size_t i = 0; i < n[v]->post.weights.size()-1; ++i)
        {
            std::vector<size_t> no = orderLike(n[v]->post.weights[i],n[0]->post.weights[i]);
            reorder(n[v]->post.weights[i],no);
            reorder(n[v]->post.actfp[i],no);
            for(size_t j = 0; j < n[v]->post.weights[i+1].size(); ++j)
            {
                reorder(n[v]->post.weights[i+1][j],no);
            }
        }
    }
}

void reorderSimModel(std::vector<LSTM*> n, size_t m)
{
    for(size_t i = 0; i < (n[0]->post.weights.size()>0)?n[0]->main.weights.size():n[0]->main.weights.size()-1; ++i)
    {
        std::vector<std::valarray<double>> wr(n[0]->main.weights[i].size());
        for(size_t j = 0; j < n[0]->main.weights[i].size(); ++j)
        {
            wr[j].resize(n[0]->main.weights[i][j].size()<<2);
            for(size_t k = 0; k < n[0]->main.weights[i][j].size(); ++k)
            {
                wr[j][k] = n[0]->main.weights[i][j][k];
                wr[j][k+n[0]->forg.weights[i].size()] = n[0]->forg.weights[i][j][k];
                wr[j][k+(n[0]->inpu.weights[i].size()<<1)] = n[0]->inpu.weights[i][j][k];
                wr[j][k+(n[0]->outp.weights[i].size()*3)] = n[0]->outp.weights[i][j][k];
            }
        }
        for(size_t v = 1; v < n.size(); ++v)
        {
            std::vector<std::valarray<double>> w(n[v]->main.weights[i].size());
            for(size_t j = 0; j < n[v]->main.weights[i].size(); ++j)
            {
                w[j].resize(n[v]->main.weights[i][j].size()<<2);
                for(size_t k = 0; k < n[v]->main.weights[i][j].size(); ++k)
                {
                    w[j][k] = n[v]->main.weights[i][j][k];
                    w[j][k+n[v]->forg.weights[i].size()] = n[v]->forg.weights[i][j][k];
                    w[j][k+(n[v]->inpu.weights[i].size()<<1)] = n[v]->inpu.weights[i][j][k];
                    w[j][k+(n[v]->outp.weights[i].size()*3)] = n[v]->outp.weights[i][j][k];
                }
            }
            std::vector<size_t> no = orderLikePartial(w,n[v]->sliceBeg[m][i+1],wr,n[0]->sliceBeg[m][i+1],n[v]->sliceSize[m][i+1]);
            reorderPartial(n[v]->main.weights[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->forg.weights[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->inpu.weights[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->outp.weights[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->main.actfp[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->forg.actfp[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->inpu.actfp[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->outp.actfp[i],n[v]->sliceBeg[m][i+1],no);
            if(i == n[v]->main.weights.size()-1)
            {
                for(size_t j = 0; j < n[v]->post.weights[0].size(); ++j)
                {
                    reorderPartial(n[v]->post.weights[0][j],n[v]->sliceBeg[m][i+1],no);
                }
            }
            else
            {
                for(size_t j = 0; j < n[v]->main.weights[i+1].size(); ++j)
                {
                    reorderPartial(n[v]->main.weights[i+1][j],n[v]->sliceBeg[m][i+1],no);
                    reorderPartial(n[v]->forg.weights[i+1][j],n[v]->sliceBeg[m][i+1],no);
                    reorderPartial(n[v]->inpu.weights[i+1][j],n[v]->sliceBeg[m][i+1],no);
                    reorderPartial(n[v]->outp.weights[i+1][j],n[v]->sliceBeg[m][i+1],no);
                }
            }
        }
    }
    for(size_t v = 1; v < n.size(); ++v)
    {
        for(size_t i = 0; i < n[v]->post.weights.size()-1; ++i)
        {
            std::vector<size_t> no = orderLikePartial(n[v]->post.weights[i],n[v]->sliceBegp[m][i],n[0]->post.weights[i],n[0]->sliceBegp[m][i],n[v]->sliceSizep[m][i]);
            reorderPartial(n[v]->post.weights[i],n[v]->sliceBegp[m][i],no);
            reorderPartial(n[v]->post.actfp[i],n[v]->sliceBegp[m][i],no);
            for(size_t j = 0; j < n[v]->post.weights[i+1].size(); ++j)
            {
                reorderPartial(n[v]->post.weights[i+1][j],n[v]->sliceBegp[m][i],no);
            }
        }
    }
}

void reorderSimModelDef(std::vector<LSTM*> n, size_t m)
{
    for(size_t i = 0; i < (n[0]->post.weights.size()>0)?n[0]->main.weights.size():n[0]->main.weights.size()-1; ++i)
    {
        std::vector<std::valarray<double>> wr(n[0]->main.weights[i].size());
        for(size_t j = 0; j < n[0]->main.weights[i].size(); ++j)
        {
            wr[j].resize(n[0]->main.weights[i][j].size()<<2);
            for(size_t k = 0; k < n[0]->main.weights[i][j].size(); ++k)
            {
                wr[j][k] = n[0]->main.weights[i][j][k];
                wr[j][k+n[0]->forg.weights[i].size()] = n[0]->forg.weights[i][j][k];
                wr[j][k+(n[0]->inpu.weights[i].size()<<1)] = n[0]->inpu.weights[i][j][k];
                wr[j][k+(n[0]->outp.weights[i].size()*3)] = n[0]->outp.weights[i][j][k];
            }
        }
        for(size_t v = 1; v < n.size(); ++v)
        {
            std::vector<std::valarray<double>> w(n[v]->main.weights[i].size());
            for(size_t j = 0; j < n[v]->main.weights[i].size(); ++j)
            {
                w[j].resize(n[v]->main.weights[i][j].size()<<2);
                for(size_t k = 0; k < n[v]->main.weights[i][j].size(); ++k)
                {
                    w[j][k] = n[v]->main.weights[i][j][k];
                    w[j][k+n[v]->forg.weights[i].size()] = n[v]->forg.weights[i][j][k];
                    w[j][k+(n[v]->inpu.weights[i].size()<<1)] = n[v]->inpu.weights[i][j][k];
                    w[j][k+(n[v]->outp.weights[i].size()*3)] = n[v]->outp.weights[i][j][k];
                }
            }
            std::vector<size_t> no = orderLikePartialDefFull(w,n[v]->sliceBeg[m][i+1],wr,n[0]->sliceBeg[m][i+1],n[v]->sliceSize[m][i+1]);
            reorderPartial(n[v]->main.weights[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->forg.weights[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->inpu.weights[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->outp.weights[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->main.actfp[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->forg.actfp[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->inpu.actfp[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->outp.actfp[i],n[v]->sliceBeg[m][i+1],no);
            if(i == n[v]->main.weights.size()-1)
            {
                for(size_t j = 0; j < n[v]->post.weights[0].size(); ++j)
                {
                    reorderPartial(n[v]->post.weights[0][j],n[v]->sliceBeg[m][i+1],no);
                }
            }
            else
            {
                for(size_t j = 0; j < n[v]->main.weights[i+1].size(); ++j)
                {
                    reorderPartial(n[v]->main.weights[i+1][j],n[v]->sliceBeg[m][i+1],no);
                    reorderPartial(n[v]->forg.weights[i+1][j],n[v]->sliceBeg[m][i+1],no);
                    reorderPartial(n[v]->inpu.weights[i+1][j],n[v]->sliceBeg[m][i+1],no);
                    reorderPartial(n[v]->outp.weights[i+1][j],n[v]->sliceBeg[m][i+1],no);
                }
            }
        }
    }
    for(size_t v = 1; v < n.size(); ++v)
    {
        for(size_t i = 0; i < n[v]->post.weights.size()-1; ++i)
        {
            std::vector<size_t> no = orderLikePartialDefFull(n[v]->post.weights[i],n[v]->sliceBegp[m][i],n[0]->post.weights[i],n[0]->sliceBegp[m][i],n[v]->sliceSizep[m][i]);
            reorderPartial(n[v]->post.weights[i],n[v]->sliceBegp[m][i],no);
            reorderPartial(n[v]->post.actfp[i],n[v]->sliceBegp[m][i],no);
            for(size_t j = 0; j < n[v]->post.weights[i+1].size(); ++j)
            {
                reorderPartial(n[v]->post.weights[i+1][j],n[v]->sliceBegp[m][i],no);
            }
        }
    }
}

void average(LSTM& n1, LSTM& n2)
{
    for(size_t i = 0; i < n1.main.weights.size(); ++i)
    {
        averageDual(n1.main.weights[i],n1.main.actfp[i],n2.main.weights[i],n2.main.actfp[i]);
        averageDual(n1.forg.weights[i],n1.forg.actfp[i],n2.forg.weights[i],n2.forg.actfp[i]);
        averageDual(n1.inpu.weights[i],n1.inpu.actfp[i],n2.inpu.weights[i],n2.inpu.actfp[i]);
        averageDual(n1.outp.weights[i],n1.outp.actfp[i],n2.outp.weights[i],n2.outp.actfp[i]);
    }
    for(size_t i = 0; i < n1.post.weights.size(); ++i)
    {
        averageDual(n1.post.weights[i],n1.post.actfp[i],n2.post.weights[i],n2.post.actfp[i]);
    }
}

void averageModel(LSTM& n1, LSTM& n2, size_t m)
{
    for(size_t i = 0; i < n1.main.weights.size(); ++i)
    {
        averagePartialDual(n1.main.weights[i],n1.main.actfp[i],n1.sliceBeg[m][i+1],n1.sliceDepBeg[m][i],n2.main.weights[i],n2.main.actfp[i],n2.sliceBeg[m][i+1],n2.sliceDepBeg[m][i],n1.sliceSize[m][i+1],n1.sliceDepSize[m][i]);
        averagePartialDual(n1.forg.weights[i],n1.forg.actfp[i],n1.sliceBeg[m][i+1],n1.sliceDepBeg[m][i],n2.forg.weights[i],n2.forg.actfp[i],n2.sliceBeg[m][i+1],n2.sliceDepBeg[m][i],n1.sliceSize[m][i+1],n1.sliceDepSize[m][i]);
        averagePartialDual(n1.inpu.weights[i],n1.inpu.actfp[i],n1.sliceBeg[m][i+1],n1.sliceDepBeg[m][i],n2.inpu.weights[i],n2.inpu.actfp[i],n2.sliceBeg[m][i+1],n2.sliceDepBeg[m][i],n1.sliceSize[m][i+1],n1.sliceDepSize[m][i]);
        averagePartialDual(n1.outp.weights[i],n1.outp.actfp[i],n1.sliceBeg[m][i+1],n1.sliceDepBeg[m][i],n2.outp.weights[i],n2.outp.actfp[i],n2.sliceBeg[m][i+1],n2.sliceDepBeg[m][i],n1.sliceSize[m][i+1],n1.sliceDepSize[m][i]);
        averagePartialExt(n1.main.weights[i],n1.sliceDepBeg[m][i+1],n1.sliceBeg[m][i],n2.main.weights[i],n2.sliceDepBeg[m][i+1],n2.sliceBeg[m][i],n1.sliceDepSize[m][i+1],n1.sliceSize[m][i]);
        averagePartialExt(n1.forg.weights[i],n1.sliceDepBeg[m][i+1],n1.sliceBeg[m][i],n2.forg.weights[i],n2.sliceDepBeg[m][i+1],n2.sliceBeg[m][i],n1.sliceDepSize[m][i+1],n1.sliceSize[m][i]);
        averagePartialExt(n1.inpu.weights[i],n1.sliceDepBeg[m][i+1],n1.sliceBeg[m][i],n2.inpu.weights[i],n2.sliceDepBeg[m][i+1],n2.sliceBeg[m][i],n1.sliceDepSize[m][i+1],n1.sliceSize[m][i]);
        averagePartialExt(n1.outp.weights[i],n1.sliceDepBeg[m][i+1],n1.sliceBeg[m][i],n2.outp.weights[i],n2.sliceDepBeg[m][i+1],n2.sliceBeg[m][i],n1.sliceDepSize[m][i+1],n1.sliceSize[m][i]);
    }
    for(size_t i = 0; i < n1.post.weights.size(); ++i)
    {
        averagePartialDual(n1.post.weights[i],n1.post.actfp[i],n1.sliceBegp[m][i+1],n1.sliceDepBegp[m][i],n2.post.weights[i],n2.post.actfp[i],n2.sliceBegp[m][i+1],n2.sliceDepBegp[m][i],n1.sliceSizep[m][i+1],n1.sliceDepSizep[m][i]);
        averagePartialExt(n1.post.weights[i],n1.sliceDepBegp[m][i+1],n1.sliceBegp[m][i],n2.post.weights[i],n2.sliceDepBegp[m][i+1],n2.sliceBegp[m][i],n1.sliceDepSizep[m][i+1],n1.sliceSizep[m][i]);
    }
}

void averageModelCross(LSTM& n1, LSTM& n2, size_t m1, size_t m2)
{
    for(size_t i = 0; i < n1.main.weights.size(); ++i)
    {
        averagePartialX(n1.main.weights[i],n1.sliceBeg[m1][i+1],n1.sliceBeg[m2][i],n2.main.weights[i],n2.sliceBeg[m1][i+1],n2.sliceBeg[m1][i],n1.sliceSize[m1][i+1],n1.sliceSize[m2][i]);
        averagePartialX(n1.forg.weights[i],n1.sliceBeg[m1][i+1],n1.sliceBeg[m2][i],n2.forg.weights[i],n2.sliceBeg[m1][i+1],n2.sliceBeg[m1][i],n1.sliceSize[m1][i+1],n1.sliceSize[m2][i]);
        averagePartialX(n1.inpu.weights[i],n1.sliceBeg[m1][i+1],n1.sliceBeg[m2][i],n2.inpu.weights[i],n2.sliceBeg[m1][i+1],n2.sliceBeg[m1][i],n1.sliceSize[m1][i+1],n1.sliceSize[m2][i]);
        averagePartialX(n1.outp.weights[i],n1.sliceBeg[m1][i+1],n1.sliceBeg[m2][i],n2.outp.weights[i],n2.sliceBeg[m1][i+1],n2.sliceBeg[m1][i],n1.sliceSize[m1][i+1],n1.sliceSize[m2][i]);
        averagePartialX(n1.main.weights[i],n1.sliceBeg[m1][i],n1.sliceBeg[m2][i+1],n2.main.weights[i],n2.sliceBeg[m1][i],n2.sliceBeg[m1][i+1],n1.sliceSize[m1][i],n1.sliceSize[m2][i+1]);
        averagePartialX(n1.forg.weights[i],n1.sliceBeg[m1][i],n1.sliceBeg[m2][i+1],n2.forg.weights[i],n2.sliceBeg[m1][i],n2.sliceBeg[m1][i+1],n1.sliceSize[m1][i],n1.sliceSize[m2][i+1]);
        averagePartialX(n1.inpu.weights[i],n1.sliceBeg[m1][i],n1.sliceBeg[m2][i+1],n2.inpu.weights[i],n2.sliceBeg[m1][i],n2.sliceBeg[m1][i+1],n1.sliceSize[m1][i],n1.sliceSize[m2][i+1]);
        averagePartialX(n1.outp.weights[i],n1.sliceBeg[m1][i],n1.sliceBeg[m2][i+1],n2.outp.weights[i],n2.sliceBeg[m1][i],n2.sliceBeg[m1][i+1],n1.sliceSize[m1][i],n1.sliceSize[m2][i+1]);
    }
    for(size_t i = 0; i < n1.post.weights.size(); ++i)
    {
        averagePartialX(n1.post.weights[i],n1.sliceBegp[m1][i+1],n1.sliceBegp[m2][i],n2.post.weights[i],n2.sliceBegp[m1][i+1],n2.sliceBegp[m1][i],n1.sliceSizep[m1][i+1],n1.sliceSizep[m2][i]);
        averagePartialX(n1.post.weights[i],n1.sliceBegp[m1][i],n1.sliceBegp[m2][i+1],n2.post.weights[i],n2.sliceBegp[m1][i],n2.sliceBegp[m1][i+1],n1.sliceSizep[m1][i],n1.sliceSizep[m2][i+1]);
    }
}

void averageMult(std::vector<LSTM*> n)
{
    std::vector<std::vector<std::valarray<double>>*> w(n.size()), p(n.size());
    for(size_t i = 0; i < n[0]->main.weights.size(); ++i)
    {
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->main.weights[i]);
            p[j] = &(n[j]->main.actfp[i]);
        }
        averageDualMult(w,p);
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->forg.weights[i]);
            p[j] = &(n[j]->forg.actfp[i]);
        }
        averageDualMult(w,p);
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->inpu.weights[i]);
            p[j] = &(n[j]->inpu.actfp[i]);
        }
        averageDualMult(w,p);
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->outp.weights[i]);
            p[j] = &(n[j]->outp.actfp[i]);
        }
        averageDualMult(w,p);
    }
    for(size_t i = 0; i < n[0]->post.weights.size(); ++i)
    {
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->post.weights[i]);
            p[j] = &(n[j]->post.actfp[i]);
        }
        averageDualMult(w,p);
    }
}

void averageModelMult(std::vector<LSTM*> n, size_t m)
{
    std::vector<std::vector<std::valarray<double>>*> w(n.size()), p(n.size());
    std::vector<size_t> iv(n.size()), iv2(n.size());
    std::vector<std::list<size_t>> mv(n.size()), mv2(n.size());
    for(size_t i = 0; i < n[0]->main.weights.size(); ++i)
    {
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->main.weights[i]);
            p[j] = &(n[j]->main.actfp[i]);
            iv[j] = n[j]->sliceBeg[m][i+1];
            mv[j] = n[j]->sliceDepBeg[m][i];
            iv2[j] = n[j]->sliceBeg[m][i];
            mv2[j] = n[j]->sliceDepBeg[m][i+1];
        }
        averagePartialDualMult(w,p,iv,mv,n[0]->sliceSize[m][i+1],n[0]->sliceDepSize[m][i]);
        averagePartialExtMult(w,mv2,iv2,n[0]->sliceDepSize[m][i+1],n[0]->sliceSize[m][i]);
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->forg.weights[i]);
            p[j] = &(n[j]->forg.actfp[i]);
            iv[j] = n[j]->sliceBeg[m][i+1];
            mv[j] = n[j]->sliceDepBeg[m][i];
            iv2[j] = n[j]->sliceBeg[m][i];
            mv2[j] = n[j]->sliceDepBeg[m][i+1];
        }
        averagePartialDualMult(w,p,iv,mv,n[0]->sliceSize[m][i+1],n[0]->sliceDepSize[m][i]);
        averagePartialExtMult(w,mv2,iv2,n[0]->sliceDepSize[m][i+1],n[0]->sliceSize[m][i]);
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->inpu.weights[i]);
            p[j] = &(n[j]->inpu.actfp[i]);
            iv[j] = n[j]->sliceBeg[m][i+1];
            mv[j] = n[j]->sliceDepBeg[m][i];
            iv2[j] = n[j]->sliceBeg[m][i];
            mv2[j] = n[j]->sliceDepBeg[m][i+1];
        }
        averagePartialDualMult(w,p,iv,mv,n[0]->sliceSize[m][i+1],n[0]->sliceDepSize[m][i]);
        averagePartialExtMult(w,mv2,iv2,n[0]->sliceDepSize[m][i+1],n[0]->sliceSize[m][i]);
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->outp.weights[i]);
            p[j] = &(n[j]->outp.actfp[i]);
            iv[j] = n[j]->sliceBeg[m][i+1];
            mv[j] = n[j]->sliceDepBeg[m][i];
            iv2[j] = n[j]->sliceBeg[m][i];
            mv2[j] = n[j]->sliceDepBeg[m][i+1];
        }
        averagePartialDualMult(w,p,iv,mv,n[0]->sliceSize[m][i+1],n[0]->sliceDepSize[m][i]);
        averagePartialExtMult(w,mv2,iv2,n[0]->sliceDepSize[m][i+1],n[0]->sliceSize[m][i]);
    }
    for(size_t i = 0; i < n[0]->post.weights.size(); ++i)
    {
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->post.weights[i]);
            p[j] = &(n[j]->post.actfp[i]);
            iv[j] = n[j]->sliceBegp[m][i+1];
            mv[j] = n[j]->sliceDepBegp[m][i];
            iv2[j] = n[j]->sliceBegp[m][i];
            mv2[j] = n[j]->sliceDepBegp[m][i+1];
        }
        averagePartialDualMult(w,p,iv,mv,n[0]->sliceSizep[m][i+1],n[0]->sliceDepSizep[m][i]);
        averagePartialExtMult(w,mv2,iv2,n[0]->sliceDepSizep[m][i+1],n[0]->sliceSizep[m][i]);
    }
}

void averageModelMultCross(std::vector<LSTM*> n, size_t m1, size_t m2)
{
    std::vector<std::vector<std::valarray<double>>*> w(n.size());
    std::vector<size_t> iv1(n.size()), iv2(n.size()), mv1(n.size()), mv2(n.size());
    for(size_t i = 0; i < n[0]->main.weights.size(); ++i)
    {
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->main.weights[i]);
            iv1[j] = n[j]->sliceBeg[m1][i+1];
            mv1[j] = n[j]->sliceBeg[m2][i];
            iv2[j] = n[j]->sliceBeg[m2][i+1];
            mv2[j] = n[j]->sliceBeg[m1][i];
        }
        averagePartialXMult(w,iv1,mv1,n[0]->sliceSize[m1][i+1],n[0]->sliceSize[m2][i]);
        averagePartialXMult(w,iv2,mv2,n[0]->sliceSize[m2][i+1],n[0]->sliceSize[m1][i]);
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->forg.weights[i]);
            iv1[j] = n[j]->sliceBeg[m1][i+1];
            mv1[j] = n[j]->sliceBeg[m2][i];
            iv2[j] = n[j]->sliceBeg[m2][i+1];
            mv2[j] = n[j]->sliceBeg[m1][i];
        }
        averagePartialXMult(w,iv1,mv1,n[0]->sliceSize[m1][i+1],n[0]->sliceSize[m2][i]);
        averagePartialXMult(w,iv2,mv2,n[0]->sliceSize[m2][i+1],n[0]->sliceSize[m1][i]);
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->inpu.weights[i]);
            iv1[j] = n[j]->sliceBeg[m1][i+1];
            mv1[j] = n[j]->sliceBeg[m2][i];
            iv2[j] = n[j]->sliceBeg[m2][i+1];
            mv2[j] = n[j]->sliceBeg[m1][i];
        }
        averagePartialXMult(w,iv1,mv1,n[0]->sliceSize[m1][i+1],n[0]->sliceSize[m2][i]);
        averagePartialXMult(w,iv2,mv2,n[0]->sliceSize[m2][i+1],n[0]->sliceSize[m1][i]);
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->outp.weights[i]);
            iv1[j] = n[j]->sliceBeg[m1][i+1];
            mv1[j] = n[j]->sliceBeg[m2][i];
            iv2[j] = n[j]->sliceBeg[m2][i+1];
            mv2[j] = n[j]->sliceBeg[m1][i];
        }
        averagePartialXMult(w,iv1,mv1,n[0]->sliceSize[m1][i+1],n[0]->sliceSize[m2][i]);
        averagePartialXMult(w,iv2,mv2,n[0]->sliceSize[m2][i+1],n[0]->sliceSize[m1][i]);
    }
    for(size_t i = 0; i < n[0]->post.weights.size(); ++i)
    {
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->post.weights[i]);
            iv1[j] = n[j]->sliceBegp[m1][i+1];
            mv1[j] = n[j]->sliceBegp[m2][i];
            iv2[j] = n[j]->sliceBegp[m2][i+1];
            mv2[j] = n[j]->sliceBegp[m1][i];
        }
        averagePartialXMult(w,iv1,mv1,n[0]->sliceSizep[m1][i+1],n[0]->sliceSizep[m2][i]);
        averagePartialXMult(w,iv2,mv2,n[0]->sliceSizep[m2][i+1],n[0]->sliceSizep[m1][i]);
    }
}

void averageModelMultCheck(std::vector<LSTM*> n, size_t m)
{
    std::vector<std::vector<std::valarray<double>>*> w(n.size()), p(n.size());
    std::vector<size_t> iv(n.size()), iv2(n.size());
    std::vector<std::list<size_t>> mv(n.size()), mv2(n.size());
    for(size_t i = 0; i < n[0]->main.weights.size(); ++i)
    {
        size_t k = 0, r;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if(n[j]->sliceBeg.find(m) != n[j]->sliceBeg.end())
            {
                w[k] = &(n[j]->main.weights[i]);
                p[k] = &(n[j]->main.actfp[i]);
                iv[k] = n[j]->sliceBeg[m][i+1];
                mv[k] = n[j]->sliceDepBeg[m][i];
                iv2[k] = n[j]->sliceBeg[m][i];
                mv2[k] = n[j]->sliceDepBeg[m][i+1];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialDualMult(w,p,iv,mv,n[r]->sliceSize[m][i+1],n[r]->sliceDepSize[m][i]);
            averagePartialExtMult(w,mv2,iv2,n[r]->sliceDepSize[m][i+1],n[r]->sliceSize[m][i]);
        }
        k = 0;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if(n[j]->sliceBeg.find(m) != n[j]->sliceBeg.end())
            {
                w[k] = &(n[j]->forg.weights[i]);
                p[k] = &(n[j]->forg.actfp[i]);
                iv[k] = n[j]->sliceBeg[m][i+1];
                mv[k] = n[j]->sliceDepBeg[m][i];
                iv2[k] = n[j]->sliceBeg[m][i];
                mv2[k] = n[j]->sliceDepBeg[m][i+1];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialDualMult(w,p,iv,mv,n[r]->sliceSize[m][i+1],n[r]->sliceDepSize[m][i]);
            averagePartialExtMult(w,mv2,iv2,n[r]->sliceDepSize[m][i+1],n[r]->sliceSize[m][i]);
        }
        k = 0;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if(n[j]->sliceBeg.find(m) != n[j]->sliceBeg.end())
            {
                w[k] = &(n[j]->inpu.weights[i]);
                p[k] = &(n[j]->inpu.actfp[i]);
                iv[k] = n[j]->sliceBeg[m][i+1];
                mv[k] = n[j]->sliceDepBeg[m][i];
                iv2[k] = n[j]->sliceBeg[m][i];
                mv2[k] = n[j]->sliceDepBeg[m][i+1];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialDualMult(w,p,iv,mv,n[r]->sliceSize[m][i+1],n[r]->sliceDepSize[m][i]);
            averagePartialExtMult(w,mv2,iv2,n[r]->sliceDepSize[m][i+1],n[r]->sliceSize[m][i]);
        }
        k = 0;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if(n[j]->sliceBeg.find(m) != n[j]->sliceBeg.end())
            {
                w[k] = &(n[j]->outp.weights[i]);
                p[k] = &(n[j]->outp.actfp[i]);
                iv[k] = n[j]->sliceBeg[m][i+1];
                mv[k] = n[j]->sliceDepBeg[m][i];
                iv2[k] = n[j]->sliceBeg[m][i];
                mv2[k] = n[j]->sliceDepBeg[m][i+1];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialDualMult(w,p,iv,mv,n[r]->sliceSize[m][i+1],n[r]->sliceDepSize[m][i]);
            averagePartialExtMult(w,mv2,iv2,n[r]->sliceDepSize[m][i+1],n[r]->sliceSize[m][i]);
        }
    }
    for(size_t i = 0; i < n[0]->post.weights.size(); ++i)
    {
        size_t k = 0, r;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if(n[j]->sliceBegp.find(m) != n[j]->sliceBegp.end())
            {
                w[k] = &(n[j]->post.weights[i]);
                p[k] = &(n[j]->post.actfp[i]);
                iv[k] = n[j]->sliceBegp[m][i+1];
                mv[k] = n[j]->sliceDepBegp[m][i];
                iv2[k] = n[j]->sliceBegp[m][i];
                mv2[k] = n[j]->sliceDepBegp[m][i+1];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialDualMult(w,p,iv,mv,n[r]->sliceSizep[m][i+1],n[r]->sliceDepSizep[m][i]);
            averagePartialExtMult(w,mv2,iv2,n[r]->sliceDepSizep[m][i+1],n[r]->sliceSizep[m][i]);
        }
    }
}

void averageModelMultCrossCheck(std::vector<LSTM*> n, size_t m1, size_t m2)
{
    std::vector<std::vector<std::valarray<double>>*> w(n.size());
    std::vector<size_t> iv1(n.size()), iv2(n.size()), mv1(n.size()), mv2(n.size());
    for(size_t i = 0; i < n[0]->main.weights.size(); ++i)
    {
        std::vector<std::vector<std::valarray<double>>*> w(n.size());
        std::vector<size_t> iv1(n.size()), iv2(n.size()), mv1(n.size()), mv2(n.size());
        size_t k = 0, r;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if((n[j]->sliceBeg.find(m1) != n[j]->sliceBeg.end()) && (n[j]->sliceBeg.find(m2) != n[j]->sliceBeg.end()))
            {
                w[j] = &(n[j]->main.weights[i]);
                iv1[j] = n[j]->sliceBeg[m1][i+1];
                mv1[j] = n[j]->sliceBeg[m2][i];
                iv2[j] = n[j]->sliceBeg[m2][i+1];
                mv2[j] = n[j]->sliceBeg[m1][i];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialXMult(w,iv1,mv1,n[r]->sliceSize[m1][i+1],n[r]->sliceSize[m2][i]);
            averagePartialXMult(w,iv2,mv2,n[r]->sliceSize[m2][i+1],n[r]->sliceSize[m1][i]);
        }
        k = 0;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if((n[j]->sliceBeg.find(m1) != n[j]->sliceBeg.end()) && (n[j]->sliceBeg.find(m2) != n[j]->sliceBeg.end()))
            {
                w[j] = &(n[j]->forg.weights[i]);
                iv1[j] = n[j]->sliceBeg[m1][i+1];
                mv1[j] = n[j]->sliceBeg[m2][i];
                iv2[j] = n[j]->sliceBeg[m2][i+1];
                mv2[j] = n[j]->sliceBeg[m1][i];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialXMult(w,iv1,mv1,n[r]->sliceSize[m1][i+1],n[r]->sliceSize[m2][i]);
            averagePartialXMult(w,iv2,mv2,n[r]->sliceSize[m2][i+1],n[r]->sliceSize[m1][i]);
        }
        k = 0;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if((n[j]->sliceBeg.find(m1) != n[j]->sliceBeg.end()) && (n[j]->sliceBeg.find(m2) != n[j]->sliceBeg.end()))
            {
                w[j] = &(n[j]->inpu.weights[i]);
                iv1[j] = n[j]->sliceBeg[m1][i+1];
                mv1[j] = n[j]->sliceBeg[m2][i];
                iv2[j] = n[j]->sliceBeg[m2][i+1];
                mv2[j] = n[j]->sliceBeg[m1][i];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialXMult(w,iv1,mv1,n[r]->sliceSize[m1][i+1],n[r]->sliceSize[m2][i]);
            averagePartialXMult(w,iv2,mv2,n[r]->sliceSize[m2][i+1],n[r]->sliceSize[m1][i]);
        }
        k = 0;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if((n[j]->sliceBeg.find(m1) != n[j]->sliceBeg.end()) && (n[j]->sliceBeg.find(m2) != n[j]->sliceBeg.end()))
            {
                w[j] = &(n[j]->outp.weights[i]);
                iv1[j] = n[j]->sliceBeg[m1][i+1];
                mv1[j] = n[j]->sliceBeg[m2][i];
                iv2[j] = n[j]->sliceBeg[m2][i+1];
                mv2[j] = n[j]->sliceBeg[m1][i];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialXMult(w,iv1,mv1,n[r]->sliceSize[m1][i+1],n[r]->sliceSize[m2][i]);
            averagePartialXMult(w,iv2,mv2,n[r]->sliceSize[m2][i+1],n[r]->sliceSize[m1][i]);
        }
    }
    for(size_t i = 0; i < n[0]->post.weights.size(); ++i)
    {
        std::vector<std::vector<std::valarray<double>>*> w(n.size());
        std::vector<size_t> iv1(n.size()), iv2(n.size()), mv1(n.size()), mv2(n.size());
        size_t k = 0, r;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if((n[j]->sliceBegp.find(m1) != n[j]->sliceBegp.end()) && (n[j]->sliceBegp.find(m2) != n[j]->sliceBegp.end()))
            {
                w[j] = &(n[j]->post.weights[i]);
                iv1[j] = n[j]->sliceBegp[m1][i+1];
                mv1[j] = n[j]->sliceBegp[m2][i];
                iv2[j] = n[j]->sliceBegp[m2][i+1];
                mv2[j] = n[j]->sliceBegp[m1][i];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialXMult(w,iv1,mv1,n[r]->sliceSizep[m1][i+1],n[r]->sliceSizep[m2][i]);
            averagePartialXMult(w,iv2,mv2,n[r]->sliceSizep[m2][i+1],n[r]->sliceSizep[m1][i]);
        }
    }
}
