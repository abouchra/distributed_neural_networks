#include <limits>
#include "../../dnn_tools/order/order.h++"
#include "../../dnn_tools/avg/avg.h++"
#include "mlnn.h++"


MLNN::MLNN(const std::vector<size_t>& l, const std::function<double(double,std::valarray<double>)>& f, const std::vector<std::function<double(double,std::valarray<double>)>>& df, const std::function<double(size_t)>& wd, const std::function<std::valarray<double>(void)>& afd)
{
    actf = f;
    dactf = df;
    weightd = wd;
    actfpd = afd;
    states = std::vector<std::valarray<double>>(l.size());
    weights = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    actfp = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    states[0] = std::valarray<double>(0.0,l[0]);
    for(size_t i = 1; i < l.size(); ++i)
    {
        states[i] = std::valarray<double>(0.0,l[i]);
        weights[i-1] = std::vector<std::valarray<double>>(l[i]);
        actfp[i-1] = std::vector<std::valarray<double>>(l[i]);
        for(size_t j = 0; j < l[i]; ++j)
        {
            weights[i-1][j] = std::valarray<double>(l[i-1]);
            for(size_t k = 0; k < l[i-1]; ++k)
            {
                weights[i-1][j][k] = weightd(l[i-1]);
            }
            actfp[i-1][j] = actfpd();
        }
    }
}

MLNN::MLNN(const std::function<double(double,std::valarray<double>)>& f, const std::vector<std::function<double(double,std::valarray<double>)>>& df,const std::vector<std::vector<std::valarray<double>>>& w,const std::vector<std::vector<std::valarray<double>>>& a, const std::function<double(size_t)>& wd, const std::function<std::valarray<double>(void)>& afd)
{
    actf = f;
    dactf = df;
    weightd = wd;
    actfpd = afd;
    states = std::vector<std::valarray<double>>(w.size()+1);
    states[0] = std::valarray<double>(0.0,w[0][0].size());
    for(size_t i = 0; i < w.size(); ++i)
    {
        states[i+1] = std::valarray<double>(0.0,w[i].size());
    }
    weights = w;
    actfp = a;
}

void MLNN::train(const std::valarray<double>& x, const std::valarray<double>& y, double h)
{
    std::vector<std::valarray<double>> pstates(weights.size()), errors(weights.size());
    states[0] = x;
    for(size_t i = 0; i < weights.size(); ++i)
    {
        pstates[i].resize(weights[i].size());
        errors[i].resize(weights[i].size());
        for(size_t j = 0; j < weights[i].size(); ++j)
        {
            pstates[i][j] = (weights[i][j]*states[i]).sum();
            states[i+1][j] = actf(pstates[i][j],actfp[i][j]);
        }
    }
    errors.back() = y-states.back();
    for(size_t i = errors.size()-2; i < errors.size()-1; --i)
    {
        for(size_t j = 0; j < errors[i].size(); ++j)
        {
            errors[i][j] = 0.0;
            for(size_t k = 0; k < errors[i+1].size(); ++k)
            {
                errors[i][j]+= weights[i+1][k][j]*dactf[0](pstates[i+1][k],actfp[i+1][k])*errors[i+1][k];
            }
        }
    }
    for(size_t i = 0; i < weights.size(); ++i)
    {
        for(size_t j = 0; j < weights[i].size(); ++j)
        {
            weights[i][j]+= (h*errors[i][j]*dactf[0](pstates[i][j],actfp[i][j]))*states[i];
            for(size_t k = 0; k < actfp[i][j].size(); ++k)
            {
                actfp[i][j][k]+= h*errors[i][j]*dactf[k+1](pstates[i][j],actfp[i][j]);
            }
        }
    }
}

void MLNN::train(const std::vector<std::valarray<double>>& x, const std::vector<std::valarray<double>>& y, double h)
{
    std::vector<std::vector<std::valarray<double>>> gradw(weights.size()), gradp(weights.size());
    for(size_t i = 0; i < gradw.size(); ++i)
    {
        gradw[i] = std::vector<std::valarray<double>>(weights[i].size(),std::valarray<double>(0.0,weights[i][0].size()));
        gradp[i] = std::vector<std::valarray<double>>(actfp[i].size(),std::valarray<double>(0.0,actfp[i][0].size()));
    }
    for(size_t n = 0; n < x.size(); ++n)
    {
        std::vector<std::valarray<double>> pstates(weights.size()), errors(weights.size()), grad(weights.size());
        states[0] = x[n];
        for(size_t i = 0; i < weights.size(); ++i)
        {
            pstates[i].resize(weights[i].size());
            errors[i].resize(weights[i].size());
            for(size_t j = 0; j < weights[i].size(); ++j)
            {
                pstates[i][j] = (weights[i][j]*states[i]).sum();
                states[i+1][j] = actf(pstates[i][j],actfp[i][j]);
            }
        }
        errors.back() = y[n]-states.back();
        for(size_t i = errors.size()-2; i < errors.size()-1; --i)
        {
            for(size_t j = 0; j < errors[i].size(); ++j)
            {
                errors[i][j] = 0.0;
                for(size_t k = 0; k < errors[i+1].size(); ++k)
                {
                    errors[i][j]+= weights[i+1][k][j]*dactf[0](pstates[i+1][k],actfp[i+1][k])*errors[i+1][k];
                }
            }
        }
        for(size_t i = 0; i < weights.size(); ++i)
        {
            for(size_t j = 0; j < weights[i].size(); ++j)
            {
                gradw[i][j]+= (errors[i][j]*dactf[0](pstates[i][j],actfp[i][j]))*states[i];
                for(size_t k = 0; k < actfp[i][j].size(); ++k)
                {
                    gradp[i][j][k]+= errors[i][j]*dactf[k+1](pstates[i][j],actfp[i][j]);
                }
            }
        }
    }
    for(size_t i = 0; i < weights.size(); ++i)
    {
        for(size_t j = 0; j < weights[i].size(); ++j)
        {
            weights[i][j]+= gradw[i][j]*(h/((double)x.size()));
            actfp[i][j]+= gradp[i][j]*(h/((double)x.size()));
        }
    }
}

std::valarray<double> MLNN::compute(const std::valarray<double>& x) const
{
    states[0] = x;
    for(size_t i = 0; i < weights.size(); ++i)
    {
        for(size_t j = 0; j < weights[i].size(); ++j)
        {
            states[i+1][j] = actf((weights[i][j]*states[i]).sum(),actfp[i][j]);
        }
    }
    return states.back();
}

void MLNN::reset(void)
{
    states[0] = 0.0;
    for(size_t i = 1; i < states.size(); ++i)
    {
        states[i] = 0.0;
        for(size_t j = 0; j < states[i].size(); ++j)
        {
            for(size_t k = 0; k < states[i-1].size(); ++k)
            {
                weights[i-1][j][k] = weightd(states[i-1].size());
            }
            actfp[i-1][j] = actfpd();
        }
    }
}

void MLNN::set(const std::vector<std::vector<std::valarray<double>>>& w, const std::vector<std::vector<std::valarray<double>>>& a)
{
    weights = w;
    actfp = a;
}

std::valarray<double> MLNN::getInput(void) const
{
    return states.front();
}

std::valarray<double> MLNN::getOutput(void) const
{
    return states.back();
}

std::vector<std::valarray<double>> MLNN::getStates(void) const
{
    return states;
}

std::vector<std::vector<std::valarray<double>>> MLNN::getWeights(void) const
{
    return weights;
}

std::vector<std::vector<std::valarray<double>>> MLNN::getActfp(void) const
{
    return actfp;
}

void MLNN::reorderLike(const MLNN& n)
{
    for(size_t i = 0; i < weights.size()-1; ++i)
    {
        std::vector<size_t> no = orderLike(weights[i],n.weights[i]);
        reorder(weights[i],no);
        reorder(actfp[i],no);
        for(size_t j = 0; j < weights[i+1].size(); ++j)
        {
            reorder(weights[i+1][j],no);
        }
    }
}

void MLNN::reorderLikeModel(const MLNN& n, size_t m)
{
    for(size_t i = 0; i < weights.size()-1; ++i)
    {
        std::vector<size_t> no = orderLikePartial(weights[i],sliceBeg[m][i+1],n.weights[i],n.sliceBeg.at(m)[i+1],sliceSize[m][i+1]);
        reorderPartial(weights[i],sliceBeg[m][i+1],no);
        reorderPartial(actfp[i],sliceBeg[m][i+1],no);
        for(size_t j = 0; j < weights[i+1].size(); ++j)
        {
            reorderPartial(weights[i+1][j],sliceBeg[m][i+1],no);
        }
    }
}

void MLNN::reorderLikeModelDef(const MLNN& n, size_t m)
{
    for(size_t i = 0; i < weights.size()-1; ++i)
    {
        std::vector<size_t> no = orderLikePartialDefFull(weights[i],sliceBeg[m][i+1],n.weights[i],n.sliceBeg.at(m)[i],sliceSize[m][i+1]);
        reorderPartial(weights[i],sliceBeg[m][i+1],no);
        reorderPartial(actfp[i],sliceBeg[m][i+1],no);
        for(size_t j = 0; j < weights[i+1].size(); ++j)
        {
            reorderPartial(weights[i+1][j],sliceBeg[m][i+1],no);
        }
    }
}

std::vector<size_t> MLNN::layersSize(void) const
{
    std::vector<size_t> ans(states.size());
    for(size_t i = 0; i < states.size(); ++i)
    {
        ans[i] = states[i].size();
    }
    return ans;
}

void MLNN::resize(const std::vector<size_t>& l)
{
    std::vector<std::vector<std::valarray<double>>> tweights(weights), tactfp(actfp);
    states = std::vector<std::valarray<double>>(l.size());
    weights = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    actfp = std::vector<std::vector<std::valarray<double>>>(l.size()-1);
    states[0] = std::valarray<double>(0.0,l[0]);
    for(size_t i = 1; i < l.size(); ++i)
    {
        states[i] = std::valarray<double>(0.0,l[i]);
        weights[i-1] = std::vector<std::valarray<double>>(l[i]);
        actfp[i-1] = std::vector<std::valarray<double>>(l[i]);
        for(size_t j = 0; j < l[i]; ++j)
        {
            weights[i-1][j] = std::valarray<double>(l[i-1]);
            for(size_t k = 0; k < l[i-1]; ++k)
            {
                if(((i-1) < tweights.size()) && (j < tweights[i-1].size()) && (k < tweights[i-1][j].size()))
                {
                    weights[i-1][j][k] = tweights[i-1][j][k];
                }
                else
                {
                    weights[i-1][j][k] = weightd(l[i-1]);
                }
            }
            if(((i-1) < tactfp.size()) && (j < tactfp[i-1].size()))
            {
                actfp[i-1][j] = tactfp[i-1][j];
            }
            else
            {
                actfp[i-1][j] = actfpd();
            }
        }
    }
}

void MLNN::resizeRelative(const std::vector<int>& lr)
{
    std::vector<size_t> l(lr.size());
    for(size_t i = 0; i < lr.size(); ++i)
    {
        l[i] = states[i].size()+lr[i];
    }
    resize(l);
}

void MLNN::addModel(size_t n, const std::vector<size_t>& sb, const std::vector<size_t>& ss, const std::list<size_t>& deps)
{
    sliceBeg[n] = sb;
    sliceSize[n] = ss;
    sliceDepBeg[n].resize(states.size());
    sliceDepSize[n].resize(states.size());
    for(const auto& d : deps)
    {
        for(size_t i = 0; i < states.size(); ++i)
        {
            sliceDepBeg[n][i].push_back(sliceBeg[d][i]);
            sliceDepSize[n][i].push_back(sliceSize[d][i]);
        }
    }
    for(size_t i = 0; i < states.size(); ++i)
    {
        sliceDepBeg[n][i].push_back(sb[i]);
        sliceDepSize[n][i].push_back(ss[i]);
    }
}

void MLNN::addModelLast(size_t n, const std::vector<size_t>& ss, const std::list<size_t>& deps)
{
    std::vector<size_t> sb(states.size());
    for(size_t i = 0; i < states.size(); ++i)
    {
        size_t b = 0, mb = std::numeric_limits<size_t>::max();
        for(const auto& m : sliceBeg)
        {
            if(m.second[i] >= b)
            {
                mb = m.first;
                b = m.second[i];
            }
        }
        if(mb == std::numeric_limits<size_t>::max())
        {
            sb[i] = 0;
        }
        else
        {
            sb[i] = b+sliceSize[mb][i];
        }
    }
    addModel(n,sb,ss,deps);
}

void MLNN::rmModel(size_t n)
{
    sliceBeg.erase(n);
    sliceSize.erase(n);
    sliceDepBeg.erase(n);
    sliceDepSize.erase(n);
}
    
bool MLNN::hasModel(size_t m) const
{
    return (sliceBeg.find(m) != sliceBeg.end());
}
    
std::unordered_set<size_t> MLNN::models(void) const
{
    std::unordered_set<size_t> ans;
    ans.reserve(sliceBeg.size());
    for(const auto& m : sliceBeg)
    {
        ans.insert(m.first);
    }
    return ans;
}

std::pair<std::vector<size_t>,std::vector<size_t>> MLNN::modelPos(size_t m) const
{
    return std::pair<std::vector<size_t>,std::vector<size_t>>(sliceBeg.at(m),sliceSize.at(m));
}

void reorderSim(std::vector<MLNN*> n)
{
    for(size_t v = 1; v < n.size(); ++v)
    {
        for(size_t i = 0; i < n[v]->weights.size()-1; ++i)
        {
            std::vector<size_t> no = orderLike(n[v]->weights[i],n[0]->weights[i]);
            reorder(n[v]->weights[i],no);
            reorder(n[v]->actfp[i],no);
            for(size_t j = 0; j < n[v]->weights[i+1].size(); ++j)
            {
                reorder(n[v]->weights[i+1][j],no);
            }
        }
    }
}

void reorderSimModel(std::vector<MLNN*> n, size_t m)
{
    for(size_t v = 1; v < n.size(); ++v)
    {
        for(size_t i = 0; i < n[v]->weights.size()-1; ++i)
        {
            std::vector<size_t> no = orderLikePartial(n[v]->weights[i],n[v]->sliceBeg[m][i+1],n[0]->weights[i],n[0]->sliceBeg[m][i+1],n[v]->sliceSize[m][i+1]);
            reorderPartial(n[v]->weights[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->actfp[i],n[v]->sliceBeg[m][i+1],no);
            for(size_t j = 0; j < n[v]->weights[i+1].size(); ++j)
            {
                reorderPartial(n[v]->weights[i+1][j],n[v]->sliceBeg[m][i+1],no);
            }
        }
    }
}

void reorderSimModelDef(std::vector<MLNN*> n, size_t m)
{
    for(size_t v = 1; v < n.size(); ++v)
    {
        for(size_t i = 0; i < n[v]->weights.size()-1; ++i)
        {
            std::vector<size_t> no = orderLikePartialDefFull(n[v]->weights[i],n[v]->sliceBeg[m][i+1],n[0]->weights[i],n[0]->sliceBeg[m][i+1],n[v]->sliceSize[m][i+1]);
            reorderPartial(n[v]->weights[i],n[v]->sliceBeg[m][i+1],no);
            reorderPartial(n[v]->actfp[i],n[v]->sliceBeg[m][i+1],no);
            for(size_t j = 0; j < n[v]->weights[i+1].size(); ++j)
            {
                reorderPartial(n[v]->weights[i+1][j],n[v]->sliceBeg[m][i+1],no);
            }
        }
    }
}

void average(MLNN& n1, MLNN& n2)
{
    for(size_t i = 0; i < n1.weights.size(); ++i)
    {
        averageDual(n1.weights[i],n1.actfp[i],n2.weights[i],n2.actfp[i]);
    }
}

void averageModel(MLNN& n1, MLNN& n2, size_t m)
{
    for(size_t i = 0; i < n1.weights.size(); ++i)
    {
        averagePartialDual(n1.weights[i],n1.actfp[i],n1.sliceBeg[m][i+1],n1.sliceDepBeg[m][i],n2.weights[i],n2.actfp[i],n2.sliceBeg[m][i+1],n2.sliceDepBeg[m][i],n1.sliceSize[m][i+1],n1.sliceDepSize[m][i]);
        averagePartialExt(n1.weights[i],n1.sliceDepBeg[m][i+1],n1.sliceBeg[m][i],n2.weights[i],n2.sliceDepBeg[m][i+1],n2.sliceBeg[m][i],n1.sliceDepSize[m][i+1],n1.sliceSize[m][i]);
    }
}

void averageModelCross(MLNN& n1, MLNN& n2, size_t m1, size_t m2)
{
    for(size_t i = 0; i < n1.weights.size(); ++i)
    {
        averagePartialX(n1.weights[i],n1.sliceBeg[m1][i+1],n1.sliceBeg[m2][i],n2.weights[i],n2.sliceBeg[m1][i+1],n2.sliceBeg[m1][i],n1.sliceSize[m1][i+1],n1.sliceSize[m2][i]);
        averagePartialX(n1.weights[i],n1.sliceBeg[m1][i],n1.sliceBeg[m2][i+1],n2.weights[i],n2.sliceBeg[m1][i],n2.sliceBeg[m1][i+1],n1.sliceSize[m1][i],n1.sliceSize[m2][i+1]);
    }
}

void averageMult(std::vector<MLNN*> n)
{
    std::vector<std::vector<std::valarray<double>>*> w(n.size()), p(n.size());
    for(size_t i = 0; i < n[0]->weights.size(); ++i)
    {
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->weights[i]);
            p[j] = &(n[j]->actfp[i]);
        }
        averageDualMult(w,p);
    }
}

void averageModelMult(std::vector<MLNN*> n, size_t m)
{
    std::vector<std::vector<std::valarray<double>>*> w(n.size()), p(n.size());
    std::vector<size_t> iv(n.size()), iv2(n.size());
    std::vector<std::list<size_t>> mv(n.size()), mv2(n.size());
    for(size_t i = 0; i < n[0]->weights.size(); ++i)
    {
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->weights[i]);
            p[j] = &(n[j]->actfp[i]);
            iv[j] = n[j]->sliceBeg[m][i+1];
            mv[j] = n[j]->sliceDepBeg[m][i];
            iv2[j] = n[j]->sliceBeg[m][i];
            mv2[j] = n[j]->sliceDepBeg[m][i+1];
        }
        averagePartialDualMult(w,p,iv,mv,n[0]->sliceSize[m][i+1],n[0]->sliceDepSize[m][i]);
        averagePartialExtMult(w,mv2,iv2,n[0]->sliceDepSize[m][i+1],n[0]->sliceSize[m][i]);
    }
}

void averageModelMultCross(std::vector<MLNN*> n, size_t m1, size_t m2)
{
    std::vector<std::vector<std::valarray<double>>*> w(n.size());
    std::vector<size_t> iv1(n.size()), iv2(n.size()), mv1(n.size()), mv2(n.size());
    for(size_t i = 0; i < n[0]->weights.size(); ++i)
    {
        for(size_t j = 0; j < n.size(); ++j)
        {
            w[j] = &(n[j]->weights[i]);
            iv1[j] = n[j]->sliceBeg[m1][i+1];
            mv1[j] = n[j]->sliceBeg[m2][i];
            iv2[j] = n[j]->sliceBeg[m2][i+1];
            mv2[j] = n[j]->sliceBeg[m1][i];
        }
        averagePartialXMult(w,iv1,mv1,n[0]->sliceSize[m1][i+1],n[0]->sliceSize[m2][i]);
        averagePartialXMult(w,iv2,mv2,n[0]->sliceSize[m2][i+1],n[0]->sliceSize[m1][i]);
    }
}

void averageModelMultCheck(std::vector<MLNN*> n, size_t m)
{
    std::vector<std::vector<std::valarray<double>>*> w(n.size()), p(n.size());
    std::vector<size_t> iv(n.size()), iv2(n.size());
    std::vector<std::list<size_t>> mv(n.size()), mv2(n.size());
    for(size_t i = 0; i < n[0]->weights.size(); ++i)
    {
        size_t k = 0, r;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if(n[j]->sliceBeg.find(m) != n[j]->sliceBeg.end())
            {
                w[k] = &(n[j]->weights[i]);
                p[k] = &(n[j]->actfp[i]);
                iv[k] = n[j]->sliceBeg[m][i+1];
                mv[k] = n[j]->sliceDepBeg[m][i];
                iv2[k] = n[j]->sliceBeg[m][i];
                mv2[k] = n[j]->sliceDepBeg[m][i+1];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialDualMult(w,p,iv,mv,n[r]->sliceSize[m][i+1],n[r]->sliceDepSize[m][i]);
            averagePartialExtMult(w,mv2,iv2,n[r]->sliceDepSize[m][i+1],n[r]->sliceSize[m][i]);
        }
    }
}

void averageModelMultCrossCheck(std::vector<MLNN*> n, size_t m1, size_t m2)
{
    std::vector<std::vector<std::valarray<double>>*> w(n.size());
    std::vector<size_t> iv1(n.size()), iv2(n.size()), mv1(n.size()), mv2(n.size());
    for(size_t i = 0; i < n[0]->weights.size(); ++i)
    {
        size_t k = 0, r;
        for(size_t j = 0; j < n.size(); ++j)
        {
            if((n[j]->sliceBeg.find(m1) != n[j]->sliceBeg.end()) && (n[j]->sliceBeg.find(m2) != n[j]->sliceBeg.end()))
            {
                w[j] = &(n[j]->weights[i]);
                iv1[j] = n[j]->sliceBeg[m1][i+1];
                mv1[j] = n[j]->sliceBeg[m2][i];
                iv2[j] = n[j]->sliceBeg[m2][i+1];
                mv2[j] = n[j]->sliceBeg[m1][i];
                k++;
                r = j;
            }
        }
        if(k > 1)
        {
            averagePartialXMult(w,iv1,mv1,n[r]->sliceSize[m1][i+1],n[r]->sliceSize[m2][i]);
            averagePartialXMult(w,iv2,mv2,n[r]->sliceSize[m2][i+1],n[r]->sliceSize[m1][i]);
        }
    }
}
