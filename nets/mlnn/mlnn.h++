#include <cstddef>
#include <utility>
#include <list>
#include <vector>
#include <valarray>
#include <unordered_set>
#include <unordered_map>
#include <functional>


#ifndef DNN_MLNN
class MLNN
{
    std::function<double(double,std::valarray<double>)> actf;
    
    std::vector<std::function<double(double,std::valarray<double>)>> dactf;
    
    mutable std::vector<std::valarray<double>> states;
    
    std::vector<std::vector<std::valarray<double>>> weights;
    
    std::vector<std::vector<std::valarray<double>>> actfp;
    
    std::function<double(size_t)> weightd;
    
    std::function<std::valarray<double>(void)> actfpd;
    
    std::unordered_map<size_t,std::vector<size_t>> sliceBeg;
    
    std::unordered_map<size_t,std::vector<size_t>> sliceSize;
    
    std::unordered_map<size_t,std::vector<std::list<size_t>>> sliceDepBeg;
    
    std::unordered_map<size_t,std::vector<std::list<size_t>>> sliceDepSize;

public:
    MLNN(void) = default;
    
    MLNN(const MLNN&) = default;
    
    MLNN(const std::vector<size_t>&,const std::function<double(double,std::valarray<double>)>&,const std::vector<std::function<double(double,std::valarray<double>)>>&,const std::function<double(size_t)>&,const std::function<std::valarray<double>(void)>&);
    
    MLNN(const std::function<double(double,std::valarray<double>)>&,const std::vector<std::function<double(double,std::valarray<double>)>>&,const std::vector<std::vector<std::valarray<double>>>&,const std::vector<std::vector<std::valarray<double>>>&,const std::function<double(size_t)>&,const std::function<std::valarray<double>(void)>&);
    
    ~MLNN(void) = default;
    
    MLNN& operator=(const MLNN&) = default;
    
    void train(const std::valarray<double>&,const std::valarray<double>&,double);

    void train(const std::vector<std::valarray<double>>&,const std::vector<std::valarray<double>>&,double);
    
    std::valarray<double> compute(const std::valarray<double>&) const;
    
    void reset(void);
    
    void set(const std::vector<std::vector<std::valarray<double>>>&,const std::vector<std::vector<std::valarray<double>>>&);
    
    std::valarray<double> getInput(void) const;

    std::valarray<double> getOutput(void) const;

    std::vector<std::valarray<double>> getStates(void) const;

    std::vector<std::vector<std::valarray<double>>> getWeights(void) const;

    std::vector<std::vector<std::valarray<double>>> getActfp(void) const;
    
    void reorderLike(const MLNN&);
    
    void reorderLikeModel(const MLNN&,size_t);
    
    void reorderLikeModelDef(const MLNN&,size_t);
    
    std::vector<size_t> layersSize(void) const;

    void resize(const std::vector<size_t>&);

    void resizeRelative(const std::vector<int>&);

    void addModel(size_t,const std::vector<size_t>&,const std::vector<size_t>&,const std::list<size_t>&);

    void addModelLast(size_t,const std::vector<size_t>&,const std::list<size_t>&);

    void rmModel(size_t);
    
    bool hasModel(size_t) const;
    
    std::unordered_set<size_t> models(void) const;

    std::pair<std::vector<size_t>,std::vector<size_t>> modelPos(size_t) const;
    
    friend void reorderSim(std::vector<MLNN*>);

    friend void reorderSimModel(std::vector<MLNN*>,size_t);

    friend void reorderSimModelDef(std::vector<MLNN*>,size_t);
    
    friend void average(MLNN&,MLNN&);
    
    friend void averageModel(MLNN&,MLNN&,size_t);
    
    friend void averageModelCross(MLNN&,MLNN&,size_t,size_t);
    
    friend void averageMult(std::vector<MLNN*>);
    
    friend void averageModelMult(std::vector<MLNN*>,size_t);
    
    friend void averageModelMultCross(std::vector<MLNN*>,size_t,size_t);
    
    friend void averageModelMultCheck(std::vector<MLNN*>,size_t);
    
    friend void averageModelMultCrossCheck(std::vector<MLNN*>,size_t,size_t);
};
#define DNN_MLNN
#endif
