#include <cstddef>
#include <vector>
#include <valarray>
#include <iostream>


#ifndef DNN_HLP
namespace std
{
template<>
class hash<std::pair<size_t,double>>
{
public:
    size_t operator()(const std::pair<size_t,double>&) const;
};

template<>
class hash<std::vector<size_t>>
{
public:
    size_t operator()(const std::vector<size_t>&) const;
};
}

void operator+=(std::valarray<double>&,const std::valarray<unsigned>&);

std::vector<size_t> operator+(const std::vector<size_t>&,size_t);

std::ostream& operator<<(std::ostream&, const std::valarray<double>&);

std::valarray<double> vbtd(const std::vector<bool>&);

std::valarray<double> fabs(const std::valarray<double>&);

std::string flofix(double,size_t);

template<class T>
std::valarray<T> vectovaa(std::vector<T> v)
{
    std::valarray<T> ans(v.size());
    for(size_t i = 0; i < v.size(); ++i)
    {
        ans[i] = v[i];
    }
    return ans;
}
#define DNN_HLP
#endif
