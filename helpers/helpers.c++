#include <string>
#include <sstream>
#include <iomanip>
#include "helpers.h++"


size_t std::hash<std::pair<size_t,double>>::operator()(const std::pair<size_t,double>& p) const
{
    return std::hash<size_t>{}(p.first)^std::hash<double>{}(p.second);
}

size_t std::hash<std::vector<size_t>>::operator()(const std::vector<size_t>& p) const
{
    size_t ans = 0;
    for(const auto& e: p)
    {
        ans^= std::hash<size_t>{}(e);
    }
    return ans;
}

void operator+=(std::valarray<double>& v1, const std::valarray<unsigned>& v2)
{
    for(size_t i = 0; i < ((v1.size()<v2.size())?v1.size():v2.size()); ++i)
    {
        v1[i]+= v2[i];
    }
}

std::vector<size_t> operator+(const std::vector<size_t>& v, size_t n)
{
    std::vector<size_t> ans(v);
    for(size_t i = 0; i < v.size(); ++i)
    {
        ans[i]+= n;
    }
    return ans;
}

std::ostream& operator<<(std::ostream& os, const std::valarray<double>& v)
{
    os << '[';
    if(v.size() > 0)
    {
        os << v[0];
        for(size_t i = 1; i < v.size(); ++i)
        {
            os << ',';
            os << v[i];
        }
    }
    os << ']';
    return os;
}

std::valarray<double> vbtd(const std::vector<bool>& s)
{
    std::valarray<double> ans(s.size());
    for(size_t i = 0; i < s.size(); ++i)
    {
        ans[i] = s[i]?1.0:0.0;
    }
    return ans;
}

std::valarray<double> fabs(const std::valarray<double>& p)
{
    std::valarray<double> ans(p.size());
    for(size_t i = 0; i < p.size(); ++i)
    {
        ans[i] = fabs(p[i]);
    }
    return ans;
}

std::string flofix(double x, size_t n)
{
    std::string ans;
    std::stringstream ss;
    ss << std::fixed << std::setprecision(n-2) << x;
    std::getline(ss,ans);
    ans.resize(n);
    return ans;
}
